﻿using Dapper;
using SoftNetCore.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Reflection;

namespace SoftNetCore.DataAccess.Repository
{
    public interface IProductRepository
    {
        Task<ProductViewModel> GetByIdAsync(int Id);
        Task<List<ProductViewModel>> GetBySaleAsync(int item);
        Task<List<ProductViewModel>> GetByHotAsync(int item);
        Task<List<ProductViewModel>> GetByNewAsync(int item);
        Task<List<ProductViewModel>> GetByRandomAsync(int item);
        Task<List<ProductViewModel>> GetAllAsync(int pageSize, int pageNumber, string sortBy, string sortDir);
        Task<List<ProductViewModel>> GetAllAsync(int pageSize, int pageNumber, string keyword, string sortBy, string sortDir);
        Task<List<ProductViewModel>> GetAllAsync(int categoryId, int pageSize, int pageNumber, string keyword, string sortBy, string sortDir);
        Task<List<ProductViewModel>> GetByBannerAsync(int bannerId, int pageSize, int pageNumber, string keyword, string sortBy, string sortDir);
    }

    public class ProductRepository : IProductRepository
    {
        private ConnectData data = new ConnectData();
        public async Task<List<ProductViewModel>> GetAllAsync(int pageSize, int pageNumber, string sortBy, string sortDir)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("PageNumber", pageNumber);
                parameters.Add("PageSize", pageSize);
                parameters.Add("SortBy", sortBy);
                parameters.Add("SortDir", sortDir);
                return (await db.QueryAsync<ProductViewModel>("sp_GetProducts", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<ProductViewModel>> GetAllAsync(int pageSize, int pageNumber, string keyword, string sortBy, string sortDir)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("PageNumber", pageNumber);
                parameters.Add("PageSize", pageSize);
                parameters.Add("Keyword", keyword);
                parameters.Add("SortBy", sortBy);
                parameters.Add("SortDir", sortDir);
                return (await db.QueryAsync<ProductViewModel>("sp_GetProductCondition", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<ProductViewModel>> GetAllAsync(int categoryId, int pageSize, int pageNumber, string keyword, string sortBy, string sortDir)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("CatId", categoryId);
                parameters.Add("PageNumber", pageNumber);
                parameters.Add("PageSize", pageSize);
                parameters.Add("Keyword", keyword);
                parameters.Add("SortBy", sortBy);
                parameters.Add("SortDir", sortDir);
                return (await db.QueryAsync<ProductViewModel>("sp_GetProductConditionByCatId", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<ProductViewModel>> GetByBannerAsync(int bannerId, int pageSize, int pageNumber, string keyword, string sortBy, string sortDir)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("BannerId", bannerId);
                parameters.Add("PageNumber", pageNumber);
                parameters.Add("PageSize", pageSize);
                parameters.Add("Keyword", keyword);
                parameters.Add("SortBy", sortBy);
                parameters.Add("SortDir", sortDir);
                return (await db.QueryAsync<ProductViewModel>("sp_GetProductByBanner", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<ProductViewModel> GetByIdAsync(int Id)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                //foreach (PropertyInfo propertyInfo in new ProductViewModel().GetType().GetProperties())
                //{
                //    parameters.Add(propertyInfo.Name, propertyInfo.GetValue(new ProductViewModel(), null));
                //}
                parameters.Add("Id", Id);
                return (await db.QuerySingleOrDefaultAsync<ProductViewModel>("sp_GetProductById", parameters, commandType: CommandType.StoredProcedure));
            }
        }

        public async Task<List<ProductViewModel>> GetByNewAsync(int item)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("Number", item);
                return (await db.QueryAsync<ProductViewModel>("sp_GetProductNew", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<ProductViewModel>> GetByRandomAsync(int item)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("Number", item);
                return (await db.QueryAsync<ProductViewModel>("sp_GetProductRandom", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<ProductViewModel>> GetBySaleAsync(int item)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("Number", item);
                return (await db.QueryAsync<ProductViewModel>("sp_GetProductSale", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<ProductViewModel>> GetByHotAsync(int item)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("Number", item);
                return (await db.QueryAsync<ProductViewModel>("sp_GetProductHot", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<ProductViewModel>> GetByRelatedAsync(int id, int catId, int item)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("Id", id);
                parameters.Add("CatId", catId);
                parameters.Add("Number", item);
                return (await db.QueryAsync<ProductViewModel>("sp_GetProductRelated", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }
    }
}
