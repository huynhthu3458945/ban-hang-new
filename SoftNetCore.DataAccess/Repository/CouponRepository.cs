﻿using Dapper;
using SoftNetCore.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Reflection;

namespace SoftNetCore.DataAccess.Repository
{
    public interface ICouponRepository
    {
        Task<CouponViewModel> GetByIdAsync(int Id);
        Task<List<CouponViewModel>> GetBySaleAsync(int item);
        Task<List<CouponViewModel>> GetByHotAsync(int item);
        Task<List<CouponViewModel>> GetByNewAsync(int item);
        Task<List<CouponViewModel>> GetByRandomAsync(int item);
        Task<List<CouponViewModel>> GetByRelatedAsync(int id, int catId, int item);
        Task<List<CouponViewModel>> GetAllAsync(int pageSize, int pageNumber, string sortBy, string sortDir);
        Task<List<CouponViewModel>> GetAllAsync(int pageSize, int pageNumber, string keyword, string sortBy, string sortDir);
        Task<List<CouponViewModel>> GetAllAsync(int categoryId, int pageSize, int pageNumber, string keyword, string sortBy, string sortDir);
    }

    public class CouponRepository : ICouponRepository
    {
        private ConnectData data = new ConnectData();
        public async Task<List<CouponViewModel>> GetAllAsync(int pageSize, int pageNumber, string sortBy, string sortDir)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("PageNumber", pageNumber);
                parameters.Add("PageSize", pageSize);
                parameters.Add("SortBy", sortBy);
                parameters.Add("SortDir", sortDir);
                return (await db.QueryAsync<CouponViewModel>("sp_GetCoupons", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<CouponViewModel>> GetAllAsync(int pageSize, int pageNumber, string keyword, string sortBy, string sortDir)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("PageNumber", pageNumber);
                parameters.Add("PageSize", pageSize);
                parameters.Add("Keyword", keyword);
                parameters.Add("SortBy", sortBy);
                parameters.Add("SortDir", sortDir);
                return (await db.QueryAsync<CouponViewModel>("sp_GetCouponCondition", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<CouponViewModel>> GetAllAsync(int categoryId, int pageSize, int pageNumber, string keyword, string sortBy, string sortDir)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("CatId", categoryId);
                parameters.Add("PageNumber", pageNumber);
                parameters.Add("PageSize", pageSize);
                parameters.Add("Keyword", keyword);
                parameters.Add("SortBy", sortBy);
                parameters.Add("SortDir", sortDir);
                return (await db.QueryAsync<CouponViewModel>("sp_GetCouponConditionByCatId", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<CouponViewModel> GetByIdAsync(int Id)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                //foreach (PropertyInfo propertyInfo in new CouponViewModel().GetType().GetProperties())
                //{
                //    parameters.Add(propertyInfo.Name, propertyInfo.GetValue(new CouponViewModel(), null));
                //}
                parameters.Add("Id", Id);
                return (await db.QuerySingleOrDefaultAsync<CouponViewModel>("sp_GetCouponById", parameters, commandType: CommandType.StoredProcedure));
            }
        }

        public async Task<List<CouponViewModel>> GetByNewAsync(int item)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("Number", item);
                return (await db.QueryAsync<CouponViewModel>("sp_GetCouponNew", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<CouponViewModel>> GetByRandomAsync(int item)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("Number", item);
                return (await db.QueryAsync<CouponViewModel>("sp_GetCouponRandom", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<CouponViewModel>> GetBySaleAsync(int item)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("Number", item);
                return (await db.QueryAsync<CouponViewModel>("sp_GetCouponSale", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<CouponViewModel>> GetByHotAsync(int item)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("Number", item);
                return (await db.QueryAsync<CouponViewModel>("sp_GetCouponHot", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task<List<CouponViewModel>> GetByRelatedAsync(int id, int catId, int item)
        {
            using (IDbConnection db = new SqlConnection(ConnectData.ConnectionString))
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                var parameters = new Dictionary<string, object>();
                parameters.Add("Id", id);
                parameters.Add("CatId", catId);
                parameters.Add("Number", item);
                return (await db.QueryAsync<CouponViewModel>("sp_GetCouponRelated", parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }
    }
}
