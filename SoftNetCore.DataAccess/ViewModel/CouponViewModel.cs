﻿using SoftNetCore.Model.Catalog;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.DataAccess.ViewModel
{
    public class CouponViewModel
    {
        public int Id { get; set; }

        /// <summary>
        /// This value get from id of accesstrade response
        /// </summary>
        public string CouponId { get; set; }
        public int? CouponTypeId { get; set; }

        public string Code { get; set; }
        public decimal? PriceDiscount { get; set; }
        public bool? IsPublic { get; set; }
        public bool? IsUse { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }

        /// <summary>
        /// All fields from accesstrade
        /// </summary>
        public string CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string Content { get; set; }
        public string Coupons { get; set; } // Object
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Image { get; set; }
        public bool IsHot { get; set; }
        public string Link { get; set; }
        public string AffilateLink { get; set; }
        public string Merchant { get; set; }
        public string Title { get; set; }
        public int PercentageUsed { get; set; }
        public string TimeLeft { get; set; }
        public int Register { get; set; }
        public int Status { get; set; }
        public List<CouponDetailViewModel> CouponDetails { get; set; }

        // virtual
        public bool IsReturn { get; set; }

        // public virtual ICollection<AccountCouponModel> AccountCoupons { get; set; }
        public virtual CouponTypeModel CouponType { get; set; }
    }

    public class CouponDetailViewModel
    {
        public string coupon_code { get; set; }
        public string coupon_desc { get; set; }
        public string coupon_save { get; set; }
        public string coupon_type { get; set; }
    }

    public class CouponAndTotalTypeModel
    {
        public CouponTypeModel CouponTypeModel { get; set; }
        public int Total { get; set; }
    }
}
