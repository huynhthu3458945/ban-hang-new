﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SoftNetCore.DataAccess
{
    public class ConnectData
    {
        public static IConfigurationRoot Configuration { get; set; }
        public ConnectData()
        {
            Configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
        }

        public static string ConnectionString => Configuration["ConnectionStrings:DBConnection"].ToString();
    }
}
