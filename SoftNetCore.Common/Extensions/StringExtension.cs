﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace SoftNetCore.Common.Extensions
{
    public static class StringExtension
    {
        public static string Peel(this string str, int numner){
            if (!string.IsNullOrEmpty(str))
            {
                if (str.Length > numner)
                return $"{str.Substring(0, numner)}...";
            else
                return str;
            }
            return string.Empty;
        }
        public static string PeelDescription(this string str, int numner)
        {
            if (!string.IsNullOrEmpty(str))
            {
                if (str.Length > numner)
                    return $"{HttpUtility.HtmlDecode(str.Substring(0, numner))}...";
                else
                    return HttpUtility.HtmlDecode(str);
            }
            return string.Empty;
        }
    }
}
