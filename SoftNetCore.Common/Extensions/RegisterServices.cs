﻿using Microsoft.Extensions.DependencyInjection;
using Scrutor;

namespace SoftNetCore.Common.Extensions
{
    public static class RegisterServices
    {
        public static IServiceCollection WithScopedLifetime<T>(this IServiceCollection services)
        {
            services.Scan(scan => scan
                .FromAssemblyOf<T>()
                .AddClasses()
                .AsImplementedInterfaces()
                .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                .AsMatchingInterface()
                .WithScopedLifetime());

            return services;
        }
    }
}
