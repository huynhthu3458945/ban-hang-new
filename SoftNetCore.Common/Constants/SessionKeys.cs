﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Common.Constants
{
    public static class SessionKeys
    {
        public const string USERNAME = "USERNAME";
        public const string PASSWORD = "PASSWORD";
    }
}
