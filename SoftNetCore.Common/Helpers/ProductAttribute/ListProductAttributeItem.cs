﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SoftNetCore.Common.Helpers;

/// <summary>
/// Summary description for ListItem
/// </summary>
public class ListProductAttributeItem
{
    public static List<ProductAttributeItem> AddList(int? id, int? productId, string productName, int? colorId, string colorName, int? sizeId, string sizeName, string avatar1, string avatar2, string listImg, double price, double pricePromo, int quantity, bool status, int? parentId)
    {
        List<ProductAttributeItem> productAttributeItem = new List<ProductAttributeItem>()
        {
            new ProductAttributeItem {Id = id, ProductId = productId, ProductName = productName, ColorId = colorId, ColorName = colorName, SizeId = sizeId, SizeName = sizeName, Avatar1=avatar1 , Avatar2=avatar2, ListImg=listImg, Price = price, PricePromo = pricePromo, Quantity = quantity, Status = status, ParentId = parentId}
        };
        return productAttributeItem;
    }
    public static List<ProductAttributeItem> Update(List<ProductAttributeItem> productAttributeItem, int? id, int? productId, string productName, int? colorId, string colorName, int? sizeId, string sizeName, string avatar1, string avatar2, string listImg, double price, double pricePromo, int quantity, bool status, int? parentId)
    {
        ProductAttributeItem item = new ProductAttributeItem();
        item.Id = id;
        item.ProductId = productId;
        item.ProductName = productName;
        item.ColorId = colorId;
        item.ColorName = colorName;
        item.SizeId = sizeId;
        item.SizeName = sizeName;
        item.Avatar1 = avatar1;
        item.Avatar2 = avatar2;
        item.ListImg = listImg;
        item.Price = price;
        item.PricePromo = pricePromo;
        item.Quantity = quantity;
        item.Status = status;
        item.ParentId = parentId;
        productAttributeItem.Add(item);
        return productAttributeItem;
    }
    public static List<ProductAttributeItem> Remove(List<ProductAttributeItem> productAttributeItem, int index)
    {
        if (productAttributeItem != null)
        {
            productAttributeItem.RemoveAt(index);
        }
        return productAttributeItem;
    }

    #region Color
    public static int GetColorItemIndex(List<ProductAttributeItem> productAttributeItem, int colorId, ref int index)
    {
        bool isExist = false; int indexResut = 0;
        if (productAttributeItem != null)
        {
            indexResut = -1;
            foreach (ProductAttributeItem item in productAttributeItem)
            {
                indexResut = indexResut + 1;
                if (item.ColorId == colorId)
                {
                    isExist = true;
                    index = indexResut;
                }
            }
        }
        return isExist == true ? index : 0;
    }

    public static int GetColorItemIndex(List<ProductAttributeItem> productAttributeItem, int productId, int colorId, ref int index)
    {
        bool isExist = false; int indexResut = 0;
        if (productAttributeItem != null)
        {
            indexResut = -1;
            foreach (ProductAttributeItem item in productAttributeItem)
            {
                indexResut = indexResut + 1;
                if (item.ProductId == productId && item.ColorId == colorId)
                {
                    isExist = true;
                    index = indexResut;
                }
            }
        }
        return isExist == true ? index : 0;
    }

    public static void CheckColorExists(List<ProductAttributeItem> productAttributeItem, int colorId, ref bool isExist)
    {
        if (productAttributeItem != null)
        {
            foreach (ProductAttributeItem item in productAttributeItem)
            {
                if (item.ColorId == colorId)
                {
                    isExist = true;
                    break;
                }
            }
        }
    }

    public static void CheckColorExists(List<ProductAttributeItem> productAttributeItem, int productId, int colorId, ref bool isExist)
    {
        if (productAttributeItem != null)
        {
            foreach (ProductAttributeItem item in productAttributeItem)
            {
                if (item.ProductId == productId && item.ColorId == colorId)
                {
                    isExist = true;
                    break;
                }
            }
        }
    }
    #endregion

    public static int GetSizeItemIndex(List<ProductAttributeItem> productAttributeItem, int sizeId, int colorId, ref int index)
    {
        bool isExist = false; int indexResut = 0;
        if (productAttributeItem != null)
        {
            indexResut = -1;
            foreach (ProductAttributeItem item in productAttributeItem)
            {
                indexResut = indexResut + 1;
                if (item.SizeId == sizeId && item.ColorId == colorId)
                {
                    isExist = true;
                    index = indexResut;
                }
            }
        }
        return isExist == true ? index : 0;
    }

    public static int GetSizeItemIndex(List<ProductAttributeItem> productAttributeItem, int productId, int sizeId, int colorId, ref int index)
    {
        bool isExist = false; int indexResut = 0;
        if (productAttributeItem != null)
        {
            indexResut = -1;
            foreach (ProductAttributeItem item in productAttributeItem)
            {
                indexResut = indexResut + 1;
                if (item.ProductId == productId && item.SizeId == sizeId && item.ColorId == colorId)
                {
                    isExist = true;
                    index = indexResut;
                }
            }
        }
        return isExist == true ? index : 0;
    }

    public static void CheckSizeExists(List<ProductAttributeItem> productAttributeItem, int sizeId, int colorId, ref bool isExist)
    {
        if (productAttributeItem != null)
        {
            foreach (ProductAttributeItem item in productAttributeItem)
            {
                if (item.SizeId == sizeId && item.ColorId == colorId)
                {
                    isExist = true;
                    break;
                }
            }
        }
    }

    public static void CheckSizeExists(List<ProductAttributeItem> productAttributeItem, int productId, int sizeId, int colorId, ref bool isExist)
    {
        if (productAttributeItem != null)
        {
            foreach (ProductAttributeItem item in productAttributeItem)
            {
                if (item.ProductId == productId && item.SizeId == sizeId && item.ColorId == colorId)
                {
                    isExist = true;
                    break;
                }
            }
        }
    }
}