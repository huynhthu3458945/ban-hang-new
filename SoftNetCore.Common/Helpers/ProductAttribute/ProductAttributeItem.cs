﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftNetCore.Common.Helpers
{
    public class ProductAttributeItem
    {
        private int? _Id;
        private int? _productId;
        private string _productName;
        private int? _colorId;
        private string _colorName;
        private int? _sizeId;
        private string _sizeName;
        private string _avatar1;
        private string _avatar2;
        private string _listImg;
        private double _price;
        private double _pricePromo;
        private int _quantity;
        private bool _status;
        private int? _partentId;

        public ProductAttributeItem()
        {

        }

        public ProductAttributeItem(int? Id, int? ProductId, string ProductName, int? ColorId, string ColorName, int? SizeId, string SizeName, string Avatar1, string Avatar2, string ListImg, double Price, double PricePromo,int Quantity, bool Status, int? ParentId)
        {
            _Id = Id;
            _productId = ProductId;
            _productName = ProductName;
            _colorId = ColorId;
            _colorName = ColorName;
            _sizeId = SizeId;
            _sizeName = SizeName;
            _avatar1 = Avatar1;
            _avatar2 = Avatar2;
            _listImg = ListImg;
            _price = Price;
            _pricePromo = PricePromo;
            _quantity = Quantity;
            _status = Status;
            _partentId = ParentId;
        }

        public int? Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        public int? ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public string ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }
        public int? ColorId
        {
            get { return _colorId; }
            set { _colorId = value; }
        }
        public string ColorName
        {
            get { return _colorName; }
            set { _colorName = value; }
        }
        public int? SizeId
        {
            get { return _sizeId; }
            set { _sizeId = value; }
        }
        public string SizeName
        {
            get { return _sizeName; }
            set { _sizeName = value; }
        }
        public string Avatar1
        {
            get { return _avatar1; }
            set { _avatar1 = value; }
        }
        public string Avatar2
        {
            get { return _avatar2; }
            set { _avatar2 = value; }
        }
        public string ListImg
        {
            get { return _listImg; }
            set { _listImg = value; }
        }
        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public double PricePromo
        {
            get { return _pricePromo; }
            set { _pricePromo = value; }
        }
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int? ParentId
        {
            get { return _partentId; }
            set { _partentId = value; }
        }
    }
}