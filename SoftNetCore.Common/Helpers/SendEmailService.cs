﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using System;

namespace SoftNetCore.Common.Helpers
{
    public interface ISendEmailService
    {
        void Send(string to, string cc, string subject, string html, string from = null);
    }

    public class SendEmailService : ISendEmailService
    {
        private readonly AppSettings _appSettings;

        public SendEmailService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public void Send(string to, string cc, string subject, string html, string from = null)
        {
            try
            {
                var email = new MimeMessage();
                email.From.Add(MailboxAddress.Parse(from ?? _appSettings.SmtpUser));
                email.To.Add(MailboxAddress.Parse(to));
                if (!string.IsNullOrEmpty(cc))
                    email.Cc.Add(MailboxAddress.Parse(cc));
                email.Subject = subject;
                email.Body = new TextPart(TextFormat.Html) { Text = html };

                using var client = new SmtpClient();
                client.Connect(_appSettings.SmtpHost, _appSettings.SmtpPort);
                client.AuthenticationMechanisms.Remove("XOAUTH2"); // Must be removed for Gmail SMTP
                client.Authenticate(_appSettings.SmtpUser, _appSettings.SmtpPass);
                client.Send(email);
                client.Disconnect(true);
            }
            catch (Exception ex) {
            }
        }
    }
}
