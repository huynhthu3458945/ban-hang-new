﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SoftNetCore.Common.Helpers;

/// <summary>
/// Summary description for ListItem
/// </summary>
public class ListProductItem
{
    public static List<ProductItem> AddList(int productId, string productName, string avatar, double price, int? tempId)
    {
        List<ProductItem> productItem = new List<ProductItem>()
        {
            new ProductItem { ProductId = productId, ProductName = productName, Avatar=avatar , Price = price, TempId = tempId}
        };
        return productItem;
    }
    public static List<ProductItem> Update(List<ProductItem> productItem, int productId, string productName, string avatar, double price, int? tempId)
    {
        ProductItem item = new ProductItem();
        item.ProductId = productId;
        item.ProductName = productName;
        item.Avatar = avatar;
        item.Price = price;
        item.TempId = tempId;
        productItem.Add(item);
        return productItem;
    }
    public static List<ProductItem> Remove(List<ProductItem> productItem, int index)
    {
        if (productItem != null)
        {
            productItem.RemoveAt(index);
        }
        return productItem;
    }
    public static int GetItemIndex(List<ProductItem> productItem, int productId, ref int index)
    {
        bool isExist = false; int indexResut = 0;
        if (productItem != null)
        {
            indexResut = -1;
            foreach (ProductItem item in productItem)
            {
                indexResut = indexResut + 1;
                if (item.ProductId == productId)
                {
                    isExist = true;
                    index = indexResut;
                }
            }

        }
        return isExist == true ? index : 0;
    }

    public static void CheckExists(List<ProductItem> productItem, int productId, ref bool isExist)
    {
        if (productItem != null)
        {
            foreach (ProductItem item in productItem)
            {
                if (item.ProductId == productId)
                {
                    isExist = true;
                    break;
                }
            }
        }
    }
}