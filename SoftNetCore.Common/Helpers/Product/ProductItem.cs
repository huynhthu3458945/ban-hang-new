﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftNetCore.Common.Helpers
{
    public class ProductItem
    {
        private int _pId;
        private string _pName;
        private string _avatar;
        private double _price;
        private int? _tempId;

        public ProductItem()
        {

        }

        public ProductItem(int ProductId, string Avatar, string ProductName, double Price, int? TempId)
        {
            _pId = ProductId;
            _pName = ProductName;
            _avatar = Avatar;
            _price = Price;
            _tempId = TempId;
        }

        public int ProductId
        {
            get { return _pId; }
            set { _pId = value; }
        }

        public string Avatar
        {
            get { return _avatar; }
            set { _avatar = value; }
        }

        public string ProductName
        {
            get { return _pName; }
            set { _pName = value; }
        }


        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public int? TempId
        {
            get { return _tempId; }
            set { _tempId = value; }
        }
    }
}