﻿

namespace SoftNetCore.Common.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }

        public string[] AllowOrigins { get; set; }

        /// <summary>
        /// Refresh token time to live (in days), inactive tokens are
        /// automatically deleted from the database after this time
        /// </summary>
        public int RefreshTokenTTL { get; set; }

        public string EmailFrom { get; set; }

        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        public string SmtpUser { get; set; }

        public string SmtpPass { get; set; }

        public string EmailTo { get; set; }

        public string EmailCompany { get; set; }

        public string SiteName { get; set; }

        public AccessTrade AccessTrade { get; set; }
    }

    public class AccessTrade
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string APIKey { get; set; }
    }
}
