﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Common.Helpers
{
    public static class EnumsTypeMessage
    {
        public static string Success = "gritter-success";
        public static string Error = "gritter-error";
        public static string Info = "gritter-info";
        public static string Warning = "gritter-warning";
    }

    public static class Message
    {
        public static string Success = "Thành công";
        public static string Error = "Thất bại";
    }
}
