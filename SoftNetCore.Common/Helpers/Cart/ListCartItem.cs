﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SoftNetCore.Common.Helpers;

/// <summary>
/// Summary description for ListItem
/// </summary>
public class ListCartItem
{
    public static List<CartItem> AddList(int productId, string productName, string avatar, int colorId, string colorName, int sizeId, string sizeName, int quantity, double price, double originalPrice, int catId)
    {
        List<CartItem> cart = new List<CartItem>()
        {
            new CartItem { ProductId = productId, ProductName = productName, Avatar = avatar, ColorId = colorId, ColorName = colorName, SizeId = sizeId, SizeName = sizeName, Price = price, OriginalPrice = originalPrice, Quantity = quantity, CatId = catId}
        };
        return cart;
    }
    public static List<CartItem> Update(List<CartItem> cart, int productId, string productName, string avatar, int colorId, string colorName, int sizeId, string sizeName, int quantity, double price, double originalPrice, int catId)
    {
        CartItem item = new CartItem();
        item.ProductId = productId;
        item.ProductName = productName;
        item.ColorId = colorId;
        item.ColorName = colorName;
        item.SizeId = sizeId;
        item.SizeName = sizeName;
        item.Quantity = quantity;
        item.Price = price;
        item.OriginalPrice = originalPrice;
        item.Avatar = avatar;
        item.CatId = catId;
        foreach (CartItem i in cart)
        {
            if (i.ProductId == productId && i.ColorId == colorId && i.SizeId == sizeId)
            {
                i.Quantity += quantity;
                return cart;
            }
        }
        cart.Add(item);
        return cart;
    }
    public static List<CartItem> UpdateQuantity(List<CartItem> cart, int productId, int colorId, int sizeId, int quantity)
    {
        foreach (CartItem i in cart)
        {
            if (i.ProductId == productId && i.ColorId == colorId && i.SizeId == sizeId)
            {
                i.Quantity = quantity;
                return cart;
            }
        }
        return cart;
    }
    public static List<CartItem> Remove(List<CartItem> cart, int index)
    {
        if (cart != null)
        {
            cart.RemoveAt(index);
        }
        return cart;
    }

    public static int GetItemIndex(List<CartItem> cart, int productId, ref int index)
    {
        bool isExist = false; int indexResut = 0;
        if (cart != null)
        {
            indexResut = -1;
            foreach (CartItem item in cart)
            {
                indexResut = indexResut + 1;
                if (item.ProductId == productId)
                {
                    isExist = true;
                    index = indexResut;
                }
            }

        }
        return isExist == true ? index : 0;
    }

    public static int GetItemIndex(List<CartItem> cart, int productId, int colorId, int sizeId, ref int index)
    {
        bool isExist = false; int indexResut = 0;
        if (cart != null)
        {
            indexResut = -1;
            foreach (CartItem item in cart)
            {
                indexResut = indexResut + 1;
                if (item.ProductId == productId && item.ColorId == colorId && item.SizeId == sizeId)
                {
                    isExist = true;
                    index = indexResut;
                }
            }

        }
        return isExist == true ? index : 0;
    }
    public static double GetTotal(List<CartItem> cart)
    {
        double total = 0;
        if (cart != null)
        {

            foreach (CartItem item in cart)
            {
                total += item.SubTotal;
            }

        }
        return total;
    }
    public static double GetSubTotal(List<CartItem> cart, int productId, int colorId, int sizeId)
    {
        double subTotal = 0;
        if (cart != null)
        {
            foreach (CartItem item in cart)
            {
                if (item.ProductId == productId && item.ColorId == colorId && item.SizeId == sizeId)
                {
                    subTotal = item.Quantity * item.Price;
                }
            }
        }
        return subTotal;
    }
    public static bool IsNullTotal(List<CartItem> order)
    {
        if (order != null)
        {
            foreach (CartItem item in order)
            {
                if (item.SubTotal != 0)
                {
                    return false;
                }
                else
                    return true;
            }
        }
        return false;
    }
}