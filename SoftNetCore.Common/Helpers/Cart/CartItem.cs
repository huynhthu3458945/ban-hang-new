﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftNetCore.Common.Helpers
{
    public class CartItem
    {
        private int _pId;
        private string _productName;
        private string _aVatar;
        private int _quantity;
        private int _colorId;
        private string _colorName;
        private int _sizeId;
        private string _sizeName;
        private double _price;
        private double _originalPrice; // giá cũ
        private double _subTotal;
        private int _pCatId;


        public CartItem()
        {

        }

        public CartItem(int ProductId, string ProductName, string Avatar, int ColorId, string ColorName, int SizeId, string SizeName, int Quantity, double Price, double originalPrice, int catId)
        {
            _pId = ProductId;
            _productName = ProductName;
            _aVatar = Avatar;
            _colorId = ColorId;
            _colorName = ColorName;
            _sizeId = SizeId;
            _sizeName = SizeName;
            _quantity = Quantity;
            _price = Price;
            _originalPrice = originalPrice;
            _subTotal = Quantity * Price;
            _pCatId = catId;
        }
        public int ProductId
        {
            get { return _pId; }
            set { _pId = value; }
        }
        public string ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }
        public string Avatar
        {
            get { return _aVatar; }
            set { _aVatar = value; }
        }
        public int ColorId
        {
            get { return _colorId; }
            set { _colorId = value; }
        }
        public string ColorName
        {
            get { return _colorName; }
            set { _colorName = value; }
        }
        public int SizeId
        {
            get { return _sizeId; }
            set { _sizeId = value; }
        }
        public string SizeName
        {
            get { return _sizeName; }
            set { _sizeName = value; }
        }
        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public double OriginalPrice
        {
            get { return _originalPrice; }
            set { _originalPrice = value; }
        }
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        public int CatId
        {
            get { return _pCatId; }
            set { _pCatId = value; }
        }
        public double SubTotal
        {
            get { return _quantity * _price; }
        }

    }
}