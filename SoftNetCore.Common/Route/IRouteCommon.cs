﻿using System;
using System.Collections.Generic;
using System.Text;
using SoftNetCore.Model.System;

namespace SoftNetCore.Common.Route
{
    public partial interface IRouteCommon
    {
        List<ApiUrl> GetAllApiUrl();
        List<ActionName> GetAllAction();
    }
}
