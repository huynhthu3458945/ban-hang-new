﻿using Hangfire.Annotations;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hangfire.Services
{
    public interface IBackgroundJobService
    {
        /// <summary>
        /// Các công việc bị trì hoãn cũng chỉ được thực hiện một lần, nhưng không phải ngay lập tức, sau một khoảng thời gian nhất định tính bằng giây
        /// </summary>
        /// <param name="methodCall"></param>
        /// <param name="time"></param>
        public void Schedule([InstantHandle][NotNull] Expression<Func<Task>> methodCall, int time);

        /// <summary>
        /// Lên lịch trình công việc lập đi lập lại
        /// times: get from Cron. Example: Cron.Daily
        /// </summary>
        /// <param name="jobName"></param>
        /// <param name="methodCall"></param>
        /// <param name="times"></param>
        public void ScheduleInterval([InstantHandle][NotNull] Expression<Func<Task>> methodCall, string title, string times);
    }
}
