﻿using Hangfire.Annotations;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hangfire.Services
{
    public class BackgroundJobService : IBackgroundJobService
    {
        public BackgroundJobService()
        {

        }

        public void Schedule([InstantHandle][NotNull] Expression<Func<Task>> methodCall, int time)
        {
            BackgroundJob.Schedule(methodCall, TimeSpan.FromSeconds(time));
        }
       
        public void ScheduleInterval([InstantHandle][NotNull] Expression<Func<Task>> methodCall, string title, string times)
        {
            RecurringJob.AddOrUpdate(title, methodCall, times);
        }
    }
}
