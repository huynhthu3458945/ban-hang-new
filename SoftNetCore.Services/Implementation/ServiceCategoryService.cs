﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class ServiceCategoryService : IServiceCategoryService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<ServiceCategory> _serviceCategoryRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public ServiceCategoryService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<ServiceCategory> serviceCategoryRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _serviceCategoryRepository = serviceCategoryRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<ServiceCategoryModel> FindById(int id)
        {
            return await _serviceCategoryRepository.FindAll().ProjectTo<ServiceCategoryModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<ServiceCategoryModel>> GetAllAsync()
        {
            return await _serviceCategoryRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<ServiceCategoryModel>(_configMapper).ToListAsync();
        }
        public async Task<List<ServiceCategory>> GetDataAsync()
        {
            return await _serviceCategoryRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<ServiceCategoryModel>> GetByStatusAsync(bool status)
        {
            return await _serviceCategoryRepository.FindAll(x => x.Status == status).OrderByDescending(x => x.CreateTime).ProjectTo<ServiceCategoryModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ServiceCategoryModel>> GetAllGroupAsync(int parentId)
        {
            return await _serviceCategoryRepository.FindAll(x => x.ParentId == parentId).OrderByDescending(x => x.CreateTime).ProjectTo<ServiceCategoryModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ServiceCategoryModel>> GetTopAsync(int numberItem)
        {
            var query = _serviceCategoryRepository.FindAll(x => x.Status == true).ProjectTo<ServiceCategoryModel>(_configMapper);
            return await query.OrderByDescending(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(ServiceCategoryModel model)
        {
            try
            {
                var item = _mapper.Map<ServiceCategory>(model);
                item.CreateTime = DateTime.Now;
                _serviceCategoryRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(ServiceCategoryModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<ServiceCategory>(model);
                _serviceCategoryRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            var item = _serviceCategoryRepository.FindById(id);
            try
            {
                item.Status = item.Status == true ? false : true;
                _serviceCategoryRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _serviceCategoryRepository.FindById(id);
                _serviceCategoryRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _serviceCategoryRepository.FindAll().ProjectTo<ServiceCategory>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
