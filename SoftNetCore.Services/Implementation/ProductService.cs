﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;
using System.Net.Http;
using AffilateServices.Models.Dimuadi;
using AffilateServices.Helpers;
using System.Text.Json;
using SoftNetCore.Common.Extensions;
using AffilateServices.Models.Accesstrade;
using SoftNetCore.Model.vnz;

namespace SoftNetCore.Services.Implementation
{
    public class ProductService : IProductService
    {
        private readonly IHttpClientFactory _clientFactory;

        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Product> _productRepository;
        private readonly IBaseRepository<ProductCategory> _productCategoryRepository;
        private readonly IBaseRepository<ProductAttribute> _productAttributeRepository;
        private readonly IBaseRepository<MerchantSetting> _merchantSettingRepository;
        private readonly IBaseRepository<FetchDataAudit> _fetchDataAuditRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;

        private ProcessingResult processingResult;

        public ProductService(
            IHttpClientFactory httpClientFactory,
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Product> productRepository,
            IBaseRepository<ProductCategory> productCategoryRepository,
            IBaseRepository<ProductAttribute> productAttributeRepository,
            IBaseRepository<MerchantSetting> merchantSettingRepository,
            IBaseRepository<FetchDataAudit> fetchDataAuditRepository,
            IHostingEnvironment currentEnvironment,
            IUnitOfWork unitOfWork)
        {
            _clientFactory = httpClientFactory;
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _productRepository = productRepository;
            _productCategoryRepository = productCategoryRepository;
            _productAttributeRepository = productAttributeRepository;
            _merchantSettingRepository = merchantSettingRepository;
            _fetchDataAuditRepository = fetchDataAuditRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        #region product
        public async Task<ProductModel> FindById(int id)
        {
            return await _productRepository.FindAll().ProjectTo<ProductModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<ProductModel>> GetAllProductAsync()
        {
            return await _productRepository.FindAll(z => z.Status == true).OrderByDescending(x => x.CreateTime).ProjectTo<ProductModel>(_configMapper).ToListAsync();
        }

        public IQueryable<ProductModel> GetAll()
        {
            return _productRepository.FindAll(z => z.Status == true).OrderByDescending(x => x.CreateTime).ProjectTo<ProductModel>(_configMapper);
        }

        public async Task<List<ProductModel>> GetByCategoryAsync(int catId)
        {
            return await _productRepository.FindAll(x => x.ProductCategoryId == catId && x.Status == true).OrderBy(x => x.Position).ProjectTo<ProductModel>(_configMapper).ToListAsync();
        }

        public IQueryable<ProductModel> GetProductsByCategoryId(int productCateforyId, int sort)
        {
            var result = _productRepository
                .FindAll(x => x.Status == true && x.ProductCategoryId == productCateforyId)
                .ProjectTo<ProductModel>(_configMapper);

            if (sort == 0) { result = result.OrderByDescending(x => x.CreateTime); }
            else if (sort == 1) { result = result.OrderBy(x => x.Position); }
            else if (sort == 2) { result = result.OrderBy(x => x.PriceSale); }
            else if (sort == 3) { result = result.OrderByDescending(x => x.PriceSale); }

            return result;
        }

        public async Task<List<ProductModel>> GetSortByCategoryAsync(int catId, int sort)
        {
            //IQueryable<int> subCatList = Enumerable.Empty<int>().AsQueryable(); ;
            //var childCat = _productCategoryRepository.FindAll(x => x.ParentId == catId).FirstOrDefault();
            //if (childCat != null)
            //{
            //    subCatList = _productCategoryRepository.FindAll(x => x.ParentId == childCat.Id && x.Status == true).Select(x => x.Id);
            //}

            //var result = _productRepository.FindAll(x => (x.ProductCategoryId == catId
            //                                          || (x.ProductCategory != null && x.ProductCategory.ParentId == catId)
            //                                          || (subCatList.Count() > 0 && subCatList.Any(y => y == x.ProductCategoryId)))
            //                                          && x.Status == true).ProjectTo<ProductModel>(_configMapper);
            //if (sort == 0) { result = result.OrderByDescending(x => x.CreateTime); }
            //else if (sort == 1) { result = result.OrderBy(x => x.Position); }
            //else if (sort == 2) { result = result.OrderBy(x => x.PriceSale); }
            //else if (sort == 3) { result = result.OrderByDescending(x => x.PriceSale); }
            //return await result.ToListAsync();
            IQueryable<ProductModel> result;
            if (catId == 0)
            {
                result = _productRepository.FindAll(x => x.ProductCategory.Status == true && x.Status == true).ProjectTo<ProductModel>(_configMapper);
            }
            else
            {
                result = _productRepository.FindAll(x => x.ProductCategory.Status == true && x.ProductCategoryId == catId && x.Status == true).ProjectTo<ProductModel>(_configMapper);
            }

            if (sort == 0) { result = result.OrderByDescending(x => x.CreateTime); }
            else if (sort == 1) { result = result.OrderBy(x => x.Position); }
            else if (sort == 2) { result = result.OrderBy(x => x.PriceSale); }
            else if (sort == 3) { result = result.OrderByDescending(x => x.PriceSale); }
            return await result.ToListAsync();
        }

        public async Task<List<ProductModel>> GetBySaleAsync(int saleId)
        {
            return await _productRepository.FindAll(x => x.SaleId == saleId && x.Status == true).OrderBy(x => x.Position).ProjectTo<ProductModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ProductModel>> GetByCategoryAsync(int catId, int numberItem)
        {
            return await _productRepository.FindAll(x => x.ProductCategoryId == catId && x.Status == true).OrderBy(x => x.Position).Take(numberItem).ProjectTo<ProductModel>(_configMapper).ToListAsync();
        }

        public async Task<List<Product>> GetDataAsync()
        {
            return await _productRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public Product GetById(int id)
        {
            return _productRepository.FindById(id);
        }

        public async Task<List<ProductModel>> GetByStatusAsync(bool status)
        {
            return await _productRepository.FindAll(x => x.Status == status).ProjectTo<ProductModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ProductModel>> SearchAsync(string keyword)
        {
            return await _productRepository.FindAll(x => x.Alias.Contains(keyword) && x.Status == true).ProjectTo<ProductModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(ProductModel model)
        {
            try
            {
                var item = _mapper.Map<Product>(model);
                item.Alias = item.Title.ToUrlFormat(true);
                item.CreateTime = DateTime.Now;
                _productRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(ProductModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Product>(model);
                item.Alias = item.Title.ToUrlFormat(true);
                _productRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _productRepository.FindById(id);
                _productRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _productRepository.FindAll().ProjectTo<Product>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<Pager> SearchPaginationAsync(SearchPagination paramater)
        {
            var query = _productRepository.FindAll(x => x.Title.Contains(paramater.strSearch)).ProjectTo<Product>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<Pager> Search2PaginationAsync(Search2Pagination paramater)
        {
            var query = _productRepository.FindAll(x => x.Title.Contains(paramater.strSearch) && x.ProductCategoryId == paramater.catId).ProjectTo<Product>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<Pager> FilterPaginationAsync(FilterPagination paramater)
        {
            var query = _productRepository.FindAll().ProjectTo<Product>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<List<ProductModel>> GetHotAsync(int numberItem)
        {
            var query = _productRepository
                .FindAll(x => x.ProductCategory.Status == true && x.Status == true && x.IsProductHot == true)
                .AsNoTracking()
                .ProjectTo<ProductModel>(_configMapper)
                .Take(numberItem);

            return await query.OrderBy(x => x.Position).ToListAsync();
        }

        public async Task<List<ProductModel>> GetNewAsync(int numberItem)
        {
            var query = _productRepository
                .FindAll(x => x.Status == true && x.IsProductNew == true)
                .AsNoTracking()
                .Take(numberItem)
                .ProjectTo<ProductModel>(_configMapper);

            return await query.OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<ProductModel>> GetRandomAsync(int numberItem)
        {
            var query = _productRepository
                .FindAll(x => x.ProductCategory.Status == true && x.Status == true)
                .AsNoTracking()
                .ProjectTo<ProductModel>(_configMapper)
                .Take(numberItem);

            var results = await query.ToListAsync();

            return results;
        }

        public async Task<List<ProductModel>> GetRelatedAsync(int id, int catId, int numberItem)
        {
            var query = _productRepository.FindAll(x => x.ProductCategoryId == catId && x.Id != id && x.Status == true).ProjectTo<ProductModel>(_configMapper);
            return await query.OrderBy(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<List<ProductModel>> GetTopAsync(int numberItem)
        {
            var query = _productRepository.FindAll(x => x.Status == true).ProjectTo<ProductModel>(_configMapper);
            return await query.OrderBy(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<List<ProductModel>> GetSaleAsync(int numberItem)
        {
            var query = _productRepository
                .FindAll(x => x.ProductCategory.Status == true && x.Status == true && x.CommissionType.Equals("percent"))
                .AsNoTracking()
                .Take(numberItem)
                .ProjectTo<ProductModel>(_configMapper);

            return await query.OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            var item = _productRepository.FindById(id);
            try
            {
                item.Status = item.Status.ToBool() == true ? false : true;
                _productRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateViewAsync(int id)
        {
            var item = _productRepository.FindById(id);
            try
            {
                item.ViewTime += item.ViewTime.ToInt();
                _productRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateSaleAsync(int id, int? saleId)
        {
            try
            {
                var item = _productRepository.FindById(id);
                item.SaleId = saleId;
                _productRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateSaleRangeAsync(List<ProductItem> updateProducts, int? saleId)
        {
            try
            {
                var productIds = updateProducts.Select(p => p.ProductId).ToList();
                var products = await _productRepository.FindAll(p => productIds.Any(pid => pid == p.Id)).ToListAsync();
                var updatedProducts = new List<Product>();
                foreach (var item in updateProducts)
                {
                    var product = products.FirstOrDefault(p => p.Id == item.ProductId);
                    if (product == null) continue;
                    product.SaleId = saleId;
                    updatedProducts.Add(product);
                }
                _productRepository.UpdateRange(updatedProducts);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { saleId = saleId });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateSaleRange2Async(List<ProductModel> updateProducts, int? saleId)
        {
            try
            {
                var productIds = updateProducts.Select(p => p.Id).ToList();
                var products = await _productRepository.FindAll(p => productIds.Any(pid => pid == p.Id)).ToListAsync();
                var updatedProducts = new List<Product>();
                foreach (var item in updateProducts)
                {
                    var product = products.FirstOrDefault(p => p.Id == item.Id);
                    if (product == null) continue;
                    product.SaleId = saleId;
                    updatedProducts.Add(product);
                }
                _productRepository.UpdateRange(updatedProducts);
                await _unitOfWork.SaveChangesAsync();

                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { saleId = saleId });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateRatingAsync(int id, int? total, decimal? value)
        {
            try
            {
                var data = _productRepository.FindAll(x => x.Id == id).FirstOrDefault();
                data.TotalAssess = total;
                data.ValueAssess = value;
                _productRepository.Update(data);
                await _unitOfWork.SaveChangesAsync();

                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
        #endregion

        #region Lấy dữ liệu từ nguồn đối tác (ACCESSTRADE, DIMUADI)
        /// <summary>
        /// Lấy sản phẩm từ DIMUADI theo loại (API của DIMUADI)
        /// THUAN - ConvertingImageUrls, add insert desc, content
        /// </summary>
        /// <param name="productCategoryId">Loại sản phẩm</param>
        /// <param name="pageNumber">page</param>
        /// <param name="pageSize">limit</param>
        /// <returns></returns>
        public async Task<ProcessingResult> FetchProductFromDimuadi(int productCategoryId, string token, int pageNumber, int pageSize)
        {
            try
            {
                var isFinished = false;
                var productCategory = await _productCategoryRepository.FindByIdAsync(productCategoryId);
                if (productCategory == null) throw new Exception("Không tìm thấy danh mục");

                var requestUrl = $"https://apis.dimuadi.vn/d2c-service/product/?category_id={productCategory.ProductCategoryId}&page={pageNumber}&page_size={pageSize}";
                var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);

                var client = _clientFactory.CreateClient();
                var response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var serializeOptions = new JsonSerializerOptions();
                    serializeOptions.Converters.Add(new StringConverter());
                    var responseData = await JsonSerializer.DeserializeAsync<GetProductResponse>(responseStream, serializeOptions);

                    var allProduct = await _productRepository.GetAll().AsNoTracking().ToListAsync();
                    var newProducts = new List<Product>();
                    var updateProducts = new List<Product>();
                    isFinished = pageNumber >= responseData.paging.totalPage;
                    foreach (var res in responseData.data)
                    {
                        var images = new List<string>();
                        var avatarUrl = string.Empty;
                        var thumbUrl = string.Empty;
                        if (!string.IsNullOrEmpty(res.thumbImage))
                        {
                            //ConvertingImageUrls - Nếu convert được sẽ lấy ảnh mới, ngược lại lấy ảnh gốc 
                            var newAvatarUrl = await ConvertingImageUrlAsync(res.thumbImage, 600, 600);
                            if (!string.IsNullOrEmpty(newAvatarUrl)) avatarUrl = newAvatarUrl;

                            var newThumbUrl = await ConvertingImageUrlAsync(res.thumbImage, 245, 245);
                            if (!string.IsNullOrEmpty(newThumbUrl)) thumbUrl = newThumbUrl;
                        }
                        if (res.images.Any())
                        {
                            //ConvertingImageUrls
                            foreach (var image in res.images)
                            {
                                var newImageUrl = await ConvertingImageUrlAsync(image, 600, 600);
                                if (string.IsNullOrEmpty(newImageUrl)) continue;

                                images.Add(newImageUrl);
                            }
                        }

                        var existed = allProduct.FirstOrDefault(p => p.ProductId == res.id);
                        if (existed != null)
                        {
                            existed.ImageListProduct = images.ToJson();
                            existed.IsProductHot = res.isHot;
                            existed.Title = res.name;
                            existed.Price = res.price;
                            existed.Sku = res.sku;
                            existed.ShipPrice = res.shipPrice;
                            existed.Avatar = avatarUrl ?? res.thumbImage;
                            existed.Thumb = thumbUrl ?? res.thumbImage;
                            existed.ModifyTime = res.updatedAt;
                            existed.CreateTime = res.createdAt;
                            existed.Status = res.status == 3 ? false : true;

                            var responseContent = await FetchDimuadiProductDetailAsync(existed.ProductId, token);
                            if (responseContent != null)
                            {
                                var productData = responseContent.data;
                                existed.Content = productData.contents.ToJson();
                                existed.Slug = productData.slug;
                                existed.Description = productData.description;
                                existed.CommissionValue = productData.commissionValue;
                                existed.CommissionType = productData.commissionType;
                            }
                            updateProducts.Add(existed);
                        }
                        else
                        {
                            var responseContent = await FetchDimuadiProductDetailAsync(res.id, token);
                            var productData = responseContent?.data;
                            var newProduct = new Product()
                            {
                                ProductCategoryId = productCategory.Id,
                                ProductId = res.id,
                                ImageListProduct = images.ToJson(),
                                IsProductHot = res.isHot,
                                Title = res.name,
                                Price = res.price,
                                Sku = res.sku,
                                ShipPrice = res.shipPrice,
                                Avatar = avatarUrl ?? res.thumbImage,
                                Thumb = thumbUrl ?? res.thumbImage,
                                ModifyTime = res.updatedAt,
                                CreateTime = res.createdAt,
                                Status = res.status == 3 ? false : true, /// hiển thị sản phẩm
                                //
                                Content = productData?.contents.ToJson(),
                                Slug = productData?.slug,
                                Description = productData?.description,
                                CommissionValue = productData?.commissionValue,
                                CommissionType = productData?.commissionType,
                            };
                            newProducts.Add(newProduct);
                        }
                    }

                    if (updateProducts.Count > 0)
                    {
                        _productRepository.UpdateRange(updateProducts);
                    }

                    if (newProducts.Count > 0)
                    {
                        _productRepository.AddRange(newProducts);
                    }
                    await _unitOfWork.SaveChangesAsync();
                }

                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = isFinished ? "Hoàn tất" : "Thành công", Success = !isFinished };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = ex.Message, Success = false };
            }
            return processingResult;
        }

        /// <summary>
        /// Cập nhật content cho sản phẩm từ dimuadi
        /// </summary>
        /// <param name="bearerToken"></param>
        /// <returns></returns>
        public async Task UpdateDimuadiProducts(string bearerToken)
        {
            var dimuadiProducts = await _productCategoryRepository
                .FindAll(p => p.Domain == EnumDomains.DIMUADI.GetName())
                .Include(pc => pc.Products)
                .SelectMany(pc => pc.Products)
                .ToListAsync();

            var updateProducts = new List<Product>();
            foreach (var product in dimuadiProducts)
            {
                var response = await FetchDimuadiProductDetailAsync(product.ProductId, bearerToken);
                if (response == null) continue;

                var productData = response.data;
                product.Content = productData.contents.ToJson();
                product.Slug = productData.slug;
                product.Description = productData.description;
                product.CommissionValue = productData.commissionValue;
                product.CommissionType = productData.commissionType;

                updateProducts.Add(product);
            }

            _productRepository.UpdateRange(updateProducts);
            await _unitOfWork.SaveChangesAsync();
        }


        public async Task<bool> GetAccessTradeProductsAsync(string domain, string token, int page, int pageSize)
        {
            var requestUrl = $"https://api.accesstrade.vn/v1/datafeeds?domain={domain}&page={page}&limit={pageSize}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);

            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Token", token);

            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var data = await JsonSerializer.DeserializeAsync<GetProductResponseModel>(responseStream, serializeOptions);

                await SaveAccresstradeProductToLocalAsync(data);

                return true;
            }

            return false;
        }

        private async Task<GetProductDetailResponse> FetchDimuadiProductDetailAsync(string productId, string bearerToken)
        {
            try
            {
                var requestUrl = $"https://apis.dimuadi.vn/d2c-service/product/{productId}";
                var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);
                var client = _clientFactory.CreateClient();
                var response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var serializeOptions = new JsonSerializerOptions();
                    serializeOptions.Converters.Add(new StringConverter());
                    var responseData = await JsonSerializer.DeserializeAsync<GetProductDetailResponse>(responseStream, serializeOptions);

                    return responseData;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task SaveAccresstradeProductToLocalAsync(GetProductResponseModel getProductResponse)
        {
            if (getProductResponse == null) return;

            var data = getProductResponse.Data;
            var allProducts = await _productRepository.GetAll().AsNoTracking().ToListAsync();
            var allProductCategories = await _productCategoryRepository.GetAll().AsNoTracking().ToListAsync();
            var newProducts = new List<Product>();
            foreach (var product in data)
            {
                var productExisted = allProducts.FirstOrDefault(p => p.Aff_link == product.Aff_link);
                if (productExisted != null) continue;

                var productCategory = allProductCategories.FirstOrDefault(p => p.Domain.ToLower().Contains(product.Domain.ToLower()));
                if (productCategory == null) continue;

                var productEntity = await MappingToProductAsync(product, productCategory.Id); // mới
                //var productEntity = await MappingToProduct(product, productCategory.Id); // cũ
                newProducts.Add(productEntity);
            }

            if (newProducts.Any())
            {
                _productRepository.AddRange(newProducts);
                await _unitOfWork.SaveChangesAsync();
            }
        }

        /// <summary>
        /// THUAN
        /// </summary>
        /// <param name="datum"></param>
        /// <param name="productCategoryId"></param>
        /// <returns></returns>
        private async Task<Product> MappingToProductAsync(GetProductResponseModel.ProductDatum datum, int? productCategoryId)
        {
            var avatarUrl = string.Empty;
            var thumbUrl = string.Empty;
            if (!string.IsNullOrEmpty(datum.Image))
            {
                //ConvertingImageUrls - Nếu convert được sẽ lấy ảnh mới, ngược lại lấy ảnh gốc 
                var newAvatarUrl = await ConvertingImageUrlAsync(datum.Image, 600, 600);
                if (!string.IsNullOrEmpty(newAvatarUrl)) avatarUrl = newAvatarUrl;

                var newThumbUrl = await ConvertingImageUrlAsync(datum.Image, 245, 245);
                if (!string.IsNullOrEmpty(newThumbUrl)) thumbUrl = newThumbUrl;
            }
            return new Product()
            {
                ProductCategoryId = productCategoryId,
                Cate = datum.Cate,
                Price = datum.Price,
                Aff_link = datum.Aff_link,
                Description = datum.Desc,
                PriceAfterDiscount = datum.Discount,
                DiscountAmount = datum.Discount_amount,
                DiscountRate = datum.Discount_rate,
                Domain = datum.Domain,
                Avatar = avatarUrl ?? datum.Image,
                Thumb = thumbUrl ?? datum.Image,
                Merchant = datum.Merchant,
                Title = datum.Name,
                ProductId = datum.Product_id,
                Sku = datum.Sku,
                UpdateTime = datum.UpdateTime,
                RootUrl = datum.Url,
            };
        }

        /// <summary>
        /// Tú
        /// </summary>
        /// <param name="datum"></param>
        /// <param name="productCategoryId"></param>
        /// <returns></returns>
        private static Product MappingToProduct(GetProductResponseModel.ProductDatum datum, int? productCategoryId)
        {
            return new Product()
            {
                ProductCategoryId = productCategoryId,
                Cate = datum.Cate,
                Price = datum.Price,
                Aff_link = datum.Aff_link,
                Description = datum.Desc,
                PriceAfterDiscount = datum.Discount,
                DiscountAmount = datum.Discount_amount,
                DiscountRate = datum.Discount_rate,
                Domain = datum.Domain,
                Avatar = datum.Image,
                Thumb = datum.Image,
                Merchant = datum.Merchant,
                Title = datum.Name,
                ProductId = datum.Product_id,
                Sku = datum.Sku,
                UpdateTime = datum.UpdateTime,
                RootUrl = datum.Url,
            };
        }

        public async Task<int> GetTotalProductByCategory(int categoryId)
        {
            return _productRepository.FindAll().Count(z => z.ProductCategoryId == categoryId);
        }

        /// <summary>
        /// Kéo all sản phẩm từ accesstrade
        /// </summary>
        /// <param name="url"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="tokenValue"></param>
        /// <returns></returns>
        public async Task FetchAccesstradeData(string url, int page, int pageSize, string tokenValue)
        {
            var requestUrl = $"{url}&page={page}&limit={pageSize}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Token", tokenValue);
            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var responseData = await JsonSerializer.DeserializeAsync<GetProductResponseModel>(responseStream, serializeOptions);
                var allProducts = await _productRepository.GetAll().AsNoTracking().ToListAsync();
                var allProductCategories = await _productCategoryRepository.GetAll().AsNoTracking().ToListAsync();
                var newProducts = new List<Product>();
                var updateProducts = new List<Product>();
                foreach (var product in responseData.Data)
                {
                    var productExisted = allProducts.FirstOrDefault(p => p.Aff_link == product.Aff_link);
                    if (productExisted != null)
                    {
                        var avatarUrl = string.Empty;
                        var thumbUrl = string.Empty;
                        if (!string.IsNullOrEmpty(product.Image))
                        {
                            //ConvertingImageUrls - Nếu convert được sẽ lấy ảnh mới, ngược lại lấy ảnh gốc 
                            var newAvatarUrl = await ConvertingImageUrlAsync(product.Image, 600, 600);
                            if (!string.IsNullOrEmpty(newAvatarUrl)) avatarUrl = newAvatarUrl;

                            var newThumbUrl = await ConvertingImageUrlAsync(product.Image, 245, 245);
                            if (!string.IsNullOrEmpty(newThumbUrl)) thumbUrl = newThumbUrl;
                        }

                        productExisted.ProductCategoryId = null;
                        productExisted.Cate = product.Cate;
                        productExisted.Price = product.Price;
                        productExisted.Aff_link = product.Aff_link;
                        productExisted.Description = product.Desc;
                        productExisted.PriceAfterDiscount = product.Discount;
                        productExisted.DiscountAmount = product.Discount_amount;
                        productExisted.DiscountRate = product.Discount_rate;
                        productExisted.Domain = product.Domain;
                        productExisted.Avatar = avatarUrl ?? product.Image;
                        productExisted.Thumb = thumbUrl ?? product.Image;
                        productExisted.Merchant = product.Merchant;
                        productExisted.Title = product.Name;
                        productExisted.ProductId = product.Product_id;
                        productExisted.Sku = product.Sku;
                        productExisted.UpdateTime = product.UpdateTime;
                        productExisted.RootUrl = product.Url;
                        updateProducts.Add(productExisted);
                    }
                    else
                    {
                        var productEntity = await MappingToProductAsync(product, null); // mới
                        //var productEntity =  MappingToProduct(product, null); // cũ
                        newProducts.Add(productEntity);
                    }
                }

                if (updateProducts.Count > 0)
                {
                    _productRepository.UpdateRange(updateProducts);
                }

                if (newProducts.Count > 0)
                {
                    _productRepository.AddRange(newProducts);
                }

                await _unitOfWork.SaveChangesAsync();
            }
        }

        #region Chức năng dùng cho tiến trình tự động cập nhật sản phẩm từ các nền tảng TMĐT

        public async Task UpdateAccessTradeTokenAsync()
        {
            var requestUrl = $"https://accesstrade.vnsosoft.com/?key=8bb6f75d673f4a7d9d88041f34cb5777";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var responseData = await JsonSerializer.DeserializeAsync<GetRefreshAccesstradeTokenResponse>(responseStream, serializeOptions);
                var merchantSetting = await _merchantSettingRepository.FindAll().FirstOrDefaultAsync(m => m.MerchantId == EnumDomains.ACCESSTRADE);
                if (responseData.success)
                {
                    merchantSetting.TokenValue = responseData.data.token;
                    _merchantSettingRepository.Update(merchantSetting);
                    await _unitOfWork.SaveChangesAsync();
                }

            }
        }

        /// <summary>
        /// Tự động cập nhật theo danh mục đã được định nghĩa tại ProductCategory table
        /// </summary>
        /// <param name="productCategoryId"></param>
        /// <returns></returns>
        public async Task FetchProductByCategoryIdAsync(int productCategoryId)
        {
            //await UpdateAccessTradeTokenAsync();

            var productCategory = await _productCategoryRepository
                .FindAll(p => p.Id == productCategoryId)
                .Include(p => p.MerchantProductByCategories)
                .FirstOrDefaultAsync();

            if (productCategory != null && productCategory.MerchantProductByCategories != null && productCategory.MerchantProductByCategories.Count() > 0)
            {
                _logging.Log($"Chạy tiến trình cho danh mục {productCategory.Title} lúc {DateTime.Now.ToShortDateString()}");

                var merchantSettings = await _merchantSettingRepository.FindAll().AsNoTracking().ToListAsync();
                foreach (var merchantConfig in productCategory.MerchantProductByCategories)
                {
                    await UpdateDataFromDomainAsync(merchantConfig, merchantSettings);
                }
            }
        }

        /// <summary>
        /// Gọi hàm thực thi theo từng nền tảng
        /// </summary>
        /// <param name="config"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        private async Task UpdateDataFromDomainAsync(MerchantProductByCategory config, List<MerchantSetting> merchantSettings)
        {
            switch (config.DomainId)
            {
                case EnumDomains.ACCESSTRADE:
                    var accessTradeTokenSettings = merchantSettings.FirstOrDefault(m => m.MerchantId == EnumDomains.ACCESSTRADE);
                    await ProcessFromAccesstradeAsync(config, 1, accessTradeTokenSettings, true);
                    break;
                case EnumDomains.DIMUADI:
                    var dimuadiTokenSettings = merchantSettings.FirstOrDefault(m => m.MerchantId == EnumDomains.DIMUADI);
                    await ProcessFromDimuadiAsync(config, 1, dimuadiTokenSettings, true);
                    break;
            }
        }

        /// <summary>
        /// Cấu hình cố định kéo dữ liệu sản phẩm từ accesstrade
        /// </summary>
        /// <param name="config"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        private async Task ProcessFromAccesstradeAsync(MerchantProductByCategory config, int page, MerchantSetting merchantSetting, bool isCallback)
        {
            var requestUrl = $"{config.Url}&page={page}&limit={config.PageSize}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(merchantSetting.TokenKey, merchantSetting.TokenValue);
            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var responseData = await JsonSerializer.DeserializeAsync<GetProductResponseModel>(responseStream, serializeOptions);
                await UpdateProductFromAccessTradeAsync(responseData, config);

                await SaveDataAuditAsync(response.IsSuccessStatusCode, requestUrl, $"{merchantSetting.TokenKey} {merchantSetting.TokenValue}", responseData.ToJson());

                var currentTotal = page * config.PageSize;
                isCallback = currentTotal < responseData.Total;
                if (isCallback)
                {
                    await ProcessFromAccesstradeAsync(config, page + 1, merchantSetting, isCallback);
                }
            }
        }

        /// <summary>
        /// Tiến trình cập nhật sản phẩm đã kéo từ accesstrade
        /// </summary>
        /// <param name="getProductResponse"></param>
        /// <returns></returns>
        public async Task UpdateProductFromAccessTradeAsync(GetProductResponseModel getProductResponse, MerchantProductByCategory config)
        {
            if (getProductResponse == null) return;

            var data = getProductResponse.Data;
            var allProducts = await _productRepository.GetAll().AsNoTracking().ToListAsync();
            var allProductCategories = await _productCategoryRepository.GetAll().AsNoTracking().ToListAsync();
            var newProducts = new List<Product>();
            var updateProducts = new List<Product>();
            foreach (var product in data)
            {
                var productExisted = allProducts.FirstOrDefault(p => p.Aff_link == product.Aff_link);

                var avatarUrl = string.Empty;
                var thumbUrl = string.Empty;
                if (!string.IsNullOrEmpty(product.Image))
                {
                    //ConvertingImageUrls - Nếu convert được sẽ lấy ảnh mới, ngược lại lấy ảnh gốc
                    var newAvatarUrl = await ConvertingImageUrlAsync(product.Image, 600, 600);
                    if (!string.IsNullOrEmpty(newAvatarUrl)) avatarUrl = newAvatarUrl;

                    var newThumbUrl = await ConvertingImageUrlAsync(product.Image, 245, 245);
                    if (!string.IsNullOrEmpty(newThumbUrl)) thumbUrl = newThumbUrl;
                }

                if (productExisted != null)
                {
                    productExisted.ProductCategoryId = config.ProductCategoryId;
                    productExisted.Price = product.Price;
                    productExisted.Aff_link = product.Aff_link;
                    productExisted.Description = product.Desc;
                    productExisted.PriceAfterDiscount = product.Discount;
                    productExisted.DiscountAmount = product.Discount_amount;
                    productExisted.DiscountRate = product.Discount_rate;
                    productExisted.Domain = product.Domain;
                    productExisted.Avatar = avatarUrl ?? product.Image;
                    productExisted.Thumb = thumbUrl ?? product.Image;
                    productExisted.Merchant = product.Merchant;
                    productExisted.Title = product.Name;
                    productExisted.ProductId = product.Product_id;
                    productExisted.Sku = product.Sku;
                    productExisted.UpdateTime = product.UpdateTime;
                    productExisted.RootUrl = product.Url;

                    updateProducts.Add(productExisted);
                }
                else
                {
                    var newProduct = new Product()
                    {
                        Provider = config.DomainId.GetName(),
                        ProductCategoryId = config.ProductCategoryId,
                        Cate = product.Cate,
                        Price = product.Price,
                        Aff_link = product.Aff_link,
                        Description = product.Desc,
                        PriceAfterDiscount = product.Discount,
                        DiscountAmount = product.Discount_amount,
                        DiscountRate = product.Discount_rate,
                        Domain = product.Domain,
                        Avatar = avatarUrl ?? product.Image,
                        Thumb = thumbUrl ?? product.Image,
                        Merchant = product.Merchant,
                        Title = product.Name,
                        ProductId = product.Product_id,
                        Sku = product.Sku,
                        UpdateTime = product.UpdateTime,
                        RootUrl = product.Url,
                    };

                    newProducts.Add(newProduct);
                }
            }

            if (updateProducts.Count > 0)
            {
                _productRepository.UpdateRange(updateProducts);
            }

            if (newProducts.Count > 0)
            {
                _productRepository.AddRange(newProducts);
            }

            await _unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Cấu hình cố định kéo dữ liệu sản phẩm từ Dimuadi
        /// </summary>
        /// <param name="config"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="isCallback"></param>
        /// <returns></returns>
        private async Task ProcessFromDimuadiAsync(MerchantProductByCategory config, int page, MerchantSetting merchantSetting, bool isCallback)
        {
            /// Url kéo sản phẩm theo danh muc
            var requestUrl = $"{config.Url}&page={page}&page_size={config.PageSize}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(merchantSetting.TokenKey, merchantSetting.TokenValue);

            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var responseData = await JsonSerializer.DeserializeAsync<GetProductResponse>(responseStream, serializeOptions);

                var allProduct = await _productRepository.GetAll().AsNoTracking().ToListAsync();
                var newProducts = new List<Product>();
                var updateProducts = new List<Product>();
                foreach (var res in responseData.data)
                {
                    var images = new List<string>();
                    var avatarUrl = string.Empty;
                    var thumbUrl = string.Empty;
                    if (!string.IsNullOrEmpty(res.thumbImage))
                    {
                        //ConvertingImageUrls - Nếu convert được sẽ lấy ảnh mới, ngược lại lấy ảnh gốc
                        var newAvatarUrl = await ConvertingImageUrlAsync(res.thumbImage, 600, 600);
                        if (!string.IsNullOrEmpty(newAvatarUrl)) avatarUrl = newAvatarUrl;

                        var newThumbUrl = await ConvertingImageUrlAsync(res.thumbImage, 245, 245);
                        if (!string.IsNullOrEmpty(newThumbUrl)) thumbUrl = newThumbUrl;
                    }
                    if (res.images.Any())
                    {
                        //ConvertingImageUrls
                        foreach (var image in res.images)
                        {
                            var newImageUrl = await ConvertingImageUrlAsync(image, 600, 600);
                            if (string.IsNullOrEmpty(newImageUrl)) continue;

                            images.Add(newImageUrl);
                        }
                    }

                    var existed = allProduct.FirstOrDefault(p => p.ProductId == res.id);
                    if (existed != null)
                    {
                        existed.ProductCategoryId = config.ProductCategoryId;
                        existed.ProductId = res.id;
                        existed.ImageListProduct = images.ToJson();
                        existed.IsProductHot = res.isHot;
                        existed.Title = res.name;
                        existed.Price = res.price;
                        existed.Sku = res.sku;
                        existed.ShipPrice = res.shipPrice;
                        existed.Avatar = avatarUrl ?? res.thumbImage;
                        existed.Thumb = thumbUrl ?? res.thumbImage;
                        existed.ModifyTime = res.updatedAt;
                        existed.CreateTime = res.createdAt;
                        existed.CommissionType = res.commissionType;
                        //existed.CommissionValue = res.commissionValue;
                        existed.Status = true; /// hiển thị sản phẩm

                        updateProducts.Add(existed);
                    }

                    var newProduct = new Product()
                    {
                        Provider = config.DomainId.GetName(),
                        Domain = "dimuadi.vn",
                        ProductCategoryId = config.ProductCategoryId,
                        ProductId = res.id,
                        ImageListProduct = images.ToJson(),
                        IsProductHot = res.isHot,
                        Title = res.name,
                        Price = res.price,
                        Sku = res.sku,
                        ShipPrice = res.shipPrice,
                        Avatar = avatarUrl ?? res.thumbImage,
                        Thumb = thumbUrl ?? res.thumbImage,
                        ModifyTime = res.updatedAt,
                        CreateTime = res.createdAt,
                        CommissionType = res.commissionType,
                        //CommissionValue = res.commissionValue,
                        Status = true /// hiển thị sản phẩm
                    };

                    newProducts.Add(newProduct);
                }

                if (updateProducts.Count > 0)
                {
                    _productRepository.UpdateRange(updateProducts);
                }

                if (newProducts.Count > 0)
                {
                    _productRepository.AddRange(newProducts);
                }

                await _unitOfWork.SaveChangesAsync();

                await SaveDataAuditAsync(response.IsSuccessStatusCode, requestUrl, $"{merchantSetting.TokenKey} {merchantSetting.TokenValue}", responseData.ToJson());

                isCallback = page < responseData.paging.totalPage;
                if (isCallback)
                {
                    await ProcessFromDimuadiAsync(config, page + 1, merchantSetting, isCallback);
                }
            }
        }

        #endregion

        private async Task SaveDataAuditAsync(bool isSuccess, string url, string token, string contents)
        {
            var audit = new FetchDataAudit()
            {
                IsSuccess = isSuccess,
                Url = url,
                Token = token,
                Contents = contents,
                CreatedTime = DateTime.UtcNow
            };

            _fetchDataAuditRepository.Add(audit);
            await _unitOfWork.SaveChangesAsync();
        }

        public Task<List<ProductModel>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<string> ConvertingImageUrlAsync(string rootUrl, int width, int height)
        {
            var client = _clientFactory.CreateClient();
            var requestUrl = $"https://img.vnsoshop.com/api/image-processing?type=center&w={width}&h={height}&url={rootUrl}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var responseData = await JsonSerializer.DeserializeAsync<GetImageUrlResponse>(responseStream, serializeOptions);
                if (responseData.Status == "Success")
                {
                    return responseData.Content;
                }
            }

            return string.Empty;
        }
        #endregion

        public int GetTotalByProductCategory(int? catId, bool status)
        {
            if (catId.ToInt() == 0)
            {
                return _productRepository.FindAll(x => x.Status == status && string.IsNullOrEmpty(x.Thumb) == false).Count();
            }
            else
            {
                return _productRepository.FindAll(x => x.ProductCategoryId == catId && x.Status == status && string.IsNullOrEmpty(x.Thumb) == false).Count();
            }
        }

        public int GetTotalByBanner(int? bannerId, bool status)
        {
            return _productRepository.FindAll(x => x.BannerId == bannerId && x.Status == status && string.IsNullOrEmpty(x.Thumb) == false).Count();
        }
    }
}
