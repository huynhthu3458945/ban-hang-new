﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;

namespace SoftNetCore.Services.Implementation
{
    public class AccountCouponService : IAccountCouponService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<AccountCoupon> _accountCouponRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public AccountCouponService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<AccountCoupon> accountCouponRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _accountCouponRepository = accountCouponRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public Task<AccountCouponModel> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<AccountCouponModel> FindByReferenceId(int accountId, int couponId)
        {
            return await _accountCouponRepository.FindAll(x => x.AccountId == accountId && x.CouponId == couponId).ProjectTo<AccountCouponModel>(_configMapper).FirstOrDefaultAsync();
        }

        public async Task<List<AccountCouponModel>> GetAllAsync()
        {
            return await _accountCouponRepository.FindAll().ProjectTo<AccountCouponModel>(_configMapper).ToListAsync();
        }

        public async Task<List<AccountCouponModel>> GetByAccountAsync(int accountId)
        {
            return await _accountCouponRepository.FindAll(x => x.AccountId == accountId).ProjectTo<AccountCouponModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(AccountCouponModel model)
        {
            try
            {
                var item = _mapper.Map<AccountCoupon>(model);
                _accountCouponRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(AccountCouponModel model)
        {
            try
            {
                var item = _mapper.Map<AccountCoupon>(model);
                _accountCouponRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _accountCouponRepository.FindById(id);
                _accountCouponRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByAccountAsync(int accountId)
        {
            try
            {
                var list = _accountCouponRepository.FindAll(x => x.AccountId == accountId);
                foreach (var item in list)
                {
                    _accountCouponRepository.Remove(item);
                }
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = list };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { accountId = accountId });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByReferenceIdAsync(int accountId, int couponId)
        {
            try
            {
                var item = await _accountCouponRepository.FindAll(x => x.AccountId == accountId && x.CouponId == couponId).FirstOrDefaultAsync();
                _accountCouponRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { accountId = accountId, couponId = couponId });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _accountCouponRepository.FindAll().ProjectTo<AccountCoupon>(_configMapper);
            return await query.ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
