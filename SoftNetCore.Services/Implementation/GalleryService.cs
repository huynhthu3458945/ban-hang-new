﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class GalleryService : IGalleryService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Gallery> _galleryRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public GalleryService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Gallery> galleryRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _galleryRepository = galleryRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<GalleryModel> FindById(int id)
        {
            return await _galleryRepository.FindAll().ProjectTo<GalleryModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<GalleryModel>> GetAllAsync()
        {
            return await _galleryRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<GalleryModel>(_configMapper).ToListAsync();
        }

        public async Task<List<GalleryModel>> GetByStatusAsync(bool status)
        {
            return await _galleryRepository.FindAll(x => x.Status == status).ProjectTo<GalleryModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(GalleryModel model)
        {
            try
            {
                var item = _mapper.Map<Gallery>(model);
                item.CreateTime = DateTime.Now;
                _galleryRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(GalleryModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Gallery>(model);
                _galleryRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _galleryRepository.FindById(id);
                _galleryRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _galleryRepository.FindAll().ProjectTo<Gallery>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<List<GalleryModel>> GetNewAsync(int numberItem)
        {
            var query = _galleryRepository.FindAll(x => x.Status == true).ProjectTo<GalleryModel>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<GalleryModel>> GetRandomAsync(int numberItem)
        {
            Random rand = new Random();
            var query = _galleryRepository.FindAll(x => x.Status == true).ProjectTo<GalleryModel>(_configMapper);

            int random = rand.Next(0, query.Count());
            return await query.OrderBy(x => Guid.NewGuid()).Take(numberItem).ToListAsync();
        }

        //public async Task<List<GalleryModel>> GetRelatedAsync(int id, int catId, int numberItem)
        //{
        //    var query = _galleryRepository.FindAll(x => x.GalleryCategoryId == catId && x.Id != id && x.Status == 1).ProjectTo<GalleryModel>(_configMapper);
        //    return await query.OrderByDescending(x => x.Position).Take(numberItem).ToListAsync();
        //}

        public async Task<List<GalleryModel>> GetTopAsync(int numberItem)
        {
            var query = _galleryRepository.FindAll(x => x.Status == true).ProjectTo<GalleryModel>(_configMapper);
            return await query.OrderByDescending(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            var item = _galleryRepository.FindById(id);
            try
            {
                item.Status = item.Status == true ? false : true;
                _galleryRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
    }
}
