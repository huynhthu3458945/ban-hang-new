﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class SaleService : ISaleService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Sale> _saleRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public SaleService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Sale> saleRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _saleRepository = saleRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<SaleModel> FindById(int id)
        {
            return await _saleRepository.FindAll(x => x.Id == id).AsNoTracking().ProjectTo<SaleModel>(_configMapper).FirstOrDefaultAsync();
        }

        public async Task<List<SaleModel>> GetAllAsync()
        {
            return await _saleRepository.FindAll().AsNoTracking().ProjectTo<SaleModel>(_configMapper).OrderByDescending(x => x.CreateTime).ToListAsync();
        }
        public async Task<List<Sale>> GetDataAsync()
        {
            return await _saleRepository.FindAll().AsNoTracking().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<SaleModel>> GetByStatusAsync(bool status)
        {
            return await _saleRepository.FindAll(x => x.Status == status).AsNoTracking().ProjectTo<SaleModel>(_configMapper).OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<SaleModel>> GetActiveAsync()
        {
            return await _saleRepository
                .FindAll(x => x.Status == true && x.TimeStart <= DateTime.Now && x.TimeEnd >= DateTime.Now)
                .AsNoTracking()
                .ProjectTo<SaleModel>(_configMapper)
                .ToListAsync();
        }

        public SaleModel GetById(int id)
        {
            return _saleRepository.FindAll(x => x.Id == id).AsNoTracking().ProjectTo<SaleModel>(_configMapper).FirstOrDefault();
        }

        public async Task<ProcessingResult> AddAsync(SaleModel model)
        {
            try
            {
                var item = _mapper.Map<Sale>(model);
                item.CreateTime = DateTime.Now;
                _saleRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(SaleModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Sale>(model);
                _saleRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _saleRepository.FindById(id);
                _saleRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _saleRepository.FindAll().AsNoTracking().ProjectTo<Sale>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            var item = _saleRepository.FindById(id);
            try
            {
                item.Status = item.Status == true ? false : true;
                _saleRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
    }
}
