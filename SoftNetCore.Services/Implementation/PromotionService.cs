﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class PromotionService : IPromotionService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Promotion> _promotionRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public PromotionService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Promotion> promotionRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _promotionRepository = promotionRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<PromotionModel> FindById(int id)
        {
            var result = await _promotionRepository.FindAll().ProjectTo<PromotionModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
            if (result != null)
            {
                result.ViewTime = result.ViewTime.ToInt() + 1;
                var item = _mapper.Map<Promotion>(result);
                _promotionRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
            }
            return result;
        }

        public async Task<List<PromotionModel>> GetAllAsync()
        {
            return await _promotionRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<PromotionModel>(_configMapper).ToListAsync();
        }

        public async Task<List<Promotion>> GetDataAsync()
        {
            return await _promotionRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<PromotionModel>> GetByCategoryAsync(int catId)
        {
            return await _promotionRepository.FindAll(x => x.PromotionCategoryId == catId && x.Status == true).OrderByDescending(x => x.CreateTime).ProjectTo<PromotionModel>(_configMapper).ToListAsync();
        }

        public async Task<List<PromotionModel>> GetByStatusAsync(bool status)
        {
            return await _promotionRepository.FindAll(x => x.Status == status).OrderByDescending(x => x.CreateTime).ProjectTo<PromotionModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(PromotionModel model)
        {
            try
            {
                var item = _mapper.Map<Promotion>(model);
                item.Alias = item.Title.ToUrlFormat(true);
                item.CreateTime = DateTime.Now;
                _promotionRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(PromotionModel model)
        {
            //if (await FindById(model.Id) == null)
            //{
            //    return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            //}
            try
            {
                var item = _mapper.Map<Promotion>(model);
                item.Alias = item.Title.ToUrlFormat(true);
                _promotionRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _promotionRepository.FindById(id);
                _promotionRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _promotionRepository.FindAll().ProjectTo<Promotion>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<List<PromotionModel>> GetHotAsync(int numberItem)
        {
            var query = _promotionRepository.FindAll(x => x.Status == true).ProjectTo<PromotionModel>(_configMapper);
            return await query.OrderByDescending(x => x.ViewTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<PromotionModel>> GetHotAsync(int numberItem, int catId)
        {
            var query = _promotionRepository.FindAll(x => x.Status == true && x.PromotionCategoryId == catId).ProjectTo<PromotionModel>(_configMapper);
            return await query.OrderByDescending(x => x.ViewTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<PromotionModel>> GetNewAsync(int numberItem)
        {
            var query = _promotionRepository.FindAll(x => x.Status == true).ProjectTo<PromotionModel>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<PromotionModel>> GetRandomAsync(int numberItem)
        {
            Random rand = new Random();
            var query = _promotionRepository.FindAll(x => x.Status == true).ProjectTo<PromotionModel>(_configMapper);

            int random = rand.Next(0, query.Count());
            return await query.OrderBy(x => Guid.NewGuid()).Take(numberItem).ToListAsync();
        }

        public async Task<List<PromotionModel>> GetRelatedAsync(int id, int catId, int numberItem)
        {
            var query = _promotionRepository.FindAll(x => x.PromotionCategoryId == catId && x.Id != id && x.Status == true).ProjectTo<PromotionModel>(_configMapper);
            return await query.OrderBy(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<List<PromotionModel>> GetTopAsync(int numberItem)
        {
            var query = _promotionRepository.FindAll(x => x.Status == true).ProjectTo<PromotionModel>(_configMapper);
            return await query.OrderBy(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            var item = _promotionRepository.FindById(id);
            try
            {
                item.Status = item.Status.ToBool() == true ? false : true;
                _promotionRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateViewAsync(int id)
        {
            var item = _promotionRepository.FindById(id);
            try
            {
                item.ViewTime += item.ViewTime.ToInt();
                _promotionRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
    }
}
