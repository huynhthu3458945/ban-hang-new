﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;
using AffilateServices.Models.Accesstrade;
using System.Net.Http;
using System.Text.Json;
using AffilateServices.Helpers;
using SoftNetCore.Common.Extensions;
using SoftNetCore.Model.vnz;

namespace SoftNetCore.Services.Implementation
{
    public class CouponService : ICouponService
    {
        private readonly IHttpClientFactory _clientFactory;

        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Coupon> _couponRepository;
        private readonly IBaseRepository<CouponType> _couponTypeRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public CouponService(
            IHttpClientFactory httpClientFactory,
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Coupon> couponRepository,
            IBaseRepository<CouponType> couponTypeRepository,
            IHostingEnvironment currentEnvironment,
            IUnitOfWork unitOfWork)
        {
            _clientFactory = httpClientFactory;

            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _couponRepository = couponRepository;
            _couponTypeRepository = couponTypeRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<CouponModel> FindById(int id)
        {
            return await _couponRepository.FindAll().ProjectTo<CouponModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<CouponModel>> GetAllAsync()
        {
            return await _couponRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<CouponModel>(_configMapper).ToListAsync();
        }

        public async Task<List<Coupon>> GetDataAsync()
        {
            return await _couponRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<CouponModel>> GetByUseAsync(bool status)
        {
            return await _couponRepository.FindAll(x => x.IsUse == status).ProjectTo<CouponModel>(_configMapper).ToListAsync();
        }
        public async Task<List<CouponModel>> GetByStatusAsync(int? typeId, int status)
        {
            if (typeId == 0)
                return await _couponRepository.FindAll(x => x.Status == status).ProjectTo<CouponModel>(_configMapper).ToListAsync();
            else
                return await _couponRepository.FindAll(x => x.CouponTypeId == typeId && x.Status == status).ProjectTo<CouponModel>(_configMapper).ToListAsync();
        }
        public int GetTotalByCouponType(int? typeId, int status)
        {
            if (typeId == 0)
                return _couponRepository.FindAll(x => x.Status == status && string.IsNullOrEmpty(x.Image) == false).Count();
            else
                return _couponRepository.FindAll(x => x.CouponTypeId == typeId && x.Status == status && string.IsNullOrEmpty(x.Image) == false).Count();
        }

        public async Task<ProcessingResult> AddAsync(CouponModel model)
        {
            try
            {
                var item = _mapper.Map<Coupon>(model);
                item.CreateTime = DateTime.Now;
                _couponRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Gửi liên hệ thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Gửi liên hệ thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(CouponModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Coupon>(model);
                _couponRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _couponRepository.FindById(id);
                _couponRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _couponRepository.FindAll().ProjectTo<Coupon>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<ProcessingResult> UpdateUseAsync(int id)
        {
            var item = _couponRepository.FindById(id);
            try
            {
                item.IsUse = true;
                _couponRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }


        /// <summary>
        /// Lấy danh sách giảm giá tu accesstrade
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="bearerToken"></param>
        /// <returns></returns>
        public async Task<ProcessingResult> FetchAccesstradeCouponAsync(string bearerToken, int page, int limit)
        {
            processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };

            try
            {
                var isFinished = false;
                var requestUrl = $"https://pub2-api.accesstrade.vn/v1/creative/coupon?page={page}&limit={limit}";
                var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);
                var client = _clientFactory.CreateClient();
                var response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var serializeOptions = new JsonSerializerOptions();
                    serializeOptions.Converters.Add(new StringConverter());
                    var responseData = await JsonSerializer.DeserializeAsync<GetCouponResponseModel>(responseStream, serializeOptions);
                    isFinished = (page * limit) >= responseData.data.count_current_day_coupon;

                    await AddCouponAsync(responseData);

                    processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = isFinished ? "Hoàn tất" : "Thành công", Success = !isFinished };
                }

            }
            catch (Exception ex)
            {
            }

            return processingResult;
        }

        /// <summary>
        /// Thêm mã giảm giá
        /// Thuận - thêm convert ảnh và cập nhật thông tin
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private async Task AddCouponAsync(GetCouponResponseModel response)
        {
            var allCoupon = await _couponRepository.GetAll().AsNoTracking().ToListAsync();
            var newCoupons = new List<Coupon>();
            var updateCoupons = new List<Coupon>();
            foreach (var res in response.data.data)
            {
                var couponsObject = res.coupons.ToJson();
                var image = await ConvertingImageUrlAsync(res.image, 245, 245);
                var existed = allCoupon.FirstOrDefault(p => p.CouponId == res.id);
                if (existed != null)
                {
                    existed.CampaignId = res.campaign_id;
                    existed.CampaignName = res.campaign_name;
                    existed.Content = res.content;
                    existed.Coupons = couponsObject;
                    existed.StartDate = res.start_date;
                    existed.EndDate = res.end_date;
                    existed.Image = image ?? res.image; // Nếu convert được sẽ lấy ảnh mới, ngược lại lấy ảnh gốc
                    existed.IsHot = !res.is_hot.Equals("False");
                    existed.Link = res.link;
                    existed.AffilateLink = res.prod_link;
                    existed.Merchant = res.merchant;
                    existed.Title = res.name;
                    existed.PercentageUsed = res.percentage_used;
                    existed.TimeLeft = res.time_left;
                    existed.Register = res.register;
                    existed.Status = res.status;

                    updateCoupons.Add(existed);
                }
                else
                {
                    var newCoupon = new Coupon()
                    {
                        CouponId = res.id,
                        CampaignId = res.campaign_id,
                        CampaignName = res.campaign_name,
                        Content = res.content,
                        Coupons = couponsObject,
                        StartDate = res.start_date,
                        EndDate = res.end_date,
                        Image = image ?? res.image, // Nếu convert được sẽ lấy ảnh mới, ngược lại lấy ảnh gốc
                        IsHot = !res.is_hot.Equals("False"),
                        Link = res.link,
                        AffilateLink = res.prod_link,
                        Merchant = res.merchant,
                        Title = res.name,
                        PercentageUsed = res.percentage_used,
                        TimeLeft = res.time_left,
                        Register = res.register,
                        Status = res.status
                    };
                    newCoupons.Add(newCoupon);
                }
            }

            if (updateCoupons.Count > 0)
            {
                _couponRepository.UpdateRange(updateCoupons);
            }

            if (newCoupons.Count > 0)
            {
                _couponRepository.AddRange(newCoupons);
            }

            await _unitOfWork.SaveChangesAsync();
        }

        //private async Task AddCouponAsync(GetCouponResponseModel response)
        //{
        //    var allCoupon = await _couponRepository.GetAll().AsNoTracking().ToListAsync();
        //    var coupons = new List<Coupon>();
        //    foreach (var res in response.data.data)
        //    {
        //        var existed = allCoupon.FirstOrDefault(p => p.CouponId == res.id);
        //        if (existed != null) continue;

        //        var couponsObject = res.coupons.ToJson();
        //        var image = await ConvertingImageUrlAsync(res.image);
        //        var newCoupon = new Coupon()
        //        {
        //            CouponId = res.id,
        //            CampaignId = res.campaign_id,
        //            CampaignName = res.campaign_name,
        //            Content = res.content,
        //            Coupons = couponsObject,
        //            StartDate = res.start_date,
        //            EndDate = res.end_date,
        //            Image = image,
        //            IsHot = !res.is_hot.Equals("False"),
        //            Link = res.link,
        //            AffilateLink = res.prod_link,
        //            Merchant = res.merchant,
        //            Title = res.name,
        //            PercentageUsed = res.percentage_used,
        //            TimeLeft = res.time_left,
        //            Register = res.register,
        //            Status = res.status
        //        };

        //        coupons.Add(newCoupon);
        //    }
        //    _couponRepository.AddRange(coupons);
        //    await _unitOfWork.SaveChangesAsync();
        //}

        public async Task UpdateCouponTypesAsync()
        {
            var coupons = _couponRepository.GetAll().AsNoTracking();
            var merchants = await coupons.GroupBy(c => c.Merchant).Select(c => c.Key).ToListAsync();

            var allCouponTypes = await _couponTypeRepository.GetAll().ToListAsync();
            var newCouponTypes = new List<CouponType>();
            foreach (var merchant in merchants)
            {
                var existedCouponType = allCouponTypes.FirstOrDefault(c => c.MerchantName.Equals(merchant));
                if (existedCouponType != null) continue;

                var newCouponType = new CouponType()
                {
                    Title = merchant.ToUpperCase(),
                    MerchantName = merchant,
                };
                newCouponTypes.Add(newCouponType);
            }
            _couponTypeRepository.AddRange(newCouponTypes);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> UpdateCouponTypeForCouponsAsync()
        {
            var coupons = await _couponRepository.GetAll().ToListAsync();
            var allCouponTypes = await _couponTypeRepository.GetAll().AsNoTracking().ToListAsync();

            var updateCounpons = new List<Coupon>();
            foreach (var couponType in allCouponTypes)
            {
                var couponsByMerchant = coupons.Where(c => c.Merchant.Equals(couponType.MerchantName)).ToList();
                if (couponsByMerchant.Count() == 0) continue;
                couponsByMerchant.ForEach(c =>
                {
                    c.CouponTypeId = couponType.Id;
                });
                updateCounpons = updateCounpons.Concat(couponsByMerchant).ToList();
            }
            _couponRepository.UpdateRange(updateCounpons);
            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<string> ConvertingImageUrlAsync(string rootUrl, int width, int height)
        {
            var client = _clientFactory.CreateClient();
            var requestUrl = $"https://img.vnsoshop.com/api/image-processing?type=center&w={width}&h={height}&url={rootUrl}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var responseData = await JsonSerializer.DeserializeAsync<GetImageUrlResponse>(responseStream, serializeOptions);
                if (responseData.Status == "Success")
                {
                    return responseData.Content;
                }
            }

            return string.Empty;
        }
    }
}
