﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using AffilateServices.Helpers;
using AffilateServices.Models.Common;
using AffilateServices.Models.Dimuadi;
using SoftNetCore.Common.Extensions;
using SoftNetCore.Model.vnz;

namespace SoftNetCore.Services.Implementation
{
    public class BannerService : IBannerService
    {
        private readonly IHttpClientFactory _clientFactory;

        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Banner> _bannerRepository;
        private readonly IBaseRepository<Product> _productRepository;
        private readonly IBaseRepository<ConfigSystem> _configRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public BannerService(
            IHttpClientFactory httpClientFactory,
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Banner> bannerRepository,
            IBaseRepository<Product> productRepository,
            IBaseRepository<ConfigSystem> configRepository,
            IHostingEnvironment currentEnvironment,
            IUnitOfWork unitOfWork)
        {
            _clientFactory = httpClientFactory;
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _bannerRepository = bannerRepository;
            _productRepository = productRepository;
            _configRepository = configRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<BannerModel> FindById(int id)
        {
            return await _bannerRepository.FindAll().ProjectTo<BannerModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<BannerModel>> GetAllAsync()
        {
            return await _bannerRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<BannerModel>(_configMapper).ToListAsync();
        }

        public async Task<List<Banner>> GetDataAsync()
        {
            return await _bannerRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<BannerModel>> GetNewAsync(int numberItem)
        {
            var query = _bannerRepository.FindAll(x => x.Status == 1).ProjectTo<BannerModel>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).Take(numberItem).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(BannerModel model)
        {
            try
            {
                var item = _mapper.Map<Banner>(model);
                item.CreateTime = DateTime.Now;
                _bannerRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(BannerModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Banner>(model);
                _bannerRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }
        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            try
            {
                var item = _bannerRepository.FindById(id);
                item.Status = item.Status.ToInt() == 1 ? 0 : 1;
                _bannerRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _bannerRepository.FindById(id);
                _bannerRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _bannerRepository.FindAll().ProjectTo<Banner>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        /// <summary>
        /// Lấy hotdeal từ dimuadi
        /// THUAN - chỉnh sửa, bổ sung
        /// </summary>
        /// <param name="token"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<bool> FetchBannerFromDimuadi(string token)
        {
           var config = await _configRepository.FindAll().ProjectTo<ConfigSystemModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == 1);
            int hotDealPage = config.HotDealPage.ToInt();
            int hotDealSize = config.HotDealLimit.ToInt();
            int productPage = config.HotDealProductPage.ToInt();
            int productSize = config.HotDealProductLimit.ToInt();

            var requestUrl = $"https://apis.dimuadi.vn/d2c-service/deal-hot?position_home={hotDealPage}&page_size={hotDealSize}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var responseData = await JsonSerializer.DeserializeAsync<DimuadiResponse<GetBannerResponse>>(responseStream, serializeOptions);

                /// Save banner
                var allBanners = await _bannerRepository.GetAll().AsNoTracking().ToListAsync();
                var newBanners = new List<Banner>();
                foreach (var res in responseData.data)
                {
                    var existed = allBanners.FirstOrDefault(x => x.BannerId == res.id);
                    if (existed != null) continue;

                    var avatarDetail = await ConvertingImageUrlAsync(res.avatarDetail.First(), 1200, 400);
                    var avatarDetailMobile = await ConvertingImageUrlAsync(res.avatarDetailMobile.First(), 250, 250);
                    var avatarHome = await ConvertingImageUrlAsync(res.avatarHome.First(), 250, 250);

                    var banner = new Banner()
                    {
                        BannerId = res.id,
                        AvatarDetail = avatarDetail ?? res.avatarDetail.First(), // Nếu convert được sẽ lấy ảnh mới, ngược lại lấy ảnh gốc
                        AvatarDetailMobile = avatarDetailMobile ?? res.avatarDetailMobile.First(), // Nếu convert được sẽ lấy ảnh mới, ngược lại lấy ảnh gốc
                        AvatarHome = avatarHome ?? res.avatarHome.First(), // Nếu convert được sẽ lấy ảnh mới, ngược lại lấy ảnh gốc
                        Description = res.description,
                        Position = res.position,
                        Status = res.status,
                        TimeStart = res.timeStart,
                        TimeStop = res.timeStop,
                        Title = res.title
                    };
                    newBanners.Add(banner);

                    await ProcessFromDimuadiByBannerIdAsync(res.id.ToInt(), productPage, productSize, true);
                }
                _bannerRepository.AddRange(newBanners);
                await _unitOfWork.SaveChangesAsync();
            }
            return true;
        }

        //public async Task<bool> FetchBannerFromDimuadi(string token, int page, int limit)
        //{
        //    var requestUrl = $"https://apis.dimuadi.vn/d2c-service/deal-hot?position_home={page}&page_size={limit}";
        //    var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
        //    request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
        //    var client = _clientFactory.CreateClient();
        //    var response = await client.SendAsync(request);
        //    if (response.IsSuccessStatusCode)
        //    {
        //        using var responseStream = await response.Content.ReadAsStreamAsync();
        //        var serializeOptions = new JsonSerializerOptions();
        //        serializeOptions.Converters.Add(new StringConverter());
        //        var responseData = await JsonSerializer.DeserializeAsync<DimuadiResponse<GetBannerResponse>>(responseStream, serializeOptions);

        //        /// Save banner
        //        var allBanners = await _bannerRepository.GetAll().AsNoTracking().ToListAsync();
        //        var newBanners = new List<Banner>();
        //        responseData.data.ForEach(async res =>
        //       {
        //           var existed = allBanners.FirstOrDefault(x => x.BannerId == res.id);
        //           if (existed != null) return;
        //           var banner = new Banner()
        //           {
        //               BannerId = res.id,
        //               AvatarDetail = res.avatarDetail.First(),
        //               AvatarDetailMobile = res.avatarDetailMobile.First(),
        //               AvatarHome = res.avatarHome.First(),
        //               Description = res.description,
        //               Position = res.position,
        //               Status = res.status,
        //               TimeStart = res.timeStart,
        //               TimeStop = res.timeStop,
        //               Title = res.title
        //           };
        //           newBanners.Add(banner);
        //       });
        //        _bannerRepository.AddRange(newBanners);
        //        await _unitOfWork.SaveChangesAsync();
        //    }
        //    return true;
        //}

        private async Task ProcessFromDimuadiByBannerIdAsync(int bannerId, int page, int size, bool isCallback)
        {
            /// Url kéo sản phẩm theo danh muc
            var requestUrl = $"https://apis.dimuadi.vn/d2c-service/product/deal/{bannerId}?page_size={size}&page={page}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);

            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var responseData = await JsonSerializer.DeserializeAsync<GetProductResponse>(responseStream, serializeOptions);

                var allProduct = await _productRepository.GetAll().AsNoTracking().ToListAsync();
                var updateProducts = new List<Product>();
                foreach (var res in responseData.data)
                {
                    var existed = allProduct.FirstOrDefault(p => p.ProductId == res.id);
                    if (existed != null)
                    {
                        existed.BannerId = bannerId;
                        //
                        updateProducts.Add(existed);
                    }
                }

                if (updateProducts.Count > 0)
                {
                    _productRepository.UpdateRange(updateProducts);
                }

                await _unitOfWork.SaveChangesAsync();

                isCallback = page < responseData.paging.totalPage;
                if (isCallback)
                {
                    await ProcessFromDimuadiByBannerIdAsync(bannerId, page + 1, size, isCallback);
                }
            }
        }

        public async Task<string> ConvertingImageUrlAsync(string rootUrl, int width, int height)
        {
            var client = _clientFactory.CreateClient();
            var requestUrl = $"https://img.vnsoshop.com/api/image-processing?type=center&w={width}&h={height}&url={rootUrl}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var responseData = await JsonSerializer.DeserializeAsync<GetImageUrlResponse>(responseStream, serializeOptions);
                if (responseData.Status == "Success")
                {
                    return responseData.Content;
                }
            }

            return string.Empty;
        }
    }
}
