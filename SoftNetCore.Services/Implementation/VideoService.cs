﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class VideoService : IVideoService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Video> _articleRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public VideoService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Video> articleRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _articleRepository = articleRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<VideoModel> FindById(int id)
        {
            return await _articleRepository.FindAll().ProjectTo<VideoModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<VideoModel>> GetAllAsync()
        {
            return await _articleRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<VideoModel>(_configMapper).ToListAsync();
        }

        public async Task<List<Video>> GetDataAsync()
        {
            return await _articleRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<VideoModel>> GetByCategoryAsync(int catId)
        {
            return await _articleRepository.FindAll(x => x.VideoCategoryId == catId && x.Status == true).OrderByDescending(x => x.CreateTime).ProjectTo<VideoModel>(_configMapper).ToListAsync();
        }

        public async Task<List<VideoModel>> GetByStatusAsync(bool status)
        {
            return await _articleRepository.FindAll(x => x.Status == status).OrderByDescending(x => x.CreateTime).ProjectTo<VideoModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(VideoModel model)
        {
            try
            {
                var item = _mapper.Map<Video>(model);
                item.CreateTime = DateTime.Now;
                _articleRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(VideoModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Video>(model);
                _articleRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _articleRepository.FindById(id);
                _articleRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _articleRepository.FindAll().ProjectTo<Video>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<List<VideoModel>> GetHotAsync(int numberItem)
        {
            var query = _articleRepository.FindAll(x => x.Status == true).ProjectTo<VideoModel>(_configMapper);
            return await query.OrderByDescending(x => x.ViewTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<VideoModel>> GetNewAsync(int numberItem)
        {
            var query = _articleRepository.FindAll(x => x.Status == true).ProjectTo<VideoModel>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<VideoModel>> GetRandomAsync(int numberItem)
        {
            Random rand = new Random();
            var query = _articleRepository.FindAll(x => x.Status == true).ProjectTo<VideoModel>(_configMapper);

            int random = rand.Next(0, query.Count());
            return await query.OrderBy(x => Guid.NewGuid()).Take(numberItem).ToListAsync();
        }

        public async Task<List<VideoModel>> GetRelatedAsync(int id, int catId, int numberItem)
        {
            var query = _articleRepository.FindAll(x => x.VideoCategoryId == catId && x.Id != id && x.Status == true).ProjectTo<VideoModel>(_configMapper);
            return await query.OrderBy(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<List<VideoModel>> GetTopAsync(int numberItem)
        {
            var query = _articleRepository.FindAll(x => x.Status == true).ProjectTo<VideoModel>(_configMapper);
            return await query.OrderBy(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            var item = _articleRepository.FindById(id);
            try
            {
                item.Status = item.Status.ToBool() == true ? false : true;
                _articleRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateViewAsync(int id)
        {
            var item = _articleRepository.FindById(id);
            try
            {
                item.ViewTime += item.ViewTime.ToInt();
                _articleRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
    }
}
