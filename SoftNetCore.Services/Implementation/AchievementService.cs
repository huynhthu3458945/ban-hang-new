﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class AchievementService : IAchievementService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Achievement> _achievementRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public AchievementService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Achievement> achievementRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _achievementRepository = achievementRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<AchievementModel> FindById(int id)
        {
            return await _achievementRepository.FindAll().ProjectTo<AchievementModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<AchievementModel>> GetAllAsync()
        {
            return await _achievementRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<AchievementModel>(_configMapper).ToListAsync();
        }

        public async Task<AchievementModel> GetFisrtAsync()
        {
            return await _achievementRepository.FindAll().ProjectTo<AchievementModel>(_configMapper).FirstOrDefaultAsync();
        }

        public async Task<ProcessingResult> AddAsync(AchievementModel model)
        {
            try
            {
                var item = _mapper.Map<Achievement>(model);
                item.CreateTime = DateTime.Now;
                _achievementRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(AchievementModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Achievement>(model);
                _achievementRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _achievementRepository.FindById(id);
                _achievementRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _achievementRepository.FindAll().ProjectTo<Achievement>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
