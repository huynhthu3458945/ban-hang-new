﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class BranchService : IBranchService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Branch> _branchRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public BranchService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Branch> branchRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _branchRepository = branchRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<BranchModel> FindById(int id)
        {
            return await _branchRepository.FindAll().ProjectTo<BranchModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<BranchModel>> GetAllAsync()
        {
            return await _branchRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<BranchModel>(_configMapper).ToListAsync();
        }

        public async Task<List<BranchModel>> GetByStatusAsync(bool status)
        {
            return await _branchRepository.FindAll(x => x.Status == status).OrderByDescending(x => x.CreateTime).ProjectTo<BranchModel>(_configMapper).ToListAsync();
        }

        public async Task<List<BranchModel>> GetByAddressAsync(int? provinceId, int? districtId)
        {
            return await _branchRepository.FindAll(x => x.ProvinceId == provinceId && x.DistrictId == districtId).OrderByDescending(x => x.CreateTime).ProjectTo<BranchModel>(_configMapper).ToListAsync();
        }

        public async Task<List<BranchModel>> GetRandomAsync(int numberItem)
        {
            Random rand = new Random();
            var query = _branchRepository.FindAll(x => x.Status == true).ProjectTo<BranchModel>(_configMapper);
            int random = rand.Next(0, query.Count());
            return await query.OrderBy(x => Guid.NewGuid()).Take(numberItem).ToListAsync();
        }

        public async Task<List<Branch>> GetDataAsync()
        {
            return await _branchRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(BranchModel model)
        {
            try
            {
                var item = _mapper.Map<Branch>(model);
                item.CreateTime = DateTime.Now;
                _branchRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(BranchModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Branch>(model);
                _branchRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _branchRepository.FindById(id);
                _branchRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _branchRepository.FindAll().ProjectTo<Branch>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<Pager> SearchPaginationAsync(BranchPagination paramater)
        {
            var query = _branchRepository.FindAll(x => (x.PartnerId == paramater.partnerId || paramater.partnerId == 0)
                                                    && (x.ProvinceId == paramater.provinceId || paramater.provinceId == 0)
                                                    && (x.DistrictId == paramater.districtId || paramater.districtId == 0)).ProjectTo<Branch>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
