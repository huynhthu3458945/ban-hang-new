﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;
using System.Net.Http;

namespace SoftNetCore.Services.Implementation
{
    public class MerchantSettingService : IMerchantSettingService
    {
        private readonly IHttpClientFactory _clientFactory;

        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<MerchantSetting> _merchantSettingRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public MerchantSettingService(
            IHttpClientFactory httpClientFactory,
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<MerchantSetting> merchantSettingRepository,
            IHostingEnvironment currentEnvironment,
            IUnitOfWork unitOfWork)
        {
            _clientFactory = httpClientFactory;
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _merchantSettingRepository = merchantSettingRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<MerchantSettingModel> FindById(int id)
        {
            return await _merchantSettingRepository.FindAll().ProjectTo<MerchantSettingModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<MerchantSettingModel> FindByMerchantId(EnumDomains id)
        {
            return await _merchantSettingRepository.FindAll().ProjectTo<MerchantSettingModel>(_configMapper).FirstOrDefaultAsync(x => x.MerchantId == id);
        }

        public async Task<List<MerchantSettingModel>> GetAllAsync()
        {
            return await _merchantSettingRepository.FindAll().OrderBy(x => x.Id).ProjectTo<MerchantSettingModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(MerchantSettingModel model)
        {
            try
            {
                var item = _mapper.Map<MerchantSetting>(model);
                _merchantSettingRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(MerchantSettingModel model)
        {
            var item = await _merchantSettingRepository.FindByIdAsync(model.Id);
            if (item == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                item.MerchantId = model.MerchantId;
                item.TokenKey = model.TokenKey;
                item.TokenValue = model.TokenValue;
                _merchantSettingRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();

                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = await _merchantSettingRepository
                    .FindAll(p => p.Id == id)
                    .FirstOrDefaultAsync();

                _merchantSettingRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _merchantSettingRepository.FindAll().ProjectTo<ProductCategory>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
