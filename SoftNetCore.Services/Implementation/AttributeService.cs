﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;
using SoftNetCore.Data.Entities;
using SoftNetCore.Model.Request;

namespace SoftNetCore.Services.Implementation
{
    public class AttributeService : IAttributeService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Data.Entities.Attribute> _attributeRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public AttributeService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Data.Entities.Attribute> attributeRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _attributeRepository = attributeRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<AttributeModel> FindById(int id)
        {
            return await _attributeRepository.FindAll(x => x.Id == id).ProjectTo<AttributeModel>(_configMapper).FirstOrDefaultAsync();
        }
        public Data.Entities.Attribute GetById(int id)
        {
            return _attributeRepository.FindById(id);
        }
        public async Task<List<AttributeModel>> GetAllAsync()
        {
            return await _attributeRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<AttributeModel>(_configMapper).ToListAsync();
        }
        public async Task<List<AttributeModel>> GetByTypeAsync(int typeId)
        {
            return await _attributeRepository.FindAll(x => x.TypeId == typeId).ProjectTo<AttributeModel>(_configMapper).ToListAsync();
        }
        public async Task<List<Data.Entities.Attribute>> GetDataAsync()
        {
            return await _attributeRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public List<AttributeTypeRequest> GetTypeAllAsync()
        {
            List<AttributeTypeRequest> data = new List<AttributeTypeRequest>();
            data.Add(new AttributeTypeRequest { Id = 0, Name = "Màu sắc" });
            data.Add(new AttributeTypeRequest { Id = 1, Name = "Kích cỡ" });
            return data;
        }

        public async Task<ProcessingResult> AddAsync(AttributeModel model)
        {
            try
            {
                var item = _mapper.Map<Data.Entities.Attribute>(model);
                item.CreateTime = DateTime.Now;
                _attributeRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(AttributeModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = await _attributeRepository.FindAll().FirstOrDefaultAsync(x => x.Id == model.Id);
                if (item != null)
                {
                    item.Title = model.Title;
                    item.Code = model.Code;
                    item.Position = model.Position;
                    item.Status = model.Status;
                    item.TypeId = model.TypeId;
                    _attributeRepository.Update(item);
                }
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _attributeRepository.FindById(id);
                _attributeRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _attributeRepository.FindAll().ProjectTo<Data.Entities.Attribute>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
