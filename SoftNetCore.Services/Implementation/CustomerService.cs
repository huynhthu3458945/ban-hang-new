﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using Microsoft.Extensions.Options;
using SoftNetCore.Model.Request;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class CustomerService : ICustomerService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly AppSettings _appSettings;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Customer> _customerRepository;
        private readonly IBaseRepository<Account> _accountRepository;
        private readonly ISendEmailService _sendEmailService;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public CustomerService(
            IMapper mapper,
            MapperConfiguration configMapper,
            IOptions<AppSettings> appSettings,
            ILogging logging,
            IBaseRepository<Customer> customerRepository,
            IBaseRepository<Account> accountRepository,
            ISendEmailService sendEmailService,
            IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _appSettings = appSettings.Value;
            _logging = logging;
            _customerRepository = customerRepository;
            _accountRepository = accountRepository;
            _sendEmailService = sendEmailService;
            _unitOfWork = unitOfWork;
        }

        public async Task<ProcessingResult> AddAsync(CustomerModel model)
        {
            try
            {
                var item = _mapper.Map<Customer>(model);
                //
                if (await _customerRepository.FindAll(x => x.Email == model.Email).AnyAsync())
                {
                    processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Email đã được được sử dụng!", Success = false, Data = item };
                    return processingResult;
                }
                if (await _customerRepository.FindAll(x => x.Phone == model.Phone).AnyAsync())
                {
                    processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Số điện thoại đã được sử dụng!", Success = false, Data = item };
                    return processingResult;
                }
                item.Status = true;
                _customerRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(CustomerModel model)
        {
            var item = await _customerRepository.FindAll().FirstOrDefaultAsync(x => x.Id == model.Id);
            if (item == null)
            {
                return processingResult;
            }
            if (await _customerRepository.FindAll(x => x.Email == model.Email && x.Id != model.Id).AnyAsync())
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Email đã được sử dụng!", Success = false, Data = item };
                return processingResult;
            }
            if (await _customerRepository.FindAll(x => x.Phone == model.Phone && x.Id != model.Id).AnyAsync())
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Số điện thoại đã được sử dụng!", Success = false, Data = item };
                return processingResult;
            }
            try
            {
                item.Email = model.Email;
                item.FullName = model.FullName;
                item.Avatar = model.Avatar;
                item.Phone = model.Phone;
                item.Tel = model.Tel;
                item.ProvinceId = model.ProvinceId;
                item.DistrictId = model.DistrictId;
                item.Address = model.Address;
                _customerRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = await _customerRepository.FindAll().FirstOrDefaultAsync(x => x.Id == id);
                //_refreshTokenRepository.RemoveMultiple(item.RefreshTokens.ToList());
                //_accountFunctionRepository.RemoveMultiple(item.CustomerFunctions.ToList());
                _customerRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<CustomerModel> FindById(int id)
        {
            var item = await _customerRepository.FindAll().FirstOrDefaultAsync(x => x.Id == id);
            return _mapper.Map<CustomerModel>(item);
        }

        public async Task<CustomerModel> FindByIdHaveOrder(int id)
        {
            var item = await _customerRepository.FindAll(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            return _mapper.Map<CustomerModel>(item);
        }

        public async Task<CustomerModel> GetByCondition(string value)
        {
            var item = await _customerRepository.FindAll().FirstOrDefaultAsync(x => x.Email == value || x.Phone == value);
            return _mapper.Map<CustomerModel>(item);
        }

        public async Task<List<CustomerModel>> GetAllAsync()
        {
            return await _customerRepository.FindAll().Include(x => x.CustomerType).OrderByDescending(x => x.CreateTime).ProjectTo<CustomerModel>(_configMapper).ToListAsync();
        }

        public async Task<List<Customer>> GetDataAsync()
        {
            return await _customerRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _customerRepository.FindAll().ProjectTo<CustomerModel>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<ProcessingResult> ChangePassword(ChangePasswordRequest model)
        {
            var item = await _accountRepository.FindAll().FirstOrDefaultAsync(x => x.Password == model.OldPassword && x.Id == model.Id);
            if (item == null) return new ProcessingResult { MessageType = MessageTypeEnum.Danger, Message = "Mật khẩu hiện tại không đúng!", Success = false };

            try
            {
                item.Password = model.NewPassword;
                _accountRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Đổi mật khẩu thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Đổi mật khẩu thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<AuthenticateCustomerResponse> Authenticate(AuthenticateRequest model)
        {
            // return null if user not found
            var user = await _accountRepository.FindAll(x => (x.UserName == model.Username || x.Email == model.Username) && x.Password == model.Password)
                                               .FirstOrDefaultAsync();
            if (user == null) return null;
            var customer = await _customerRepository.FindAll().FirstOrDefaultAsync(x => x.AccountId == user.Id);
            // authentication successful so generate jwt and refresh tokens
            var jwtToken = generateJwtToken(customer);
            //var refreshToken = generateRefreshToken(ipAddress);

            // save refresh token
            //user.RefreshTokens.Add(refreshToken);
            _accountRepository.Update(user);
            await _unitOfWork.SaveChangesAsync();

            return new AuthenticateCustomerResponse(customer, jwtToken);
        }

        /// <summary>
        /// Gửi mã xác thực email
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        //public async Task<ProcessingResult> SendTokenVerifyEmail(int accountId, string email, string origin)
        //{
        //    var account = _customerRepository.FindAll().SingleOrDefault(x => x.Id == accountId);
        //    if (account == null) return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xác thực thất bại", Success = false };
        //    try
        //    {
        //        account.VerificationToken = randomTokenString(6);
        //        account.VerificationTokenExpiresTime = DateTime.UtcNow.AddMinutes(5);

        //        _customerRepository.Update(account);
        //        await _unitOfWork.SaveChangesAsync();

        //        // send email
        //        sendVerificationEmail(account, email, origin);
        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Vui lòng kiểm tra email", Success = true, Data = account };
        //    }
        //    catch (Exception ex)
        //    {
        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
        //        _logging.LogException(ex, new { accountId = accountId, email = email });
        //    }
        //    return processingResult;
        //}

        /// /// <summary>
        /// Xác thực email - token: mã dc gửi qua email
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        //public async Task<ProcessingResult> VerifyEmail(string token)
        //{
        //    var account = _customerRepository.FindAll().SingleOrDefault(x => x.VerificationToken == token &&
        //        x.VerificationTokenExpiresTime > DateTime.UtcNow);

        //    if (account == null) return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xác thực thất bại", Success = false };
        //    try
        //    {
        //        account.VerifiedTime = DateTime.UtcNow;
        //        account.VerificationToken = null;

        //        _customerRepository.Update(account);
        //        await _unitOfWork.SaveChangesAsync();
        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xác thực thành công", Success = true, Data = account };
        //    }
        //    catch (Exception ex)
        //    {
        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
        //        _logging.LogException(ex, new
        //        {
        //            token = token
        //        });
        //    }
        //    return processingResult;
        //}

        /// <summary>
        /// Gửi mã khôi phục mật khẩu vào email
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ProcessingResult> ForgotPassword(ForgotPasswordRequest model, string origin)
        {
            try
            {
                var customer = await _customerRepository.FindAll().Include(x => x.Account).FirstOrDefaultAsync(x => x.Email == model.Email);
                if (customer == null) return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thông tin không tồn tại!", Success = false };
                // create reset token that expires after 1 day
                customer.Account.ResetToken = randomTokenString(6);
                customer.Account.ResetTokenExpiresTime = DateTime.UtcNow.AddDays(1);
                //
                _customerRepository.Update(customer);
                await _unitOfWork.SaveChangesAsync();
                // send email
                sendPasswordResetEmail(customer, model.Email, origin);
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Vui lòng kiểm tra email!", Success = true, Data = customer };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Gửi yêu cầu thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        /// <summary>
        /// Kiểm tra xác thực tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //public async Task<ProcessingResult> ValidateResetToken(ValidateResetTokenRequest model)
        //{
        //    try
        //    {
        //        var account = await _customerRepository.FindAll().SingleOrDefaultAsync(x =>
        //            x.ResetToken == model.Token &&
        //            x.ResetTokenExpiresTime > DateTime.UtcNow);

        //        if (account == null)
        //            return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xác thực thất bại", Success = false };

        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xác thực không thành công", Success = true, Data = account };
        //    }
        //    catch (Exception ex)
        //    {
        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
        //        _logging.LogException(ex, model);
        //    }
        //    return processingResult;
        //}

        public async Task<ProcessingResult> ResetPassword(ResetPasswordRequest model)
        {
            var account = await _accountRepository.FindAll().SingleOrDefaultAsync(x =>
                x.ResetToken == model.Token &&
                x.ResetTokenExpiresTime > DateTime.UtcNow);

            if (account == null)
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thông tin không tồn tại", Success = false };
            try
            {
                // update password and remove reset token
                account.Password = model.Password;
                account.PasswordResetTime = DateTime.UtcNow;
                account.ResetToken = null;
                account.ResetTokenExpiresTime = null;

                _accountRepository.Update(account);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Đổi mật khẩu thành công", Success = true, Data = account };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        private string generateJwtToken(Customer user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        //private RefreshToken generateRefreshToken(string ipAddress)
        //{
        //    using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
        //    {
        //        var randomBytes = new byte[64];
        //        rngCryptoServiceProvider.GetBytes(randomBytes);
        //        return new RefreshToken
        //        {
        //            Token = Convert.ToBase64String(randomBytes),
        //            Expires = DateTime.UtcNow.AddDays(7),
        //            CreateTime = DateTime.UtcNow,
        //            CreatedByIp = ipAddress
        //        };
        //    }
        //}

        //private void removeOldRefreshTokens(Customer account)
        //{
        //    var ressult = account.RefreshTokens.Where(x =>
        //          !x.IsActive &&
        //          x.CreateTime.GetValueOrDefault().AddDays(_appSettings.RefreshTokenTTL) <= DateTime.UtcNow);
        //}

        private string randomTokenString(int maxlength = 5)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(Enumerable.Repeat(chars, maxlength).Select(s => s[random.Next(s.Length)]).ToArray());
            return result;
        }

        //private void sendVerificationEmail(Customer account, string email, string origin)
        //{
        //    string message;
        //    if (!string.IsNullOrEmpty(origin))
        //    {
        //        var verifyUrl = $"{origin}/account/verify-email?token={account.VerificationToken}";
        //        message = $@"<p>Vui lòng nhấp vào liên kết dưới đây để xác thực địa chỉ email của bạn:</p>
        //                     <p><a href=""{verifyUrl}"">{verifyUrl}</a></p>";
        //    }
        //    else
        //    {
        //        message = $@"<p>Đây là mã kích hoạt tài khoản của bạn:</p>
        //                     <p><code>{account.VerificationToken}</code></p>";
        //    }

        //    _sendEmailService.Send(
        //        to: email,
        //        subject: "[KFood] Mã xác thực tài khoản",
        //        html: $@"<h4>Chào mừng bạn đến với KFood!</h4>
        //                 {message}
        //                <p>Đây là một tin nhắn tự động, vui lòng không trả lời!</p>"
        //    );
        //}

        //private void sendAlreadyRegisteredEmail(string email, string origin)
        //{
        //    string message;
        //    if (!string.IsNullOrEmpty(origin))
        //        message = $@"<p>Nếu bạn không biết mật khẩu của mình, vui lòng truy cập <a href=""{origin}/account/forgot-password"">Quên mật khẩu</a>.</p>";
        //    else
        //        message = "<p>Nếu bạn quên mật khẩu của mình, bạn có chọn <strong>Quên mật khẩu</strong>.</p>";

        //    _sendEmailService.Send(
        //        to: email,
        //        subject: "[KFood] Đăng ký tài khoản thành công",
        //        html: $@"<h4>Email đã đăng ký</h4>
        //                 <p>Email của bạn <strong> {email} </strong> đã được đăng ký.</p>
        //                 {message}"
        //    );
        //}

        private void sendPasswordResetEmail(Customer customer, string email, string origin)
        {
            string message;
            if (!string.IsNullOrEmpty(origin))
            {
                var resetUrl = $"{origin}/account/reset-password?token={customer.Account.ResetToken}";
                message = $@"<p>Please click the below link to reset your password, the link will be valid for 1 day:</p>
                             <p><a href=""{resetUrl}"">{resetUrl}</a></p>";
            }
            else
            {
                message = $@"<p>Đây là mã xác thực dùng để lấy lại mật khẩu:</p>
                             <p><code>{customer.Account.ResetToken}</code></p>";
            }

            _sendEmailService.Send(
                to: email,
                cc: "",
                subject: "[KFood] Mã xác thực lấy lại mật khẩu",
                html: $@"<h4>Lấy lại mật khẩu</h4>
                         {message}"
            );
        }

        public async Task<ProcessingResult> Lock(int id)
        {
            try
            {
                var customer = _customerRepository.FindById(id);
                customer.Status = customer.Status.ToBool() == true ? false : true;
                _customerRepository.Update(customer);

                if (customer.AccountId != null)
                {
                    var account = _accountRepository.FindById(customer.AccountId);
                    account.Status = customer.Status;
                    _accountRepository.Update(account);
                }
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật trạng thái thành công!", Success = true, Data = customer };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật trạng thái thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
    }
}
