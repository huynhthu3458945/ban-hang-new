﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using Microsoft.Extensions.Options;
using SoftNetCore.Model.Request;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class AccountService : IAccountService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly AppSettings _appSettings;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Account> _accountRepository;
        private readonly ISendEmailService _sendEmailService;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public AccountService(
            IMapper mapper,
            MapperConfiguration configMapper,
            IOptions<AppSettings> appSettings,
            ILogging logging,
            IBaseRepository<Account> accountRepository,
            ISendEmailService sendEmailService,
            IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _appSettings = appSettings.Value;
            _logging = logging;
            _accountRepository = accountRepository;
            _sendEmailService = sendEmailService;
            _unitOfWork = unitOfWork;
        }

        public async Task<ProcessingResult> AddAsync(AccountModel model)
        {
            try
            {
                var item = _mapper.Map<Account>(model);
                item.CreateTime = DateTime.Now;
                //
                if (await _accountRepository.FindAll(x => x.UserName == model.UserName).AnyAsync())
                {
                    processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Tài khoản đã được sử dụng!", Success = false, Data = item };
                    return processingResult;
                }
                if (await _accountRepository.FindAll(x => x.Email == model.Email).AnyAsync())
                {
                    processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Email đã được được sử dụng!", Success = false, Data = item };
                    return processingResult;
                }
                //if (await _accountRepository.FindAll(x => x.PhoneNumber == model.PhoneNumber).AnyAsync())
                //{
                //    processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Số điện thoại đã được sử dụng!", Success = false, Data = item };
                //    return processingResult;
                //}

                _accountRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(AccountModel model)
        {
            var item = _accountRepository.FindById(model.Id);
            if (item == null)
            {
                return processingResult;

            }
            if (await _accountRepository.FindAll(x => x.UserName == model.UserName && x.Id != model.Id).AnyAsync())
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Tài khoản đang được sử dụng!", Success = false, Data = item };
                return processingResult;
            }
            if (await _accountRepository.FindAll(x => x.Email == model.Email && x.Id != model.Id).AnyAsync())
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Email đã được sử dụng!", Success = false, Data = item };
                return processingResult;
            }
            //if (await _accountRepository.FindAll(x => x.PhoneNumber == model.PhoneNumber && x.Id != model.Id).AnyAsync())
            //{
            //    processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Số điện thoại đã được sử dụng!", Success = false, Data = item };
            //    return processingResult;
            //}
            try
            {
                item.UserName = model.UserName;
                item.Password = model.Password;
                //item.FullName = model.FullName;
                //item.Avatar = model.Avatar;
                //item.Email = model.Email;
                //item.PhoneNumber = model.PhoneNumber;
                //item.Address = model.Address;
                _accountRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = await _accountRepository.FindAll().FirstOrDefaultAsync(x => x.Id == id);
                //_refreshTokenRepository.RemoveMultiple(item.RefreshTokens.ToList());
                //_accountFunctionRepository.RemoveMultiple(item.AccountFunctions.ToList());
                _accountRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<AccountModel> FindById(int id)
        {
            var item = await _accountRepository.FindAll().FirstOrDefaultAsync(x => x.Id == id);
            return _mapper.Map<AccountModel>(item);
        }

        public async Task<AccountModel> GetByCondition(string value)
        {
            var item = await _accountRepository.FindAll().FirstOrDefaultAsync(x => x.UserName == value);
            return _mapper.Map<AccountModel>(item);
        }

        public async Task<List<AccountModel>> GetAllAsync()
        {
            return await _accountRepository.FindAll().Include(x => x.AccountType).OrderByDescending(x => x.CreateTime).ProjectTo<AccountModel>(_configMapper).ToListAsync();
        }

        public async Task<List<Account>> GetDataAsync(int typeId)
        {
            return await _accountRepository.FindAll(x => x.AccountTypeId == typeId).OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _accountRepository.FindAll().ProjectTo<AccountModel>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<ProcessingResult> ChangePassword(ChangePasswordRequest model)
        {
            var item = await _accountRepository.FindAll().FirstOrDefaultAsync(x => x.Password == model.OldPassword && x.Id == model.Id);
            if (item == null) return new ProcessingResult { MessageType = MessageTypeEnum.Danger, Message = "Mật khẩu hiện tại không đúng!", Success = false };

            try
            {
                item.Password = model.NewPassword;
                _accountRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Đổi mật khẩu thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Đổi mật khẩu thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<AuthenticateAccountResponse> Authenticate(AuthenticateRequest model)
        {
            // return null if user not found
            var user = await _accountRepository.FindAll().FirstOrDefaultAsync(x => x.UserName == model.Username && x.Password == model.Password);
            if (user == null) return null;

            // authentication successful so generate jwt and refresh tokens
            var jwtToken = generateJwtToken(user);
            //var refreshToken = generateRefreshToken(ipAddress);

            // save refresh token
            //user.RefreshTokens.Add(refreshToken);
            //_accountRepository.Update(user);
            //await _unitOfWork.SaveChangesAsync();

            return new AuthenticateAccountResponse(user, jwtToken);
        }

        /// <summary>
        /// Gửi mã xác thực email
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        //public async Task<ProcessingResult> SendTokenVerifyEmail(int accountId, string email, string origin)
        //{
        //    var account = _accountRepository.FindAll().SingleOrDefault(x => x.Id == accountId);
        //    if (account == null) return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xác thực thất bại", Success = false };
        //    try
        //    {
        //        account.VerificationToken = randomTokenString(6);
        //        account.VerificationTokenExpiresTime = DateTime.UtcNow.AddMinutes(5);

        //        _accountRepository.Update(account);
        //        await _unitOfWork.SaveChangesAsync();

        //        // send email
        //        sendVerificationEmail(account, email, origin);
        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Vui lòng kiểm tra email", Success = true, Data = account };
        //    }
        //    catch (Exception ex)
        //    {
        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
        //        _logging.LogException(ex, new { accountId = accountId, email = email });
        //    }
        //    return processingResult;
        //}

        /// /// <summary>
        /// Xác thực email - token: mã dc gửi qua email
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        //public async Task<ProcessingResult> VerifyEmail(string token)
        //{
        //    var account = _accountRepository.FindAll().SingleOrDefault(x => x.VerificationToken == token &&
        //        x.VerificationTokenExpiresTime > DateTime.UtcNow);

        //    if (account == null) return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xác thực thất bại", Success = false };
        //    try
        //    {
        //        account.VerifiedTime = DateTime.UtcNow;
        //        account.VerificationToken = null;

        //        _accountRepository.Update(account);
        //        await _unitOfWork.SaveChangesAsync();
        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xác thực thành công", Success = true, Data = account };
        //    }
        //    catch (Exception ex)
        //    {
        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
        //        _logging.LogException(ex, new
        //        {
        //            token = token
        //        });
        //    }
        //    return processingResult;
        //}

        /// <summary>
        /// Gửi mã khôi phục mật khẩu vào email
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ProcessingResult> ForgotPassword(ForgotPasswordRequest model, string origin)
        {
            try
            {
                var account = await _accountRepository.FindAll().FirstOrDefaultAsync(x => x.Email == model.Email);
                if (account == null) return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Email không tồn tại!", Success = false };
                // create reset token that expires after 1 day
                account.Password = randomTokenString(6);

                //account.ResetToken = randomTokenString(6);
                //account.ResetTokenExpiresTime = DateTime.UtcNow.AddDays(1);
                //
                _accountRepository.Update(account);
                await _unitOfWork.SaveChangesAsync();
                // send email
                sendPasswordResetEmail(account, model.Email, origin);
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Vui lòng kiểm tra email!", Success = true, Data = account };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Gửi yêu cầu thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        /// <summary>
        /// Kiểm tra xác thực tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //public async Task<ProcessingResult> ValidateResetToken(ValidateResetTokenRequest model)
        //{
        //    try
        //    {
        //        var account = await _accountRepository.FindAll().SingleOrDefaultAsync(x =>
        //            x.ResetToken == model.Token &&
        //            x.ResetTokenExpiresTime > DateTime.UtcNow);

        //        if (account == null)
        //            return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xác thực thất bại", Success = false };

        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xác thực không thành công", Success = true, Data = account };
        //    }
        //    catch (Exception ex)
        //    {
        //        processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
        //        _logging.LogException(ex, model);
        //    }
        //    return processingResult;
        //}

        public async Task<ProcessingResult> ResetPassword(ResetPasswordRequest model)
        {
            var account = await _accountRepository.FindAll().SingleOrDefaultAsync(x =>
                x.ResetToken == model.Token &&
                x.ResetTokenExpiresTime > DateTime.UtcNow);

            if (account == null)
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thông tin không tồn tại", Success = false };
            try
            {
                // update password and remove reset token
                account.Password = model.Password;
                account.PasswordResetTime = DateTime.UtcNow;
                account.ResetToken = null;
                account.ResetTokenExpiresTime = null;

                _accountRepository.Update(account);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Đổi mật khẩu thành công", Success = true, Data = account };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        private string generateJwtToken(Account user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        //private RefreshToken generateRefreshToken(string ipAddress)
        //{
        //    using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
        //    {
        //        var randomBytes = new byte[64];
        //        rngCryptoServiceProvider.GetBytes(randomBytes);
        //        return new RefreshToken
        //        {
        //            Token = Convert.ToBase64String(randomBytes),
        //            Expires = DateTime.UtcNow.AddDays(7),
        //            CreateTime = DateTime.UtcNow,
        //            CreatedByIp = ipAddress
        //        };
        //    }
        //}

        //private void removeOldRefreshTokens(Account account)
        //{
        //    var ressult = account.RefreshTokens.Where(x =>
        //          !x.IsActive &&
        //          x.CreateTime.GetValueOrDefault().AddDays(_appSettings.RefreshTokenTTL) <= DateTime.UtcNow);
        //}

        private string randomTokenString(int maxlength = 5)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(Enumerable.Repeat(chars, maxlength).Select(s => s[random.Next(s.Length)]).ToArray());
            return result;
        }

        //private void sendVerificationEmail(Account account, string email, string origin)
        //{
        //    string message;
        //    if (!string.IsNullOrEmpty(origin))
        //    {
        //        var verifyUrl = $"{origin}/account/verify-email?token={account.VerificationToken}";
        //        message = $@"<p>Vui lòng nhấp vào liên kết dưới đây để xác thực địa chỉ email của bạn:</p>
        //                     <p><a href=""{verifyUrl}"">{verifyUrl}</a></p>";
        //    }
        //    else
        //    {
        //        message = $@"<p>Đây là mã kích hoạt tài khoản của bạn:</p>
        //                     <p><code>{account.VerificationToken}</code></p>";
        //    }

        //    _sendEmailService.Send(
        //        to: email,
        //        subject: "[Orrianna.vn] Mã xác thực tài khoản",
        //        html: $@"<h4>Chào mừng bạn đến với Orrianna.vn!</h4>
        //                 {message}
        //                <p>Đây là một tin nhắn tự động, vui lòng không trả lời!</p>"
        //    );
        //}

        //private void sendAlreadyRegisteredEmail(string email, string origin)
        //{
        //    string message;
        //    if (!string.IsNullOrEmpty(origin))
        //        message = $@"<p>Nếu bạn không biết mật khẩu của mình, vui lòng truy cập <a href=""{origin}/account/forgot-password"">Quên mật khẩu</a>.</p>";
        //    else
        //        message = "<p>Nếu bạn quên mật khẩu của mình, bạn có chọn <strong>Quên mật khẩu</strong>.</p>";

        //    _sendEmailService.Send(
        //        to: email,
        //        subject: "[Orrianna.vn] Đăng ký tài khoản thành công",
        //        html: $@"<h4>Email đã đăng ký</h4>
        //                 <p>Email của bạn <strong> {email} </strong> đã được đăng ký.</p>
        //                 {message}"
        //    );
        //}
        private void sendPasswordResetEmail(Account account, string email, string origin)
        {
            var resetUrl = $"{origin}/dang-nhap";
            string message = $@"<p>Mật khẩu mới của quý khách là:</p>
                             <p><h4>{account.Password}</h4></p>
                             <p>Để đảm bảo an toàn. Quý khách vui lòng đăng nhập vào tài khoản và tiến hành đổi mật khẩu mới.</p>";

            _sendEmailService.Send(
                to: email,
                cc: "",
                subject: "[Orrianna.vn] Quên mật khẩu",
                html: $@"<h4>Xin chào quý khách!</h4>
                         {message}"
            );
        }

        //private void sendPasswordResetEmail(Account account, string email, string origin)
        //{
        //    string message;
        //    if (!string.IsNullOrEmpty(origin))
        //    {
        //        var resetUrl = $"{origin}/account/reset-password?token={account.ResetToken}";
        //        message = $@"<p>Please click the below link to reset your password, the link will be valid for 1 day:</p>
        //                     <p><a href=""{resetUrl}"">{resetUrl}</a></p>";
        //    }
        //    else
        //    {
        //        message = $@"<p>Đây là mã xác thực dùng để lấy lại mật khẩu:</p>
        //                     <p><code>{account.ResetToken}</code></p>";
        //    }

        //    _sendEmailService.Send(
        //        to: email,
        //        cc: "",
        //        subject: "[Orrianna.vn] Mã xác thực lấy lại mật khẩu",
        //        html: $@"<h4>Lấy lại mật khẩu</h4>
        //                 {message}"
        //    );
        //}

        public async Task<ProcessingResult> Lock(int id)
        {
            var item = _accountRepository.FindById(id);
            try
            {
                item.Status = item.Status.ToBool() == true ? false : true;
                _accountRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
    }
}
