﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class ProductWishlistService : IProductWishlistService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<ProductWishlist> _productWishlistRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public ProductWishlistService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<ProductWishlist> productWishlistRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _productWishlistRepository = productWishlistRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public Task<ProductWishlistModel> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<ProductWishlistModel> FindByReferenceId(int customerId, int productId)
        {
            return await _productWishlistRepository.FindAll(x => x.CustomerId == customerId && x.ProductId == productId).ProjectTo<ProductWishlistModel>(_configMapper).FirstOrDefaultAsync();
        }

        public async Task<List<ProductWishlistModel>> GetAllAsync()
        {
            return await _productWishlistRepository.FindAll().ProjectTo<ProductWishlistModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ProductWishlistModel>> GetByCustomerAsync(int customerId)
        {
            return await _productWishlistRepository.FindAll(x => x.CustomerId == customerId).ProjectTo<ProductWishlistModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(ProductWishlistModel model)
        {
            try
            {
                var item = _mapper.Map<ProductWishlist>(model);
                _productWishlistRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(ProductWishlistModel model)
        {
            if (await FindById(model.ProductWishlistId) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<ProductWishlist>(model);
                _productWishlistRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _productWishlistRepository.FindById(id);
                _productWishlistRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByCustomerAsync(int customerId)
        {
            try
            {
                var list = _productWishlistRepository.FindAll(x => x.CustomerId == customerId);
                foreach (var item in list)
                {
                    _productWishlistRepository.Remove(item);
                }
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = list };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { customerId = customerId });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByReferenceIdAsync(int customerId, int productId)
        {
            try
            {
                var item = await _productWishlistRepository.FindAll(x => x.CustomerId == customerId && x.ProductId == productId).FirstOrDefaultAsync();
                _productWishlistRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { customerId = customerId, productId = productId });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _productWishlistRepository.FindAll().ProjectTo<ProductWishlist>(_configMapper);
            return await query.ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
