﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class ProductAttributeService : IProductAttributeService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<ProductAttribute> _productAttributeRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public ProductAttributeService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<ProductAttribute> productAttributeRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _productAttributeRepository = productAttributeRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<ProductAttributeModel> FindById(int id)
        {
            return await _productAttributeRepository.FindAll(x => x.Id == id).ProjectTo<ProductAttributeModel>(_configMapper).FirstOrDefaultAsync();
        }
        public async Task<ProductAttributeModel> FindByReferenceId(int productId, int attributeId)
        {
            return await _productAttributeRepository.FindAll(x => x.ProductId == productId && x.AttributeId == attributeId).ProjectTo<ProductAttributeModel>(_configMapper).FirstOrDefaultAsync();
        }
        public async Task<ProductAttributeModel> FindByReferenceId(int productId, int colorId, int sizeId)
        {
            return await _productAttributeRepository.FindAll(x => x.ProductId == productId && x.Parent.AttributeId == colorId && x.AttributeId == sizeId).ProjectTo<ProductAttributeModel>(_configMapper).FirstOrDefaultAsync();
        }
        public async Task<List<ProductAttributeModel>> GetByParentId(int parentId)
        {
            return await _productAttributeRepository.FindAll(x => x.ParentId == parentId && x.Quantity > 0 && x.Status == true).Include(x => x.Attribute).ProjectTo<ProductAttributeModel>(_configMapper).ToListAsync();
        }
        public async Task<List<ProductAttributeModel>> GetAllAsync()
        {
            return await _productAttributeRepository.FindAll().ProjectTo<ProductAttributeModel>(_configMapper).ToListAsync();
        }
        public async Task<List<ProductAttributeModel>> GetAllByProductId(int productId, bool isParent)
        {
            if (!isParent)
            {
                return await _productAttributeRepository.FindAll(x => x.ProductId == productId && x.ParentId != null && x.Status == true)
                                                        .Include(x => x.Attribute).ProjectTo<ProductAttributeModel>(_configMapper).ToListAsync();
            }
            else
            {
                return await _productAttributeRepository.FindAll(x => x.ProductId == productId && x.ParentId == null && x.Status == true)
                                                        .Include(x => x.Attribute).ProjectTo<ProductAttributeModel>(_configMapper).ToListAsync();
            }
        }
        public async Task<List<ProductAttribute>> GetByProductId(int productId, bool isParent)
        {
            if (!isParent)
            {
                return await _productAttributeRepository.FindAll(x => x.ProductId == productId && x.ParentId != null)
                                                        .Include(x => x.Product).Include(x => x.Parent).Include(x => x.Attribute).ToListAsync();
            }
            else
            {
                return await _productAttributeRepository.FindAll(x => x.ProductId == productId && x.ParentId == null)
                                                        .Include(x => x.Product).Include(x => x.Parent).Include(x => x.Attribute).ToListAsync();
            }
        }
        public async Task<ProcessingResult> AddAsync(ProductAttributeModel model)
        {
            try
            {
                var item = _mapper.Map<ProductAttribute>(model);
                _productAttributeRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(ProductAttributeModel model)
        {
            try
            {
                var item = _productAttributeRepository.FindById(model.Id);
                item.ProductId = model.ProductId;
                item.AttributeId = model.AttributeId;
                item.Avatar1 = model.Avatar1;
                item.Avatar2 = model.Avatar2;
                item.ImageList = model.ImageList;
                item.Price = model.Price;
                item.PricePromo = model.PricePromo;
                item.Quantity = model.Quantity;
                item.Status = model.Status;
                item.ParentId = model.ParentId;
                //
                _productAttributeRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _productAttributeRepository.FindById(id);
                _productAttributeRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByReferenceIdAsync(int productId, int attributeId)
        {
            try
            {
                var item = _productAttributeRepository.FindAll(x => x.ProductId == productId && x.AttributeId == attributeId).FirstOrDefault();
                _productAttributeRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { productId = productId, attributeId = attributeId });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByReferenceIdAsync(int productId, int colorId, int sizeId)
        {
            try
            {
                var item = _productAttributeRepository.FindAll(x => x.ProductId == productId && x.Parent.AttributeId == colorId && x.AttributeId == sizeId).FirstOrDefault();
                _productAttributeRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { productId = productId, colorId = colorId, sizeId = sizeId });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByProductIdAsync(int productId)
        {
            try
            {
                var result = _productAttributeRepository.FindAll(x => x.ProductId == productId);
                foreach (var item in result)
                {
                    _productAttributeRepository.Remove(item);
                }
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { productId = productId });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _productAttributeRepository.FindAll().ProjectTo<ProductAttribute>(_configMapper);
            return await query.ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
