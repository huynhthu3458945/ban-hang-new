﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;
using AffilateServices.Models.Accesstrade;
using System.Net.Http;
using System.Text.Json;
using AffilateServices.Helpers;
using SoftNetCore.Common.Extensions;
using SoftNetCore.Services.BaseServices;

namespace SoftNetCore.Services.Implementation
{
    public class CouponTypeService : ICouponTypeService
    {
        private readonly IHttpClientFactory _clientFactory;

        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<CouponType> _couponTypeRepository;
        private readonly ICouponService _couponService;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public CouponTypeService(
            IHttpClientFactory httpClientFactory,
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<CouponType> couponTypeRepository,
            ICouponService couponService,
            IHostingEnvironment currentEnvironment,
            IUnitOfWork unitOfWork)
        {
            _clientFactory = httpClientFactory;

            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _couponTypeRepository = couponTypeRepository;
            _couponService = couponService;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<ProcessingResult> AddAsync(CouponTypeModel model)
        {
            try
            {
                var item = _mapper.Map<CouponType>(model);
                _couponTypeRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var productCategory = await _couponTypeRepository
                    .FindAll(p => p.Id == id)
                    .FirstOrDefaultAsync();
                _couponTypeRepository.Remove(productCategory);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = productCategory };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<CouponTypeModel> FindById(int id)
        {
            return await _couponTypeRepository.FindAll().ProjectTo<CouponTypeModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<CouponAndTotalTypeModel>> GetAllTotalTypeAsync()
        {
            var couponType = await _couponTypeRepository.FindAll().ProjectTo<CouponTypeModel>(_configMapper).ToListAsync();
            var res = new List<CouponAndTotalTypeModel>();
            foreach (var item in couponType)
            {
                var model = new CouponAndTotalTypeModel();
                model.CouponTypeModel = item;
                model.Total = _couponService.GetTotalByCouponType(item.Id, 1);
                res.Add(model);
            }
            return res;
        }

        public async Task<List<CouponTypeModel>> GetDataAsync()
        {
            var result = await _couponTypeRepository.FindAll().ProjectTo<CouponTypeModel>(_configMapper).ToListAsync();
            return result;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _couponTypeRepository.FindAll().ProjectTo<CouponType>(_configMapper);
            return await query.ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<ProcessingResult> UpdateAsync(CouponTypeModel model)
        {
            var item = await _couponTypeRepository.FindByIdAsync(model.Id);
            if (item == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                item.Title = model.Title;
                item.Logo = model.Logo;
                _couponTypeRepository.Update(item);

                await _unitOfWork.SaveChangesAsync();

                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        async Task<List<CouponTypeModel>> IBaseService<CouponTypeModel>.GetAllAsync()
        {
            return await _couponTypeRepository.FindAll().OrderBy(x => x.Title).ProjectTo<CouponTypeModel>(_configMapper).ToListAsync();
        }
    }
}
