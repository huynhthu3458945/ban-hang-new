﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class ProductCollectionService : IProductCollectionService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<ProductCollection> _productCollectionRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public ProductCollectionService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<ProductCollection> productCollectionRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _productCollectionRepository = productCollectionRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<ProductCollectionModel> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<ProductCollectionModel> FindByReferenceId(int collectionId, int productId)
        {
            return await _productCollectionRepository.FindAll(x => x.CollectionId == collectionId && x.ProductId == productId).ProjectTo<ProductCollectionModel>(_configMapper).FirstOrDefaultAsync();
        }

        public async Task<List<ProductCollectionModel>> GetAllAsync()
        {
            return await _productCollectionRepository.FindAll().ProjectTo<ProductCollectionModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ProductCollectionModel>> GetByCollectionAsync(int collectionId)
        {
            return await _productCollectionRepository.FindAll(x => x.CollectionId == collectionId).ProjectTo<ProductCollectionModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(ProductCollectionModel model)
        {
            try
            {
                var item = _mapper.Map<ProductCollection>(model);
                _productCollectionRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(ProductCollectionModel model)
        {
            try
            {
                var item = _mapper.Map<ProductCollection>(model);
                _productCollectionRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _productCollectionRepository.FindById(id);
                _productCollectionRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByCollectionAsync(int collectionId)
        {
            try
            {
                var list = _productCollectionRepository.FindAll(x => x.CollectionId == collectionId);
                foreach (var item in list)
                {
                    _productCollectionRepository.Remove(item);
                }
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = list };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { collectionId = collectionId });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByReferenceIdAsync(int collectionId, int productId)
        {
            try
            {
                var item = await _productCollectionRepository.FindAll(x => x.CollectionId == collectionId && x.ProductId == productId).FirstOrDefaultAsync();
                _productCollectionRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { collectionId = collectionId, productId = productId });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _productCollectionRepository.FindAll().ProjectTo<ProductCollection>(_configMapper);
            return await query.ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
