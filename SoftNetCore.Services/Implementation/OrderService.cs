﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;
using SoftNetCore.Model.Request;

namespace SoftNetCore.Services.Implementation
{
    public class OrderService : IOrderService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Order> _orderRepository;
        private readonly IBaseRepository<OrderDetail> _orderDetailRepository;
        //private readonly IOrderDetailService _orderDetailService;
        private readonly ICustomerService _customerService;
        private readonly IProvinceService _provinceService;
        //private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public OrderService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Order> orderRepository,
            IBaseRepository<OrderDetail> orderDetailRepository,
            //IOrderDetailService orderDetailService,
            ICustomerService customerService,
            IProvinceService provinceService,
            //IHostingEnvironment currentEnvironment,
            IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            //_orderDetailService = orderDetailService;
            _customerService = customerService;
            _provinceService = provinceService;
            //_currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<OrderModel> FindById(int id)
        {
            return await _orderRepository.FindAll().ProjectTo<OrderModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<OrderModel> FindFullByIdAsync(int id)
        {
            var result = await _orderRepository.FindAll().Include(x => x.OrderDetails).ProjectTo<OrderModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }

        public async Task<List<OrderModel>> GetAllAsync()
        {
            return await _orderRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<OrderModel>(_configMapper).ToListAsync();
        }

        public async Task<List<OrderModel>> GetByStatusAsync(int status)
        {
            return await _orderRepository.FindAll(x => x.Status == status).ProjectTo<OrderModel>(_configMapper).ToListAsync();
        }

        public async Task<List<OrderModel>> GetByCustomerAsync(int customerId)
        {
            return await _orderRepository.FindAll(x => x.CustomerId == customerId).OrderByDescending(x => x.CreateTime).ProjectTo<OrderModel>(_configMapper).ToListAsync();
        }

        public async Task<List<Order>> GetDataAsync()
        {
            return await _orderRepository.FindAll().Include(x => x.OrderStatus).Include(x => x.PayType).OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<ProcessingResult> SaveOrderAsync(OrderModel order, List<CartItem> listCart, CustomerModel customer)
        {
            try
            {
                ProcessingResult resultCust = new ProcessingResult();
                CustomerModel customerNew = new CustomerModel();
                //neu tao tai khoan
                //if (customer == null)
                //{
                //    customerNew.FullName = order.FullName;
                //    customerNew.Email = order.Email;
                //    customerNew.Password = order.Mobi;
                //    customerNew.Phone = order.Mobi;
                //    customerNew.Address = order.Address;
                //    customerNew.CustomerTypeId = 2;
                //    customerNew.CreateTime = DateTime.Now;
                //    resultCust = await _customerService.AddAsync(customerNew);
                //}

                // Lấy phí ship
                //var province = await _provinceService.FindById(order.ProvinceId.ToInt());
                //order.CreateBy = 2;
                order.Code = FileExtension.RandomString(6).ToUpper();
                order.SaleOff = 0;
                order.FeeShip = 0; //province?.FeeShip
                order.CustomerId = customer?.Id;
                order.NoteSale = string.Empty;//cho xu lý hàng hóa
                order.TotalPrice = (decimal?)ListCartItem.GetTotal(listCart); //  + province?.FeeShip
                order.Status = 1;
                order.OrderStatusId = 1; // chưa xử lý
                order.CreateTime = DateTime.Now;
                var item = _mapper.Map<Order>(order);
                _orderRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();

                int Oid = item.Id;

                // Thêm chi tiết đơn hàng
                listCart.ToList().ForEach(x =>
                {
                    OrderDetail item = new OrderDetail();
                    item.OrderId = Oid;
                    item.ProductId = x.ProductId;
                    //item.ColorId = x.ColorId;
                    //item.SizeId = x.SizeId;
                    item.Quantity = x.Quantity;
                    item.Price = (decimal?)x.Price;
                    item.OriginalPrice = (decimal?)x.OriginalPrice;
                    _orderDetailRepository.Add(item);
                });
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Đặt hàng thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Đặt hàng thất bại!", Success = false };
                _logging.LogException(ex, order);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> AddAsync(OrderModel model)
        {
            try
            {
                var item = _mapper.Map<Order>(model);
                item.CreateTime = DateTime.Now;
                _orderRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(OrderModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Order>(model);
                _orderRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateRatingAsync(RatingRequest model)
        {
            if (await FindById(model.OrderId) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Đánh giá thất bại!", Success = false };
            }
            try
            {
                var item = _orderRepository.FindById(model.OrderId);
                item.Rating = model.Rating;
                item.RatingContent = model.Content;
                item.IsRating = model.IsRating;
                item.RatingTime = DateTime.Now;
                //
                _orderRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Đánh giá thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Đánh giá thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _orderRepository.FindById(id);
                _orderRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _orderRepository.FindAll().ProjectTo<Order>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id, int status)
        {

            try
            {
                var item = _orderRepository.FindById(id);
                item.OrderStatusId = status;
                _orderRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateStatusAsync(StatusOrderRequest model)
        {
            if (await FindById(model.OrderId) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Đánh giá thất bại!", Success = false };
            }
            try
            {
                var item = _orderRepository.FindById(model.OrderId);
                item.OrderStatusId = 5;
                item.Remark = model.Remark;
                //
                _orderRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Đánh giá thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Đánh giá thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }
    }
}
