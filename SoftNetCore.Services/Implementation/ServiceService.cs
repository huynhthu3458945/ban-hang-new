﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class ServiceService : IServiceService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Service> _serviceRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public ServiceService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Service> serviceRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _serviceRepository = serviceRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<ServiceModel> FindById(int id)
        {
            return await _serviceRepository.FindAll().ProjectTo<ServiceModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<ServiceModel>> GetAllAsync()
        {
            return await _serviceRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<ServiceModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ServiceModel>> GetByStatusAsync(bool status)
        {
            return await _serviceRepository.FindAll(x => x.Status == status).ProjectTo<ServiceModel>(_configMapper).ToListAsync();
        }
        public async Task<List<Service>> GetDataAsync()
        {
            return await _serviceRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<ServiceModel>> GetRelatedAsync(int id, int catId, int numberItem)
        {
            var query = _serviceRepository.FindAll(x => x.ServiceCategoryId == catId && x.Id != id && x.Status == true).ProjectTo<ServiceModel>(_configMapper);
            return await query.OrderByDescending(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<List<ServiceModel>> GetTopAsync(int numberItem)
        {
            var query = _serviceRepository.FindAll(x => x.Status == true).ProjectTo<ServiceModel>(_configMapper);
            return await query.OrderByDescending(x => x.Position).Take(numberItem).ToListAsync();
        }
        public async Task<List<ServiceModel>> GetByCategoryAsync(int catId)
        {
            return await _serviceRepository.FindAll(x => x.ServiceCategoryId == catId && x.Status == true).OrderByDescending(x => x.Position).ProjectTo<ServiceModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(ServiceModel model)
        {
            try
            {
                var item = _mapper.Map<Service>(model);
                item.CreateTime = DateTime.Now;
                _serviceRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(ServiceModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Service>(model);
                _serviceRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _serviceRepository.FindById(id);
                _serviceRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _serviceRepository.FindAll().ProjectTo<Service>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<List<ServiceModel>> GetNewAsync(int numberItem)
        {
            var query = _serviceRepository.FindAll(x => x.Status == true).ProjectTo<ServiceModel>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<ServiceModel>> GetRandomAsync(int numberItem)
        {
            Random rand = new Random();
            var query = _serviceRepository.FindAll(x => x.Status == true).ProjectTo<ServiceModel>(_configMapper);

            int random = rand.Next(0, query.Count());
            return await query.OrderBy(x => Guid.NewGuid()).Take(numberItem).ToListAsync();
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            var item = _serviceRepository.FindById(id);
            try
            {
                item.Status = item.Status.ToBool() == true ? false : true;
                _serviceRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateViewAsync(int id)
        {
            var item = _serviceRepository.FindById(id);
            try
            {
                item.ViewTime += item.ViewTime.ToInt();
                _serviceRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
    }
}
