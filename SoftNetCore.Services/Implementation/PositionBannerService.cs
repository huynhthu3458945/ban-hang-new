﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class PositionBannerService : IPositionBannerService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<PositionBanner> _positionBannerRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public PositionBannerService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<PositionBanner> positionBannerRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _positionBannerRepository = positionBannerRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<PositionBannerModel> FindById(int id)
        {
            return await _positionBannerRepository.FindAll().ProjectTo<PositionBannerModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<PositionBannerModel>> GetAllAsync()
        {
            return await _positionBannerRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<PositionBannerModel>(_configMapper).ToListAsync();
        }
        public async Task<List<PositionBannerModel>> GetByStatusAsync(bool status)
        {
            return await _positionBannerRepository.FindAll(x => x.Status == status).ProjectTo<PositionBannerModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(PositionBannerModel model)
        {
            try
            {
                var item = _mapper.Map<PositionBanner>(model);
                item.CreateTime = DateTime.Now;
                _positionBannerRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(PositionBannerModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<PositionBanner>(model);
                _positionBannerRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _positionBannerRepository.FindById(id);
                _positionBannerRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _positionBannerRepository.FindAll().ProjectTo<PositionBanner>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            try
            {
                var item = _positionBannerRepository.FindById(id);
                item.Status = item.Status == true ? false : true;
                _positionBannerRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
    }
}
