﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class ArticleService : IArticleService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Article> _articleRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public ArticleService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Article> articleRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _articleRepository = articleRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<ArticleModel> FindById(int id)
        {
            var result = await _articleRepository.FindAll().ProjectTo<ArticleModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
            if (result != null)
            {
                result.ViewTime = result.ViewTime.ToInt() + 1;
                var item = _mapper.Map<Article>(result);
                _articleRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
            }
            return result;
        }

        public async Task<List<ArticleModel>> GetAllAsync()
        {
            return await _articleRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<ArticleModel>(_configMapper).ToListAsync();
        }

        public async Task<List<Article>> GetDataAsync()
        {
            return await _articleRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<ArticleModel>> GetByCategoryAsync(int catId)
        {
            return await _articleRepository.FindAll(x => x.ArticleCategoryId == catId && x.Status == true).OrderByDescending(x => x.CreateTime).ProjectTo<ArticleModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ArticleModel>> GetByStatusAsync(bool status)
        {
            return await _articleRepository.FindAll(x => x.Status == status).OrderByDescending(x => x.CreateTime).ProjectTo<ArticleModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(ArticleModel model)
        {
            try
            {
                var item = _mapper.Map<Article>(model);
                item.Alias = item.Title.ToUrlFormat(true);
                item.CreateTime = DateTime.Now;
                _articleRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(ArticleModel model)
        {
            //if (await FindById(model.Id) == null)
            //{
            //    return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            //}
            try
            {
                var item = _mapper.Map<Article>(model);
                item.Alias = item.Title.ToUrlFormat(true);
                _articleRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _articleRepository.FindById(id);
                _articleRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _articleRepository.FindAll().ProjectTo<Article>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<List<ArticleModel>> GetHotAsync(int numberItem)
        {
            var query = _articleRepository.FindAll(x => x.Status == true).ProjectTo<ArticleModel>(_configMapper);
            return await query.OrderByDescending(x => x.ViewTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<ArticleModel>> GetHotAsync(int numberItem, int catId)
        {
            var query = _articleRepository.FindAll(x => x.Status == true && x.ArticleCategoryId == catId).ProjectTo<ArticleModel>(_configMapper);
            return await query.OrderByDescending(x => x.ViewTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<ArticleModel>> GetNewAsync(int numberItem)
        {
            var query = _articleRepository.FindAll(x => x.Status == true).ProjectTo<ArticleModel>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<ArticleModel>> GetRandomAsync(int numberItem)
        {
            Random rand = new Random();
            var query = _articleRepository.FindAll(x => x.Status == true).ProjectTo<ArticleModel>(_configMapper);

            int random = rand.Next(0, query.Count());
            return await query.OrderBy(x => Guid.NewGuid()).Take(numberItem).ToListAsync();
        }

        public async Task<List<ArticleModel>> GetRelatedAsync(int id, int catId, int numberItem)
        {
            var query = _articleRepository.FindAll(x => x.ArticleCategoryId == catId && x.Id != id && x.Status == true).ProjectTo<ArticleModel>(_configMapper);
            return await query.OrderBy(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<List<ArticleModel>> GetTopAsync(int numberItem)
        {
            var query = _articleRepository.FindAll(x => x.Status == true).ProjectTo<ArticleModel>(_configMapper);
            return await query.OrderBy(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            var item = _articleRepository.FindById(id);
            try
            {
                item.Status = item.Status.ToBool() == true ? false : true;
                _articleRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateViewAsync(int id)
        {
            var item = _articleRepository.FindById(id);
            try
            {
                item.ViewTime += item.ViewTime.ToInt();
                _articleRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
    }
}
