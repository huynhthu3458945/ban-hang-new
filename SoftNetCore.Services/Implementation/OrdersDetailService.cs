﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<OrderDetail> _orderDetailRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public OrderDetailService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<OrderDetail> orderDetailRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _orderDetailRepository = orderDetailRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<OrderDetailModel> FindById(int id)
        {
            return await _orderDetailRepository.FindAll(x => x.Id == id).ProjectTo<OrderDetailModel>(_configMapper).FirstOrDefaultAsync();
        }

        public async Task<OrderDetailModel> FindByReferenceId(int orderId, int productId)
        {
            return await _orderDetailRepository.FindAll(x => x.OrderId == orderId && x.ProductId == productId).ProjectTo<OrderDetailModel>(_configMapper).FirstOrDefaultAsync();
        }

        public async Task<List<OrderDetailModel>> GetAllAsync()
        {
            return await _orderDetailRepository.FindAll().ProjectTo<OrderDetailModel>(_configMapper).ToListAsync();
        }
        public List<OrderDetail> GetByOrder(int orderId)
        {
            return  _orderDetailRepository.FindAll(x => x.OrderId == orderId).ToList();
        }

        public async Task<List<OrderDetailModel>> GetByOrderAsync(int orderId)
        {
            return await _orderDetailRepository.FindAll(x => x.OrderId == orderId).ProjectTo<OrderDetailModel>(_configMapper).ToListAsync();
        }

        public async Task<List<OrderDetailModel>> GetByOrderFullAsync(int orderId)
        {
            return await _orderDetailRepository.FindAll(x => x.OrderId == orderId).Include(x => x.Order).Include(x => x.Product).ProjectTo<OrderDetailModel>(_configMapper).ToListAsync();
        }


        public async Task<ProcessingResult> AddAsync(OrderDetailModel model)
        {
            try
            {
                var item = _mapper.Map<OrderDetail>(model);
                _orderDetailRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(OrderDetailModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<OrderDetail>(model);
                _orderDetailRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _orderDetailRepository.FindById(id);
                _orderDetailRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByOrderAsync(int orderId)
        {
            try
            {
                var list = _orderDetailRepository.FindAll(x => x.OrderId == orderId);
                foreach (var item in list)
                {
                    _orderDetailRepository.Remove(item);
                }
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = list };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { orderId = orderId });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteByReferenceIdAsync(int orderId, int productId)
        {
            try
            {
                var item = await _orderDetailRepository.FindAll(x => x.OrderId == orderId && x.ProductId == productId).FirstOrDefaultAsync();
                _orderDetailRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { orderId = orderId, productId = productId });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _orderDetailRepository.FindAll().ProjectTo<OrderDetail>(_configMapper);
            return await query.ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
