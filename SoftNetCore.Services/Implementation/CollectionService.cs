﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class CollectionService : ICollectionService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Collection> _collectionRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public CollectionService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Collection> collectionRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _collectionRepository = collectionRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<CollectionModel> FindById(int id)
        {
            return await _collectionRepository.FindAll().ProjectTo<CollectionModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<CollectionModel>> GetAllAsync()
        {
            return await _collectionRepository.FindAll().OrderByDescending(x => x.CreateTime).ProjectTo<CollectionModel>(_configMapper).ToListAsync();
        }

        public async Task<List<Collection>> GetDataAsync()
        {
            return await _collectionRepository.FindAll().OrderByDescending(x => x.CreateTime).ToListAsync();
        }

        public async Task<List<CollectionModel>> GetByCategoryAsync(int catId)
        {
            return await _collectionRepository.FindAll(x => x.CollectionCategoryId == catId && x.Status == true).OrderByDescending(x => x.CreateTime).ProjectTo<CollectionModel>(_configMapper).ToListAsync();
        }

        public async Task<List<CollectionModel>> GetByStatusAsync(bool status)
        {
            return await _collectionRepository
                .FindAll(x => x.Status == status)
                .AsNoTracking()
                .ProjectTo<CollectionModel>(_configMapper)
                .OrderByDescending(x => x.CreateTime)
                .ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(CollectionModel model)
        {
            try
            {
                var item = _mapper.Map<Collection>(model);
                item.CreateTime = DateTime.Now;
                _collectionRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(CollectionModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Collection>(model);
                _collectionRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _collectionRepository.FindById(id);
                _collectionRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _collectionRepository.FindAll().ProjectTo<Collection>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<List<CollectionModel>> GetNewAsync(int numberItem)
        {
            var query = _collectionRepository.FindAll(x => x.Status == true).ProjectTo<CollectionModel>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).Take(numberItem).ToListAsync();
        }

        public async Task<List<CollectionModel>> GetRandomAsync(int numberItem)
        {
            Random rand = new Random();
            var query = _collectionRepository.FindAll(x => x.Status == true).ProjectTo<CollectionModel>(_configMapper);

            int random = rand.Next(0, query.Count());
            return await query.OrderBy(x => Guid.NewGuid()).Take(numberItem).ToListAsync();
        }

        public async Task<List<CollectionModel>> GetRelatedAsync(int id, int catId, int numberItem)
        {
            var query = _collectionRepository.FindAll(x => x.CollectionCategoryId == catId && x.Id != id && x.Status == true).ProjectTo<CollectionModel>(_configMapper);
            return await query.OrderByDescending(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<List<CollectionModel>> GetTopAsync(int numberItem)
        {
            var query = _collectionRepository.FindAll(x => x.Status == true).ProjectTo<CollectionModel>(_configMapper);
            return await query.OrderByDescending(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            var item = _collectionRepository.FindById(id);
            try
            {
                item.Status = item.Status.ToBool() == true ? false : true;
                _collectionRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }
    }
}
