﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class ProvinceService : IProvinceService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<Province> _provinceRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public ProvinceService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<Province> provinceRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _provinceRepository = provinceRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<ProvinceModel> FindById(int id)
        {
            return await _provinceRepository.FindAll().ProjectTo<ProvinceModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<ProvinceModel>> GetAllAsync()
        {
            return await _provinceRepository.FindAll(x => x.IsDelete == false).OrderBy(x => x.Title).ProjectTo<ProvinceModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ProvinceModel>> GetByProvinceAsync(int provinceId)
        {
            return await _provinceRepository.FindAll(x => x.Id == provinceId).ProjectTo<ProvinceModel>(_configMapper).ToListAsync();
        }
        public async Task<List<Province>> GetDataAsync()
        {
            return await _provinceRepository.FindAll().OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<List<ProvinceModel>> GetByBranchExists()
        {
            return await _provinceRepository.FindAll(x => x.Branches.Count() > 0).OrderBy(x => x.Title).ProjectTo<ProvinceModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(ProvinceModel model)
        {
            try
            {
                var item = _mapper.Map<Province>(model);
                _provinceRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(ProvinceModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<Province>(model);
                _provinceRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _provinceRepository.FindById(id);
                _provinceRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _provinceRepository.FindAll().ProjectTo<Province>(_configMapper);
            return await query.OrderBy(x => x.Title).ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
