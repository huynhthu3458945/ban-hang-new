﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using AffilateServices.Helpers;
using AffilateServices.Models.Dimuadi;
using SoftNetCore.Model.vnz;

namespace SoftNetCore.Services.Implementation
{
    public class ProductCategoryService : IProductCategoryService
    {
        private readonly IHttpClientFactory _clientFactory;

        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<ProductCategory> _productCategoryRepository;
        private readonly IBaseRepository<MerchantProductByCategory> _merchantProductByCategoryRepository;
        private readonly IProductService _productService;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public ProductCategoryService(
            IHttpClientFactory httpClientFactory,
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<ProductCategory> productCategoryRepository,
            IBaseRepository<MerchantProductByCategory> merchantProductByCategoryRepository,
            IProductService productService,
        IHostingEnvironment currentEnvironment,
            IUnitOfWork unitOfWork)
        {
            _clientFactory = httpClientFactory;
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _productCategoryRepository = productCategoryRepository;
            _merchantProductByCategoryRepository = merchantProductByCategoryRepository;
            _productService = productService;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<ProductCategoryModel> FindById(int id)
        {
            return await _productCategoryRepository.FindAll().ProjectTo<ProductCategoryModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<ProductCategoryModel>> GetAllAsync()
        {
            return await _productCategoryRepository.FindAll().OrderBy(x => x.Position).ProjectTo<ProductCategoryModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ProductCategoryModel>> GetByStatusAsync(bool status)
        {
            return await _productCategoryRepository
                .FindAll(x => x.Status == status)
                .AsNoTracking()
                .Include(c => c.Products)
                .OrderBy(x => x.Position)
                .ProjectTo<ProductCategoryModel>(_configMapper)
                .ToListAsync();
        }

        public async Task<List<ProductCategoryModel>> GetAllGroupAsync(int? parentId)
        {
            return await _productCategoryRepository.FindAll(x => x.ParentId == parentId && x.Status == true).OrderBy(x => x.Position).ProjectTo<ProductCategoryModel>(_configMapper).ToListAsync();
        }

        public async Task<List<ProductCategoryModel>> GetParentAsync(bool isParent, bool status)
        {
            if (isParent)
            {
                var query = await _productCategoryRepository
                    .FindAll(x => x.ParentId == null && x.Status == status)
                    .AsNoTracking()
                    .OrderBy(x => x.Position)
                    .ProjectTo<ProductCategoryModel>(_configMapper)
                    .ToListAsync();

                foreach (var item in query)
                {
                    item.IsExistsChild = _productCategoryRepository.FindAll(x => x.ParentId == item.Id && x.Status == true)?.Count() > 0 ? true : false;
                }

                return query;
            }
            else
            {
                return await _productCategoryRepository.FindAll(x => x.ParentId != null && x.Status == status).OrderBy(x => x.Position).ProjectTo<ProductCategoryModel>(_configMapper).ToListAsync();
            }
        }

        public async Task<List<ProductCategoryModel>> GetParentAsync()
        {
            var query = await _productCategoryRepository
                .FindAll(x => x.Status == true)
                .AsNoTracking()
                .ProjectTo<ProductCategoryModel>(_configMapper)
                .OrderBy(x => x.Position)
                .ToListAsync();

            return query;
        }


        public async Task<List<ProductCategoryModel>> GetTopAsync(int numberItem)
        {
            var query = _productCategoryRepository.FindAll(x => x.Status == true).ProjectTo<ProductCategoryModel>(_configMapper);
            return await query.OrderByDescending(x => x.Position).Take(numberItem).ToListAsync();
        }

        public async Task<List<ProductCategoryModel>> GetDataAsync()
        {
            var result = await _productCategoryRepository.FindAll().OrderBy(x => x.Position).ProjectTo<ProductCategoryModel>(_configMapper).ToListAsync();
            foreach (var item in result)
            {
                item.ParentName = _productCategoryRepository.FindAll(x => x.Id == item.ParentId.ToInt()).FirstOrDefault()?.Title;
            }
            return result;
        }

        public async Task<List<ProductCategoryModel>> GetMenuAsync()
        {
            var result = await _productCategoryRepository.FindAll(x => x.Status == true).ProjectTo<ProductCategoryModel>(_configMapper).ToListAsync();
            foreach (var item in result)
            {
                var proCat = _productCategoryRepository.FindAll(x => x.Id == item.ParentId.ToInt()).FirstOrDefault();
                if (proCat != null)
                {
                    if (proCat.ParentId == null)
                    {
                        item.Title = "2. " + item.Title + " - " + proCat.Title;
                    }
                    else
                    {
                        item.Title = "3. " + item.Title + " - " + proCat.Title;
                    }
                }
                //else
                //{
                //    item.Title = "1. " + item.Title;
                //}
            }
            return result.OrderBy(x => x.Title).ThenBy(x => x.ParentId).ToList();
        }

        public async Task<ProcessingResult> AddAsync(ProductCategoryModel model)
        {
            try
            {
                var item = _mapper.Map<ProductCategory>(model);
                item.CreateTime = DateTime.Now;
                _productCategoryRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(ProductCategoryModel model)
        {
            var item = await _productCategoryRepository.FindByIdAsync(model.Id);
            if (item == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                item.Title = model.Title;
                item.Description = model.Description;
                item.MetaTitle = model.MetaTitle;
                item.MetaDescription = model.MetaDescription;
                item.MetaKeywords = model.MetaKeywords;
                item.Logo = model.Logo;
                item.Avatar = model.Avatar;
                item.Thumb = model.Thumb;
                item.Alias = model.Alias;
                item.Schemas = model.Schemas;
                item.Position = model.Position;
                item.Status = model.Status;
                item.ParentId = model.ParentId;
                _productCategoryRepository.Update(item);

                if (model.MerchantProductByCategories.Count > 0)
                {
                    var newMerchantProductByCategories = new List<MerchantProductByCategory>();
                    var merchantProductByCategories = await _merchantProductByCategoryRepository.FindAll(m => m.ProductCategoryId == item.Id).ToListAsync();
                    _merchantProductByCategoryRepository.RemoveMultiple(merchantProductByCategories);
                    foreach (var newItem in model.MerchantProductByCategories)
                    {
                        newMerchantProductByCategories.Add(new MerchantProductByCategory()
                        {
                            ProductCategoryId = item.Id,
                            Title = newItem.Title,
                            Description = newItem.Description,
                            Url = newItem.Url,
                            PageSize = newItem.PageSize,
                            AccessToken = newItem.AccessToken,
                            DomainId = newItem.DomainId,
                        });
                    }
                    _merchantProductByCategoryRepository.AddRange(newMerchantProductByCategories);
                }

                await _unitOfWork.SaveChangesAsync();

                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateStatusAsync(int id)
        {
            var item = _productCategoryRepository.FindById(id);
            try
            {
                item.Status = item.Status.ToBool() == true ? false : true;
                _productCategoryRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var productCategory = await _productCategoryRepository
                    .FindAll(p => p.Id == id)
                    .Include(p => p.MerchantProductByCategories)
                    .Include(p => p.Products)
                    .FirstOrDefaultAsync();

                _productCategoryRepository.Remove(productCategory);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = productCategory };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _productCategoryRepository.FindAll().ProjectTo<ProductCategory>(_configMapper);
            return await query.OrderByDescending(x => x.CreateTime).ToPaginationAsync(paramater.page, paramater.pageSize);
        }

        public async Task<bool> FetchProductCategoryAsync(string domain)
        {
            if (domain.Equals(EnumDomains.DIMUADI.GetName()))
            {
                var requestUrl = $"https://apis.dimuadi.vn/d2c-service/category?page_size=100";
                var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);

                var client = _clientFactory.CreateClient();
                var response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var serializeOptions = new JsonSerializerOptions();
                    serializeOptions.Converters.Add(new StringConverter());
                    var responseData = await JsonSerializer.DeserializeAsync<GetProductCategoryResponse>(responseStream, serializeOptions);

                    var productCategoryIds = responseData.data.Select(p => p.id);
                    var productCategoriesExisted = await _productCategoryRepository
                        .FindAll()
                        .AsNoTracking()
                        .ToListAsync();

                    var newProductCategories = new List<ProductCategory>();
                    foreach (var res in responseData.data)
                    {
                        var existed = productCategoriesExisted.FirstOrDefault(p => p.ProductCategoryCode == res.code);
                        if (existed != null) continue;

                        var image = await ConvertingImageUrlAsync(res.image);

                        var productCategory = new ProductCategory()
                        {
                            ProductCategoryId = res.id.ToString(),
                            Title = res.name,
                            ProductCategoryCode = res.code,
                            Description = res.description,
                            Avatar = image, // hình dc convert
                            Thumb = res.image, // lưu hình gốc
                            Status = res.status.ToBool(),
                            Position = res.sortOrder,
                            Domain = EnumDomains.DIMUADI.GetName(),
                        };

                        newProductCategories.Add(productCategory);
                    }

                    _productCategoryRepository.AddRange(newProductCategories);
                    await _unitOfWork.SaveChangesAsync();

                    return true;
                }
            }

            return false;
        }

        public async Task<string> ConvertingImageUrlAsync(string rootUrl)
        {
            var client = _clientFactory.CreateClient();
            var width = 245;
            var height = 245;
            var requestUrl = $"https://img.vnsoshop.com/api/image-processing?type=center&w={width}&h={height}&url={rootUrl}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new StringConverter());
                var responseData = await JsonSerializer.DeserializeAsync<GetImageUrlResponse>(responseStream, serializeOptions);
                if (responseData.Status == "Success")
                {
                    return responseData.Content;
                }
            }

            return string.Empty;
        }

        public async Task<ProcessingResult> DeleteItemAsync(int id)
        {
            try
            {
                var item = await _productCategoryRepository
                    .FindAll(p => p.Id == id)
                    .Include(p => p.MerchantProductByCategories)
                    .Include(p => p.Products)
                    .FirstOrDefaultAsync();

                _productCategoryRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id });
            }

            return processingResult;
        }

        public async Task<List<ProductCategoryModel>> GetAllActivatedProductCategoryAsync()
        {
            return await _productCategoryRepository
                .FindAll(x => x.Status == true)
                .AsNoTracking()
                .Include(p => p.Products)
                .OrderBy(x => x.Position)
                .ProjectTo<ProductCategoryModel>(_configMapper)
                .ToListAsync();

        }

        public async Task<List<ProductAndTotalCategoryModel>> GetAllTotalCategoryAsync()
        {
            var productCategory = await _productCategoryRepository.FindAll().ProjectTo<ProductCategoryModel>(_configMapper).ToListAsync();
            var res = new List<ProductAndTotalCategoryModel>();
            foreach (var item in productCategory)
            {
                var model = new ProductAndTotalCategoryModel();
                model.ProductCategoryModel = item;
                model.Total = _productService.GetTotalByProductCategory(item.Id, true);
                res.Add(model);
            }
            return res;
        }
    }
}
