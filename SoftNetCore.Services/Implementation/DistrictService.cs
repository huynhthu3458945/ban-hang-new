﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using System.Linq;

namespace SoftNetCore.Services.Implementation
{
    public class DistrictService : IDistrictService
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        private readonly ILogging _logging;
        private readonly IBaseRepository<District> _districtRepository;
        private readonly IHostingEnvironment _currentEnvironment;
        private readonly IUnitOfWork _unitOfWork;
        private ProcessingResult processingResult;

        public DistrictService(
            IMapper mapper,
            MapperConfiguration configMapper,
            ILogging logging,
            IBaseRepository<District> districtRepository,
            IHostingEnvironment currentEnvironment,
        IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _configMapper = configMapper;
            _logging = logging;
            _districtRepository = districtRepository;
            _currentEnvironment = currentEnvironment;
            _unitOfWork = unitOfWork;
        }

        public async Task<DistrictModel> FindById(int id)
        {
            return await _districtRepository.FindAll().ProjectTo<DistrictModel>(_configMapper).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<DistrictModel>> GetAllAsync()
        {
            return await _districtRepository.FindAll().OrderBy(x => x.Title).ProjectTo<DistrictModel>(_configMapper).ToListAsync();
        }

        public List<District> GetByProvinceId(int provinceId)
        {
            return _districtRepository.FindAll(x => x.ProvinceId == provinceId).ToList();
        }

        public async Task<List<DistrictModel>> GetByProvinceAsync(int provinceId)
        {
            return await _districtRepository.FindAll(x => x.ProvinceId == provinceId).ProjectTo<DistrictModel>(_configMapper).ToListAsync();
        }

        public  List<District> GetByBranchExistsAndProvinceId(int provinceId)
        {
            return  _districtRepository.FindAll(x => x.ProvinceId == provinceId && x.Branches.Count() > 0).ToList();
        }

        public async Task<List<DistrictModel>> GetByBranchExists()
        {
            return await _districtRepository.FindAll(x => x.Branches.Count() > 0).OrderBy(x => x.Title).ProjectTo<DistrictModel>(_configMapper).ToListAsync();
        }

        public async Task<ProcessingResult> AddAsync(DistrictModel model)
        {
            try
            {
                var item = _mapper.Map<District>(model);
                _districtRepository.Add(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Thêm mới thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Thêm mới thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> UpdateAsync(DistrictModel model)
        {
            if (await FindById(model.Id) == null)
            {
                return new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
            }
            try
            {
                var item = _mapper.Map<District>(model);
                _districtRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Cập nhật thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Cập nhật thất bại!", Success = false };
                _logging.LogException(ex, model);
            }
            return processingResult;
        }

        public async Task<ProcessingResult> DeleteAsync(int id)
        {
            try
            {
                var item = _districtRepository.FindById(id);
                _districtRepository.Remove(item);
                await _unitOfWork.SaveChangesAsync();
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Success, Message = "Xóa thành công!", Success = true, Data = item };
            }
            catch (Exception ex)
            {
                processingResult = new ProcessingResult() { MessageType = MessageTypeEnum.Danger, Message = "Xóa thất bại!", Success = false };
                _logging.LogException(ex, new { id = id });
            }
            return processingResult;
        }

        public async Task<Pager> PaginationAsync(ParamaterPagination paramater)
        {
            var query = _districtRepository.FindAll().ProjectTo<District>(_configMapper);
            return await query.OrderBy(x => x.Title).ToPaginationAsync(paramater.page, paramater.pageSize);
        }
    }
}
