﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace SoftNetCore.Services.BaseServices
{
    public class SessionService : ISessionService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        public SessionService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void AddToSession(string key, object value)
        {
            _session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public T GetSessionValue<T>(string key)
        {
            if (_httpContextAccessor.HttpContext == null || _session == null)
            {
                return default(T);
            }

            var value = _session.GetString(key);
            if (value == null)
            {
                return default(T);
            }

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        public void Clear()
        {
            _session.Clear();
        }
    }
}
