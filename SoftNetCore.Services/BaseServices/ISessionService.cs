﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Services.BaseServices
{
    public interface ISessionService
    {
        T GetSessionValue<T>(string sessionKey);

        void AddToSession(string key, object value);

        void Clear();
    }
}
