﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Services.BaseServices
{
    public interface IBaseService<T> where T : class
    {
        Task<ProcessingResult> AddAsync(T model);
        Task<ProcessingResult> UpdateAsync(T model);
        Task<ProcessingResult> DeleteAsync(int id);
        Task<List<T>> GetAllAsync();
        Task<T> FindById(int id);
        Task<Pager> PaginationAsync(ParamaterPagination paramater);
    }
}
