﻿using AutoMapper;
using SoftNetCore.Data.Entities;
using SoftNetCore.Model.Catalog;

namespace SoftNetCore.Services.AutoMapper
{
    public class ModelToObjectMappingProfile : Profile
    {
        public ModelToObjectMappingProfile()
        {
            CreateMap<AboutModel, About>();
            CreateMap<AccountCouponModel, AccountCoupon>();
            CreateMap<AccountModel, Account>();
            CreateMap<AccountTypeModel, AccountType>();
            CreateMap<AchievementModel, Achievement>();
            CreateMap<ArticleModel, Article>();
            CreateMap<ArticleCategoryModel, ArticleCategory>();
            CreateMap<PromotionModel, Promotion>();
            CreateMap<PromotionCategoryModel, PromotionCategory>();
            CreateMap<AssessModel, Assess>();
            CreateMap<AttributeModel, Attribute>();
            CreateMap<BannerModel, Banner>();
            CreateMap<BranchModel, Branch>();
            CreateMap<CollectionModel, Collection>();
            CreateMap<CollectionCategoryModel, CollectionCategory>();
            CreateMap<ContactModel, Contact>();
            CreateMap<CouponModel, Coupon>();
            CreateMap<CouponTypeModel, CouponType>();
            CreateMap<CustomerModel, Customer>();
            CreateMap<CustomerTypeModel, CustomerType>();
            CreateMap<DistrictModel, District>();
            CreateMap<FaqModel, Faq>();
            CreateMap<FeedbackModel, Feedback>();
            CreateMap<GalleryCategoryModel, GalleryCategory>();
            CreateMap<GalleryModel, Gallery>();
            CreateMap<OrderModel, Order>();
            CreateMap<OrderDetailModel, OrderDetail>();
            CreateMap<OrderStatusModel, OrderStatus>();
            CreateMap<PartnerModel, Partner>();
            CreateMap<PayTypeModel, PayType>();
            CreateMap<PositionBannerModel, PositionBanner>();
            CreateMap<ProductModel, Product>();
            CreateMap<ProductAttributeModel, ProductAttribute>();
            CreateMap<ProductCategoryModel, ProductCategory>();
            CreateMap<ProductCollectionModel, ProductCollection>();
            CreateMap<ProductWishlistModel, ProductWishlist>();
            CreateMap<ProvinceModel, Province>();
            CreateMap<SaleModel, Sale>();
            CreateMap<ServiceModel, Service>();
            CreateMap<ServiceCategoryModel, ServiceCategory>();
            CreateMap<ShippingFeeModel, ShippingFee>();
            CreateMap<SliderModel, Slider>();
            CreateMap<SubscribeModel, Subscribe>();
            CreateMap<VideoCategoryModel, VideoCategory>();
            CreateMap<VideoModel, Video>();
            CreateMap<MerchantProductByCategoryDto, MerchantProductByCategory>();
            CreateMap<ConfigSystemModel, ConfigSystem>();
        }
    }
}
