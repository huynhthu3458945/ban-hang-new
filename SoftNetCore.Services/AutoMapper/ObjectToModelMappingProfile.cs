﻿using AutoMapper;
using SoftNetCore.Data.Entities;
using SoftNetCore.Model.Catalog;
using System;

namespace SoftNetCore.Services.AutoMapper
{
    public class ObjectToModelMappingProfile : Profile
    {
        public ObjectToModelMappingProfile()
        {
            CreateMap<About, AboutModel>();
            CreateMap<AccountCoupon, AccountCouponModel>();
            CreateMap<Account, AccountModel>();
            CreateMap<AccountType, AccountTypeModel>();
            CreateMap<Achievement, AchievementModel>();
            CreateMap<Article, ArticleModel>();
            CreateMap<ArticleCategory, ArticleCategoryModel>();
            CreateMap<Promotion, PromotionModel>();
            CreateMap<PromotionCategory, PromotionCategoryModel>();
            CreateMap<Assess, AssessModel>();
            CreateMap<Data.Entities.Attribute, AttributeModel>();
            CreateMap<Banner, BannerModel>();
            CreateMap<Branch, BranchModel>();
            CreateMap<Collection, CollectionModel>();
            CreateMap<CollectionCategory, CollectionCategoryModel>();
            CreateMap<Contact, ContactModel>();
            CreateMap<Coupon, CouponModel>();
            CreateMap<CouponType, CouponTypeModel>();
            CreateMap<Customer, CustomerModel>().ForMember(d => d.UserName, o => o.MapFrom(x => x.Account.UserName));
            CreateMap<CustomerType, CustomerTypeModel>();
            CreateMap<District, DistrictModel>();
            CreateMap<Faq, FaqModel>();
            CreateMap<Feedback, FeedbackModel>();
            CreateMap<GalleryCategory, GalleryCategoryModel>();
            CreateMap<Gallery, GalleryModel>();
            CreateMap<Order, OrderModel>();
            CreateMap<OrderDetail, OrderDetailModel>();
            CreateMap<OrderStatus, OrderStatusModel>();
            CreateMap<Partner, PartnerModel>();
            CreateMap<PayType, PayTypeModel>();
            CreateMap<PositionBanner, PositionBannerModel>();
            // Lấy theo giá giảm nhập sẵn
            CreateMap<Product, ProductModel>();
            //.ForMember(d => d.IsSale, o => o.MapFrom(x => (x.SaleId != null && x.SaleNavigation.TimeStart <= DateTime.UtcNow && x.SaleNavigation.TimeEnd >= DateTime.UtcNow) || (x.Sale > 0 && x.SaleDeadLine >= DateTime.UtcNow) ? true : false))
            //.ForMember(d => d.PriceSale, o => o.MapFrom(x => (x.SaleId != null && x.SaleNavigation.TimeStart <= DateTime.UtcNow && x.SaleNavigation.TimeEnd >= DateTime.UtcNow) || (x.Sale > 0 && x.SaleDeadLine >= DateTime.UtcNow) ? (x.PricePromo > 0 ? x.PricePromo : x.Price) : x.Price))
            //.ForMember(d => d.Discount, o => o.MapFrom(x => x.SaleId != null ? x.SaleNavigation.PercentSale : x.Sale));
            CreateMap<ProductAttribute, ProductAttributeModel>();
                //.ForMember(d => d.IsSale, o => o.MapFrom(x => (x.Product.SaleId != null && x.Product.SaleNavigation.TimeStart <= DateTime.UtcNow && x.Product.SaleNavigation.TimeEnd >= DateTime.UtcNow) || (x.Product.Sale > 0 && x.Product.SaleDeadLine >= DateTime.UtcNow) ? true : false))
                //.ForMember(d => d.PriceSale, o => o.MapFrom(x => (x.Product.SaleId != null && x.Product.SaleNavigation.TimeStart <= DateTime.UtcNow && x.Product.SaleNavigation.TimeEnd >= DateTime.UtcNow) || (x.Product.Sale > 0 && x.Product.SaleDeadLine >= DateTime.UtcNow) ? (x.PricePromo > 0 ? x.PricePromo : x.Price) : x.Price))
                //.ForMember(d => d.Discount, o => o.MapFrom(x => x.Product.SaleId != null ? x.Product.SaleNavigation.PercentSale : x.Product.Sale));

            // Tự tính theo %
            //CreateMap<Product, ProductModel>()
            //    .ForMember(d => d.IsSale, o => o.MapFrom(x => (x.SaleId != null && x.SaleNavigation.TimeStart <= DateTime.UtcNow && x.SaleNavigation.TimeEnd >= DateTime.UtcNow) || (x.Sale > 0 && x.SaleDeadLine >= DateTime.UtcNow) ? true : false))
            //    .ForMember(d => d.PriceSale, o => o.MapFrom(x => (x.SaleId != null && x.SaleNavigation.TimeStart <= DateTime.UtcNow && x.SaleNavigation.TimeEnd >= DateTime.UtcNow) || (x.Sale > 0 && x.SaleDeadLine >= DateTime.UtcNow) ? x.SaleId != null ? (x.Price * (100 - x.SaleNavigation.PercentSale)) / 100 : (x.Price * (100 - x.Sale)) / 100 : x.Price))
            //    .ForMember(d => d.Discount, o => o.MapFrom(x => x.SaleId != null ? x.SaleNavigation.PercentSale : x.Sale));
            //CreateMap<ProductAttribute, ProductAttributeModel>()
            //    .ForMember(d => d.IsSale, o => o.MapFrom(x => (x.Product.SaleId != null && x.Product.SaleNavigation.TimeStart <= DateTime.UtcNow && x.Product.SaleNavigation.TimeEnd >= DateTime.UtcNow) || (x.Product.Sale > 0 && x.Product.SaleDeadLine >= DateTime.UtcNow) ? true : false))
            //    .ForMember(d => d.PriceSale, o => o.MapFrom(x => (x.Product.SaleId != null && x.Product.SaleNavigation.TimeStart <= DateTime.UtcNow && x.Product.SaleNavigation.TimeEnd >= DateTime.UtcNow) || (x.Product.Sale > 0 && x.Product.SaleDeadLine >= DateTime.UtcNow) ? x.Product.SaleId != null ? (x.Price * (100 - x.Product.SaleNavigation.PercentSale)) / 100 : (x.Price * (100 - x.Product.Sale)) / 100 : x.Price))
            //    .ForMember(d => d.Discount, o => o.MapFrom(x => x.Product.SaleId != null ? x.Product.SaleNavigation.PercentSale : x.Product.Sale));

            CreateMap<ProductCategory, ProductCategoryModel>();
            CreateMap<ProductCollection, ProductCollectionModel>();
            CreateMap<ProductWishlist, ProductWishlistModel>();
            CreateMap<Province, ProvinceModel>();
            CreateMap<Sale, SaleModel>();
            CreateMap<Service, ServiceModel>();
            CreateMap<ServiceCategory, ServiceCategoryModel>();
            CreateMap<ShippingFee, ShippingFeeModel>();
            CreateMap<Slider, SliderModel>();
            CreateMap<Subscribe, SubscribeModel>();
            CreateMap<VideoCategory, VideoCategoryModel>();
            CreateMap<Video, VideoModel>();

            CreateMap<MerchantProductByCategory, MerchantProductByCategoryDto>();
            CreateMap<MerchantSetting, MerchantSettingModel>().ReverseMap();

            CreateMap<ConfigSystem, ConfigSystemModel>();
        }
    }
}
