﻿using System;

namespace SoftNetCore.Services.Loggings
{
    public partial interface ILogging
    {
        /// <summary>
        /// LogException
        /// </summary>
        /// <param name="exception"></param>
        void LogException(Exception exception);

        /// <summary>
        /// create log exception
        /// </summary>
        /// <param name="exception">Error message</param>
        /// <param name="param">param value</param>
        void LogException(Exception exception, object param);

        void Log(string message);
    }
}
