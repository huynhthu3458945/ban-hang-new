﻿using System;
using Serilog;
using Serilog.Events;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Services.Loggings
{
    public partial class Logging : ILogging
    {
        private readonly IWebHelper _webHelper;

        public Logging(IWebHelper webHelper)
        {
            _webHelper = webHelper;
        }

        public void LogException(Exception exception)
        {
            string url = _webHelper.GetThisPageUrl(true);
            var logger = new LoggerConfiguration()
                         .MinimumLevel.Error()
                         .WriteTo.File("Logs/Exception/.txt",
                         LogEventLevel.Error, // Minimum Log level
                                 rollingInterval: RollingInterval.Day, // This will append time period to the filename like 20180316.txt
                                 retainedFileCountLimit: null,
                                 fileSizeLimitBytes: null,
                                 outputTemplate: "DateTime: {Timestamp:yyyy-MM-dd HH:mm:ss}{NewLine}{Message}{NewLine}Exception: {Exception}{NewLine}",  // Set custom file format
                                 shared: true // Shared between multi-process shared log files
                                 )
                         .CreateLogger();

            logger.Error(exception, "Url: {url}\r\n", url);
        }

        public void LogException(Exception exception, object param)
        {
            string url = _webHelper.GetThisPageUrl(true);
            var logger = new LoggerConfiguration()
                         .MinimumLevel.Error()
                         .WriteTo.File("Logs/Exception/.txt",
                         LogEventLevel.Error, // Minimum Log level
                                 rollingInterval: RollingInterval.Day, // This will append time period to the filename like 20180316.txt
                                 retainedFileCountLimit: null,
                                 fileSizeLimitBytes: null,
                                 outputTemplate: "DateTime: {Timestamp:yyyy-MM-dd HH:mm:ss}{NewLine}{Message}{NewLine}Exception: {Exception}{NewLine}",  // Set custom file format
                                 shared: true // Shared between multi-process shared log files
                                 )
                         .CreateLogger();

            logger.Error(exception, "Url: {url}\r\nParam: {@param}", url, param);
        }

        public void Log(string message)
        {
            var logger = new LoggerConfiguration()
                         .MinimumLevel.Error()
                         .WriteTo.File("Logs/Exception/.txt",
                         LogEventLevel.Error, // Minimum Log level
                                 rollingInterval: RollingInterval.Day, // This will append time period to the filename like 20180316.txt
                                 retainedFileCountLimit: null,
                                 fileSizeLimitBytes: null,
                                 outputTemplate: "DateTime: {Timestamp:yyyy-MM-dd HH:mm:ss}{NewLine}{Message}{NewLine}Exception: {Exception}{NewLine}",  // Set custom file format
                                 shared: true
                                 )
                         .CreateLogger();


            logger.Error($"Log: {message}");
        }

    }
}
