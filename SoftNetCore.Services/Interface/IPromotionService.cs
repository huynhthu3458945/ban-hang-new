﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IPromotionService : IBaseService<PromotionModel>
    {
        Task<List<Promotion>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<PromotionModel>> GetByCategoryAsync(int catId);
        Task<List<PromotionModel>> GetByStatusAsync(bool status);
        Task<List<PromotionModel>> GetRelatedAsync(int id, int catId, int numberItem);
        Task<List<PromotionModel>> GetNewAsync(int numberItem);
        Task<List<PromotionModel>> GetHotAsync(int numberItem);
        Task<List<PromotionModel>> GetHotAsync(int numberItem, int catId);
        Task<List<PromotionModel>> GetRandomAsync(int numberItem);
        Task<List<PromotionModel>> GetTopAsync(int numberItem);
        Task<ProcessingResult> UpdateViewAsync(int id);
    }
}
