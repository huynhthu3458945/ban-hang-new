﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Services.Interface
{
    public interface ISubscribeService : IBaseService<SubscribeModel>
    {
    }
}
