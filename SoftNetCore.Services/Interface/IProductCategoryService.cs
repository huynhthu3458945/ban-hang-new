﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IProductCategoryService : IBaseService<ProductCategoryModel>
    {
        Task<List<ProductCategoryModel>> GetDataAsync();
        Task<List<ProductCategoryModel>> GetMenuAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<ProductCategoryModel>> GetAllGroupAsync(int? parentId);
        Task<List<ProductCategoryModel>> GetParentAsync(bool isParent, bool status);
        Task<List<ProductCategoryModel>> GetByStatusAsync(bool status);
        Task<List<ProductCategoryModel>> GetTopAsync(int numberItem);

        Task<List<ProductAndTotalCategoryModel>> GetAllTotalCategoryAsync();

        /// <summary>
        /// Kéo dữ liệu từ domain
        /// </summary>
        /// <param name="domainId"></param>
        /// <returns></returns>
        Task<bool> FetchProductCategoryAsync(string domainId);

        Task<ProcessingResult> DeleteItemAsync(int id);

        Task<List<ProductCategoryModel>> GetAllActivatedProductCategoryAsync();

        Task<List<ProductCategoryModel>> GetParentAsync();
    }
}
