﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IProductAttributeService : IBaseService<ProductAttributeModel>
    {
        Task<List<ProductAttributeModel>> GetByParentId(int parentId);
        Task<ProductAttributeModel> FindByReferenceId(int productId, int attributeId);
        Task<ProductAttributeModel> FindByReferenceId(int productId, int colorId, int sizeId);
        Task<ProcessingResult> DeleteByReferenceIdAsync(int productId, int attributeId);
        Task<ProcessingResult> DeleteByReferenceIdAsync(int productId, int colorId, int sizeId);
        Task<ProcessingResult> DeleteByProductIdAsync(int productId);
        Task<List<ProductAttribute>> GetByProductId(int productId, bool isParent);
        Task<List<ProductAttributeModel>> GetAllByProductId(int productId, bool isParent);
    }
}
