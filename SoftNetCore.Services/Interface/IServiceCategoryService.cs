﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IServiceCategoryService : IBaseService<ServiceCategoryModel>
    {
        Task<List<ServiceCategory>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<ServiceCategoryModel>> GetAllGroupAsync(int parentId);
        Task<List<ServiceCategoryModel>> GetByStatusAsync(bool status);
        Task<List<ServiceCategoryModel>> GetTopAsync(int numberItem);
    }
}
