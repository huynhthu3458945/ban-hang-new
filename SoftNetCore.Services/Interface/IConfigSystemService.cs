﻿using System.Threading.Tasks;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;

namespace SoftNetCore.Services.Interface
{
    public interface IConfigSystemService : IBaseService<ConfigSystemModel>
    {

    }
}
