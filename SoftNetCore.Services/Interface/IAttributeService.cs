﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Data.Entities;
using SoftNetCore.Model.Request;

namespace SoftNetCore.Services.Interface
{
    public interface IAttributeService : IBaseService<AttributeModel>
    {
        Attribute GetById(int id);
        Task<List<Attribute>> GetDataAsync();
        Task<List<AttributeModel>> GetByTypeAsync(int typeId);
        List<AttributeTypeRequest> GetTypeAllAsync();
    }
}
