﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using System.Linq;

namespace SoftNetCore.Services.Interface
{
    public interface IProductService : IBaseService<ProductModel>
    {
        Task<List<Product>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<ProductModel>> GetByCategoryAsync(int catId);
        Task<List<ProductModel>> GetSortByCategoryAsync(int catId, int sort);
        Task<List<ProductModel>> GetBySaleAsync(int saleId);
        Task<List<ProductModel>> GetByCategoryAsync(int catId, int numberItem);
        Task<List<ProductModel>> GetByStatusAsync(bool status);
        Task<Pager> SearchPaginationAsync(SearchPagination paramater);
        Task<Pager> Search2PaginationAsync(Search2Pagination paramater);
        Task<Pager> FilterPaginationAsync(FilterPagination paramater);
        Task<List<ProductModel>> GetRelatedAsync(int id, int catId, int numberItem);
        Task<List<ProductModel>> GetNewAsync(int numberItem);
        Task<List<ProductModel>> GetHotAsync(int numberItem);
        Task<List<ProductModel>> GetRandomAsync(int numberItem);
        Task<List<ProductModel>> GetTopAsync(int numberItem);
        Task<List<ProductModel>> GetSaleAsync(int numberItem);
        Task<ProcessingResult> UpdateViewAsync(int id);
        Task<List<ProductModel>> SearchAsync(string keyword);
        Product GetById(int id);

        Task<ProcessingResult> UpdateSaleAsync(int id, int? saleId);

        Task<ProcessingResult> UpdateSaleRangeAsync(List<ProductItem> list, int? saleId);

        Task<ProcessingResult> UpdateSaleRange2Async(List<ProductModel> list, int? saleId);

        Task<ProcessingResult> UpdateRatingAsync(int id, int? total, decimal? value);

        Task<ProcessingResult> FetchProductFromDimuadi(int productCategoryId, string token, int pageNumber, int pageSize);

        Task UpdateDimuadiProducts(string bearerToken);

        Task<bool> GetAccessTradeProductsAsync(string domain, string token, int page, int pageSize);
        Task<int> GetTotalProductByCategory(int categoryId);

        /// <summary>
        /// Tự động cập nhật theo danh mục đã được định nghĩa tại ProductCategory table
        /// </summary>
        /// <param name="productCategoryId"></param>
        /// <returns></returns>
        Task FetchProductByCategoryIdAsync(int productCategoryId);

        /// <summary>
        /// Kéo sp từ accesstreade
        /// </summary>
        /// <param name="url"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="tokenValue"></param>
        /// <returns></returns>
        Task FetchAccesstradeData(string url, int page, int pageSize, string tokenValue);

        IQueryable<ProductModel> GetProductsByCategoryId(int productCateforyId, int sort);

        IQueryable<ProductModel> GetAll();

        Task<List<ProductModel>> GetAllProductAsync();

        int GetTotalByProductCategory(int? catId, bool status);

        int GetTotalByBanner(int? bannerId, bool status);
    }
}
