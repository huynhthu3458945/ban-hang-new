﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IVideoService : IBaseService<VideoModel>
    {
        Task<List<Video>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<VideoModel>> GetByCategoryAsync(int catId);
        Task<List<VideoModel>> GetByStatusAsync(bool status);
        Task<List<VideoModel>> GetRelatedAsync(int id, int catId, int numberItem);
        Task<List<VideoModel>> GetNewAsync(int numberItem);
        Task<List<VideoModel>> GetHotAsync(int numberItem);
        Task<List<VideoModel>> GetRandomAsync(int numberItem);
        Task<List<VideoModel>> GetTopAsync(int numberItem);
        Task<ProcessingResult> UpdateViewAsync(int id);
    }
}
