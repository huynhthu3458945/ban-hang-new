﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IArticleService : IBaseService<ArticleModel>
    {
        Task<List<Article>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<ArticleModel>> GetByCategoryAsync(int catId);
        Task<List<ArticleModel>> GetByStatusAsync(bool status);
        Task<List<ArticleModel>> GetRelatedAsync(int id, int catId, int numberItem);
        Task<List<ArticleModel>> GetNewAsync(int numberItem);
        Task<List<ArticleModel>> GetHotAsync(int numberItem);
        Task<List<ArticleModel>> GetHotAsync(int numberItem, int catId);
        Task<List<ArticleModel>> GetRandomAsync(int numberItem);
        Task<List<ArticleModel>> GetTopAsync(int numberItem);
        Task<ProcessingResult> UpdateViewAsync(int id);
    }
}
