﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IDistrictService : IBaseService<DistrictModel>
    {
        Task<List<DistrictModel>> GetByBranchExists();

        List<District> GetByBranchExistsAndProvinceId(int provinceId);

        List<District> GetByProvinceId(int provinceId);

        Task<List<DistrictModel>> GetByProvinceAsync(int provinceId);
    }
}
