﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface ICouponService : IBaseService<CouponModel>
    {
        Task<List<Coupon>> GetDataAsync();
        Task<List<CouponModel>> GetByUseAsync(bool status);
        Task<List<CouponModel>> GetByStatusAsync(int? typeId, int status);
        int GetTotalByCouponType(int? typeId, int status);
        Task<ProcessingResult> UpdateUseAsync(int id);

        Task<ProcessingResult> FetchAccesstradeCouponAsync(string bearerToken, int page, int limit);

        Task UpdateCouponTypesAsync();

        Task<bool> UpdateCouponTypeForCouponsAsync();
    }
}
