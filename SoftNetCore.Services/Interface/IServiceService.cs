﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IServiceService : IBaseService<ServiceModel>
    {
        Task<List<Service>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<ServiceModel>> GetByCategoryAsync(int catId);
        Task<List<ServiceModel>> GetByStatusAsync(bool status);
        Task<List<ServiceModel>> GetRelatedAsync(int id, int catId, int numberItem);
        Task<List<ServiceModel>> GetNewAsync(int numberItem);
        Task<List<ServiceModel>> GetRandomAsync(int numberItem);
        Task<List<ServiceModel>> GetTopAsync(int numberItem);
        Task<ProcessingResult> UpdateViewAsync(int id);
    }
}
