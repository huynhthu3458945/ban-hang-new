﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Services.Interface
{
    public interface ICustomerTypeService : IBaseService<CustomerTypeModel>
    {
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<CustomerTypeModel>> GetByStatusAsync(bool status);
    }
}
