﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Data.Entities;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Services.Interface
{
    public interface IBannerService : IBaseService<BannerModel>
    {
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<Banner>> GetDataAsync();
        Task<List<BannerModel>> GetNewAsync(int numberItem);
        Task<bool> FetchBannerFromDimuadi(string token);
    }
}
