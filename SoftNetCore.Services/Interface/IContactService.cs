﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IContactService : IBaseService<ContactModel>
    {
        Task<List<Contact>> GetDataAsync();
        Task<List<ContactModel>> GetByStatusAsync(bool status);
        Task<ProcessingResult> UpdateStatusAsync(int id);
    }
}
