﻿using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.BaseServices;

namespace SoftNetCore.Services.Interface
{
    public interface IAchievementService : IBaseService<AchievementModel>
    {
    }
}
