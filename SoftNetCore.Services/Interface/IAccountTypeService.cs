﻿using System;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;

namespace SoftNetCore.Services.Interface
{
    public interface IAccountTypeService : IBaseService<AccountTypeModel>
    {

    }
}
