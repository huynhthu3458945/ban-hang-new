﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Data.Entities;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Services.Interface
{
    public interface IAssessService : IBaseService<AssessModel>
    {
        Task<Pager2> GetAllByKeyPaginationAsync(KeyPagination paramater, string keyName);
        Task<List<AssessModel>> GetAllByKey(int keyId, string keyName);
        Task<decimal[]> GetMathByKeyAsync(int keyId, string keyName);
        Pager GetAllByAccountPagination(ParamaterPagination paramater, string keyName, int accountId);

        Task<ProcessingResult> AddRatingAsync(List<AssessModel> list);
    }
}
