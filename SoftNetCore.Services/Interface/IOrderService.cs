﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Model.Request;

namespace SoftNetCore.Services.Interface
{
    public interface IOrderService : IBaseService<OrderModel>
    {
        Task<ProcessingResult> UpdateRatingAsync(RatingRequest model);
        Task<ProcessingResult> SaveOrderAsync(OrderModel order, List<CartItem> listCart, CustomerModel customer);
        Task<List<Order>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id, int status);
        Task<ProcessingResult> UpdateStatusAsync(StatusOrderRequest model);
        Task<List<OrderModel>> GetByStatusAsync(int status);
        Task<List<OrderModel>> GetByCustomerAsync(int customerId);
        Task<OrderModel> FindFullByIdAsync(int id);

    }
}
