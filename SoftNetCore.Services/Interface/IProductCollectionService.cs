﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Services.Interface
{
    public interface IProductCollectionService : IBaseService<ProductCollectionModel>
    {
        Task<ProductCollectionModel> FindByReferenceId(int collectionId, int productId);
        Task<ProcessingResult> DeleteByReferenceIdAsync(int collectionId, int productId);
        Task<ProcessingResult> DeleteByCollectionAsync(int collectionId);
        Task<List<ProductCollectionModel>> GetByCollectionAsync(int collectionId);
    }
}
