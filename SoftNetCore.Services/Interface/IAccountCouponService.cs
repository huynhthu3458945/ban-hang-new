﻿using System;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Common.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace SoftNetCore.Services.Interface
{
    public interface IAccountCouponService : IBaseService<AccountCouponModel>
    {
        Task<AccountCouponModel> FindByReferenceId(int accountId, int couponId);
        Task<ProcessingResult> DeleteByReferenceIdAsync(int accountId, int couponId);
        Task<ProcessingResult> DeleteByAccountAsync(int accountId);
        Task<List<AccountCouponModel>> GetByAccountAsync(int accountId);
    }
}
