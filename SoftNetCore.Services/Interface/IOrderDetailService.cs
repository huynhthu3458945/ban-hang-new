﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IOrderDetailService : IBaseService<OrderDetailModel>
    {
        List<OrderDetail> GetByOrder(int orderId);
        Task<List<OrderDetailModel>> GetByOrderAsync(int orderId);
        Task<List<OrderDetailModel>> GetByOrderFullAsync(int orderId);
        Task<OrderDetailModel> FindByReferenceId(int orderId, int productId);
        Task<ProcessingResult> DeleteByOrderAsync(int orderId);
        Task<ProcessingResult> DeleteByReferenceIdAsync(int orderId, int productId);
    }
}
