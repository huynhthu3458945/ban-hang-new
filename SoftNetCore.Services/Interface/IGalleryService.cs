﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Services.Interface
{
    public interface IGalleryService : IBaseService<GalleryModel>
    {
        Task<ProcessingResult> UpdateStatusAsync(int id);
        //Task<List<GalleryModel>> GetByCategoryAsync(int catId);
        Task<List<GalleryModel>> GetByStatusAsync(bool status);
        //Task<List<GalleryModel>> GetRelatedAsync(int id, int catID, int numberItem);
        Task<List<GalleryModel>> GetNewAsync(int numberItem);
        //Task<List<GalleryModel>> GetHotAsync(int numberItem);
        Task<List<GalleryModel>> GetRandomAsync(int numberItem);
        Task<List<GalleryModel>> GetTopAsync(int numberItem);
        //Task<ProcessingResult> UpdateViewAsync(int id);
    }
}
