﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Services.Interface
{
    public interface IProductWishlistService : IBaseService<ProductWishlistModel>
    {
        Task<ProductWishlistModel> FindByReferenceId(int customerId, int productId);
        Task<ProcessingResult> DeleteByReferenceIdAsync(int customerId, int productId);
        Task<ProcessingResult> DeleteByCustomerAsync(int customerId);
        Task<List<ProductWishlistModel>> GetByCustomerAsync(int customerId);
    }
}
