﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Model.Request;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IAccountService : IBaseService<AccountModel>
    {
        Task<List<Account>> GetDataAsync(int typeId);
        Task<AuthenticateAccountResponse> Authenticate(AuthenticateRequest model);
        Task<AccountModel> GetByCondition(string value);
        Task<ProcessingResult> ForgotPassword(ForgotPasswordRequest model, string origin);
        Task<ProcessingResult> ChangePassword(ChangePasswordRequest request);
        Task<ProcessingResult> Lock(int id);
    }
}
