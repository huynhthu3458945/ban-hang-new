﻿using System.Threading.Tasks;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;

namespace SoftNetCore.Services.Interface
{
    public interface IAboutService : IBaseService<AboutModel>
    {
        Task<AboutModel> GetFisrtAsync(int id);
    }
}
