﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Model.Request;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface ICustomerService : IBaseService<CustomerModel>
    {
        Task<List<Customer>> GetDataAsync();
        Task<AuthenticateCustomerResponse> Authenticate(AuthenticateRequest model);
        Task<CustomerModel> GetByCondition(string value);
        Task<ProcessingResult> ForgotPassword(ForgotPasswordRequest model, string origin);
        Task<ProcessingResult> ChangePassword(ChangePasswordRequest request);
        Task<ProcessingResult> Lock(int id);
        Task<CustomerModel> FindByIdHaveOrder(int customerId);

    }
}
