﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using SoftNetCore.Data.Enums;

namespace SoftNetCore.Services.Interface
{
    public interface IMerchantSettingService : IBaseService<MerchantSettingModel>
    {
        Task<MerchantSettingModel> FindByMerchantId(EnumDomains id);
    }
}
    