﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IProvinceService : IBaseService<ProvinceModel>
    {
        Task<List<Province>> GetDataAsync();
        Task<List<ProvinceModel>> GetByBranchExists();
    }
}
