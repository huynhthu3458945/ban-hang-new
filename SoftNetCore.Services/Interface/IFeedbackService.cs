﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IFeedbackService : IBaseService<FeedbackModel>
    {
        Task<List<Feedback>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<FeedbackModel>> GetByStatusAsync(bool status);
    }
}
