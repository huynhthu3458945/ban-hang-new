﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface ICollectionService : IBaseService<CollectionModel>
    {
        Task<List<Collection>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<CollectionModel>> GetByCategoryAsync(int catId);
        Task<List<CollectionModel>> GetByStatusAsync(bool status);
        Task<List<CollectionModel>> GetRelatedAsync(int id, int catId, int numberItem);
        Task<List<CollectionModel>> GetNewAsync(int numberItem);
        Task<List<CollectionModel>> GetRandomAsync(int numberItem);
        Task<List<CollectionModel>> GetTopAsync(int numberItem);
    }
}
