﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Services.Interface
{
    public interface IShippingFeeService : IBaseService<ShippingFeeModel>
    {
    }
}
