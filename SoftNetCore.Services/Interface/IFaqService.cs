﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IFaqService : IBaseService<FaqModel>
    {
        Task<List<Faq>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<FaqModel>> GetByStatusAsync(bool status);
    }
}
