﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Model.Request;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IBranchService : IBaseService<BranchModel>
    {
        Task<List<BranchModel>> GetRandomAsync(int numberItem);
        Task<List<BranchModel>> GetByAddressAsync(int? provinceId, int? districtId);
        Task<List<Branch>> GetDataAsync();
        Task<List<BranchModel>> GetByStatusAsync(bool status);
        Task<Pager> SearchPaginationAsync(BranchPagination paramater);
    }
}
