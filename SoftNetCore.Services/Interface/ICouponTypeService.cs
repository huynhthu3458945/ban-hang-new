﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface ICouponTypeService : IBaseService<CouponTypeModel>
    {
        Task<List<CouponTypeModel>> GetDataAsync();
        Task<List<CouponAndTotalTypeModel>> GetAllTotalTypeAsync();
    }
}
