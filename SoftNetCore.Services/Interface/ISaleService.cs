﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface ISaleService : IBaseService<SaleModel>
    {
        SaleModel GetById(int id);
        Task<List<Sale>> GetDataAsync();
        Task<List<SaleModel>> GetByStatusAsync(bool status);
        Task<List<SaleModel>> GetActiveAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
    }
}
