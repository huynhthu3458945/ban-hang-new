﻿using SoftNetCore.Services.BaseServices;
using SoftNetCore.Model.Catalog;
using System.Threading.Tasks;
using System.Collections.Generic;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Services.Interface
{
    public interface IArticleCategoryService : IBaseService<ArticleCategoryModel>
    {
        Task<List<ArticleCategory>> GetDataAsync();
        Task<ProcessingResult> UpdateStatusAsync(int id);
        Task<List<ArticleCategoryModel>> GetByStatusAsync(bool status);
    }
}
