﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;

namespace SoftNetCore.Services.UnitOfWork
{
    public interface IUnitOfWork
    {
        Task SaveChangesAsync();

        Task<IDbContextTransaction> BeginTransactionAsync();

        void SaveChanges();
    }
}
