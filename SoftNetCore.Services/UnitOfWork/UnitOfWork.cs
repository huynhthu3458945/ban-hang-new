﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;
using SoftNetCore.Data.Context;

namespace SoftNetCore.Services.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbFactory _dbFactory;
        public DbFactory DbFactory => _dbFactory;

        public UnitOfWork(DbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public Task SaveChangesAsync()
        {
            return _dbFactory.Context.SaveChangesAsync();
        }

        public Task<IDbContextTransaction> BeginTransactionAsync()
        {
            return _dbFactory.Context.Database.BeginTransactionAsync();
        }

        public void SaveChanges()
        {
            _dbFactory.Context.SaveChanges();
        }

    }
}
