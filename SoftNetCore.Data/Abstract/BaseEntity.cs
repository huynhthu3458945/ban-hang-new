﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SoftNetCore.Data.Abstract
{
    public abstract class BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateTime { get; set; }

        public string ModifyBy { get; set; }

        public DateTime? ModifyTime { get; set; }

        public bool? IsDelete { get; set; }
    }
}
