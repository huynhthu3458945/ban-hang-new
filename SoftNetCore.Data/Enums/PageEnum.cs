﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Data.Enums
{
    public static class PageEnum
    {
        public const int PageSize = 10;
        public const int MaxPagePagination = 4;
    }
}
