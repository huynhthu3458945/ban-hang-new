﻿
namespace SoftNetCore.Data.Enums
{
    public enum EnumDomains
    {
        DIMUADI,
        ACCESSTRADE
    }

    public static class EnumDomainsExtensions
    {
        public static string GetName(this EnumDomains enums) => enums switch
        {
            EnumDomains.DIMUADI => "DIMUADI",
            EnumDomains.ACCESSTRADE => "ACCESSTRADE",
            _ => string.Empty
        };

        public static int GetId(this EnumDomains enums) => enums switch
        {
            EnumDomains.DIMUADI => (int)EnumDomains.DIMUADI,
            EnumDomains.ACCESSTRADE => (int)EnumDomains.ACCESSTRADE,
            _ => 0
        };
    }
}
