﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SoftNetCore.Data.Migrations
{
    public partial class UpdateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PricePromo",
                table: "Product");

            migrationBuilder.AlterColumn<double>(
                name: "CommissionValue",
                table: "Product",
                type: "float",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountAmount",
                table: "Product",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "DiscountRate",
                table: "Product",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PriceAfterDiscount",
                table: "Product",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RootUrl",
                table: "Product",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdateTime",
                table: "Product",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "Banner",
                type: "int",
                nullable: true,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AvatarDetail",
                table: "Banner",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AvatarDetailMobile",
                table: "Banner",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AvatarHome",
                table: "Banner",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BannerId",
                table: "Banner",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Banner",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Position",
                table: "Banner",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TimeStart",
                table: "Banner",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimeStop",
                table: "Banner",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscountAmount",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "DiscountRate",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "PriceAfterDiscount",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "RootUrl",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "UpdateTime",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "AvatarDetail",
                table: "Banner");

            migrationBuilder.DropColumn(
                name: "AvatarDetailMobile",
                table: "Banner");

            migrationBuilder.DropColumn(
                name: "AvatarHome",
                table: "Banner");

            migrationBuilder.DropColumn(
                name: "BannerId",
                table: "Banner");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Banner");

            migrationBuilder.DropColumn(
                name: "Position",
                table: "Banner");

            migrationBuilder.DropColumn(
                name: "TimeStart",
                table: "Banner");

            migrationBuilder.DropColumn(
                name: "TimeStop",
                table: "Banner");

            migrationBuilder.AlterColumn<int>(
                name: "CommissionValue",
                table: "Product",
                type: "int",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AddColumn<decimal>(
                name: "PricePromo",
                table: "Product",
                type: "decimal(18,0)",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Status",
                table: "Banner",
                type: "bit",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
