﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SoftNetCore.Data.Migrations
{
    public partial class UpdateCouponType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CouponTypeId",
                table: "Coupon",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CouponTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MerchantName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CouponTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Coupon_CouponTypeId",
                table: "Coupon",
                column: "CouponTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Coupon_CouponTypes_CouponTypeId",
                table: "Coupon",
                column: "CouponTypeId",
                principalTable: "CouponTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coupon_CouponTypes_CouponTypeId",
                table: "Coupon");

            migrationBuilder.DropTable(
                name: "CouponTypes");

            migrationBuilder.DropIndex(
                name: "IX_Coupon_CouponTypeId",
                table: "Coupon");

            migrationBuilder.DropColumn(
                name: "CouponTypeId",
                table: "Coupon");
        }
    }
}
