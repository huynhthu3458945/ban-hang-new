﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SoftNetCore.Data.Migrations
{
    public partial class AddProductCategoryCodeIntoProductCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProductCategoryCode",
                table: "ProductCategory",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductCategoryCode",
                table: "ProductCategory");
        }
    }
}
