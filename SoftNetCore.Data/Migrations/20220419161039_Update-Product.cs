﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SoftNetCore.Data.Migrations
{
    public partial class UpdateProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Domain",
                table: "ProductCategory",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CommissionType",
                table: "Product",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CommissionValue",
                table: "Product",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ShipPrice",
                table: "Product",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Domain",
                table: "ProductCategory");

            migrationBuilder.DropColumn(
                name: "CommissionType",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "CommissionValue",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "ShipPrice",
                table: "Product");
        }
    }
}
