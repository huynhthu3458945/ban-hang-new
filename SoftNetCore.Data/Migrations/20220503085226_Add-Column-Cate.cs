﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SoftNetCore.Data.Migrations
{
    public partial class AddColumnCate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Cate",
                table: "Product",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cate",
                table: "Product");
        }
    }
}
