﻿using SoftNetCore.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Data.Context
{
    public class DbFactory : IDisposable
    {
        private bool _disposed;

        private readonly DBContext _dbContext;

        public DBContext Context => _dbContext;

        public DbFactory(DBContext dbContextFactory)
        {
            _dbContext = dbContextFactory;
        }

        public void Dispose()
        {
            if (!_disposed && _dbContext != null)
            {
                _disposed = true;
                _dbContext.Dispose();
            }
        }
    }
}
