﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SoftNetCore.Data.Abstract;
using SoftNetCore.Data.Services;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class DBContext : DbContext
    {
        private readonly IDateTimeService _dateTime;

        public DBContext() { }

        public DBContext(DbContextOptions<DBContext> options, IDateTimeService dateTimeService)
            : base(options)
        {
            _dateTime = dateTimeService;
        }

        public virtual DbSet<About> Abouts { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AccountApi> AccountApis { get; set; }
        public virtual DbSet<AccountCash> AccountCashes { get; set; }
        public virtual DbSet<AccountCashType> AccountCashTypes { get; set; }
        public virtual DbSet<AccountCoupon> AccountCoupons { get; set; }
        public virtual DbSet<AccountFunction> AccountFunctions { get; set; }
        public virtual DbSet<AccountGroupAccount> AccountGroupAccounts { get; set; }
        public virtual DbSet<AccountSystem> AccountSystems { get; set; }
        public virtual DbSet<AccountType> AccountTypes { get; set; }
        public virtual DbSet<Achievement> Achievements { get; set; }
        public virtual DbSet<AggregatedCounter> AggregatedCounters { get; set; }
        public virtual DbSet<Api> Apis { get; set; }
        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<ArticleCategory> ArticleCategories { get; set; }
        public virtual DbSet<Assess> Assesses { get; set; }
        public virtual DbSet<Attribute> Attributes { get; set; }
        public virtual DbSet<Banner> Banners { get; set; }
        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Cash> Cashes { get; set; }
        public virtual DbSet<CashGroup> CashGroups { get; set; }
        public virtual DbSet<CashObject> CashObjects { get; set; }
        public virtual DbSet<CashType> CashTypes { get; set; }
        public virtual DbSet<Collection> Collections { get; set; }
        public virtual DbSet<CollectionCategory> CollectionCategories { get; set; }
        public virtual DbSet<CompanySystem> CompanySystems { get; set; }
        public virtual DbSet<ConfigSystem> ConfigSystems { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Counter> Counters { get; set; }
        public virtual DbSet<Coupon> Coupons { get; set; }
        public virtual DbSet<CouponType> CouponTypes { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerType> CustomerTypes { get; set; }
        public virtual DbSet<Debt> Debts { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Faq> Faqs { get; set; }
        public virtual DbSet<Feedback> Feedbacks { get; set; }
        public virtual DbSet<FetchDataAudit> FetchDataAudits { get; set; }
        public virtual DbSet<Function> Functions { get; set; }
        public virtual DbSet<FunctionApi> FunctionApis { get; set; }
        public virtual DbSet<FunctionGroupFunction> FunctionGroupFunctions { get; set; }
        public virtual DbSet<Gallery> Galleries { get; set; }
        public virtual DbSet<GalleryCategory> GalleryCategories { get; set; }
        public virtual DbSet<GroupAccount> GroupAccounts { get; set; }
        public virtual DbSet<GroupFunction> GroupFunctions { get; set; }
        public virtual DbSet<Hash> Hashes { get; set; }
        public virtual DbSet<HistoryType> HistoryTypes { get; set; }
        public virtual DbSet<Introduction> Introductions { get; set; }
        public virtual DbSet<IntroductionType> IntroductionTypes { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobParameter> JobParameters { get; set; }
        public virtual DbSet<JobQueue> JobQueues { get; set; }
        public virtual DbSet<List> Lists { get; set; }
        public virtual DbSet<MerchantProductByCategory> MerchantProductByCategories { get; set; }
        public virtual DbSet<MerchantSetting> MerchantSettings { get; set; }
        public virtual DbSet<MiniService> MiniServices { get; set; }
        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<OrderStatus> OrderStatuses { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<Page> Pages { get; set; }
        public virtual DbSet<PageType> PageTypes { get; set; }
        public virtual DbSet<Partner> Partners { get; set; }
        public virtual DbSet<PartnerType> PartnerTypes { get; set; }
        public virtual DbSet<PayType> PayTypes { get; set; }
        public virtual DbSet<PositionBanner> PositionBanners { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductAttribute> ProductAttributes { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<ProductCollection> ProductCollections { get; set; }
        public virtual DbSet<ProductShelf> ProductShelves { get; set; }
        public virtual DbSet<ProductTab> ProductTabs { get; set; }
        public virtual DbSet<ProductWardrobe> ProductWardrobes { get; set; }
        public virtual DbSet<ProductWarehouse> ProductWarehouses { get; set; }
        public virtual DbSet<ProductWishlist> ProductWishlists { get; set; }
        public virtual DbSet<Promotion> Promotions { get; set; }
        public virtual DbSet<PromotionCategory> PromotionCategories { get; set; }
        public virtual DbSet<Province> Provinces { get; set; }
        public virtual DbSet<Recruitment> Recruitments { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
        public virtual DbSet<Schema> Schemas { get; set; }
        public virtual DbSet<Server> Servers { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<ServiceCategory> ServiceCategories { get; set; }
        public virtual DbSet<Set> Sets { get; set; }
        public virtual DbSet<Shelf> Shelves { get; set; }
        public virtual DbSet<ShippingFee> ShippingFees { get; set; }
        public virtual DbSet<Slider> Sliders { get; set; }
        public virtual DbSet<StaffDepartment> StaffDepartments { get; set; }
        public virtual DbSet<StaffType> StaffTypes { get; set; }
        public virtual DbSet<StaffWarehouse> StaffWarehouses { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Subscribe> Subscribes { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<Tab> Tabs { get; set; }
        public virtual DbSet<Transport> Transports { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<Video> Videos { get; set; }
        public virtual DbSet<VideoCategory> VideoCategories { get; set; }
        public virtual DbSet<Wardrobe> Wardrobes { get; set; }
        public virtual DbSet<WardrobeCategory> WardrobeCategories { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<WarehouseHistory> WarehouseHistories { get; set; }
        public virtual DbSet<WarehouseHistoryDetail> WarehouseHistoryDetails { get; set; }
        public virtual DbSet<staff> staff { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("DBConnection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<About>(
                entity =>
                {
                    entity.ToTable("About");

                    entity.Property(e => e.Address).HasMaxLength(300);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Blog).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Email).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.Facebook).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.Fax).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.Hotline).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.Instagram).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.Linkedin).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.Logo).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.PageId).HasMaxLength(255);

                    entity.Property(e => e.MetaImage).HasMaxLength(1024);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Phone).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.Pinterest).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.ShortContent).HasColumnType("ntext");

                    entity.Property(e => e.Map).HasColumnType("ntext");

                    entity.Property(e => e.Description).HasMaxLength(1024);

                    entity.Property(e => e.Tel).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.Twitter).HasMaxLength(1024);

                    entity.Property(e => e.Website).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.Youtube).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.Zalo).HasMaxLength(255).IsUnicode(false);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Abouts)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_About_Account");
                }
            );

            modelBuilder.Entity<Account>(
                entity =>
                {
                    entity.ToTable("Account");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Email).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Password).HasMaxLength(500).IsUnicode(false);

                    entity.Property(e => e.PasswordResetTime).HasColumnType("datetime");

                    entity.Property(e => e.ResetToken).HasMaxLength(1024);

                    entity.Property(e => e.ResetTokenExpiresTime).HasColumnType("datetime");

                    entity.Property(e => e.UserName).HasMaxLength(100);

                    entity.Property(e => e.VerificationToken).HasMaxLength(1024);

                    entity.Property(e => e.VerificationTokenExpiresTime).HasColumnType("datetime");

                    entity.Property(e => e.VerifiedTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.AccountType)
                        .WithMany(p => p.Accounts)
                        .HasForeignKey(d => d.AccountTypeId)
                        .HasConstraintName("FK_Account_AccountType");
                }
            );

            modelBuilder.Entity<AccountApi>(
                entity =>
                {
                    entity.HasNoKey();

                    entity.ToTable("AccountApi");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Account)
                        .WithMany()
                        .HasForeignKey(d => d.AccountId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_AccountApi_Account");

                    entity
                        .HasOne(d => d.Api)
                        .WithMany()
                        .HasForeignKey(d => d.ApiId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_AccountApi_Api");
                }
            );

            modelBuilder.Entity<AccountCash>(
                entity =>
                {
                    entity.ToTable("AccountCash");

                    entity.Property(e => e.BalanceValue).HasColumnType("decimal(18, 2)");

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<AccountCashType>(
                entity =>
                {
                    entity.ToTable("AccountCashType");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<AccountCoupon>(
                entity =>
                {
                    entity.HasKey(e => new { e.AccountId, e.CouponId });

                    entity.ToTable("AccountCoupon");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Account)
                        .WithMany(p => p.AccountCoupons)
                        .HasForeignKey(d => d.AccountId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_AccountCoupon_AccountCoupon");

                    entity
                        .HasOne(d => d.Coupon)
                        .WithMany(p => p.AccountCoupons)
                        .HasForeignKey(d => d.CouponId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_AccountCoupon_Coupon");
                }
            );

            modelBuilder.Entity<AccountFunction>(
                entity =>
                {
                    entity.HasKey(e => new { e.FunctionId, e.AccountId });

                    entity.ToTable("AccountFunction");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Account)
                        .WithMany(p => p.AccountFunctions)
                        .HasForeignKey(d => d.AccountId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_AccountFunction_Account");

                    entity
                        .HasOne(d => d.Function)
                        .WithMany(p => p.AccountFunctions)
                        .HasForeignKey(d => d.FunctionId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_AccountFunction_Function");
                }
            );

            modelBuilder.Entity<AccountGroupAccount>(
                entity =>
                {
                    entity.HasKey(e => new { e.AccountId, e.GroupAccountId });

                    entity.ToTable("AccountGroupAccount");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Account)
                        .WithMany(p => p.AccountGroupAccounts)
                        .HasForeignKey(d => d.AccountId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_AccountGroupAccount_Account");

                    entity
                        .HasOne(d => d.GroupAccount)
                        .WithMany(p => p.AccountGroupAccounts)
                        .HasForeignKey(d => d.GroupAccountId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_AccountGroupAccount_GroupAccount");
                }
            );

            modelBuilder.Entity<AccountSystem>(
                entity =>
                {
                    entity.ToTable("AccountSystem");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Password).HasMaxLength(500).IsUnicode(false);

                    entity.Property(e => e.UserName).HasMaxLength(100);
                }
            );

            modelBuilder.Entity<AccountType>(
                entity =>
                {
                    entity.ToTable("AccountType");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.Key).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Achievement>(
                entity =>
                {
                    entity.ToTable("Achievement");

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Achievements)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Achievement_Account");
                }
            );

            modelBuilder.Entity<Api>(
                entity =>
                {
                    entity.ToTable("Api");

                    entity.Property(e => e.Action).HasMaxLength(300);

                    entity.Property(e => e.Controller).HasMaxLength(300);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.Url).HasMaxLength(1024).IsUnicode(false);

                    entity
                        .HasOne(d => d.Module)
                        .WithMany(p => p.Apis)
                        .HasForeignKey(d => d.ModuleId)
                        .HasConstraintName("FK_Api_Module");
                }
            );

            modelBuilder.Entity<Article>(
                entity =>
                {
                    entity.ToTable("Article");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.SourceLink).HasMaxLength(1024);

                    entity.Property(e => e.SourcePage).HasMaxLength(1024);

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.ValueAssess).HasColumnType("decimal(5, 1)");

                    entity
                        .HasOne(d => d.ArticleCategory)
                        .WithMany(p => p.Articles)
                        .HasForeignKey(d => d.ArticleCategoryId)
                        .HasConstraintName("FK_Article_ArticleCategory");

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Articles)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Article_Account");
                }
            );

            modelBuilder.Entity<ArticleCategory>(
                entity =>
                {
                    entity.ToTable("ArticleCategory");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Assess>(
                entity =>
                {
                    entity.ToTable("Assess");

                    entity.Property(e => e.Content).HasMaxLength(500);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.FullName).HasMaxLength(300);

                    entity.Property(e => e.KeyName).HasMaxLength(500).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Note).HasMaxLength(500);

                    entity
                        .HasOne(d => d.Account)
                        .WithMany(p => p.Assesses)
                        .HasForeignKey(d => d.AccountId)
                        .HasConstraintName("FK_Assess_Account");
                }
            );

            modelBuilder.Entity<Attribute>(
                entity =>
                {
                    entity.ToTable("Attribute");

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Attributes)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Properties_Account");
                }
            );

            modelBuilder.Entity<Banner>(
                entity =>
                {
                    entity.ToTable("Banner");

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.Url).HasMaxLength(1024).IsUnicode(false);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Banners)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Banner_Account");

                    entity
                        .HasOne(d => d.PositionBanner)
                        .WithMany(p => p.Banners)
                        .HasForeignKey(d => d.PositionBannerId)
                        .HasConstraintName("FK_Banner_PositionBanner");
                }
            );

            modelBuilder.Entity<Branch>(
                entity =>
                {
                    entity.ToTable("Branch");

                    entity.Property(e => e.Address).HasMaxLength(250);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(1024);

                    entity.Property(e => e.MetaDescription).HasColumnName("Meta_Description");

                    entity
                        .Property(e => e.MetaKeywords)
                        .HasMaxLength(255)
                        .HasColumnName("Meta_Keywords");

                    entity.Property(e => e.MetaTitle).HasMaxLength(255).HasColumnName("Meta_Title");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Phone).HasMaxLength(50).IsUnicode(false);

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(255);

                    entity.Property(e => e.Url).HasMaxLength(255);

                    entity
                        .HasOne(d => d.District)
                        .WithMany(p => p.Branches)
                        .HasForeignKey(d => d.DistrictId)
                        .HasConstraintName("FK_Branch_District");

                    entity
                        .HasOne(d => d.Partner)
                        .WithMany(p => p.Branches)
                        .HasForeignKey(d => d.PartnerId)
                        .HasConstraintName("FK_Branch_Partner");

                    entity
                        .HasOne(d => d.Province)
                        .WithMany(p => p.Branches)
                        .HasForeignKey(d => d.ProvinceId)
                        .HasConstraintName("FK_Branch_Province");
                }
            );

            modelBuilder.Entity<Cash>(
                entity =>
                {
                    entity.ToTable("Cash");

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Note).HasMaxLength(500);

                    entity.Property(e => e.ReceiverName).HasMaxLength(500);

                    entity.Property(e => e.Value).HasColumnType("decimal(18, 2)");

                    entity
                        .HasOne(d => d.AccountCash)
                        .WithMany(p => p.Cashes)
                        .HasForeignKey(d => d.AccountCashId)
                        .HasConstraintName("FK_Cash_AccountCash");

                    entity
                        .HasOne(d => d.CashGroup)
                        .WithMany(p => p.Cashes)
                        .HasForeignKey(d => d.CashGroupId)
                        .HasConstraintName("FK_Cash_CashGroup");

                    entity
                        .HasOne(d => d.CashObject)
                        .WithMany(p => p.Cashes)
                        .HasForeignKey(d => d.CashObjectId)
                        .HasConstraintName("FK_Cash_CashSubject");

                    entity
                        .HasOne(d => d.CashType)
                        .WithMany(p => p.Cashes)
                        .HasForeignKey(d => d.CashTypeId)
                        .HasConstraintName("FK_Cash_CashType");
                }
            );

            modelBuilder.Entity<CashGroup>(
                entity =>
                {
                    entity.ToTable("CashGroup");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<CashObject>(
                entity =>
                {
                    entity.ToTable("CashObject");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<CashType>(
                entity =>
                {
                    entity.ToTable("CashType");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Collection>(
                entity =>
                {
                    entity.ToTable("Collection");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(1024);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CollectionCategory)
                        .WithMany(p => p.Collections)
                        .HasForeignKey(d => d.CollectionCategoryId)
                        .HasConstraintName("FK_Collection_CollectionCategory");
                }
            );

            modelBuilder.Entity<CollectionCategory>(
                entity =>
                {
                    entity.ToTable("CollectionCategory");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<CompanySystem>(
                entity =>
                {
                    entity.ToTable("CompanySystem");

                    entity.Property(e => e.Address).HasMaxLength(300);

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Email).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.Fax).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.Hotline).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Phone).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.ShortContent).HasColumnType("ntext");

                    entity.Property(e => e.Tel).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.Website).HasMaxLength(255).IsUnicode(false);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.CompanySystems)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_CompanySystem_Account");
                }
            );

            modelBuilder.Entity<ConfigSystem>(
                entity =>
                {
                    entity.ToTable("ConfigSystem");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.Key).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Values).HasMaxLength(1000).IsUnicode(false);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.ConfigSystems)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_ConfigSystem_Account");
                }
            );

            modelBuilder.Entity<Contact>(
                entity =>
                {
                    entity.ToTable("Contact");

                    entity.Property(e => e.Address).HasMaxLength(300);

                    entity.Property(e => e.Content).HasMaxLength(4000);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Email).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.FullName).HasMaxLength(300);

                    entity.Property(e => e.Mobi).HasMaxLength(13);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Subject).HasMaxLength(250);

                    entity
                        .HasOne(d => d.ApproveByNavigation)
                        .WithMany(p => p.Contacts)
                        .HasForeignKey(d => d.ApproveBy)
                        .HasConstraintName("FK_Contact_Account");
                }
            );

            modelBuilder.Entity<Coupon>(
                entity =>
                {
                    entity.ToTable("Coupon");

                    entity.Property(e => e.Code).HasMaxLength(50).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.PriceDiscount).HasColumnType("decimal(18, 0)");
                }
            );

            modelBuilder.Entity<Customer>(
                entity =>
                {
                    entity.ToTable("Customer");

                    entity.Property(e => e.Address).HasMaxLength(300);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Birthday).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.Code).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.CompanyName).HasMaxLength(1000);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Email).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.FullName).HasMaxLength(300);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Phone).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.Tel).HasMaxLength(300).IsUnicode(false);

                    entity
                        .HasOne(d => d.Account)
                        .WithMany(p => p.Customers)
                        .HasForeignKey(d => d.AccountId)
                        .HasConstraintName("FK_Customer_Account");

                    entity
                        .HasOne(d => d.CustomerType)
                        .WithMany(p => p.Customers)
                        .HasForeignKey(d => d.CustomerTypeId)
                        .HasConstraintName("FK_Customer_CustomerType");

                    entity
                        .HasOne(d => d.District)
                        .WithMany(p => p.Customers)
                        .HasForeignKey(d => d.DistrictId)
                        .HasConstraintName("FK_Customer_District");

                    entity
                        .HasOne(d => d.Organization)
                        .WithMany(p => p.Customers)
                        .HasForeignKey(d => d.OrganizationId)
                        .HasConstraintName("FK_Customer_Organization");

                    entity
                        .HasOne(d => d.Province)
                        .WithMany(p => p.Customers)
                        .HasForeignKey(d => d.ProvinceId)
                        .HasConstraintName("FK_Customer_Province");
                }
            );

            modelBuilder.Entity<CustomerType>(
                entity =>
                {
                    entity.ToTable("CustomerType");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Debt>(
                entity =>
                {
                    entity.ToTable("Debt");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity
                        .Property(e => e.Debt1)
                        .HasColumnType("decimal(18, 2)")
                        .HasColumnName("Debt");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Receivable).HasColumnType("decimal(18, 2)");

                    entity.Property(e => e.ReceiverName).HasMaxLength(500);

                    entity.Property(e => e.SumDebt).HasColumnType("decimal(18, 2)");

                    entity.Property(e => e.SumReceivable).HasColumnType("decimal(18, 2)");

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Debts)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Debt_Account");
                }
            );

            modelBuilder.Entity<Department>(
                entity =>
                {
                    entity.ToTable("Department");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<District>(
                entity =>
                {
                    entity.ToTable("District");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Districts)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_District_Account");

                    entity
                        .HasOne(d => d.Province)
                        .WithMany(p => p.Districts)
                        .HasForeignKey(d => d.ProvinceId)
                        .HasConstraintName("FK_District_Province");
                }
            );

            modelBuilder.Entity<Faq>(
                entity =>
                {
                    entity.ToTable("Faq");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Faqs)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Faq_Account");
                }
            );

            modelBuilder.Entity<Feedback>(
                entity =>
                {
                    entity.ToTable("Feedback");

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Content).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.FullName).HasMaxLength(300);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Regency).HasMaxLength(1024);

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Feedbacks)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Feedback_Account");
                }
            );

            modelBuilder.Entity<Function>(
                entity =>
                {
                    entity.ToTable("Function");

                    entity.Property(e => e.Action).HasMaxLength(500).IsUnicode(false);

                    entity.Property(e => e.Controller).HasMaxLength(500).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Note).HasColumnType("ntext");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Functions)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Function_Account");
                }
            );

            modelBuilder.Entity<FunctionApi>(
                entity =>
                {
                    entity.HasKey(e => new { e.ApiId, e.FunctionId });

                    entity.ToTable("FunctionApi");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Api)
                        .WithMany(p => p.FunctionApis)
                        .HasForeignKey(d => d.ApiId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_FunctionApi_Api");

                    entity
                        .HasOne(d => d.Function)
                        .WithMany(p => p.FunctionApis)
                        .HasForeignKey(d => d.FunctionId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_FunctionApi_Function");
                }
            );

            modelBuilder.Entity<FunctionGroupFunction>(
                entity =>
                {
                    entity.HasKey(e => new { e.GroupFunctionId, e.FunctionId });

                    entity.ToTable("FunctionGroupFunction");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Function)
                        .WithMany(p => p.FunctionGroupFunctions)
                        .HasForeignKey(d => d.FunctionId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_FunctionGroupFunction_Function");

                    entity
                        .HasOne(d => d.GroupFunction)
                        .WithMany(p => p.FunctionGroupFunctions)
                        .HasForeignKey(d => d.GroupFunctionId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_FunctionGroupFunction_GroupFunction");
                }
            );

            modelBuilder.Entity<Gallery>(
                entity =>
                {
                    entity.ToTable("Gallery");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Galleries)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Gallery_Account");

                    entity
                        .HasOne(d => d.GalleryCategory)
                        .WithMany(p => p.Galleries)
                        .HasForeignKey(d => d.GalleryCategoryId)
                        .HasConstraintName("FK_Gallery_GalleryCategory");
                }
            );

            modelBuilder.Entity<GalleryCategory>(
                entity =>
                {
                    entity.ToTable("GalleryCategory");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<GroupAccount>(
                entity =>
                {
                    entity.ToTable("GroupAccount");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.Key).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<GroupFunction>(
                entity =>
                {
                    entity.ToTable("GroupFunction");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<HistoryType>(
                entity =>
                {
                    entity.ToTable("HistoryType");

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Introduction>(
                entity =>
                {
                    entity.ToTable("Introduction");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.Icon).HasMaxLength(200).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.Url).HasMaxLength(1024).IsUnicode(false);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Introductions)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Introduction_Account");

                    entity
                        .HasOne(d => d.IntroductionType)
                        .WithMany(p => p.Introductions)
                        .HasForeignKey(d => d.IntroductionTypeId)
                        .HasConstraintName("FK_Introduction_IntroductionType");
                }
            );

            modelBuilder.Entity<IntroductionType>(
                entity =>
                {
                    entity.ToTable("IntroductionType");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<MiniService>(
                entity =>
                {
                    entity.ToTable("MiniService");

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.ShortTitle).HasMaxLength(300);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.Url).HasMaxLength(1024).IsUnicode(false);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.MiniServices)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_MiniService_Account");
                }
            );

            modelBuilder.Entity<Module>(
                entity =>
                {
                    entity.ToTable("Module");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Order>(
                entity =>
                {
                    entity.ToTable("Order");

                    entity.Property(e => e.Address).HasMaxLength(300);

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Email).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.FeeShip).HasColumnType("decimal(18, 2)");

                    entity.Property(e => e.FullName).HasMaxLength(300);

                    entity.Property(e => e.Mobi).HasMaxLength(13);

                    entity.Property(e => e.Mobi2).HasMaxLength(13);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.NoteFeeShip).HasMaxLength(200);

                    entity.Property(e => e.NoteSale).HasMaxLength(200);

                    entity.Property(e => e.Remark).HasMaxLength(1024);

                    entity.Property(e => e.RatingContent).HasMaxLength(1024);

                    entity.Property(e => e.RatingTime).HasColumnType("datetime");

                    entity.Property(e => e.SaleOff).HasColumnType("decimal(18, 2)");

                    entity.Property(e => e.TotalPrice).HasColumnType("decimal(18, 2)");

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Orders)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Order_Account");

                    entity
                        .HasOne(d => d.Customer)
                        .WithMany(p => p.Orders)
                        .HasForeignKey(d => d.CustomerId)
                        .HasConstraintName("FK_Order_Customer");

                    entity
                        .HasOne(d => d.District)
                        .WithMany(p => p.Orders)
                        .HasForeignKey(d => d.DistrictId)
                        .HasConstraintName("FK_Order_District");

                    entity
                        .HasOne(d => d.OrderStatus)
                        .WithMany(p => p.Orders)
                        .HasForeignKey(d => d.OrderStatusId)
                        .HasConstraintName("FK_Order_OrderStatus");

                    entity
                        .HasOne(d => d.PayType)
                        .WithMany(p => p.Orders)
                        .HasForeignKey(d => d.PayTypeId)
                        .HasConstraintName("FK_Order_PayType");

                    entity
                        .HasOne(d => d.Province)
                        .WithMany(p => p.Orders)
                        .HasForeignKey(d => d.ProvinceId)
                        .HasConstraintName("FK_Order_Province");
                }
            );

            modelBuilder.Entity<OrderDetail>(
                entity =>
                {
                    entity.ToTable("OrderDetail");

                    entity.Property(e => e.Option).HasColumnType("ntext");

                    entity.Property(e => e.OriginalPrice).HasColumnType("decimal(18, 2)");

                    entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                    entity
                        .HasOne(d => d.Color)
                        .WithMany(p => p.OrderDetailColors)
                        .HasForeignKey(d => d.ColorId)
                        .HasConstraintName("FK_OrderDetail_Color");

                    entity
                        .HasOne(d => d.Order)
                        .WithMany(p => p.OrderDetails)
                        .HasForeignKey(d => d.OrderId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_OrderDetail_Order");

                    entity
                        .HasOne(d => d.Product)
                        .WithMany(p => p.OrderDetails)
                        .HasForeignKey(d => d.ProductId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_OrderDetail_Product");

                    entity
                        .HasOne(d => d.Size)
                        .WithMany(p => p.OrderDetailSizes)
                        .HasForeignKey(d => d.SizeId)
                        .HasConstraintName("FK_OrderDetail_Size");
                }
            );

            modelBuilder.Entity<OrderStatus>(
                entity =>
                {
                    entity.ToTable("OrderStatus");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Organization>(
                entity =>
                {
                    entity.ToTable("Organization");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Key).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Page>(
                entity =>
                {
                    entity.ToTable("Page");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.Url).HasMaxLength(1024).IsUnicode(false);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Pages)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Page_Account");

                    entity
                        .HasOne(d => d.PageType)
                        .WithMany(p => p.Pages)
                        .HasForeignKey(d => d.PageTypeId)
                        .HasConstraintName("FK_Page_PageType");
                }
            );

            modelBuilder.Entity<PageType>(
                entity =>
                {
                    entity.ToTable("PageType");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.PageTypes)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_PageType_Account");
                }
            );

            modelBuilder.Entity<Partner>(
                entity =>
                {
                    entity.ToTable("Partner");

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.Url).HasMaxLength(1024).IsUnicode(false);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Partners)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Partner_Account");
                }
            );

            modelBuilder.Entity<PayType>(
                entity =>
                {
                    entity.ToTable("PayType");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.PayTypes)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_PayType_Account");
                }
            );

            modelBuilder.Entity<PositionBanner>(
                entity =>
                {
                    entity.ToTable("PositionBanner");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Key).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Product>(
                entity =>
                {
                    entity.ToTable("Product");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ImageListProduct).HasMaxLength(4000);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");

                    entity.Property(e => e.SaleDeadLine).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.TotalAssess).HasDefaultValueSql("((0))");

                    entity
                        .Property(e => e.ValueAssess)
                        .HasColumnType("decimal(5, 1)")
                        .HasDefaultValueSql("((0))");

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Products)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Product_Account");

                    entity
                        .HasOne(d => d.ProductCategory)
                        .WithMany(p => p.Products)
                        .HasForeignKey(d => d.ProductCategoryId)
                        .HasConstraintName("FK_Product_ProductCategory");

                    entity
                        .HasOne(d => d.SaleNavigation)
                        .WithMany(p => p.Products)
                        .HasForeignKey(d => d.SaleId)
                        .HasConstraintName("FK_Product_Sales");
                }
            );

            modelBuilder.Entity<ProductAttribute>(
                entity =>
                {
                    entity.ToTable("ProductAttribute");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ImageList).HasColumnType("ntext");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");

                    entity.Property(e => e.PricePromo).HasColumnType("decimal(18, 0)");

                    entity
                        .HasOne(d => d.Attribute)
                        .WithMany(p => p.ProductAttributes)
                        .HasForeignKey(d => d.AttributeId)
                        .HasConstraintName("FK_ProductAttribute_Attribute");

                    entity
                        .HasOne(d => d.Parent)
                        .WithMany(p => p.InverseParent)
                        .HasForeignKey(d => d.ParentId)
                        .HasConstraintName("FK_ProductAttribute_ProductAttribute");

                    entity
                        .HasOne(d => d.Product)
                        .WithMany(p => p.ProductAttributes)
                        .HasForeignKey(d => d.ProductId)
                        .HasConstraintName("FK_ProductAttribute_Product");
                }
            );

            modelBuilder.Entity<ProductCategory>(
                entity =>
                {
                    entity.ToTable("ProductCategory");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.Logo).HasMaxLength(1024);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<ProductCollection>(
                entity =>
                {
                    entity
                        .HasKey(e => new { e.ProductId, e.CollectionId })
                        .HasName("PK_ProductCollection_1");

                    entity.ToTable("ProductCollection");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Collection)
                        .WithMany(p => p.ProductCollections)
                        .HasForeignKey(d => d.CollectionId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductCollection_Collection");

                    entity
                        .HasOne(d => d.Product)
                        .WithMany(p => p.ProductCollections)
                        .HasForeignKey(d => d.ProductId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductCollection_Product");
                }
            );

            modelBuilder.Entity<ProductShelf>(
                entity =>
                {
                    entity.HasKey(e => new { e.ProductId, e.ShelvesId });

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Product)
                        .WithMany(p => p.ProductShelves)
                        .HasForeignKey(d => d.ProductId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductShelves_Product");

                    entity
                        .HasOne(d => d.Shelves)
                        .WithMany(p => p.ProductShelves)
                        .HasForeignKey(d => d.ShelvesId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductShelves_Shelves");
                }
            );

            modelBuilder.Entity<ProductTab>(
                entity =>
                {
                    entity.HasKey(e => new { e.TabId, e.ProductId });

                    entity.ToTable("ProductTab");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Product)
                        .WithMany(p => p.ProductTabs)
                        .HasForeignKey(d => d.ProductId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductTab_Product");

                    entity
                        .HasOne(d => d.Tab)
                        .WithMany(p => p.ProductTabs)
                        .HasForeignKey(d => d.TabId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductTab_Tab");
                }
            );

            modelBuilder.Entity<ProductWardrobe>(
                entity =>
                {
                    entity.HasKey(e => new { e.ProductId, e.WardrobeId });

                    entity.ToTable("ProductWardrobe");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Product)
                        .WithMany(p => p.ProductWardrobes)
                        .HasForeignKey(d => d.ProductId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductWardrobe_ProductWardrobe");

                    entity
                        .HasOne(d => d.Wardrobe)
                        .WithMany(p => p.ProductWardrobes)
                        .HasForeignKey(d => d.WardrobeId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductWardrobe_Wardrobe");
                }
            );

            modelBuilder.Entity<ProductWarehouse>(
                entity =>
                {
                    entity.HasKey(e => new { e.ProductId, e.WarehouseId });

                    entity.ToTable("ProductWarehouse");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Product)
                        .WithMany(p => p.ProductWarehouses)
                        .HasForeignKey(d => d.ProductId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductWarehouse_Product");

                    entity
                        .HasOne(d => d.Warehouse)
                        .WithMany(p => p.ProductWarehouses)
                        .HasForeignKey(d => d.WarehouseId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductWarehouse_Warehouse");
                }
            );

            modelBuilder.Entity<ProductWishlist>(
                entity =>
                {
                    entity.HasKey(e => new { e.ProductId, e.CustomerId });

                    entity.ToTable("ProductWishlist");

                    entity.Property(e => e.CreateTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Customer)
                        .WithMany(p => p.ProductWishlists)
                        .HasForeignKey(d => d.CustomerId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductWishlist_Customer");

                    entity
                        .HasOne(d => d.Product)
                        .WithMany(p => p.ProductWishlists)
                        .HasForeignKey(d => d.ProductId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_ProductWishlist_ProductWishlist");
                }
            );

            modelBuilder.Entity<Province>(
                entity =>
                {
                    entity.ToTable("Province");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.FeeShip).HasColumnType("decimal(18, 0)");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Recruitment>(
                entity =>
                {
                    entity.ToTable("Recruitment");

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Recruitments)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Recruitment_Account");
                }
            );

            modelBuilder.Entity<Sale>(
                entity =>
                {
                    entity.ToTable("Sale");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.KeySale).HasMaxLength(500).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.PriceSale).HasColumnType("decimal(18, 2)");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.ShortTitle).HasMaxLength(300);

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.TimeEnd).HasColumnType("datetime");

                    entity.Property(e => e.TimeStart).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Service>(
                entity =>
                {
                    entity.ToTable("Service");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.ValueAssess).HasColumnType("decimal(5, 1)");

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Services)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Service_Account");

                    entity
                        .HasOne(d => d.ServiceCategory)
                        .WithMany(p => p.Services)
                        .HasForeignKey(d => d.ServiceCategoryId)
                        .HasConstraintName("FK_Service_ServiceCategory");
                }
            );

            modelBuilder.Entity<ServiceCategory>(
                entity =>
                {
                    entity.ToTable("ServiceCategory");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Shelf>(
                entity =>
                {
                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.Warehouse)
                        .WithMany(p => p.Shelves)
                        .HasForeignKey(d => d.WarehouseId)
                        .HasConstraintName("FK_Shelves_Warehouse");
                }
            );

            modelBuilder.Entity<ShippingFee>(
                entity =>
                {
                    entity.ToTable("ShippingFee");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(1000);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.ShippingFees)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_ShippingFee_Account");
                }
            );

            modelBuilder.Entity<Slider>(
                entity =>
                {
                    entity.ToTable("Slider");

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ListImage).IsUnicode(false).HasColumnName("LIstImage");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.Title1).HasMaxLength(255);

                    entity.Property(e => e.Title2).HasMaxLength(255);

                    entity.Property(e => e.Title3).HasMaxLength(255);

                    entity.Property(e => e.Url).HasMaxLength(1024).IsUnicode(false);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Sliders)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Slider_Account");
                }
            );

            modelBuilder.Entity<StaffDepartment>(
                entity =>
                {
                    entity.HasKey(e => new { e.StaffId, e.DepartmentId });

                    entity.ToTable("StaffDepartment");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Department)
                        .WithMany(p => p.StaffDepartments)
                        .HasForeignKey(d => d.DepartmentId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_StaffDepartment_Department");

                    entity
                        .HasOne(d => d.Staff)
                        .WithMany(p => p.StaffDepartments)
                        .HasForeignKey(d => d.StaffId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_StaffDepartment_Staff");
                }
            );

            modelBuilder.Entity<StaffType>(
                entity =>
                {
                    entity.ToTable("StaffType");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<StaffWarehouse>(
                entity =>
                {
                    entity.HasKey(e => new { e.StaffId, e.WarehouseId });

                    entity.ToTable("StaffWarehouse");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity
                        .HasOne(d => d.Staff)
                        .WithMany(p => p.StaffWarehouses)
                        .HasForeignKey(d => d.StaffId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_StaffWarehouse_Staff");

                    entity
                        .HasOne(d => d.Warehouse)
                        .WithMany(p => p.StaffWarehouses)
                        .HasForeignKey(d => d.WarehouseId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_StaffWarehouse_Warehouse");
                }
            );

            modelBuilder.Entity<Subscribe>(
                entity =>
                {
                    entity.ToTable("Subscribe");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Email).HasMaxLength(255);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(255);
                }
            );

            modelBuilder.Entity<Supplier>(
                entity =>
                {
                    entity.ToTable("Supplier");

                    entity.Property(e => e.Address).HasMaxLength(300);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.Email).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.Fax).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.Hotline).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Phone).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.Tel).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Suppliers)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Supplier_Account");
                }
            );

            modelBuilder.Entity<Tab>(
                entity =>
                {
                    entity.ToTable("Tab");

                    entity.Property(e => e.Code).HasMaxLength(200).IsUnicode(false);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Transport>(
                entity =>
                {
                    entity.ToTable("Transport");

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Phone).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.PhoneStaffContact).HasMaxLength(500);

                    entity.Property(e => e.StaffContact).HasMaxLength(500);

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Unit>(
                entity =>
                {
                    entity.ToTable("Unit");

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Units)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Unit_Account");
                }
            );

            modelBuilder.Entity<Video>(
                entity =>
                {
                    entity.ToTable("Video");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity.Property(e => e.ValueAssess).HasColumnType("decimal(5, 1)");

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.Videos)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_Video_Account");

                    entity
                        .HasOne(d => d.VideoCategory)
                        .WithMany(p => p.Videos)
                        .HasForeignKey(d => d.VideoCategoryId)
                        .HasConstraintName("FK_Video_VideoCategory");
                }
            );

            modelBuilder.Entity<VideoCategory>(
                entity =>
                {
                    entity.ToTable("VideoCategory");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Wardrobe>(
                entity =>
                {
                    entity.ToTable("Wardrobe");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity.Property(e => e.Content).HasColumnType("ntext");

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(1024);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.WardrobeCategory)
                        .WithMany(p => p.Wardrobes)
                        .HasForeignKey(d => d.WardrobeCategoryId)
                        .HasConstraintName("FK_Wardrobe_WardrobeCategory");
                }
            );

            modelBuilder.Entity<WardrobeCategory>(
                entity =>
                {
                    entity.ToTable("WardrobeCategory");

                    entity.Property(e => e.Alias).HasMaxLength(1000).IsUnicode(false);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Schemas).HasColumnType("ntext");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<Warehouse>(
                entity =>
                {
                    entity.ToTable("Warehouse");

                    entity.Property(e => e.Address).HasMaxLength(300);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Title).HasMaxLength(1024);
                }
            );

            modelBuilder.Entity<WarehouseHistory>(
                entity =>
                {
                    entity.ToTable("WarehouseHistory");

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Note).HasMaxLength(500);

                    entity.Property(e => e.Title).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.CreateByNavigation)
                        .WithMany(p => p.WarehouseHistories)
                        .HasForeignKey(d => d.CreateBy)
                        .HasConstraintName("FK_WarehouseHistory_Account");

                    entity
                        .HasOne(d => d.HistoryType)
                        .WithMany(p => p.WarehouseHistories)
                        .HasForeignKey(d => d.HistoryTypeId)
                        .HasConstraintName("FK_WarehouseHistory_HistoryType");
                }
            );

            modelBuilder.Entity<WarehouseHistoryDetail>(
                entity =>
                {
                    entity.HasKey(e => new { e.WarehouseHistoryId, e.ProductId, e.WarehouseId });

                    entity.ToTable("WarehouseHistoryDetail");

                    entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                    entity.Property(e => e.PriceSale).HasColumnType("decimal(18, 2)");

                    entity
                        .HasOne(d => d.WarehouseHistory)
                        .WithMany(p => p.WarehouseHistoryDetails)
                        .HasForeignKey(d => d.WarehouseHistoryId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_WarehouseHistoryDetail_WarehouseHistory");
                }
            );

            modelBuilder.Entity<Staff>(
                entity =>
                {
                    entity.ToTable("Staff");

                    entity.Property(e => e.Address).HasMaxLength(300);

                    entity.Property(e => e.Avatar).HasMaxLength(1024);

                    entity.Property(e => e.Birthday).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.Code).HasMaxLength(500).IsUnicode(false);

                    entity
                        .Property(e => e.CreateTime)
                        .HasColumnType("datetime")
                        .HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Description).HasMaxLength(500);

                    entity.Property(e => e.Email).HasMaxLength(300).IsUnicode(false);

                    entity.Property(e => e.Facebook).HasMaxLength(255).IsUnicode(false);

                    entity.Property(e => e.FullName).HasMaxLength(300);

                    entity.Property(e => e.Google).HasMaxLength(1024);

                    entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                    entity.Property(e => e.Phone).HasMaxLength(100).IsUnicode(false);

                    entity.Property(e => e.Regency).HasMaxLength(1024);

                    entity.Property(e => e.Skype).HasMaxLength(1024);

                    entity.Property(e => e.StaffTypeId).HasColumnName("StaffTypeID");

                    entity.Property(e => e.Thumb).HasMaxLength(1024);

                    entity.Property(e => e.Twitter).HasMaxLength(1024);

                    entity.Property(e => e.Viber).HasMaxLength(1024);

                    entity.Property(e => e.Zalo).HasMaxLength(1024);

                    entity
                        .HasOne(d => d.Organization)
                        .WithMany(p => p.staff)
                        .HasForeignKey(d => d.OrganizationId)
                        .HasConstraintName("FK_Staff_Organization");

                    entity
                        .HasOne(d => d.StaffType)
                        .WithMany(p => p.staff)
                        .HasForeignKey(d => d.StaffTypeId)
                        .HasConstraintName("FK_Staff_StaffType");
                }
            );

            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync(
            CancellationToken cancellationToken = new CancellationToken()
        )
        {
            foreach (var entry in ChangeTracker.Entries<BaseEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreateTime = _dateTime.NowUtc;
                        entry.Entity.ModifyTime = _dateTime.NowUtc;
                        break;

                    case EntityState.Modified:
                        entry.Entity.ModifyTime = _dateTime.NowUtc;
                        break;

                    case EntityState.Deleted:
                        entry.Entity.IsDelete = true;
                        break;
                }
            }
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
