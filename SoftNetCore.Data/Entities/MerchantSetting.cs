﻿using SoftNetCore.Data.Enums;
using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class MerchantSetting
    {
        public int Id { get; set; }
        public EnumDomains MerchantId { get; set; }
        public string TokenKey { get; set; }
        public string TokenValue { get; set; }
    }
}
