﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Warehouse
    {
        public Warehouse()
        {
            ProductWarehouses = new HashSet<ProductWarehouse>();
            Shelves = new HashSet<Shelf>();
            StaffWarehouses = new HashSet<StaffWarehouse>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public int? Position { get; set; }
        public bool? Status { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<ProductWarehouse> ProductWarehouses { get; set; }
        public virtual ICollection<Shelf> Shelves { get; set; }
        public virtual ICollection<StaffWarehouse> StaffWarehouses { get; set; }
    }
}
