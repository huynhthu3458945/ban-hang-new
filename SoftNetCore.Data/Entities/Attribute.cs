﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Attribute
    {
        public Attribute()
        {
            OrderDetailColors = new HashSet<OrderDetail>();
            OrderDetailSizes = new HashSet<OrderDetail>();
            ProductAttributes = new HashSet<ProductAttribute>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public int? TypeId { get; set; }
        public string Content { get; set; }
        public int? Position { get; set; }
        public bool? Status { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        public virtual Account CreateByNavigation { get; set; }
        public virtual ICollection<OrderDetail> OrderDetailColors { get; set; }
        public virtual ICollection<OrderDetail> OrderDetailSizes { get; set; }
        public virtual ICollection<ProductAttribute> ProductAttributes { get; set; }
    }
}
