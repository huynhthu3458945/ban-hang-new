﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class FetchDataAudit
    {
        public int Id { get; set; }
        public bool IsSuccess { get; set; }
        public string Url { get; set; }
        public string Token { get; set; }
        public string Contents { get; set; }
        public DateTime CreatedTime { get; set; }
    }
}
