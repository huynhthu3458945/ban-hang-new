﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class FunctionGroupFunction
    {
        public int GroupFunctionId { get; set; }
        public int FunctionId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }

        public virtual Function Function { get; set; }
        public virtual GroupFunction GroupFunction { get; set; }
    }
}
