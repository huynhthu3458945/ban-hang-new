﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class StaffDepartment
    {
        public int StaffId { get; set; }
        public int DepartmentId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }

        public virtual Department Department { get; set; }
        public virtual staff Staff { get; set; }
    }
}
