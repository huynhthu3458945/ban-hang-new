﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Product
    {
        public Product()
        {
            OrderDetails = new HashSet<OrderDetail>();
            ProductAttributes = new HashSet<ProductAttribute>();
            ProductCollections = new HashSet<ProductCollection>();
            ProductShelves = new HashSet<ProductShelf>();
            ProductTabs = new HashSet<ProductTab>();
            ProductWardrobes = new HashSet<ProductWardrobe>();
            ProductWarehouses = new HashSet<ProductWarehouse>();
            ProductWishlists = new HashSet<ProductWishlist>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public string ImageListProduct { get; set; }
        public int? Position { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string Alias { get; set; }
        public decimal? Price { get; set; }
        public int? Sale { get; set; }
        public DateTime? SaleDeadLine { get; set; }
        public int? ViewTime { get; set; }
        public bool? StockStatus { get; set; }
        public int? WishlistView { get; set; }
        public int? SoldView { get; set; }
        public string Schemas { get; set; }
        public int? TotalAssess { get; set; }
        public decimal? ValueAssess { get; set; }
        public bool? IsProductNew { get; set; }
        public bool? IsProductHot { get; set; }
        public bool? Status { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? CreateBy { get; set; }
        public int? ModifyBy { get; set; }
        public int? ProductCategoryId { get; set; }
        public int? SaleId { get; set; }
        public int? UnitId { get; set; }
        public string Aff_link { get; set; }
        public string Domain { get; set; }
        public string Merchant { get; set; }
        public string ProductId { get; set; }
        public string ShareContent { get; set; }
        public string Sku { get; set; }
        public string CommissionType { get; set; }
        public decimal? CommissionValue { get; set; }
        public string ShipPrice { get; set; }
        public string Slug { get; set; }
        public decimal? DiscountAmount { get; set; }
        public double? DiscountRate { get; set; }
        public decimal? PriceAfterDiscount { get; set; }
        public string RootUrl { get; set; }
        public string UpdateTime { get; set; }
        public decimal? PricePromo { get; set; }
        public string Cate { get; set; }
        public string Provider { get; set; }
        public int? PartnerId { get; set; }
        public int? BannerId { get; set; }

        //public string ProductId { get; set; } // get from another platform
        //public string Sku { get; set; } // Product code
        //public string Merchant { get; set; }
        //public string Aff_link { get; set; }
        //public string Domain { get; set; }
        //public string ShareContent { get; set; }
        //public string ShipPrice { get; set; }
        //public decimal? CommissionValue { get; set; }
        //public string CommissionType { get; set; }
        //public string Slug { get; set; }

        ///// <summary>
        ///// Tên nền tảng cung cấp
        ///// </summary>
        //public string Provider { get; set; }

        ///// <summary>
        ///// Accesstrade
        ///// </summary>
        //public string RootUrl { get; set; }
        //public string UpdateTime { get; set; }

        public virtual Partner Partner { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
        public virtual ICollection<ProductAttribute> ProductAttributes { get; set; }
        public virtual ICollection<ProductCollection> ProductCollections { get; set; }
        public virtual ICollection<ProductShelf> ProductShelves { get; set; }
        public virtual ICollection<ProductTab> ProductTabs { get; set; }
        public virtual ICollection<ProductWardrobe> ProductWardrobes { get; set; }
        public virtual ICollection<ProductWarehouse> ProductWarehouses { get; set; }
        public virtual ICollection<ProductWishlist> ProductWishlists { get; set; }
    }
}
