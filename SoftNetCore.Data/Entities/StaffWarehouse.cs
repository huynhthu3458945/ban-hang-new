﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class StaffWarehouse
    {
        public int StaffId { get; set; }
        public int WarehouseId { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        public virtual staff Staff { get; set; }
        public virtual Warehouse Warehouse { get; set; }
    }
}
