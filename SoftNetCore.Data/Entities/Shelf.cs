﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Shelf
    {
        public Shelf()
        {
            ProductShelves = new HashSet<ProductShelf>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? Status { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }
        public int? WarehouseId { get; set; }
        public bool? IsDelete { get; set; }

        public virtual Warehouse Warehouse { get; set; }
        public virtual ICollection<ProductShelf> ProductShelves { get; set; }
    }
}
