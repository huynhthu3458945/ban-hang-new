﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class OrderDetail
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int? ColorId { get; set; }
        public int? SizeId { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }
        public decimal? OriginalPrice { get; set; }
        public string Option { get; set; }

        public virtual Attribute Color { get; set; }
        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
        public virtual Attribute Size { get; set; }
    }
}
