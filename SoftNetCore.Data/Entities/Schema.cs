﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Schema
    {
        public int Version { get; set; }
    }
}
