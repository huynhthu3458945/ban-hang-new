﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class PositionBanner
    {
        public PositionBanner()
        {
            Banners = new HashSet<Banner>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Key { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public bool? Status { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<Banner> Banners { get; set; }
    }
}
