﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Coupon
    {
        public Coupon()
        {
            AccountCoupons = new HashSet<AccountCoupon>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public decimal? PriceDiscount { get; set; }
        public bool? IsPublic { get; set; }
        public bool? IsUse { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public string AffilateLink { get; set; }
        public string CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string Content { get; set; }
        public string CouponId { get; set; }
        public string Coupons { get; set; }
        public string EndDate { get; set; }
        public string Image { get; set; }
        public bool? IsHot { get; set; }
        public string Link { get; set; }
        public string Merchant { get; set; }
        public int PercentageUsed { get; set; }
        public int Register { get; set; }
        public string StartDate { get; set; }
        public int Status { get; set; }
        public string TimeLeft { get; set; }
        public string Title { get; set; }
        public int? CouponTypeId { get; set; }

        public virtual CouponType CouponType { get; set; }
        public virtual ICollection<AccountCoupon> AccountCoupons { get; set; }
    }
}
