﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class FunctionApi
    {
        public int ApiId { get; set; }
        public int FunctionId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool? IsDelete { get; set; }

        public virtual Api Api { get; set; }
        public virtual Function Function { get; set; }
    }
}
