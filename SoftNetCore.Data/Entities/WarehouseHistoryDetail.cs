﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class WarehouseHistoryDetail
    {
        public int WarehouseId { get; set; }
        public int WarehouseHistoryId { get; set; }
        public int ProductId { get; set; }
        public int? RequestNumber { get; set; }
        public int? ActualNumber { get; set; }
        public int? ErrorNumber { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceSale { get; set; }

        public virtual WarehouseHistory WarehouseHistory { get; set; }
    }
}
