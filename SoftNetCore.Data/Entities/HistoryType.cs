﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class HistoryType
    {
        public HistoryType()
        {
            WarehouseHistories = new HashSet<WarehouseHistory>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<WarehouseHistory> WarehouseHistories { get; set; }
    }
}
