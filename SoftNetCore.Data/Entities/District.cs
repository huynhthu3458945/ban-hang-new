﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class District
    {
        public District()
        {
            Branches = new HashSet<Branch>();
            Customers = new HashSet<Customer>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public int? ProvinceId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool? Status { get; set; }
        public bool? IsDelete { get; set; }

        public virtual Account CreateByNavigation { get; set; }
        public virtual Province Province { get; set; }
        public virtual ICollection<Branch> Branches { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
