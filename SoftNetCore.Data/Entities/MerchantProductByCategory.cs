﻿using SoftNetCore.Data.Enums;
using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class MerchantProductByCategory
    {
        public int Id { get; set; }
        public int ProductCategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public int PageSize { get; set; }
        public string AccessToken { get; set; }
        public EnumDomains DomainId { get; set; }

        public virtual ProductCategory ProductCategory { get; set; }
    }
}
