﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class ProductWishlist
    {
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Product Product { get; set; }
    }
}
