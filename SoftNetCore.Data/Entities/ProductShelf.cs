﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class ProductShelf
    {
        public int ProductId { get; set; }
        public int ShelvesId { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        public virtual Product Product { get; set; }
        public virtual Shelf Shelves { get; set; }
    }
}
