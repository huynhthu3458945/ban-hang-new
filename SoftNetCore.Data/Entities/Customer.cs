﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Customer
    {
        public Customer()
        {
            ProductWishlists = new HashSet<ProductWishlist>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public bool? Gender { get; set; }
        public string Birthday { get; set; }
        public string Avatar { get; set; }
        public string Phone { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? CustomerTypeId { get; set; }
        public int? NumberTakeCare { get; set; }
        public string CompanyName { get; set; }
        public int? AccountId { get; set; }
        public int? OrganizationId { get; set; }
        public bool? IsDelete { get; set; }
        public string Code { get; set; }
        public bool? Status { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }

        public virtual Account Account { get; set; }
        public virtual CustomerType CustomerType { get; set; }
        public virtual District District { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual Province Province { get; set; }
        public virtual ICollection<ProductWishlist> ProductWishlists { get; set; }
    }
}
