﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class AccountCoupon
    {
        public int AccountId { get; set; }
        public int CouponId { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        public virtual Account Account { get; set; }
        public virtual Coupon Coupon { get; set; }
    }
}
