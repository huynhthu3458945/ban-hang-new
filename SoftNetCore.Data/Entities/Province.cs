﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Province
    {
        public Province()
        {
            Branches = new HashSet<Branch>();
            Customers = new HashSet<Customer>();
            Districts = new HashSet<District>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public decimal? FeeShip { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<Branch> Branches { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<District> Districts { get; set; }
    }
}
