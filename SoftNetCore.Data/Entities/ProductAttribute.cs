﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class ProductAttribute
    {
        public ProductAttribute()
        {
            InverseParent = new HashSet<ProductAttribute>();
        }

        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? AttributeId { get; set; }
        public string Avatar1 { get; set; }
        public string Avatar2 { get; set; }
        public string ImageList { get; set; }
        public decimal? Price { get; set; }
        public decimal? PricePromo { get; set; }
        public int? Quantity { get; set; }
        public string Value { get; set; }
        public bool? Status { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }
        public int? ParentId { get; set; }

        public virtual Attribute Attribute { get; set; }
        public virtual ProductAttribute Parent { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<ProductAttribute> InverseParent { get; set; }
    }
}
