﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class ConfigSystem
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Values { get; set; }
        public bool? IsActive { get; set; }
        public TimeSpan? TimeStart { get; set; }
        public TimeSpan? TimeEnd { get; set; }
        public int? CouponPage { get; set; }
        public int? CouponLimit { get; set; }
        public int? DIMUADIProductPage { get; set; }
        public int? DIMUADIProductLimit { get; set; }
        public int? ACCESSTRADEProductPage { get; set; }
        public int? ACCESSTRADEProductLimit { get; set; }
        public int? HotDealPage { get; set; }
        public int? HotDealLimit { get; set; }
        public int? HotDealProductPage { get; set; }
        public int? HotDealProductLimit { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public string Description { get; set; }

        public virtual Account CreateByNavigation { get; set; }
    }
}
