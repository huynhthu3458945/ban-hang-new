﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? FeeShip { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Mobi { get; set; }
        public string Mobi2 { get; set; }
        public string Address { get; set; }
        public int? Status { get; set; }
        public bool? IsDelete { get; set; }
        public string Remark { get; set; }
        public bool? IsCheckingGoods { get; set; }
        public string NoteSale { get; set; }
        public decimal? SaleOff { get; set; }
        public string NoteFeeShip { get; set; }
        public int? Rating { get; set; }
        public int? IsRating { get; set; }
        public string RatingContent { get; set; }
        public DateTime? RatingTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? OrderStatusId { get; set; }
        public int? CustomerId { get; set; }
        public int? PayTypeId { get; set; }
        public int? TransportId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? PartnerId { get; set; }

        public virtual Partner Partner { get; set; }
        public virtual OrderStatus OrderStatus { get; set; }
        public virtual PayType PayType { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
