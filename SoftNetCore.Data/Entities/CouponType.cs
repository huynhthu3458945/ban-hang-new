﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class CouponType
    {
        public CouponType()
        {
            Coupons = new HashSet<Coupon>();
        }

        public int Id { get; set; }
        public string Logo { get; set; }
        public string Title { get; set; }
        public string MerchantName { get; set; }
        public string Code { get; set; }
        public bool? Status { get; set; }
        public int? Idc { get; set; }
        public string Domain { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }

        public virtual ICollection<Coupon> Coupons { get; set; }
    }
}
