﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class AccountCash
    {
        public AccountCash()
        {
            Cashes = new HashSet<Cash>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public bool? Status { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public decimal? BalanceValue { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<Cash> Cashes { get; set; }
    }
}
