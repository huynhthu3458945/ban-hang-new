﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Collection
    {
        public Collection()
        {
            ProductCollections = new HashSet<ProductCollection>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public int? Position { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string Alias { get; set; }
        public string Schemas { get; set; }
        public bool? Status { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? CreateBy { get; set; }
        public int? ModifyBy { get; set; }
        public int? CollectionCategoryId { get; set; }

        public virtual CollectionCategory CollectionCategory { get; set; }
        public virtual ICollection<ProductCollection> ProductCollections { get; set; }
    }
}
