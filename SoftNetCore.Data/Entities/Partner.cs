﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Partner
    {
        public Partner()
        {
            Branches = new HashSet<Branch>();
            Orders = new HashSet<Order>();
            Products = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Url { get; set; }
        public string Avatar { get; set; }
        public string BusinessName { get; set; }
        public string BusinessItems { get; set; }
        public string Fanpage { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? Position { get; set; }
        public bool? Status { get; set; }
        public bool? IsDelete { get; set; }
        public int? AccountId { get; set; }
        public int? ProductCategoryId { get; set; }
        public int? PartnerTypeId { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        public virtual Account CreateByNavigation { get; set; }
        public virtual PartnerType PartnerType { get; set; }
        public virtual ICollection<Branch> Branches { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
