﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class ProductCategory
    {
        public ProductCategory()
        {
            Products = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }

        public string Logo { get; set; }

        /// <summary>
        /// Hình ảnh đại diện cho danh mục
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// Hình ảnh xem trước đã được tối ưu
        /// </summary>
        public string Thumb { get; set; }

        public string Alias { get; set; }
        public string Schemas { get; set; }
        public int? Position { get; set; }
        public bool? Status { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ParentId { get; set; }

        /// <summary>
        /// Mã danh mục từ nền tảng khác
        /// </summary>
        public string ProductCategoryId { get; set; }

        public string ProductCategoryCode { get; set; }

        public string Domain { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public virtual ICollection<MerchantProductByCategory> MerchantProductByCategories { get; set; }
    }
}
