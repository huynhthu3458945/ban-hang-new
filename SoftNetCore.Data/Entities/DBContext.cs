﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using SoftNetCore.Data.Abstract;
using SoftNetCore.Data.Services;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class DBContext : DbContext
    {
        private readonly IDateTimeService _dateTime;
        public DBContext()
        {
        }

        public DBContext(DbContextOptions<DBContext> options, IDateTimeService dateTimeService)
            : base(options)
        {
            _dateTime = dateTimeService;
        }

        public virtual DbSet<About> Abouts { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AccountApi> AccountApis { get; set; }
        public virtual DbSet<AccountCash> AccountCashes { get; set; }
        public virtual DbSet<AccountCashType> AccountCashTypes { get; set; }
        public virtual DbSet<AccountCoupon> AccountCoupons { get; set; }
        public virtual DbSet<AccountFunction> AccountFunctions { get; set; }
        public virtual DbSet<AccountGroupAccount> AccountGroupAccounts { get; set; }
        public virtual DbSet<AccountSystem> AccountSystems { get; set; }
        public virtual DbSet<AccountType> AccountTypes { get; set; }
        public virtual DbSet<Achievement> Achievements { get; set; }
        public virtual DbSet<AggregatedCounter> AggregatedCounters { get; set; }
        public virtual DbSet<Api> Apis { get; set; }
        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<ArticleCategory> ArticleCategories { get; set; }
        public virtual DbSet<Assess> Assesses { get; set; }
        public virtual DbSet<Attribute> Attributes { get; set; }
        public virtual DbSet<Banner> Banners { get; set; }
        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Cash> Cashes { get; set; }
        public virtual DbSet<CashGroup> CashGroups { get; set; }
        public virtual DbSet<CashObject> CashObjects { get; set; }
        public virtual DbSet<CashType> CashTypes { get; set; }
        public virtual DbSet<Collection> Collections { get; set; }
        public virtual DbSet<CollectionCategory> CollectionCategories { get; set; }
        public virtual DbSet<CompanySystem> CompanySystems { get; set; }
        public virtual DbSet<ConfigSystem> ConfigSystems { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Counter> Counters { get; set; }
        public virtual DbSet<Coupon> Coupons { get; set; }
        public virtual DbSet<CouponType> CouponTypes { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerType> CustomerTypes { get; set; }
        public virtual DbSet<Debt> Debts { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Faq> Faqs { get; set; }
        public virtual DbSet<Feedback> Feedbacks { get; set; }
        public virtual DbSet<FetchDataAudit> FetchDataAudits { get; set; }
        public virtual DbSet<Function> Functions { get; set; }
        public virtual DbSet<FunctionApi> FunctionApis { get; set; }
        public virtual DbSet<FunctionGroupFunction> FunctionGroupFunctions { get; set; }
        public virtual DbSet<Gallery> Galleries { get; set; }
        public virtual DbSet<GalleryCategory> GalleryCategories { get; set; }
        public virtual DbSet<GroupAccount> GroupAccounts { get; set; }
        public virtual DbSet<GroupFunction> GroupFunctions { get; set; }
        public virtual DbSet<Hash> Hashes { get; set; }
        public virtual DbSet<HistoryType> HistoryTypes { get; set; }
        public virtual DbSet<Introduction> Introductions { get; set; }
        public virtual DbSet<IntroductionType> IntroductionTypes { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobParameter> JobParameters { get; set; }
        public virtual DbSet<JobQueue> JobQueues { get; set; }
        public virtual DbSet<List> Lists { get; set; }
        public virtual DbSet<MerchantProductByCategory> MerchantProductByCategories { get; set; }
        public virtual DbSet<MerchantSetting> MerchantSettings { get; set; }
        public virtual DbSet<MiniService> MiniServices { get; set; }
        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<OrderStatus> OrderStatuses { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<Page> Pages { get; set; }
        public virtual DbSet<PageType> PageTypes { get; set; }
        public virtual DbSet<Partner> Partners { get; set; }
        public virtual DbSet<PartnerType> PartnerTypes { get; set; }
        public virtual DbSet<PayType> PayTypes { get; set; }
        public virtual DbSet<PositionBanner> PositionBanners { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductAttribute> ProductAttributes { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<ProductCollection> ProductCollections { get; set; }
        public virtual DbSet<ProductShelf> ProductShelves { get; set; }
        public virtual DbSet<ProductTab> ProductTabs { get; set; }
        public virtual DbSet<ProductWardrobe> ProductWardrobes { get; set; }
        public virtual DbSet<ProductWarehouse> ProductWarehouses { get; set; }
        public virtual DbSet<ProductWishlist> ProductWishlists { get; set; }
        public virtual DbSet<Promotion> Promotions { get; set; }
        public virtual DbSet<PromotionCategory> PromotionCategories { get; set; }
        public virtual DbSet<Province> Provinces { get; set; }
        public virtual DbSet<Recruitment> Recruitments { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
        public virtual DbSet<Schema> Schemas { get; set; }
        public virtual DbSet<Server> Servers { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<ServiceCategory> ServiceCategories { get; set; }
        public virtual DbSet<Set> Sets { get; set; }
        public virtual DbSet<Shelf> Shelves { get; set; }
        public virtual DbSet<ShippingFee> ShippingFees { get; set; }
        public virtual DbSet<Slider> Sliders { get; set; }
        public virtual DbSet<StaffDepartment> StaffDepartments { get; set; }
        public virtual DbSet<StaffType> StaffTypes { get; set; }
        public virtual DbSet<StaffWarehouse> StaffWarehouses { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Subscribe> Subscribes { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<Tab> Tabs { get; set; }
        public virtual DbSet<Transport> Transports { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<Video> Videos { get; set; }
        public virtual DbSet<VideoCategory> VideoCategories { get; set; }
        public virtual DbSet<Wardrobe> Wardrobes { get; set; }
        public virtual DbSet<WardrobeCategory> WardrobeCategories { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<WarehouseHistory> WarehouseHistories { get; set; }
        public virtual DbSet<WarehouseHistoryDetail> WarehouseHistoryDetails { get; set; }
        public virtual DbSet<staff> staff { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("DBConnection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("banhang")
                .HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<About>(entity =>
            {
                entity.ToTable("About", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_About_CreateBy");

                entity.Property(e => e.Address).HasMaxLength(300);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Blog)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(1024);

                entity.Property(e => e.Email)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Facebook)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Hotline)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Instagram)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Linkedin)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Logo)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Map).HasColumnType("ntext");

                entity.Property(e => e.MetaImage).HasMaxLength(1024);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.PageId).HasMaxLength(255);

                entity.Property(e => e.Phone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pinterest)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShortContent).HasColumnType("ntext");

                entity.Property(e => e.Tel)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.Twitter).HasMaxLength(1024);

                entity.Property(e => e.Website)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Youtube)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Zalo)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Abouts)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_About_Account");
            });

            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("Account", "banhang");

                entity.HasIndex(e => e.AccountTypeId, "IX_Account_AccountTypeId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordResetTime).HasColumnType("datetime");

                entity.Property(e => e.ResetToken).HasMaxLength(1024);

                entity.Property(e => e.ResetTokenExpiresTime).HasColumnType("datetime");

                entity.Property(e => e.UserName).HasMaxLength(100);

                entity.Property(e => e.VerificationToken).HasMaxLength(1024);

                entity.Property(e => e.VerificationTokenExpiresTime).HasColumnType("datetime");

                entity.Property(e => e.VerifiedTime).HasColumnType("datetime");

                entity.HasOne(d => d.AccountType)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.AccountTypeId)
                    .HasConstraintName("FK_Account_AccountType");
            });

            modelBuilder.Entity<AccountApi>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("AccountApi", "banhang");

                entity.HasIndex(e => e.AccountId, "IX_AccountApi_AccountId");

                entity.HasIndex(e => e.ApiId, "IX_AccountApi_ApiId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Account)
                    .WithMany()
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccountApi_Account");

                entity.HasOne(d => d.Api)
                    .WithMany()
                    .HasForeignKey(d => d.ApiId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccountApi_Api");
            });

            modelBuilder.Entity<AccountCash>(entity =>
            {
                entity.ToTable("AccountCash", "banhang");

                entity.Property(e => e.BalanceValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<AccountCashType>(entity =>
            {
                entity.ToTable("AccountCashType", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<AccountCoupon>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.CouponId });

                entity.ToTable("AccountCoupon", "banhang");

                entity.HasIndex(e => e.CouponId, "IX_AccountCoupon_CouponId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.AccountCoupons)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccountCoupon_AccountCoupon");

                entity.HasOne(d => d.Coupon)
                    .WithMany(p => p.AccountCoupons)
                    .HasForeignKey(d => d.CouponId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccountCoupon_Coupon");
            });

            modelBuilder.Entity<AccountFunction>(entity =>
            {
                entity.HasKey(e => new { e.FunctionId, e.AccountId });

                entity.ToTable("AccountFunction", "banhang");

                entity.HasIndex(e => e.AccountId, "IX_AccountFunction_AccountId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.AccountFunctions)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccountFunction_Account");

                entity.HasOne(d => d.Function)
                    .WithMany(p => p.AccountFunctions)
                    .HasForeignKey(d => d.FunctionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccountFunction_Function");
            });

            modelBuilder.Entity<AccountGroupAccount>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.GroupAccountId });

                entity.ToTable("AccountGroupAccount", "banhang");

                entity.HasIndex(e => e.GroupAccountId, "IX_AccountGroupAccount_GroupAccountId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.AccountGroupAccounts)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccountGroupAccount_Account");

                entity.HasOne(d => d.GroupAccount)
                    .WithMany(p => p.AccountGroupAccounts)
                    .HasForeignKey(d => d.GroupAccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccountGroupAccount_GroupAccount");
            });

            modelBuilder.Entity<AccountSystem>(entity =>
            {
                entity.ToTable("AccountSystem", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UserName).HasMaxLength(100);
            });

            modelBuilder.Entity<AccountType>(entity =>
            {
                entity.ToTable("AccountType", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Key)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Achievement>(entity =>
            {
                entity.ToTable("Achievement", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Achievement_CreateBy");

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Achievements)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Achievement_Account");
            });

            modelBuilder.Entity<AggregatedCounter>(entity =>
            {
                entity.HasKey(e => e.Key)
                    .HasName("PK_HangFire_CounterAggregated");

                entity.ToTable("AggregatedCounter", "HangFire");

                entity.HasIndex(e => e.ExpireAt, "IX_HangFire_AggregatedCounter_ExpireAt")
                    .HasFilter("([ExpireAt] IS NOT NULL)");

                entity.Property(e => e.Key).HasMaxLength(100);

                entity.Property(e => e.ExpireAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<Api>(entity =>
            {
                entity.ToTable("Api", "banhang");

                entity.HasIndex(e => e.ModuleId, "IX_Api_ModuleId");

                entity.Property(e => e.Action).HasMaxLength(300);

                entity.Property(e => e.Controller).HasMaxLength(300);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.Url)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.Apis)
                    .HasForeignKey(d => d.ModuleId)
                    .HasConstraintName("FK_Api_Module");
            });

            modelBuilder.Entity<Article>(entity =>
            {
                entity.ToTable("Article", "banhang");

                entity.HasIndex(e => e.ArticleCategoryId, "IX_Article_ArticleCategoryId");

                entity.HasIndex(e => e.CreateBy, "IX_Article_CreateBy");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.SourceLink).HasMaxLength(1024);

                entity.Property(e => e.SourcePage).HasMaxLength(1024);

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.ValueAssess).HasColumnType("decimal(5, 1)");

                entity.HasOne(d => d.ArticleCategory)
                    .WithMany(p => p.Articles)
                    .HasForeignKey(d => d.ArticleCategoryId)
                    .HasConstraintName("FK_Article_ArticleCategory");

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Articles)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Article_Account");
            });

            modelBuilder.Entity<ArticleCategory>(entity =>
            {
                entity.ToTable("ArticleCategory", "banhang");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Assess>(entity =>
            {
                entity.ToTable("Assess", "banhang");

                entity.HasIndex(e => e.AccountId, "IX_Assess_AccountId");

                entity.Property(e => e.Content).HasMaxLength(500);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FullName).HasMaxLength(300);

                entity.Property(e => e.KeyName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Assesses)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_Assess_Account");
            });

            modelBuilder.Entity<Attribute>(entity =>
            {
                entity.ToTable("Attribute", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Attribute_CreateBy");

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Attributes)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Properties_Account");
            });

            modelBuilder.Entity<Banner>(entity =>
            {
                entity.ToTable("Banner", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Banner_CreateBy");

                entity.HasIndex(e => e.PositionBannerId, "IX_Banner_PositionBannerId");

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.Url)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Banners)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Banner_Account");

                entity.HasOne(d => d.PositionBanner)
                    .WithMany(p => p.Banners)
                    .HasForeignKey(d => d.PositionBannerId)
                    .HasConstraintName("FK_Banner_PositionBanner");
            });

            modelBuilder.Entity<Branch>(entity =>
            {
                entity.ToTable("Branch", "banhang");

                entity.HasIndex(e => e.DistrictId, "IX_Branch_DistrictId");

                entity.HasIndex(e => e.PartnerId, "IX_Branch_PartnerId");

                entity.HasIndex(e => e.ProvinceId, "IX_Branch_ProvinceId");

                entity.Property(e => e.Address).HasMaxLength(250);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(1024);

                entity.Property(e => e.MetaDescription).HasColumnName("Meta_Description");

                entity.Property(e => e.MetaKeywords)
                    .HasMaxLength(255)
                    .HasColumnName("Meta_Keywords");

                entity.Property(e => e.MetaTitle)
                    .HasMaxLength(255)
                    .HasColumnName("Meta_Title");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.Url).HasMaxLength(255);

                entity.HasOne(d => d.District)
                    .WithMany(p => p.Branches)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_Branch_District");

                entity.HasOne(d => d.Partner)
                    .WithMany(p => p.Branches)
                    .HasForeignKey(d => d.PartnerId)
                    .HasConstraintName("FK_Branch_Partner");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.Branches)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_Branch_Province");
            });

            modelBuilder.Entity<Cash>(entity =>
            {
                entity.ToTable("Cash", "banhang");

                entity.HasIndex(e => e.AccountCashId, "IX_Cash_AccountCashId");

                entity.HasIndex(e => e.CashGroupId, "IX_Cash_CashGroupId");

                entity.HasIndex(e => e.CashObjectId, "IX_Cash_CashObjectId");

                entity.HasIndex(e => e.CashTypeId, "IX_Cash_CashTypeId");

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.ReceiverName).HasMaxLength(500);

                entity.Property(e => e.Value).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.AccountCash)
                    .WithMany(p => p.Cashes)
                    .HasForeignKey(d => d.AccountCashId)
                    .HasConstraintName("FK_Cash_AccountCash");

                entity.HasOne(d => d.CashGroup)
                    .WithMany(p => p.Cashes)
                    .HasForeignKey(d => d.CashGroupId)
                    .HasConstraintName("FK_Cash_CashGroup");

                entity.HasOne(d => d.CashObject)
                    .WithMany(p => p.Cashes)
                    .HasForeignKey(d => d.CashObjectId)
                    .HasConstraintName("FK_Cash_CashSubject");

                entity.HasOne(d => d.CashType)
                    .WithMany(p => p.Cashes)
                    .HasForeignKey(d => d.CashTypeId)
                    .HasConstraintName("FK_Cash_CashType");
            });

            modelBuilder.Entity<CashGroup>(entity =>
            {
                entity.ToTable("CashGroup", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<CashObject>(entity =>
            {
                entity.ToTable("CashObject", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<CashType>(entity =>
            {
                entity.ToTable("CashType", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Collection>(entity =>
            {
                entity.ToTable("Collection", "banhang");

                entity.HasIndex(e => e.CollectionCategoryId, "IX_Collection_CollectionCategoryId");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(1024);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CollectionCategory)
                    .WithMany(p => p.Collections)
                    .HasForeignKey(d => d.CollectionCategoryId)
                    .HasConstraintName("FK_Collection_CollectionCategory");
            });

            modelBuilder.Entity<CollectionCategory>(entity =>
            {
                entity.ToTable("CollectionCategory", "banhang");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<CompanySystem>(entity =>
            {
                entity.ToTable("CompanySystem", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_CompanySystem_CreateBy");

                entity.Property(e => e.Address).HasMaxLength(300);

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Hotline)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ShortContent).HasColumnType("ntext");

                entity.Property(e => e.Tel)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.Website)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.CompanySystems)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_CompanySystem_Account");
            });

            modelBuilder.Entity<ConfigSystem>(entity =>
            {
                entity.ToTable("ConfigSystem", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_ConfigSystem_CreateBy");

                entity.Property(e => e.TimeStart)
                    .HasColumnType("time(7)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TimeEnd)
                    .HasColumnType("time(7)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Key)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Values)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.ConfigSystems)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_ConfigSystem_Account");
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.ToTable("Contact", "banhang");

                entity.HasIndex(e => e.ApproveBy, "IX_Contact_ApproveBy");

                entity.Property(e => e.Address).HasMaxLength(300);

                entity.Property(e => e.Content).HasMaxLength(4000);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.FullName).HasMaxLength(300);

                entity.Property(e => e.Mobi).HasMaxLength(13);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Subject).HasMaxLength(250);

                entity.HasOne(d => d.ApproveByNavigation)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.ApproveBy)
                    .HasConstraintName("FK_Contact_Account");
            });

            modelBuilder.Entity<Counter>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Counter", "HangFire");

                entity.HasIndex(e => e.Key, "CX_HangFire_Counter")
                    .IsClustered();

                entity.Property(e => e.ExpireAt).HasColumnType("datetime");

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Coupon>(entity =>
            {
                entity.ToTable("Coupon", "banhang");

                entity.HasIndex(e => e.CouponTypeId, "IX_Coupon_CouponTypeId");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsHot)
                    .IsRequired()
                    .HasDefaultValueSql("(CONVERT([bit],(0)))");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.PriceDiscount).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.CouponType)
                    .WithMany(p => p.Coupons)
                    .HasForeignKey(d => d.CouponTypeId);
            });

            modelBuilder.Entity<CouponType>(entity =>
            {
                entity.ToTable("CouponTypes", "banhang");

                entity.Property(e => e.Code).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Domain).HasMaxLength(1024);

                entity.Property(e => e.Logo).HasMaxLength(1024);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("Customer", "banhang");

                entity.HasIndex(e => e.AccountId, "IX_Customer_AccountId");

                entity.HasIndex(e => e.CustomerTypeId, "IX_Customer_CustomerTypeId");

                entity.HasIndex(e => e.DistrictId, "IX_Customer_DistrictId");

                entity.HasIndex(e => e.OrganizationId, "IX_Customer_OrganizationId");

                entity.HasIndex(e => e.ProvinceId, "IX_Customer_ProvinceId");

                entity.Property(e => e.Address).HasMaxLength(300);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Birthday)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName).HasMaxLength(1000);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.FullName).HasMaxLength(300);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Tel)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_Customer_Account");

                entity.HasOne(d => d.CustomerType)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.CustomerTypeId)
                    .HasConstraintName("FK_Customer_CustomerType");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_Customer_District");

                entity.HasOne(d => d.Organization)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.OrganizationId)
                    .HasConstraintName("FK_Customer_Organization");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_Customer_Province");
            });

            modelBuilder.Entity<CustomerType>(entity =>
            {
                entity.ToTable("CustomerType", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Debt>(entity =>
            {
                entity.ToTable("Debt", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Debt_CreateBy");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Debt1)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("Debt");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Receivable).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ReceiverName).HasMaxLength(500);

                entity.Property(e => e.SumDebt).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SumReceivable).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Debts)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Debt_Account");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.ToTable("Department", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<District>(entity =>
            {
                entity.ToTable("District", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_District_CreateBy");

                entity.HasIndex(e => e.ProvinceId, "IX_District_ProvinceId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Districts)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_District_Account");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.Districts)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_District_Province");
            });

            modelBuilder.Entity<Faq>(entity =>
            {
                entity.ToTable("Faq", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Faq_CreateBy");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Faqs)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Faq_Account");
            });

            modelBuilder.Entity<Feedback>(entity =>
            {
                entity.ToTable("Feedback", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Feedback_CreateBy");

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Content).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FullName).HasMaxLength(300);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Regency).HasMaxLength(1024);

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Feedbacks)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Feedback_Account");
            });

            modelBuilder.Entity<FetchDataAudit>(entity =>
            {
                entity.ToTable("FetchDataAudits", "banhang");
            });

            modelBuilder.Entity<Function>(entity =>
            {
                entity.ToTable("Function", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Function_CreateBy");

                entity.Property(e => e.Action)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Controller)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Note).HasColumnType("ntext");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Functions)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Function_Account");
            });

            modelBuilder.Entity<FunctionApi>(entity =>
            {
                entity.HasKey(e => new { e.ApiId, e.FunctionId });

                entity.ToTable("FunctionApi", "banhang");

                entity.HasIndex(e => e.FunctionId, "IX_FunctionApi_FunctionId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Api)
                    .WithMany(p => p.FunctionApis)
                    .HasForeignKey(d => d.ApiId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FunctionApi_Api");

                entity.HasOne(d => d.Function)
                    .WithMany(p => p.FunctionApis)
                    .HasForeignKey(d => d.FunctionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FunctionApi_Function");
            });

            modelBuilder.Entity<FunctionGroupFunction>(entity =>
            {
                entity.HasKey(e => new { e.GroupFunctionId, e.FunctionId });

                entity.ToTable("FunctionGroupFunction", "banhang");

                entity.HasIndex(e => e.FunctionId, "IX_FunctionGroupFunction_FunctionId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Function)
                    .WithMany(p => p.FunctionGroupFunctions)
                    .HasForeignKey(d => d.FunctionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FunctionGroupFunction_Function");

                entity.HasOne(d => d.GroupFunction)
                    .WithMany(p => p.FunctionGroupFunctions)
                    .HasForeignKey(d => d.GroupFunctionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FunctionGroupFunction_GroupFunction");
            });

            modelBuilder.Entity<Gallery>(entity =>
            {
                entity.ToTable("Gallery", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Gallery_CreateBy");

                entity.HasIndex(e => e.GalleryCategoryId, "IX_Gallery_GalleryCategoryId");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Galleries)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Gallery_Account");

                entity.HasOne(d => d.GalleryCategory)
                    .WithMany(p => p.Galleries)
                    .HasForeignKey(d => d.GalleryCategoryId)
                    .HasConstraintName("FK_Gallery_GalleryCategory");
            });

            modelBuilder.Entity<GalleryCategory>(entity =>
            {
                entity.ToTable("GalleryCategory", "banhang");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<GroupAccount>(entity =>
            {
                entity.ToTable("GroupAccount", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Key)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<GroupFunction>(entity =>
            {
                entity.ToTable("GroupFunction", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Hash>(entity =>
            {
                entity.HasKey(e => new { e.Key, e.Field })
                    .HasName("PK_HangFire_Hash");

                entity.ToTable("Hash", "HangFire");

                entity.HasIndex(e => e.ExpireAt, "IX_HangFire_Hash_ExpireAt")
                    .HasFilter("([ExpireAt] IS NOT NULL)");

                entity.Property(e => e.Key).HasMaxLength(100);

                entity.Property(e => e.Field).HasMaxLength(100);
            });

            modelBuilder.Entity<HistoryType>(entity =>
            {
                entity.ToTable("HistoryType", "banhang");

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Introduction>(entity =>
            {
                entity.ToTable("Introduction", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Introduction_CreateBy");

                entity.HasIndex(e => e.IntroductionTypeId, "IX_Introduction_IntroductionTypeId");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Icon)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.Url)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Introductions)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Introduction_Account");

                entity.HasOne(d => d.IntroductionType)
                    .WithMany(p => p.Introductions)
                    .HasForeignKey(d => d.IntroductionTypeId)
                    .HasConstraintName("FK_Introduction_IntroductionType");
            });

            modelBuilder.Entity<IntroductionType>(entity =>
            {
                entity.ToTable("IntroductionType", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.ToTable("Job", "HangFire");

                entity.HasIndex(e => e.ExpireAt, "IX_HangFire_Job_ExpireAt")
                    .HasFilter("([ExpireAt] IS NOT NULL)");

                entity.HasIndex(e => e.StateName, "IX_HangFire_Job_StateName")
                    .HasFilter("([StateName] IS NOT NULL)");

                entity.Property(e => e.Arguments).IsRequired();

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ExpireAt).HasColumnType("datetime");

                entity.Property(e => e.InvocationData).IsRequired();

                entity.Property(e => e.StateName).HasMaxLength(20);
            });

            modelBuilder.Entity<JobParameter>(entity =>
            {
                entity.HasKey(e => new { e.JobId, e.Name })
                    .HasName("PK_HangFire_JobParameter");

                entity.ToTable("JobParameter", "HangFire");

                entity.Property(e => e.Name).HasMaxLength(40);

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.JobParameters)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK_HangFire_JobParameter_Job");
            });

            modelBuilder.Entity<JobQueue>(entity =>
            {
                entity.HasKey(e => new { e.Queue, e.Id })
                    .HasName("PK_HangFire_JobQueue");

                entity.ToTable("JobQueue", "HangFire");

                entity.Property(e => e.Queue).HasMaxLength(50);

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.FetchedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<List>(entity =>
            {
                entity.HasKey(e => new { e.Key, e.Id })
                    .HasName("PK_HangFire_List");

                entity.ToTable("List", "HangFire");

                entity.HasIndex(e => e.ExpireAt, "IX_HangFire_List_ExpireAt")
                    .HasFilter("([ExpireAt] IS NOT NULL)");

                entity.Property(e => e.Key).HasMaxLength(100);

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.ExpireAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<MerchantProductByCategory>(entity =>
            {
                entity.ToTable("MerchantProductByCategories", "banhang");

                entity.HasIndex(e => e.ProductCategoryId, "IX_MerchantProductByCategories_ProductCategoryId");

                entity.HasOne(d => d.ProductCategory)
                    .WithMany(p => p.MerchantProductByCategories)
                    .HasForeignKey(d => d.ProductCategoryId);
            });

            modelBuilder.Entity<MerchantSetting>(entity =>
            {
                entity.ToTable("MerchantSettings", "banhang");
            });

            modelBuilder.Entity<MiniService>(entity =>
            {
                entity.ToTable("MiniService", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_MiniService_CreateBy");

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.ShortTitle).HasMaxLength(300);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.Url)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.MiniServices)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_MiniService_Account");
            });

            modelBuilder.Entity<Module>(entity =>
            {
                entity.ToTable("Module", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("Order", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Order_CreateBy");

                entity.HasIndex(e => e.CustomerId, "IX_Order_CustomerId");

                entity.HasIndex(e => e.DistrictId, "IX_Order_DistrictId");

                entity.HasIndex(e => e.OrderStatusId, "IX_Order_OrderStatusId");

                entity.HasIndex(e => e.PayTypeId, "IX_Order_PayTypeId");

                entity.HasIndex(e => e.ProvinceId, "IX_Order_ProvinceId");

                entity.Property(e => e.Address).HasMaxLength(300);

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.FeeShip).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.FullName).HasMaxLength(300);

                entity.Property(e => e.Mobi).HasMaxLength(13);

                entity.Property(e => e.Mobi2).HasMaxLength(13);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.NoteFeeShip).HasMaxLength(200);

                entity.Property(e => e.NoteSale).HasMaxLength(200);

                entity.Property(e => e.RatingContent).HasMaxLength(1024);

                entity.Property(e => e.RatingTime).HasColumnType("datetime");

                entity.Property(e => e.Remark).HasMaxLength(1024);

                entity.Property(e => e.SaleOff).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TotalPrice).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.Partner)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.PartnerId)
                    .HasConstraintName("FK_Order_Partner");

                entity.HasOne(d => d.OrderStatus)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.OrderStatusId)
                    .HasConstraintName("FK_Order_OrderStatus");

                entity.HasOne(d => d.PayType)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.PayTypeId)
                    .HasConstraintName("FK_Order_PayType");
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.ToTable("OrderDetail", "banhang");

                entity.HasIndex(e => e.ColorId, "IX_OrderDetail_ColorId");

                entity.HasIndex(e => e.OrderId, "IX_OrderDetail_OrderId");

                entity.HasIndex(e => e.ProductId, "IX_OrderDetail_ProductId");

                entity.HasIndex(e => e.SizeId, "IX_OrderDetail_SizeId");

                entity.Property(e => e.Option).HasColumnType("ntext");

                entity.Property(e => e.OriginalPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.Color)
                    .WithMany(p => p.OrderDetailColors)
                    .HasForeignKey(d => d.ColorId)
                    .HasConstraintName("FK_OrderDetail_Color");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OrderDetail_Order");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OrderDetail_Product");

                entity.HasOne(d => d.Size)
                    .WithMany(p => p.OrderDetailSizes)
                    .HasForeignKey(d => d.SizeId)
                    .HasConstraintName("FK_OrderDetail_Size");
            });

            modelBuilder.Entity<OrderStatus>(entity =>
            {
                entity.ToTable("OrderStatus", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Organization>(entity =>
            {
                entity.ToTable("Organization", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Key)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Page>(entity =>
            {
                entity.ToTable("Page", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Page_CreateBy");

                entity.HasIndex(e => e.PageTypeId, "IX_Page_PageTypeId");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.Url)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Pages)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Page_Account");

                entity.HasOne(d => d.PageType)
                    .WithMany(p => p.Pages)
                    .HasForeignKey(d => d.PageTypeId)
                    .HasConstraintName("FK_Page_PageType");
            });

            modelBuilder.Entity<PageType>(entity =>
            {
                entity.ToTable("PageType", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_PageType_CreateBy");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.PageTypes)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_PageType_Account");
            });

            modelBuilder.Entity<Partner>(entity =>
            {
                entity.ToTable("Partner", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Partner_CreateBy");

                entity.Property(e => e.Address).HasMaxLength(300);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.BusinessItems).HasMaxLength(1024);

                entity.Property(e => e.BusinessName).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Fanpage).HasMaxLength(1024);

                entity.Property(e => e.FullName).HasMaxLength(1024);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.Url)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Partners)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Partner_Account");

                entity.HasOne(d => d.PartnerType)
                    .WithMany(p => p.Partners)
                    .HasForeignKey(d => d.PartnerTypeId)
                    .HasConstraintName("FK_Partner_PartnerType");
            });

            modelBuilder.Entity<PartnerType>(entity =>
            {
                entity.ToTable("PartnerType", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<PayType>(entity =>
            {
                entity.ToTable("PayType", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_PayType_CreateBy");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.PayTypes)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_PayType_Account");
            });

            modelBuilder.Entity<PositionBanner>(entity =>
            {
                entity.ToTable("PositionBanner", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Key)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Product_CreateBy");

                entity.HasIndex(e => e.ProductCategoryId, "IX_Product_ProductCategoryId");

                entity.HasIndex(e => e.SaleId, "IX_Product_SaleId");

                entity.Property(e => e.Aff_link).HasColumnName("Aff_link");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CommissionValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DiscountAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ImageListProduct).HasMaxLength(4000);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PriceAfterDiscount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PricePromo).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SaleDeadLine).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.TotalAssess).HasDefaultValueSql("((0))");

                entity.Property(e => e.ValueAssess)
                    .HasColumnType("decimal(5, 1)")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Partner)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.PartnerId)
                    .HasConstraintName("FK_Product_Partner");
            });

            modelBuilder.Entity<ProductAttribute>(entity =>
            {
                entity.ToTable("ProductAttribute", "banhang");

                entity.HasIndex(e => e.AttributeId, "IX_ProductAttribute_AttributeId");

                entity.HasIndex(e => e.ParentId, "IX_ProductAttribute_ParentId");

                entity.HasIndex(e => e.ProductId, "IX_ProductAttribute_ProductId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ImageList).HasColumnType("ntext");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PricePromo).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Attribute)
                    .WithMany(p => p.ProductAttributes)
                    .HasForeignKey(d => d.AttributeId)
                    .HasConstraintName("FK_ProductAttribute_Attribute");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_ProductAttribute_ProductAttribute");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductAttributes)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_ProductAttribute_Product");
            });

            modelBuilder.Entity<ProductCategory>(entity =>
            {
                entity.ToTable("ProductCategory", "banhang");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Logo).HasMaxLength(1024);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<ProductCollection>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.CollectionId })
                    .HasName("PK_ProductCollection_1");

                entity.ToTable("ProductCollection", "banhang");

                entity.HasIndex(e => e.CollectionId, "IX_ProductCollection_CollectionId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Collection)
                    .WithMany(p => p.ProductCollections)
                    .HasForeignKey(d => d.CollectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductCollection_Collection");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductCollections)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductCollection_Product");
            });

            modelBuilder.Entity<ProductShelf>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.ShelvesId });

                entity.ToTable("ProductShelves", "banhang");

                entity.HasIndex(e => e.ShelvesId, "IX_ProductShelves_ShelvesId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductShelves)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductShelves_Product");

                entity.HasOne(d => d.Shelves)
                    .WithMany(p => p.ProductShelves)
                    .HasForeignKey(d => d.ShelvesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductShelves_Shelves");
            });

            modelBuilder.Entity<ProductTab>(entity =>
            {
                entity.HasKey(e => new { e.TabId, e.ProductId });

                entity.ToTable("ProductTab", "banhang");

                entity.HasIndex(e => e.ProductId, "IX_ProductTab_ProductId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductTabs)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductTab_Product");

                entity.HasOne(d => d.Tab)
                    .WithMany(p => p.ProductTabs)
                    .HasForeignKey(d => d.TabId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductTab_Tab");
            });

            modelBuilder.Entity<ProductWardrobe>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.WardrobeId });

                entity.ToTable("ProductWardrobe", "banhang");

                entity.HasIndex(e => e.WardrobeId, "IX_ProductWardrobe_WardrobeId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductWardrobes)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductWardrobe_ProductWardrobe");

                entity.HasOne(d => d.Wardrobe)
                    .WithMany(p => p.ProductWardrobes)
                    .HasForeignKey(d => d.WardrobeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductWardrobe_Wardrobe");
            });

            modelBuilder.Entity<ProductWarehouse>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.WarehouseId });

                entity.ToTable("ProductWarehouse", "banhang");

                entity.HasIndex(e => e.WarehouseId, "IX_ProductWarehouse_WarehouseId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductWarehouses)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductWarehouse_Product");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.ProductWarehouses)
                    .HasForeignKey(d => d.WarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductWarehouse_Warehouse");
            });

            modelBuilder.Entity<ProductWishlist>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.CustomerId });

                entity.ToTable("ProductWishlist", "banhang");

                entity.HasIndex(e => e.CustomerId, "IX_ProductWishlist_CustomerId");

                entity.Property(e => e.CreateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.ProductWishlists)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductWishlist_Customer");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductWishlists)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductWishlist_ProductWishlist");
            });

            modelBuilder.Entity<Promotion>(entity =>
            {
                entity.ToTable("Promotion", "banhang");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.SourceLink).HasMaxLength(1024);

                entity.Property(e => e.SourcePage).HasMaxLength(1024);

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.ValueAssess).HasColumnType("decimal(5, 1)");

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Promotions)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Promotion_Account");

                entity.HasOne(d => d.PromotionCategory)
                    .WithMany(p => p.Promotions)
                    .HasForeignKey(d => d.PromotionCategoryId)
                    .HasConstraintName("FK_Promotion_PromotionCategory");
            });

            modelBuilder.Entity<PromotionCategory>(entity =>
            {
                entity.ToTable("PromotionCategory", "banhang");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.ToTable("Province", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FeeShip).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Recruitment>(entity =>
            {
                entity.ToTable("Recruitment", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Recruitment_CreateBy");

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Recruitments)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Recruitment_Account");
            });

            modelBuilder.Entity<Sale>(entity =>
            {
                entity.ToTable("Sale", "banhang");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.KeySale)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.PriceSale).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.ShortTitle).HasMaxLength(300);

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.TimeEnd).HasColumnType("datetime");

                entity.Property(e => e.TimeStart).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Schema>(entity =>
            {
                entity.HasKey(e => e.Version)
                    .HasName("PK_HangFire_Schema");

                entity.ToTable("Schema", "HangFire");

                entity.Property(e => e.Version).ValueGeneratedNever();
            });

            modelBuilder.Entity<Server>(entity =>
            {
                entity.ToTable("Server", "HangFire");

                entity.HasIndex(e => e.LastHeartbeat, "IX_HangFire_Server_LastHeartbeat");

                entity.Property(e => e.Id).HasMaxLength(200);

                entity.Property(e => e.LastHeartbeat).HasColumnType("datetime");
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.ToTable("Service", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Service_CreateBy");

                entity.HasIndex(e => e.ServiceCategoryId, "IX_Service_ServiceCategoryId");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.ValueAssess).HasColumnType("decimal(5, 1)");

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Services)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Service_Account");

                entity.HasOne(d => d.ServiceCategory)
                    .WithMany(p => p.Services)
                    .HasForeignKey(d => d.ServiceCategoryId)
                    .HasConstraintName("FK_Service_ServiceCategory");
            });

            modelBuilder.Entity<ServiceCategory>(entity =>
            {
                entity.ToTable("ServiceCategory", "banhang");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Set>(entity =>
            {
                entity.HasKey(e => new { e.Key, e.Value })
                    .HasName("PK_HangFire_Set");

                entity.ToTable("Set", "HangFire");

                entity.HasIndex(e => e.ExpireAt, "IX_HangFire_Set_ExpireAt")
                    .HasFilter("([ExpireAt] IS NOT NULL)");

                entity.HasIndex(e => new { e.Key, e.Score }, "IX_HangFire_Set_Score");

                entity.Property(e => e.Key).HasMaxLength(100);

                entity.Property(e => e.Value).HasMaxLength(256);

                entity.Property(e => e.ExpireAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<Shelf>(entity =>
            {
                entity.ToTable("Shelves", "banhang");

                entity.HasIndex(e => e.WarehouseId, "IX_Shelves_WarehouseId");

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.Shelves)
                    .HasForeignKey(d => d.WarehouseId)
                    .HasConstraintName("FK_Shelves_Warehouse");
            });

            modelBuilder.Entity<ShippingFee>(entity =>
            {
                entity.ToTable("ShippingFee", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_ShippingFee_CreateBy");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(1000);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.ShippingFees)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_ShippingFee_Account");
            });

            modelBuilder.Entity<Slider>(entity =>
            {
                entity.ToTable("Slider", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Slider_CreateBy");

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ListImage)
                    .IsUnicode(false)
                    .HasColumnName("LIstImage");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.Title1).HasMaxLength(255);

                entity.Property(e => e.Title2).HasMaxLength(255);

                entity.Property(e => e.Title3).HasMaxLength(255);

                entity.Property(e => e.Url)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Sliders)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Slider_Account");
            });

            modelBuilder.Entity<StaffDepartment>(entity =>
            {
                entity.HasKey(e => new { e.StaffId, e.DepartmentId });

                entity.ToTable("StaffDepartment", "banhang");

                entity.HasIndex(e => e.DepartmentId, "IX_StaffDepartment_DepartmentId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.StaffDepartments)
                    .HasForeignKey(d => d.DepartmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StaffDepartment_Department");

                entity.HasOne(d => d.Staff)
                    .WithMany(p => p.StaffDepartments)
                    .HasForeignKey(d => d.StaffId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StaffDepartment_Staff");
            });

            modelBuilder.Entity<StaffType>(entity =>
            {
                entity.ToTable("StaffType", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<StaffWarehouse>(entity =>
            {
                entity.HasKey(e => new { e.StaffId, e.WarehouseId });

                entity.ToTable("StaffWarehouse", "banhang");

                entity.HasIndex(e => e.WarehouseId, "IX_StaffWarehouse_WarehouseId");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Staff)
                    .WithMany(p => p.StaffWarehouses)
                    .HasForeignKey(d => d.StaffId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StaffWarehouse_Staff");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.StaffWarehouses)
                    .HasForeignKey(d => d.WarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StaffWarehouse_Warehouse");
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.HasKey(e => new { e.JobId, e.Id })
                    .HasName("PK_HangFire_State");

                entity.ToTable("State", "HangFire");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Reason).HasMaxLength(100);

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.States)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK_HangFire_State_Job");
            });

            modelBuilder.Entity<Subscribe>(entity =>
            {
                entity.ToTable("Subscribe", "banhang");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(255);
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.ToTable("Supplier", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Supplier_CreateBy");

                entity.Property(e => e.Address).HasMaxLength(300);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Email)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Hotline)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Tel)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Suppliers)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Supplier_Account");
            });

            modelBuilder.Entity<Tab>(entity =>
            {
                entity.ToTable("Tab", "banhang");

                entity.Property(e => e.Code)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Transport>(entity =>
            {
                entity.ToTable("Transport", "banhang");

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneStaffContact).HasMaxLength(500);

                entity.Property(e => e.StaffContact).HasMaxLength(500);

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Unit>(entity =>
            {
                entity.ToTable("Unit", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Unit_CreateBy");

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Units)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Unit_Account");
            });

            modelBuilder.Entity<Video>(entity =>
            {
                entity.ToTable("Video", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_Video_CreateBy");

                entity.HasIndex(e => e.VideoCategoryId, "IX_Video_VideoCategoryId");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.Property(e => e.ValueAssess).HasColumnType("decimal(5, 1)");

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Videos)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_Video_Account");

                entity.HasOne(d => d.VideoCategory)
                    .WithMany(p => p.Videos)
                    .HasForeignKey(d => d.VideoCategoryId)
                    .HasConstraintName("FK_Video_VideoCategory");
            });

            modelBuilder.Entity<VideoCategory>(entity =>
            {
                entity.ToTable("VideoCategory", "banhang");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Wardrobe>(entity =>
            {
                entity.ToTable("Wardrobe", "banhang");

                entity.HasIndex(e => e.WardrobeCategoryId, "IX_Wardrobe_WardrobeCategoryId");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(1024);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.WardrobeCategory)
                    .WithMany(p => p.Wardrobes)
                    .HasForeignKey(d => d.WardrobeCategoryId)
                    .HasConstraintName("FK_Wardrobe_WardrobeCategory");
            });

            modelBuilder.Entity<WardrobeCategory>(entity =>
            {
                entity.ToTable("WardrobeCategory", "banhang");

                entity.Property(e => e.Alias)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Schemas).HasColumnType("ntext");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<Warehouse>(entity =>
            {
                entity.ToTable("Warehouse", "banhang");

                entity.Property(e => e.Address).HasMaxLength(300);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(1024);
            });

            modelBuilder.Entity<WarehouseHistory>(entity =>
            {
                entity.ToTable("WarehouseHistory", "banhang");

                entity.HasIndex(e => e.CreateBy, "IX_WarehouseHistory_CreateBy");

                entity.HasIndex(e => e.HistoryTypeId, "IX_WarehouseHistory_HistoryTypeId");

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.Title).HasMaxLength(1024);

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.WarehouseHistories)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("FK_WarehouseHistory_Account");

                entity.HasOne(d => d.HistoryType)
                    .WithMany(p => p.WarehouseHistories)
                    .HasForeignKey(d => d.HistoryTypeId)
                    .HasConstraintName("FK_WarehouseHistory_HistoryType");
            });

            modelBuilder.Entity<WarehouseHistoryDetail>(entity =>
            {
                entity.HasKey(e => new { e.WarehouseHistoryId, e.ProductId, e.WarehouseId });

                entity.ToTable("WarehouseHistoryDetail", "banhang");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PriceSale).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.WarehouseHistory)
                    .WithMany(p => p.WarehouseHistoryDetails)
                    .HasForeignKey(d => d.WarehouseHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WarehouseHistoryDetail_WarehouseHistory");
            });

            modelBuilder.Entity<staff>(entity =>
            {
                entity.ToTable("Staff", "banhang");

                entity.HasIndex(e => e.OrganizationId, "IX_Staff_OrganizationId");

                entity.HasIndex(e => e.StaffTypeId, "IX_Staff_StaffTypeID");

                entity.Property(e => e.Address).HasMaxLength(300);

                entity.Property(e => e.Avatar).HasMaxLength(1024);

                entity.Property(e => e.Birthday)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Email)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Facebook)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FullName).HasMaxLength(300);

                entity.Property(e => e.Google).HasMaxLength(1024);

                entity.Property(e => e.ModifyTime).HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Regency).HasMaxLength(1024);

                entity.Property(e => e.Skype).HasMaxLength(1024);

                entity.Property(e => e.StaffTypeId).HasColumnName("StaffTypeID");

                entity.Property(e => e.Thumb).HasMaxLength(1024);

                entity.Property(e => e.Twitter).HasMaxLength(1024);

                entity.Property(e => e.Viber).HasMaxLength(1024);

                entity.Property(e => e.Zalo).HasMaxLength(1024);

                entity.HasOne(d => d.Organization)
                    .WithMany(p => p.staff)
                    .HasForeignKey(d => d.OrganizationId)
                    .HasConstraintName("FK_Staff_Organization");

                entity.HasOne(d => d.StaffType)
                    .WithMany(p => p.staff)
                    .HasForeignKey(d => d.StaffTypeId)
                    .HasConstraintName("FK_Staff_StaffType");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        public override Task<int> SaveChangesAsync(
           CancellationToken cancellationToken = new CancellationToken()
       )
        {
            foreach (var entry in ChangeTracker.Entries<BaseEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreateTime = _dateTime.NowUtc;
                        entry.Entity.ModifyTime = _dateTime.NowUtc;
                        break;

                    case EntityState.Modified:
                        entry.Entity.ModifyTime = _dateTime.NowUtc;
                        break;

                    case EntityState.Deleted:
                        entry.Entity.IsDelete = true;
                        break;
                }
            }
            return base.SaveChangesAsync(cancellationToken);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
