﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Account
    {
        public Account()
        {
            Abouts = new HashSet<About>();
            AccountCoupons = new HashSet<AccountCoupon>();
            AccountFunctions = new HashSet<AccountFunction>();
            AccountGroupAccounts = new HashSet<AccountGroupAccount>();
            Achievements = new HashSet<Achievement>();
            Articles = new HashSet<Article>();
            Assesses = new HashSet<Assess>();
            Attributes = new HashSet<Attribute>();
            Banners = new HashSet<Banner>();
            CompanySystems = new HashSet<CompanySystem>();
            ConfigSystems = new HashSet<ConfigSystem>();
            Contacts = new HashSet<Contact>();
            Customers = new HashSet<Customer>();
            Debts = new HashSet<Debt>();
            Districts = new HashSet<District>();
            Faqs = new HashSet<Faq>();
            Feedbacks = new HashSet<Feedback>();
            Functions = new HashSet<Function>();
            Galleries = new HashSet<Gallery>();
            Introductions = new HashSet<Introduction>();
            MiniServices = new HashSet<MiniService>();
            PageTypes = new HashSet<PageType>();
            Pages = new HashSet<Page>();
            Partners = new HashSet<Partner>();
            PayTypes = new HashSet<PayType>();
            Promotions = new HashSet<Promotion>();
            Recruitments = new HashSet<Recruitment>();
            Services = new HashSet<Service>();
            ShippingFees = new HashSet<ShippingFee>();
            Sliders = new HashSet<Slider>();
            Suppliers = new HashSet<Supplier>();
            Units = new HashSet<Unit>();
            Videos = new HashSet<Video>();
            WarehouseHistories = new HashSet<WarehouseHistory>();
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool? Status { get; set; }
        public bool? AcceptTerms { get; set; }
        public string VerificationToken { get; set; }
        public DateTime? VerificationTokenExpiresTime { get; set; }
        public DateTime? VerifiedTime { get; set; }
        public string ResetToken { get; set; }
        public DateTime? ResetTokenExpiresTime { get; set; }
        public DateTime? PasswordResetTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? AccountTypeId { get; set; }

        public virtual AccountType AccountType { get; set; }
        public virtual ICollection<About> Abouts { get; set; }
        public virtual ICollection<AccountCoupon> AccountCoupons { get; set; }
        public virtual ICollection<AccountFunction> AccountFunctions { get; set; }
        public virtual ICollection<AccountGroupAccount> AccountGroupAccounts { get; set; }
        public virtual ICollection<Achievement> Achievements { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<Assess> Assesses { get; set; }
        public virtual ICollection<Attribute> Attributes { get; set; }
        public virtual ICollection<Banner> Banners { get; set; }
        public virtual ICollection<CompanySystem> CompanySystems { get; set; }
        public virtual ICollection<ConfigSystem> ConfigSystems { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Debt> Debts { get; set; }
        public virtual ICollection<District> Districts { get; set; }
        public virtual ICollection<Faq> Faqs { get; set; }
        public virtual ICollection<Feedback> Feedbacks { get; set; }
        public virtual ICollection<Function> Functions { get; set; }
        public virtual ICollection<Gallery> Galleries { get; set; }
        public virtual ICollection<Introduction> Introductions { get; set; }
        public virtual ICollection<MiniService> MiniServices { get; set; }
        public virtual ICollection<PageType> PageTypes { get; set; }
        public virtual ICollection<Page> Pages { get; set; }
        public virtual ICollection<Partner> Partners { get; set; }
        public virtual ICollection<PayType> PayTypes { get; set; }
        public virtual ICollection<Promotion> Promotions { get; set; }
        public virtual ICollection<Recruitment> Recruitments { get; set; }
        public virtual ICollection<Service> Services { get; set; }
        public virtual ICollection<ShippingFee> ShippingFees { get; set; }
        public virtual ICollection<Slider> Sliders { get; set; }
        public virtual ICollection<Supplier> Suppliers { get; set; }
        public virtual ICollection<Unit> Units { get; set; }
        public virtual ICollection<Video> Videos { get; set; }
        public virtual ICollection<WarehouseHistory> WarehouseHistories { get; set; }
    }
}
