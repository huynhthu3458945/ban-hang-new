﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Data.Entities
{
    public partial class Cash
    {
        public int Id { get; set; }
        public int? AccountCashId { get; set; }
        public string Code { get; set; }
        public int? CashTypeId { get; set; }
        public int? CashGroupId { get; set; }
        public decimal? Value { get; set; }
        public int? CashObjectId { get; set; }
        public bool? IsLock { get; set; }
        public string Note { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? Receiver { get; set; }
        public string ReceiverName { get; set; }
        public bool? IsDebt { get; set; }
        public int? TransactionType { get; set; }

        public virtual AccountCash AccountCash { get; set; }
        public virtual CashGroup CashGroup { get; set; }
        public virtual CashObject CashObject { get; set; }
        public virtual CashType CashType { get; set; }
    }
}
