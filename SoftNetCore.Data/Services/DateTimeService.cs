﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Data.Services
{
    public interface IDateTimeService
    {
        DateTime NowUtc { get; }
    }

    public class DateTimeService : IDateTimeService
    {
        public DateTime NowUtc => DateTime.UtcNow;
    }
}
