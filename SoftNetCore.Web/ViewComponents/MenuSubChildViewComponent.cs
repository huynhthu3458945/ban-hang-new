﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class MenuSubChildViewComponent : ViewComponent
    {
        #region Fields
        private readonly IProductCategoryService _productCategoryService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public MenuSubChildViewComponent(IProductCategoryService productCategoryService, ILogging logging)
        {
            _productCategoryService = productCategoryService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync(int parentId)
        {
            var data = await _productCategoryService.GetAllGroupAsync(parentId);
            return View(data);
        }
    }
}
