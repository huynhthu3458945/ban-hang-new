﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoftNetCore.DataAccess.ViewModel;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class QuickCouponViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(List<CouponViewModel> model)
        {
            if (model != null && model.Count > 0)
                foreach (var pro in model)
                {
                    pro.CouponDetails = JsonConvert.DeserializeObject<List<CouponDetailViewModel>>(pro.Coupons);
                }
            return View(model);
        }
    }
}
