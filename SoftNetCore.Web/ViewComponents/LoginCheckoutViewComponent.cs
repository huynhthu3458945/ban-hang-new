﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Model.Request;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class LoginCheckoutViewComponent : ViewComponent
    {
        #region Fields
        private readonly ICustomerService _customerService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public LoginCheckoutViewComponent(ICustomerService customerService, ILogging logging)
        {
            _customerService = customerService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            AuthenticateRequest model = new AuthenticateRequest();
            model.ReturnUrl = "/thanh-toan";
            return View(model);
        }
    }
}
