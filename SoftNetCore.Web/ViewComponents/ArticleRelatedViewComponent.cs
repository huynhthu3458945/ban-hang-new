﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ArticleRelatedViewComponent : ViewComponent
    {
        #region Fields
        private readonly IArticleService _articleService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ArticleRelatedViewComponent(IArticleService articleService, ILogging logging)
        {
            _articleService = articleService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync(int id, int catId)
        {
            var result = await _articleService.GetRelatedAsync(id, catId, 2);
            return View(result);
        }
    }
}
