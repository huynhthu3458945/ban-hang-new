﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class HomeBannerViewComponent : ViewComponent
    {
        #region Fields
        private readonly IBannerService _bannerService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public HomeBannerViewComponent(IBannerService bannerService, ILogging logging)
        {
            _bannerService = bannerService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var data = await _bannerService.GetAllAsync();
            return View(data);
        }
    }
}
