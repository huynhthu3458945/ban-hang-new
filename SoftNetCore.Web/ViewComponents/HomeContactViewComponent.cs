﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class HomeContactViewComponent : ViewComponent
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public HomeContactViewComponent(IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            ViewBag.About = await _aboutService.GetFisrtAsync(1);
            ContactModel model = new ContactModel();
            return View(model);
        }
    }
}
