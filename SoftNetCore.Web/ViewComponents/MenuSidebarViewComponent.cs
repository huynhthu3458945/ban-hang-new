﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class MenuSidebarViewComponent : ViewComponent
    {
        #region Fields
        private readonly IProductCategoryService _productCategoryService;
        private readonly IArticleCategoryService _articleCategoryService;
        private readonly ISaleService _saleService;
        private readonly ICollectionService _collectionService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public MenuSidebarViewComponent(IProductCategoryService productCategoryService, IArticleCategoryService articleCategoryService,
                                 ISaleService saleService, ICollectionService collectionService, ILogging logging)
        {
            _productCategoryService = productCategoryService;
            _articleCategoryService = articleCategoryService;
            _saleService = saleService;
            _collectionService = collectionService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var productCategories = await _productCategoryService.GetParentAsync();
            ViewBag.ProductCategoryList = productCategories;

            return View();
        }
    }
}
