﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class PartnerViewComponent : ViewComponent
    {
        #region Fields
        private readonly IPartnerService _partnerService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public PartnerViewComponent(IPartnerService partnerService, ILogging logging)
        {
            _partnerService = partnerService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var data = await _partnerService.GetByStatusAsync(true);
            return View(data);
        }
    }
}
