﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ProductPriceReviewViewComponent : ViewComponent
    {
        #region Fields
        private readonly IProductAttributeService _productAttributeService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ProductPriceReviewViewComponent(IProductAttributeService productAttributeService, ILogging logging)
        {
            _productAttributeService = productAttributeService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            var result = await _productAttributeService.FindById(id);
            return View(result);
        }
    }
}
