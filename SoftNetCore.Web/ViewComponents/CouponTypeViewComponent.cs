﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class CouponTypeViewComponent : ViewComponent
    {
        #region Fields
        private readonly ICouponTypeService _couponTypeService;
        private readonly ICouponService _couponService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public CouponTypeViewComponent(ICouponTypeService couponTypeService, ICouponService couponService, ILogging logging)
        {
            _couponTypeService = couponTypeService;
            _couponService = couponService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var res = await _couponTypeService.GetAllTotalTypeAsync();

            return View(res);
        }
    }
}
