﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ProductCollectionViewComponent : ViewComponent
    {
        #region Fields
        private readonly IProductCollectionService _productCollectionService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ProductCollectionViewComponent(IProductCollectionService productCollectionService, ILogging logging)
        {
            _productCollectionService = productCollectionService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            var data = await _productCollectionService.GetByCollectionAsync(id);
            return View(data);
        }
    }
}
