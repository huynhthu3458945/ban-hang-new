﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class SliderViewComponent : ViewComponent
    {
        #region Fields
        private readonly ISliderService _sliderService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public SliderViewComponent(ISliderService sliderService, ILogging logging)
        {
            _sliderService = sliderService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var data = await _sliderService.GetByStatusAsync(true);
            return View(data);
        }
    }
}
