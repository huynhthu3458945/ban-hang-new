﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ArticleNewViewComponent : ViewComponent
    {
        #region Fields
        private readonly IArticleService _articleService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ArticleNewViewComponent(IArticleService articleService, ILogging logging)
        {
            _articleService = articleService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = await _articleService.GetHotAsync(5);
            return View(result);
        }
    }
}
