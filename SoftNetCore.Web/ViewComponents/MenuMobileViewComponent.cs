﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using SoftNetCore.Common.Helpers;

namespace SoftNetCore.Web.ViewComponents
{
    public class MenuMobileViewComponent : ViewComponent
    {
        #region Fields
        private readonly IProductCategoryService _productCategoryService;
        private readonly IArticleCategoryService _articleCategoryService;
        private readonly ISaleService _saleService;
        private readonly ICollectionService _collectionService;
        private readonly ICustomerService _customerService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public MenuMobileViewComponent(IProductCategoryService productCategoryService, IArticleCategoryService articleCategoryService,
                                 ISaleService saleService, ICustomerService customerService, ICollectionService collectionService, ILogging logging)
        {
            _productCategoryService = productCategoryService;
            _articleCategoryService = articleCategoryService;
            _saleService = saleService;
            _collectionService = collectionService;
            _customerService = customerService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            ViewBag.ProductCategoryList = await _productCategoryService.GetParentAsync(true, true);
            ViewBag.ArticleCategoryList = await _articleCategoryService.GetByStatusAsync(true);
            ViewBag.SaleList = await _saleService.GetActiveAsync();
            ViewBag.CollectionList = await _collectionService.GetByStatusAsync(true);

            int customerId = 0;
            if (!string.IsNullOrEmpty(User.Identity.Name))
                customerId = (UserClaimsPrincipal.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.Name))?.Value).ToInt();

            ViewBag.Customer = await _customerService.FindByIdHaveOrder(customerId);
            return View();
        }
    }
}
