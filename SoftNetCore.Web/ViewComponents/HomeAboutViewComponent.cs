﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class HomeAboutViewComponent : ViewComponent
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public HomeAboutViewComponent(IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var data = await _aboutService.GetFisrtAsync(1);
            return View(data);
        }
    }
}
