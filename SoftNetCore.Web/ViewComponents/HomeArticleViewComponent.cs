﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class HomeArticleViewComponent : ViewComponent
    {
        #region Fields
        private readonly IArticleService _articleService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public HomeArticleViewComponent(IArticleService articleService, ILogging logging)
        {
            _articleService = articleService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var data = await _articleService.GetNewAsync(6);
            return View(data);
        }
    }
}
