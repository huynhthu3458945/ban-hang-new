﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ProductRelatedTitleViewComponent : ViewComponent
    {
        #region Fields
        private readonly IProductService _productService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ProductRelatedTitleViewComponent(IProductService productService, ILogging logging)
        {
            _productService = productService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync(int id, int catId)
        {
            var result = await _productService.GetRelatedAsync(id, catId, 5);
            return View(result);
        }
    }
}
