﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class SearchBranchViewComponent : ViewComponent
    {
        #region Fields
        private readonly IBranchService _branchService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public SearchBranchViewComponent(IBranchService branchService, ILogging logging)
        {
            _branchService = branchService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync(int? provinceId, int? districtId)
        {
            List<BranchModel> result = new List<BranchModel>();
            if (provinceId != null)
                result = await _branchService.GetByAddressAsync(provinceId, districtId);
            else
                result = await _branchService.GetRandomAsync(10);
            return View(result);
        }
    }
}
