﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SoftNetCore.Model.Request;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class CancelOrderViewComponent : ViewComponent
    {
        #region Fields
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public CancelOrderViewComponent(ILogging logging)
        {
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            StatusOrderRequest model = new StatusOrderRequest();
            model.OrderId = id;
            return View(model);
        }
    }
}
