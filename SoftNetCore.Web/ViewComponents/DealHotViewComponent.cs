﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.DataAccess.ViewModel;
using SoftNetCore.Services.Implementation;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class DealHotViewComponent : ViewComponent
    {
        #region Fields
        private readonly IBannerService _bannerService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public DealHotViewComponent(IBannerService bannerService, ILogging logging)
        {
            _bannerService = bannerService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var list = await _bannerService.GetNewAsync(10);
            return View(list);
        }
    }
}
