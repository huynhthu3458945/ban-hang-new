﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.DataAccess.ViewModel;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class CouponNewViewComponent : ViewComponent
    {
        #region Fields
        private readonly ICouponRepository _couponRepository;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public CouponNewViewComponent(ICouponRepository couponRepository, ILogging logging)
        {
            _couponRepository = couponRepository;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var res = await _couponRepository.GetByNewAsync(5);
            foreach (var coupon in res)
            {
                coupon.CouponDetails = JsonConvert.DeserializeObject<List<CouponDetailViewModel>>(coupon.Coupons);
            }
            return View(res);
        }
    }
}
