﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly IProductCategoryService _productCategoryService;
        //private readonly IArticleCategoryService _articleCategoryService;
        //private readonly ISaleService _saleService;
        //private readonly ICollectionService _collectionService;
        //private readonly ICustomerService _customerService;
        private readonly IAboutService _aboutService;
        private readonly ILogging _logging;

        public MenuViewComponent(
            IProductCategoryService productCategoryService,
            //IArticleCategoryService articleCategoryService,
            //ISaleService saleService,
            //ICustomerService customerService,
            //ICollectionService collectionService,
            IAboutService aboutService,
            ILogging logging)
        {
            _productCategoryService = productCategoryService;
            //_articleCategoryService = articleCategoryService;
            //_saleService = saleService;
            //_customerService = customerService;
            //_collectionService = collectionService;
            _aboutService = aboutService;
            _logging = logging;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var productCategories = await _productCategoryService.GetParentAsync();
            ViewBag.ProductCategoryList = productCategories;

            //var articleCategories = await _articleCategoryService.GetByStatusAsync(true);
            //ViewBag.ArticleCategoryList = articleCategories;

            //var saleActivated = await _saleService.GetActiveAsync();
            //ViewBag.SaleList = saleActivated;

            //var collections = await _collectionService.GetByStatusAsync(true);
            //ViewBag.CollectionList = collections;

            var about = await _aboutService.GetFisrtAsync(1);
            ViewBag.About = about;

            var cart = HttpContext.Session.GetString("cart");
            if (!string.IsNullOrEmpty(cart))
            {
                List<CartItem> listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
                ViewBag.CartList = listCart;
            }

            //if (!string.IsNullOrEmpty(User.Identity.Name))
            //{
            //    int customerId = (UserClaimsPrincipal.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.Name))?.Value).ToInt();
            //    ViewBag.Customer = await _customerService.FindByIdHaveOrder(customerId);
            //}

            return View();
        }
    }
}
