﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoftNetCore.DataAccess.ViewModel;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class QuickViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(List<ProductViewModel> model)
        {
            if(model != null && model.Count > 0)
                foreach(var pro in model)
                {
                    if(!string.IsNullOrEmpty(pro.ImageListProduct))
                        pro.ImageList = JsonConvert.DeserializeObject<List<string>>(pro.ImageListProduct);
                }
            return View(model);
        }
    }
}
