﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class HeaderOptionsViewComponent : ViewComponent
    {
        //#region Fields
        //private readonly IAboutService _aboutService;
        //private readonly ILogging _logging;
        //#endregion
        //#region Ctor
        //public HeaderOptionsViewComponent(IAboutService aboutService, ILogging logging)
        //{
        //    _aboutService = aboutService;
        //    _logging = logging;
        //}
        //#endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            double total = 0;
            List<CartItem> listCart = new List<CartItem>();
            var cart = HttpContext.Session.GetString("cart");
            if (!string.IsNullOrEmpty(cart))
            {
                listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
                total = listCart.Count.ToInt();
            }
            ViewBag.CartTotal = total;
            return View(listCart);
        }
    }
}
