﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ProductNewViewComponent : ViewComponent
    {
        #region Fields
        private readonly IProductRepository _productRepository;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ProductNewViewComponent(IProductRepository productRepository, ILogging logging)
        {
            _productRepository = productRepository;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var res = await _productRepository.GetByNewAsync(5);
            return View(res);
        }
    }
}
