﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class HomeProductViewComponent : ViewComponent
    {
        private readonly IProductRepository _productRepository;
        private readonly ILogging _logging;

        public HomeProductViewComponent(IProductRepository productRepository, ILogging logging)
        {
            _productRepository = productRepository;
            _logging = logging;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var products = await _productRepository.GetByRandomAsync(10);
            return View(products);
        }
    }
}
