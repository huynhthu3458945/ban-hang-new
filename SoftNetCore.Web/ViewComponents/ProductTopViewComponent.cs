﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.DataAccess.ViewModel;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ProductTopViewComponent : ViewComponent
    {
        #region Fields
        private readonly IProductRepository _productRepository;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ProductTopViewComponent(IProductRepository productRepository, ILogging logging)
        {
            _productRepository = productRepository;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var productSaleList = await _productRepository.GetBySaleAsync(3);
            var productHotList = await _productRepository.GetByHotAsync(3);
            var productNewList = await _productRepository.GetByNewAsync(3);
            var productRandomList = await _productRepository.GetByRandomAsync(3);

            var model = new ProductTopViewModel
            {
                ProductSaleList = productSaleList,
                ProductHotList = productHotList,
                ProductNewList = productNewList,
                ProductRandomList = productRandomList
            };

            return View(model);
        }
    }
}
