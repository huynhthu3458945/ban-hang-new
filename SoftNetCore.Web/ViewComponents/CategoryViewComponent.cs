﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class CategoryViewComponent : ViewComponent
    {
        private readonly IProductCategoryService _productCategoryService;
        private readonly IProductService _productService;
        private readonly ILogging _logging;

        public CategoryViewComponent(IProductCategoryService productCategoryService, IProductService productService, ILogging logging)
        {
            _productCategoryService = productCategoryService;
            _productService = productService;
            _logging = logging;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var res = await _productCategoryService.GetAllTotalCategoryAsync();
            return View(res);
        }
    }
}
