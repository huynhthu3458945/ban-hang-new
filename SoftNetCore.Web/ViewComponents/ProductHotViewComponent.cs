﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ProductHotViewComponent : ViewComponent
    {
        private readonly IProductRepository _productRepository;
        private readonly ILogging _logging;
        public ProductHotViewComponent(IProductRepository productRepository, ILogging logging)
        {
            _productRepository = productRepository;
            _logging = logging;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var products = await _productRepository.GetByHotAsync(15);
            return View(products);
        }
    }
}
