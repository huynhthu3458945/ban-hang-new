﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class HomePromotionViewComponent : ViewComponent
    {
        #region Fields
        private readonly IProductRepository _productRepository;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public HomePromotionViewComponent(IProductRepository productRepository, ILogging logging)
        {
            _productRepository = productRepository;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var promotions = await _productRepository.GetBySaleAsync(10);

            return View(promotions);
        }
    }
}
