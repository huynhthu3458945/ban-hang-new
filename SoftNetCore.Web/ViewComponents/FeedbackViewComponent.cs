﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class FeedbackViewComponent : ViewComponent
    {
        #region Fields
        private readonly IFeedbackService _feedbackService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public FeedbackViewComponent(IFeedbackService feedbackService, ILogging logging)
        {
            _feedbackService = feedbackService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = await _feedbackService.GetByStatusAsync(true);
            return View(result);
        }
    }
}
