﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ProductRattingViewComponent : ViewComponent
    {
        #region Fields
        private readonly IAssessService _assessService;
        private readonly IProductService _productService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ProductRattingViewComponent(IAssessService assessService, IProductService productService, ILogging logging)
        {
            _assessService = assessService;
            _productService = productService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync(int id, int total, decimal value)
        {
            ViewBag.TotalAssess = total;
            ViewBag.ValueAssess = value;
            var result = await _assessService.GetAllByKey(id, "Product");
            return View(result);
        }
    }
}
