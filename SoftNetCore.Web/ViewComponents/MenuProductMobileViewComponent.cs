﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class MenuProductMobileViewComponent : ViewComponent
    {
        #region Fields
        private readonly IProductCategoryService _productCategoryService;
        private readonly ISaleService _saleService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public MenuProductMobileViewComponent(IProductCategoryService productCategoryService, ISaleService saleService, ILogging logging)
        {
            _productCategoryService = productCategoryService;
            _saleService = saleService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            ViewBag.ProductCategoryList = await _productCategoryService.GetParentAsync(true, true);
            ViewBag.SaleList = await _saleService.GetActiveAsync();

            return View();
        }
    }
}
