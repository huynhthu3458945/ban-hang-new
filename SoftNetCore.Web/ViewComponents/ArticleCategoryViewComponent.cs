﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ArticleCategoryViewComponent : ViewComponent
    {
        private readonly IArticleCategoryService _articleCategoryService;
        private readonly IArticleService _articleService;
        private readonly ILogging _logging;

        public ArticleCategoryViewComponent(IArticleCategoryService articleCategoryService, IArticleService articleService, ILogging logging)
        {
            _articleCategoryService = articleCategoryService;
            _articleService = articleService;
            _logging = logging;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var res = await _articleCategoryService.GetByStatusAsync(true);
            return View(res);
        }
    }
}
