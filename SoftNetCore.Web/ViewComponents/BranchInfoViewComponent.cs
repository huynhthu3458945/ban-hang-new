﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class BranchInfoViewComponent : ViewComponent
    {
        #region Fields
        private readonly IArticleService _articleService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public BranchInfoViewComponent(IArticleService articleService, ILogging logging)
        {
            _articleService = articleService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}
