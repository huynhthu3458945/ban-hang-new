﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class ViewOrderViewComponent : ViewComponent
    {
        #region Fields
        private readonly IOrderService _orderService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ViewOrderViewComponent(IOrderService orderService, ILogging logging)
        {
            _orderService = orderService;
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            var result = await _orderService.FindFullByIdAsync(id);
            return View(result);
        }
    }
}
