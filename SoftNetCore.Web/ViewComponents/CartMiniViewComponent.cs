﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace SoftNetCore.Web.ViewComponents
{
    public class CartMiniViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            double totalPrice = 0;
            double totalItem = 0;
            List<CartItem> listCart = new List<CartItem>();
            var cart = HttpContext.Session.GetString("cart");
            if (!string.IsNullOrEmpty(cart))
            {
                listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
                totalItem = listCart.Count.ToInt();
                totalPrice = ListCartItem.GetTotal(listCart);
            }
            ViewBag.TotalItem = totalItem;
            ViewBag.TotalPrice = totalPrice;
            return View(listCart);
        }
    }
}
