﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SoftNetCore.Model.Request;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class EvaluationRatingViewComponent : ViewComponent
    {
        #region Fields
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public EvaluationRatingViewComponent(ILogging logging)
        {
            _logging = logging;
        }
        #endregion

        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            List<SelectListItem> ratingList = new List<SelectListItem>();
            ratingList.Add(new SelectListItem { Value = "5", Text = "5 Sao" });
            ratingList.Add(new SelectListItem { Value = "4", Text = "4 Sao" });
            ratingList.Add(new SelectListItem { Value = "3", Text = "3 Sao" });
            ratingList.Add(new SelectListItem { Value = "2", Text = "2 Sao" });
            ratingList.Add(new SelectListItem { Value = "1", Text = "1 Sao" });

            ViewBag.RatingList = ratingList;
            RatingRequest model = new RatingRequest();
            model.OrderId = id;
            return View(model);
        }
    }
}
