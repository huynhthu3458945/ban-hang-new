﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.ViewComponents
{
    public class HomeCategoryViewComponent : ViewComponent
    {
        private readonly IProductCategoryService _productCategoryService;
        private readonly IProductService _productService;
        private readonly IAboutService _aboutService;
        private readonly ILogging _logging;
        public HomeCategoryViewComponent(IProductCategoryService productCategoryService, IProductService productService, IAboutService aboutService, ILogging logging)
        {
            _productCategoryService = productCategoryService;
            _productService = productService;
            _aboutService = aboutService;
            _logging = logging;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            ViewBag.About = await _aboutService.GetFisrtAsync(1);
            var productCategories = await _productCategoryService.GetByStatusAsync(true);
            //ViewBag.ListProduct = await _productService.GetAll().AsNoTracking().Take(20).ToListAsync();

            return View(productCategories);
        }
    }
}
