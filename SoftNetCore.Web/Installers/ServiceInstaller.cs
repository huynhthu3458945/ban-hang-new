﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SoftNetCore.Data.Entities;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Services.UnitOfWork;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Implementation;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Data.Services;
using SoftNetCore.Data.Context;
using SoftNetCore.Common.Extensions;
using SoftNetCore.Services;

namespace SoftNetCore.Web.Installers
{
    public class ServiceInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            var connetionString = configuration.GetConnectionString("DBConnection");
            services.AddDbContext<DBContext>(options =>
                options.UseSqlServer(
                    connetionString,
                    o => o.MigrationsAssembly("SoftNetCore.Data")));

            //services.AddScoped<IAboutService, AboutService>();
            //services.AddScoped<IAccountCouponService, AccountCouponService>();
            //services.AddScoped<IAccountService, AccountService>();
            //services.AddScoped<IAccountTypeService, AccountTypeService>();
            //services.AddScoped<IAchievementService, AchievementService>();
            //services.AddScoped<IArticleService, ArticleService>();
            //services.AddScoped<IArticleCategoryService, ArticleCategoryService>();
            //services.AddScoped<IAssessService, AssessService>();
            //services.AddScoped<IAttributeService, AttributeService>();
            //services.AddScoped<IBannerService, BannerService>();
            //services.AddScoped<IBranchService, BranchService>();
            //services.AddScoped<ICollectionService, CollectionService>();
            //services.AddScoped<ICollectionCategoryService, CollectionCategoryService>();
            //services.AddScoped<IContactService, ContactService>();
            //services.AddScoped<ICouponService, CouponService>();
            //services.AddScoped<ICustomerService, CustomerService>();
            //services.AddScoped<ICustomerTypeService, CustomerTypeService>();
            //services.AddScoped<IDistrictService, DistrictService>();
            //services.AddScoped<IFaqService, FaqService>();
            //services.AddScoped<IFeedbackService, FeedbackService>();
            //services.AddScoped<IGalleryCategoryService, GalleryCategoryService>();
            //services.AddScoped<IGalleryService, GalleryService>();
            //services.AddScoped<IOrderService, OrderService>();
            //services.AddScoped<IOrderDetailService, OrderDetailService>();
            //services.AddScoped<IOrderStatusService, OrderStatusService>();
            //services.AddScoped<IPartnerService, PartnerService>();
            //services.AddScoped<IPayTypeService, PayTypeService>();
            //services.AddScoped<IPositionBannerService, PositionBannerService>();
            //services.AddScoped<IProductService, ProductService>();
            //services.AddScoped<IProductAttributeService, ProductAttributeService>();
            //services.AddScoped<IProductCategoryService, ProductCategoryService>();
            //services.AddScoped<IProductCollectionService, ProductCollectionService>();
            //services.AddScoped<IProductWishlistService, ProductWishlistService>();
            //services.AddScoped<IProvinceService, ProvinceService>();
            //services.AddScoped<ISaleService, SaleService>();
            //services.AddScoped<IServiceService, ServiceService>();
            //services.AddScoped<IServiceCategoryService, ServiceCategoryService>();
            //services.AddScoped<IShippingFeeService, ShippingFeeService>();
            //services.AddScoped<ISliderService, SliderService>();
            //services.AddScoped<ISubscribeService, SubscribeService>();
            //services.AddScoped<IVideoCategoryService, VideoCategoryService>();
            //services.AddScoped<IVideoService, VideoService>();

            /// Đăng ký services trong tầng SoftNetCore.Services
            services.WithScopedLifetime<IServices>();

            services.AddScoped<ISendEmailService, SendEmailService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ILogging, Logging>();
            services.AddScoped<IWebHelper, WebHelper>();
            services.AddScoped<DbFactory, DbFactory>();
            services.AddScoped<IDateTimeService, DateTimeService>();

            /// Đăng ký Repository services
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>)); // Map hàm xử lý

        }
    }
}
