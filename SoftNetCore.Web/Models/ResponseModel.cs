﻿using SoftNetCore.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Models
{
    public class ResponseModel
    {
        public bool Status { get; set; } = true;
        public string Title { get; set; } = "Thông báo";
        public string Message { get; set; } = string.Empty;
        public string Type { get; set; } = EnumsTypeMessage.Success;
        public string RedirectUrl { get; set; } = string.Empty;
        public object Data { get; set; } = null;
        public bool IsReturn { get; set; } = false;
    }
}
