﻿function ValidateFrmUpdateCustomer() {
    var result = false;
    var name = $("#FullName").val();
    var phone = $("#Phone").val();
    var email = $("#Email").val();
    var province = $("#ProvinceId").val();
    //var district = $("#DistrictId").val();
    var address = $("#Address").val();
    //var ischagepass = $("#IsChangePass").is(":checked");
    //var pass = $("#Password").val();
    //var rePass = $("#RePassword").val();


    if (name == '') {
        $("#div_Alert").show();
        $("#FullName").focus();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-warning");
        $("#span_Message").text("Vui lòng nhập họ tên.");
        toastr.error("Vui lòng nhập họ tên", "Không thành công!");
        result = false;
        return false;
    } else {
        $("#div_Alert").hide();
        result = true;
    }
    if (phone == '') {
        $("#div_Alert").show();
        $("#Phone").focus();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-warning");
        $("#span_Message").text("Vui lòng nhập số điện thoại.");
        toastr.error("Vui lòng nhập số điện thoại", "Không thành công!");
        result = false;
        return false;
    } else {
        $("#div_Alert").hide();
        result = true;
    }
    if (email == '') {
        $("#div_Alert").show();
        $("#Email").focus();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-warning");
        $("#span_Message").text("Vui lòng nhập email.");
        toastr.error("Vui lòng nhập email", "Không thành công!");
        result = false;
        return false;
    } else {
        $("#div_Alert").hide();
        result = true;
    }
    if (province == null) {
        $("#div_Alert").show();
        $("#ProvinceId").focus();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-warning");
        $("#span_Message").text("Vui lòng chọn tỉnh thành.");
        toastr.error("Vui lòng chọn tỉnh thành", "Không thành công!");
        result = false;
        return false;
    } else {
        $("#div_Alert").hide();
        result = true;
    }
    //if (district == null) {
    //    $("#div_Alert").show();
    //    $("#DistrictId").focus();
    //    $("#div_Alert").removeClass();
    //    $("#div_Alert").addClass("alert alert-warning");
    //    $("#span_Message").text("Vui lòng chọn quận/huyện.");
    //    toastr.error("Vui lòng chọn quận/huyện", "Không thành công!");
    //    result = false;
    //    return false;
    //} else {
    //    $("#div_Alert").hide();
    //    result = true;
    //}
    if (address == '') {
        $("#div_Alert").show();
        $("#Address").focus();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-warning");
        $("#span_Message").text("Vui lòng nhập địa chỉ.");
        toastr.error("Vui lòng nhập địa chỉ", "Không thành công!");
        result = false;
        return false;
    } else {
        $("#div_Alert").hide();
        result = true;
    }
    //if (ischagepass == true && pass == '') {
    //    $("#div_Alert").show();
    //    $("#NewPassword").focus();
    //    $("#div_Alert").removeClass();
    //    $("#div_Alert").addClass("alert alert-warning");
    //    $("#span_Message").text("Vui lòng nhập mật khẩu mới.");
    //    toastr.error("Vui lòng nhập mật khẩu mới", "Không thành công!");
    //    result = false;
    //    return false;
    //} else {
    //    $("#div_Alert").hide();
    //    result = true;
    //}
    //if (ischagepass == true && (rePass == '' || rePass != pass)) {
    //    $("#div_Alert").show();
    //    $("#RePassword").focus();
    //    $("#div_Alert").removeClass();
    //    $("#div_Alert").addClass("alert alert-warning");
    //    $("#span_Message").text("Mật khẩu xác nhận không chính xác!");
    //    toastr.error("Mật khẩu xác nhận không chính xác", "Không thành công!");
    //    result = false;
    //    return false;
    //} else {
    //    $("#div_Alert").hide();
    //    result = true;
    //}

    if (result === true) {
        UpdateFormCustomer();
    }

    return result;
}

function OnSuccess(data) {
    if (data.Status == true) {
        toastr.success("Cập nhật thành công", "Thành công!", { timeOut: 3000, "closeButton": true });
    } else {
        $("#div_Alert").show();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-warning");
        $("#span_Message").text(data.Message);
        toastr.error(data.Message, "Không thành công!", { timeOut: 3000, "closeButton": true });
    }
}

function OnFailure(data) {
    $("#div_Alert").show();
    $("#div_Alert").removeClass();
    $("#div_Alert").addClass("alert alert-warning");
    $("#span_Message").text(data.Message);
    toastr.error(data.Message, "Không thành công!", { timeOut: 3000, "closeButton": true });
}

function UpdateFormCustomer() {
    $('#frmUpdateCustomer').submit();
}