﻿$(document).ready(function () {
    $("#frmCheckout").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "FullName": {
                required: true
            },
            //"Email": {
            //    required: true
            //},
            "Mobi": {
                required: true
            },
            //"ProvinceId": {
            //    required: true
            //},
            //"DistrictId": {
            //    required: true
            //},
            "Address": {
                required: true
            }
        },
        messages: {
            "FullName": {
                required: "Vui lòng nhập họ tên."
            },
            //"Email": {
            //    required: "Vui lòng nhập email."
            //},
            "Mobi": {
                required: "Vui lòng nhập số điện thoại."
            },
            //"ProvinceId": {
            //    required: "Chọn tỉnh/thành phố."
            //},
            //"DistrictId": {
            //    required: "Chọn quận/huyện."
            //},
            "Address": {
                required: "Vui lòng nhập địa chỉ."
            }
        }
    });
});