﻿function ValidateFrmCancelOrder() {
    var result = false;
    var pass = $("#Remark").val();

    if (pass == '') {
        $("#Remark").focus();
        toastr.error("Nhập lý do hủy đơn!");
        result = false;
        return false;
    } else {
        result = true;
    }
    if (result === true) {
        CancelOrderForm();
    }

    return result;
}

function OnSuccess(data) {
    if (data.Status == true) {
        toastr.success("Hủy đơn hàng thành công!");
        setTimeout(function () {
            window.location.reload();
        }, 500);
        //delay is in milliseconds
        //$("#partialCancelOrder").html("");
        //$("#rating_view_modal").modal("hide")
        //$('#rating_view_modal').modal('toggle');
        //$('body').removeClass('modal-open');
        //$('.modal-backdrop').remove();
    } else {
        toastr.error("Hủy đơn hàng thất bại! Vui lòng liên hệ trực tiếp");

    }
}

function OnFailure() {
    toastr.error("Hủy đơn hàng thất bại! Vui lòng liên hệ trực tiếp");
}

function CancelOrderForm() {
    $('#frmCancelOrder').submit();
}