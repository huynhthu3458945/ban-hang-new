﻿var cart = {
    init: function () {
        cart.regEvents();
    },
    updateCar: function addToCart(id, quantity) {
        $.ajax({
            type: 'GET',
            url: '/ShoppingCart/AddItem?sizeId=' + id + '&quantity=' + quantity,
            success: function (data) {
                getQutityCart();
                cart.reloadCart();
            }
        })
    },
    removeAllCart: function addToCart(id, quantity) {
        $.ajax({
            type: 'GET',
            url: '/ShoppingCart/RemoveAllCart',
            success: function (data) {
                if (data.status) {
                    getQutityCart();
                    cart.reloadCart();
                }
            }
        })
    },
    reloadCart: function reloadCart() {
        $.ajax({
            type: 'GET',
            url: '/ShoppingCart/ReloadCart',
            success: function (data) {
                if (data.status) {
                    // popup cart
                    $('#list-item').empty();
                    $('#list-item').html(data.html);
                    // total
                    $('#subTotal').text(data.total);
                    $('#Total').text(data.total);
                }
            }
        })
    },
    regEvents: function () {
        $("td.detail-info .detail-extralink .qty-down").on("click", function () {
            var $button = $(this);
            var oldValue = $button.parent().children(".qty-val").text();
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
            }
            $button.parent().children('.qty-val').val(newVal).text(newVal);
            var price = $button.parent().parent().parent().parent().children('.price').children(".text-body").data("price");
            $button.parent().parent().parent().parent().children('.price').children(".text-brand").text(formatNumber(parseFloat(price) * newVal))
            cart.updateCar($button.data('id'), -1);
        });

        $("td.detail-info .detail-extralink .qty-up").on("click", function () {
            var $button = $(this);
            var oldValue = $button.parent().children(".qty-val").text();
            var newVal = parseFloat(oldValue) + 1;
            $button.parent().children('.qty-val').val(newVal).text(newVal);
            var price = $button.parent().parent().parent().parent().children('.price').children(".text-body").data("price");
            $button.parent().parent().parent().parent().children('.price').children(".text-brand").text(formatNumber(parseFloat(price) * newVal))
            cart.updateCar($button.data('id'), 1);
        });

        $(".remove").on("click", function () {
            $(this).parent().remove();
            $('.total-item-product').text(parseFloat($('.total-item-product').text()) - 1);
            $.ajax({
                data: { id: $(this).data('id'), colorId: 0, sizeId: 0 },
                url: '/ShoppingCart/Deleted',
                dataType: 'json',
                type: 'POST',
                success: function (res) {
                    if (res.status === true) {
                        getQutityCart();
                        // cập nhật tổng tiền
                        cart.reloadCart();

                    }
                }
            });
        });

        $('#removeAllCart').click(function () {
            if (confirm("Bạn chắc chắn muốn xóa giỏ hàng?")) {
                $('.table-wishlist').children().remove()
                cart.removeAllCart();
            }
        });
    }
};
cart.init();
