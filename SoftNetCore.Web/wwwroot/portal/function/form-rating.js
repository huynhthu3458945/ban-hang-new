﻿function ValidateFrmRating() {
    var result = false;
    var pass = $("#Content").val();

    if (pass == '') {
        $("#Content").focus();
        toastr.error("Nhập nội dung đánh giá!");
        result = false;
        return false;
    } else {
        result = true;
    }
    if (result === true) {
        RatingForm();
    }

    return result;
}

function OnSuccess(data) {
    if (data.Status == true) {
        toastr.success("Cảm ơn bạn đã đánh giá chất lượng đơn hàng!", "Thành công!", { timeOut: 3000, "closeButton": true });
        setTimeout(function () {
            window.location.reload();
        }, 1000);
        //delay is in milliseconds
        //$("#partialRating").html("");
        //$("#rating_view_modal").modal("hide")
        //$('#rating_view_modal').modal('toggle');
        //$('body').removeClass('modal-open');
        //$('.modal-backdrop').remove();
    } else {
        toastr.error("Đánh giá thất bại!");

    }
}

function OnFailure() {
    toastr.error("Đánh giá thất bại!");
}

function RatingForm() {
    $('#frmRating').submit();
}