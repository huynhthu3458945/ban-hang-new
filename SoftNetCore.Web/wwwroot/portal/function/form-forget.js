﻿function ValidateFrmForget() {
    var result = false;
    var email = $("#txtEmail").val();

    if (email == '') {
        $("#txtEmail").focus();
        $("#registerForget").text("Vui lòng nhập email.");
        result = false;
        return false;
    } else {
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
        if (!pattern.test(email)) {
            $('#registerForget').text('Địa chỉ email không hợp lệ!');
            result = false;
            return false;
        }
        $("#registerForget").text();
        result = true;
    }
    //if (result === true) {
    //    ForgetForm();
    //}

    return result;
}

//function OnSuccess(data) {
//    if (data.Status == true) {
//        toastr.success("Thao tác thành công. Vui lòng kiểm tra email", "Thành công!", { timeOut: 3000, "closeButton": true });
//        setTimeout(function () {
//            window.location.href = "/dang-nhap";
//        }, 1500); //delay is in milliseconds 

//    } else {
//        $("#div_Alert").show();
//        $("#div_Alert").removeClass();
//        $("#div_Alert").addClass("alert alert-warning");
//        $("#span_Message").text("Lấy lại mật khẩu thất bại. Vui lòng kiểm tra lại.");
//        toastr.error(data.Message, "Không thành công!", { timeOut: 3000, "closeButton": true });

//    }
//}

//function OnFailure() {
//    $("#div_Alert").show();
//    $("#div_Alert").removeClass();
//    $("#div_Alert").addClass("alert alert-warning");
//    $("#span_Message").text("Lấy lại mật khẩu thất bại. Vui lòng kiểm tra lại.");
//    toastr.error(data.Message, "Không thành công!", { timeOut: 3000, "closeButton": true });
//}

//function ForgetForm() {
//    $('#frmForget').submit();
//}