﻿function SubmitLoginCheckOut(id) {
    var result = false;
    var user = $("#txtUsername").val();
    var pass = $("#txtPassword").val();

    if (user == '') {
        $("#div_Alert").show();
        $("#txtUsername").focus();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-danger ");
        $("#span_Message").text("Vui lòng nhập tài khoản.");
        result = false;
        return false;
    } else {
        $("#div_Alert").hide();
        result = true;
    }

    if (pass == '') {
        $("#div_Alert").show();
        $("#txtPassword").focus();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-danger ");
        $("#span_Message").text("Vui lòng nhập mật khẩu.");
        result = false;
        return false;
    } else {
        $("#div_Alert").hide();
        result = true;
    }

    if (result === true) {
        if ($("#" + id).valid()) {
            //success
            $.ajax({
                type: 'POST',
                url: '/Customer/PostLogin' + "?t=" + Math.random(),
                async: true,
                dataType: 'JSON',
                data: $("#" + id).serialize(),
                success: function (result) {
                    if (result.Status) {
                        $("#" + id)[0].reset();
                        toastr.success("Đăng nhập thành công", "Thành công!", { timeOut: 1000, "closeButton": true });
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000); //delay is in milliseconds 
                    }
                    else {
                        toastr.error("Đăng nhập thất bại. Kiểm tra lại thông tin", "Không thành công!", { timeOut: 1000, "closeButton": true });
                    }
                }
            })
                .fail(
                    function (jqXHR, textStatus, err) {
                        toastr.error("Đăng nhập thất bại. Kiểm tra lại thông tin", "Không thành công!", { timeOut: 1000, "closeButton": true });
                    });

        }
    }
    return result;
}
