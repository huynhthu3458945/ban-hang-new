﻿function ValidateFrmRegister() {
    var result = false;
    var name = $("#txtFullName").val();
    var userName = $("#txtUserName").val();
    var phone = $("#txtPhone").val();
    var email = $("#txtEmail").val();
    var pass = $("#txtPassword").val();
    var rePass = $("#txtRePassword").val();

    if (name == '') {
        $("#txtFullName").focus();
        $("#registerError").text("Vui lòng nhập họ tên.");
        result = false;
        return false;
    } else {
        $("#registerError").text();
        result = true;
    }

    if (phone == '') {
        $("#txtPhone").focus();
        $("#registerError").text("Vui lòng nhập số điện thoại.");
        result = false;
        return false;
    } else {
        var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
        if (!phoneno.test(phone)) {
            $('#registerError').text('Số điện thoại không hợp lệ!');
            result = false;
            return false;
        }
        $("#registerError").text();
        result = true;
    }
    if (userName == '') {
        $("#txtUserName").focus();
        $("#registerError").text("Vui lòng nhập tài khoản.");
        result = false;
        return false;
    } else {
        $("#registerError").text();
        result = true;
    }
    if (email == '') {
        $("#txtEmail").focus();
        $("#registerError").text("Vui lòng nhập email.");
        result = false;
        return false;
    } else {
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
        if (!pattern.test(email)) {
            $('#registerError').text('Địa chỉ email không hợp lệ!');
            result = false;
            return false;
        }
        $("#registerError").text();
        result = true;
    }
    if (pass == '') {
        $("#txtPassword").focus();
        $("#registerError").text("Vui lòng nhập mật khẩu.");
        result = false;
        return false;
    } else {
        $("#registerError").text();
        result = true;
    }
    if (rePass == '' || rePass != pass) {
        $("#txtRePassword").focus();
        $("#registerError").text("Mật khẩu xác nhận không chính xác");
        result = false;
        return false;
    } else {
        $("#registerError").text();
        result = true;
    }
    //if (result === true) {
    //    RegisterForm();
    //}

    return result;
}

//function OnSuccess(data) {
//    if (data.Status == true) {
//        $("#txtFullName").val('');
//        $("#txtEmail").val('');
//        $("#txtPhone").val('');
//        $("#txtPassword").val('');
//        $("#txtRePassword").val('');
//        toastr.success("Đăng ký tài khoản thành công", "Thành công!", { timeOut: 3000, "closeButton": true });
//        setTimeout(function () {
//            window.location.href = "/dang-nhap";
//        }, 1000); //delay is in milliseconds 
//    } else {
//        $("#registerError").text("Đăng ký thất bại. Vui lòng kiểm tra lại thông tin.");
//        toastr.error(data.Message, "Không thành công!", { timeOut: 3000, "closeButton": true });
//    }
//}

//function OnFailure() {
//    $("#registerError").text("Đăng ký thất bại. Vui lòng kiểm tra lại.");
//    toastr.error(data.Message, "Không thành công!", { timeOut: 3000, "closeButton": true });
//}

//function RegisterForm() {
//    $('#frmRegister').submit();
//}