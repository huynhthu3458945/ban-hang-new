﻿function ValidateFrmContact()
{
    var result = false;
    var name = $("#txtFullName").val();
    var email = $("#txtEmail").val();
    var mobi = $("#txtMobi").val();
    var content = $("#txtContent").val();

    if (name == '') {
        $("#txtFullName").focus();
        $('.form-messege').text("Vui lòng nhập họ tên.");
        result = false;
        return false;
    } else
    {
        $('.form-messege').val('');
        result = true;
    }
    if (email == '') {
        $("#txtEmail").focus();
        $('.form-messege').text("Vui lòng nhập email.");
        result = false;
        return false;
    } else {
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
        if (!pattern.test(email)) {
            $('.form-messege').text('Địa chỉ email không hợp lệ!');
            result = false;
            return false;
        }
        $('.form-messege').text('');
        result = true;
    }

    if (mobi == '') {
        $("#txtMobi").focus();
        $('.form-messege').text("Vui lòng nhập số điện thoại.");
        result = false;
        return false;
    } else {
        var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
        if (!phoneno.test(mobi)) {
            $('.form-messege').text('Số điện thoại không hợp lệ!');
            result = false;
            return false;
        }
        $('.form-messege').text('');
        result = true;
    }

    if (content == '') {
        $("#txtContent").focus();
        $('.form-messege').text("Vui lòng nhập nội dung cần hỗ trợ.");
        result = false;
        return false;
    } else {
        $('.form-messege').text('');
        result = true;
    }
    return result;

}

