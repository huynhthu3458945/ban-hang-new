﻿function ValidateFrmUpdateAccount() {
    var result = false;
    var pass = $("#Password").val();
    var newpass = $("#NewPassword").val();
    var rePass = $("#RePassword").val();

    if (pass == '') {
        $("#div_Alert").show();
        $("#Password").focus();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-warning");
        $("#span_Message").text("Vui lòng nhập mật khẩu hiện tại.");
        toastr.error("Vui lòng nhập mật khẩu hiện tại", "Không thành công!");
        result = false;
        return false;
    } else {
        $("#div_Alert").hide();
        result = true;
    }
    if (newpass == '') {
        $("#div_Alert").show();
        $("#NewPassword").focus();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-warning");
        $("#span_Message").text("Vui lòng nhập mật khẩu mới.");
        toastr.error("Vui lòng nhập mật khẩu mới", "Không thành công!");
        result = false;
        return false;
    } else {
        $("#div_Alert").hide();
        result = true;
    }
    if (rePass == '' || rePass != newpass) {
        $("#div_Alert").show();
        $("#RePassword").focus();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-warning");
        $("#span_Message").text("Mật khẩu xác nhận không chính xác!");
        toastr.error("Mật khẩu xác nhận không chính xác", "Không thành công!");
        result = false;
        return false;
    } else {
        $("#div_Alert").hide();
        result = true;
    }

    if (result === true) {
        UpdateFormAccount();
    }

    return result;
}

function OnSuccess(data) {
    if (data.Status == true) {
        toastr.success("Cập nhật thành công", "Thành công!", { timeOut: 3000, "closeButton": true });
    } else {
        $("#div_Alert").show();
        $("#div_Alert").removeClass();
        $("#div_Alert").addClass("alert alert-warning");
        $("#span_Message").text(data.Message);
        toastr.error(data.Message, "Không thành công!", { timeOut: 3000, "closeButton": true });
    }
}

function OnFailure(data) {
    $("#div_Alert").show();
    $("#div_Alert").removeClass();
    $("#div_Alert").addClass("alert alert-warning");
    $("#span_Message").text(data.Message);
    toastr.error(data.Message, "Không thành công!", { timeOut: 3000, "closeButton": true });
}

function UpdateFormAccount() {
    $('#frmUpdateAccount').submit();
}