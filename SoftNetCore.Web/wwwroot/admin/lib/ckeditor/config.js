/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    //config.language = 'vi';
    // config.uiColor = '#AADC6E';
    //CKEDITOR.timestamp = false;
    //config.syntaxhighlight_lang = 'csharp';
    //config.syntaxhighlight_hideControls = true;
    config.languages = 'vi';
    config.filebrowserBrowseUrl = '/tiny-mce/browse';
    config.filebrowserBrowseUrl = '/tiny-mce/browseavatar';
    config.filebrowserBrowseUrl = '/tiny-mce/browsethumb';
    config.filebrowserImageBrowseUrl = '/tiny-mce/browse';
    config.filebrowserUploadUrl = '/el-finder/file-system/connector';
    config.filebrowserImageUploadUrl = '/el-finder/file-system/connector';

    config.extraPlugins = 'image2';
};
