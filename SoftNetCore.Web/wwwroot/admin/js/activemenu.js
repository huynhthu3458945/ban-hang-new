﻿$(function () {
    ActionMenu();
    if ($(".Earnings span").text() != "") {
        $(".Earnings span").text("vui lòng chỉ nhập số")
    }
});

//Active Menu
function ActionMenu() {
    var urlOld = $(location).attr('pathname');
    var url = '';
    if (urlOld.split('/').length > 3) {
        url = '/' + urlOld.split('/')[1];
    }
    else {
        url = urlOld;
    }
    $(".mainmenu li").each(function (i) {
        //for sub de active
        $(this).find("a").each(function (j) {
            //kiem tra có sub ko
            href = $(this).attr('href');
            if (url === href) {
                $(this).parent().addClass('active');
            }

            //chạy tiếp nếu có sub
            $(this).closest('li').find(".submenu a").each(function (j) {
                hrefSub = $(this).attr('href');
                if (urlOld === hrefSub) {
                    $(this).closest('span').addClass('active');
                }
            });
        });

        $(".mainmenu li ul li").each(function () {
            if ($(this).hasClass("active")) {
                $(this).closest('ul').parent().addClass('active');
            }
        })
        var newUrl = urlOld.split('/')[1];
        if (newUrl == "chi-tiet-quyen-gop") {
            $(this).find("a").each(function () {
                if ($(this).attr('href') != "") {
                    urlMain = $(this).attr('href').split('/')[1];
                    if (urlMain == "quyen-gop")
                        $(this).parent().addClass('active');
                }
            })
        }
        if (newUrl == "chi-tiet-tin-tuc") {
            $(this).find("a").each(function () {
                if ($(this).attr('href') != "") {
                    urlMain = $(this).attr('href').split('/')[1];
                    if (urlMain == "tin-tuc")
                        $(this).parent().addClass('active');
                }
            })
        }
    });
}