﻿$(document).ready(function () {
    $("#frmAttribute").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "Title": {
                required: true
            },
            "Position": {
                required: true
            },
            "Code": {
                required: true
            }
        },
        messages: {
            "Title": {
                required: "Vui lòng nhập."
            },
            "Position": {
                required: "Vui lòng nhập."
            },
            "Code": {
                required: "Vui lòng nhập."
            }
        }
    });
});