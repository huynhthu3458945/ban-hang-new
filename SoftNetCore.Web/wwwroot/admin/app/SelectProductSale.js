﻿$(function () {
    RenderProductSale();
});

//product
function ShowSelectProduct() {
    var url = $("#SelectProduct").data('url');
    $.get(url, function (data) {
        $("#SelectProduct").html(data);
        $("#SelectProduct").modal('show');
        LoadProductPopup();
        setTimeout(function () {
            var parent_column = $('#product-table').closest('[class*="col-"]');
            $('#product-table').jqGrid('setGridWidth', parent_column.width(), true);
        }, 500);
    });
}

function SelectProduct() {
    var listProduct;
    listProduct = jQuery("#product-table").jqGrid('getGridParam', 'selarrrow');
    $.ajax({
        type: 'POST',
        url: '/admin/AdminSale/SelectProduct',
        async: true,
        dataType: 'json',
        data: { listProduct: listProduct },
        success: function (result) {
            if (result.Status) {
                $("#SelectProduct").modal('hide');
                RenderProductSale();
            }
        }
    })
        .fail(
            function (jqXHR, textStatus, err) {

            });
}

function DeleteProductSale(id) {
    if (id !== '') {
        $.ajax({
            type: 'POST',
            url: '/admin/AdminSale/DeleteProductSale',
            async: true,
            dataType: 'json',
            data: { id: id },
            success: function (result) {
                if (result.Status) {
                    toastr.success("Xóa thành công.", "Thành công !");
                    setTimeout(function () {
                        RenderProductSale();
                    }, 500); //delay is in milliseconds
                }
            }
        })
            .fail(
                function (jqXHR, textStatus, err) {

                });
    }
}

function RenderProductSale() {
    $.ajax({
        type: 'GET',
        url: '/admin/AdminSale/DetailProductSale' + "?t=" + Math.random(),
        async: true,
        dataType: 'html',
        success: function (result) {
            $('#tbody_productsale').empty();
            $("#tbody_productsale").html(result);
        }
    })
        .fail(
            function (jqXHR, textStatus, err) {

            });
}

//-- gird product --//
function LoadProductPopup() {
    $("#product-table").jqGrid({
        url: "/admin/AdminSale/LoadProduct",
        mtype: 'GET',
        datatype: "json",
        colNames: ['Id', 'Sản phẩm'],
        colModel: [
            //{ name: 'Thumb', index: '', width: 50, align: "center", sortable: false, fixed: true, formatter: FormatImage },
            { name: 'Id', index: 'Id', width: 60, sortname: 'Id', editable: false, key: true, hidden: true, search: false },
            { name: 'Title', index: 'Title', width: 200, sortname: 'Title', sortable: true, editable: false },
            //{ name: 'OldPrice', index: 'OldPrice', width: 100, sortname: 'OldPrice', align: "right", sortable: false, editable: false, formatter: 'currency' },
        ],
        viewrecords: false,
        emptyrecords: "Dữ liệu không tồn tại",
        rowNum: 50,
        rowList: [50, 100],
        pager: '#product-pager',
        altRows: true,
        multiselect: true,
        multikey: "row",
        //multiboxonly: true,
        rownumbers: true,
        loadComplete: function () {
            var table = this;
            setTimeout(function () {
                styleCheckbox(table);
                updatePagerIcons(table);
            }, 0);
        }
    });

    var parent_column = $('#product-table').closest('[class*="col-"]');
    //dung resize table
    $(window).on('resize.jqGrid', function () {
        $('#product-table').jqGrid('setGridWidth', parent_column.width());
    });

}

var timeoutHnd1;
function Search(ev) {
    if (timeoutHnd1)
        clearTimeout(timeoutHnd1);
    timeoutHnd1 = setTimeout(gridReload, 500);
}

//Reload gird
function gridReload() {
    var strSearch = jQuery("#form-product-search").val();
    url = "/admin/AdminProduct/LoadData?searchString=" + strSearch;
    jQuery('#product-table').jqGrid('setGridParam', { url: url, page: 1 }).trigger("reloadGrid");
}

function FormatImage(cellValue, options, rowObject) {
    var html = "<img id='img' width='30' height='30' src='" + rowObject.Thumb + "' alt='' class='img-thumbnail padding-0'>";
    return html;
}

function FormatImage2(cellValue, options, rowObject) {
    var html = "<img id='img' width='30' height='30' src='" + rowObject.Avatar + "' alt='' class='img-thumbnail padding-0'>";
    return html;
}

function enableTooltips(table) {
    $('.navtable .ui-pg-button').tooltip({ container: 'body' });
    $(table).find('.ui-pg-div').tooltip({ container: 'body' });
    $('[data-rel=tooltip]').tooltip({ container: 'body' });
}

function updatePagerIcons(table) {
    var replacement =
    {
        'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
        'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
        'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
        'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
    };
    $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function () {
        var icon = $(this);
        var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

        if ($class in replacement) icon.attr('class', 'ui-icon ' + replacement[$class]);
    });
}

function styleCheckbox(table) {

    $(table).find('input:checkbox').addClass('ace')
        .wrap('<label />')
        .after('<span class="lbl align-top" />');

    $('.ui-jqgrid-labels th[id*="_cb"]:nth-child(2)')
        .find('input.cbox[type=checkbox]').addClass('ace')
        .wrap('<label />').after('<span class="lbl align-top" />');
    $('.ui-jqgrid-labels th[id*="_cb"]:nth-child(2)').find('div').addClass('input-check-all');
}
