﻿$(document).ready(function () {

    $.validator.addMethod('minStrict', function (value, el, param) {
        return value >= param;
    });

    $("#frmProduct").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "Code": {
                required: true
            }, 
            "Title": {
                required: true
            }, 
            "Position": {
                required: true
            },
        },
        messages: {
            "Code": {
                required: "Vui lòng nhập mã sản phẩm"
            },
            "Title": {
                required: "Vui lòng nhập tên sản phẩm."
            },
            "Position": {
                required: "Vui lòng nhập độ ưu tiên."
            },
        }
    });
});