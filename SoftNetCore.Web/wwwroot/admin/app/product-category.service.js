﻿
const merchantProductByCategorySession = "#merchantProductByCategorySession";

let index = 0;
function addMerchantProductCategory() {
    $.ajax({
        type: 'GET',
        url: `/Admin/AdminProductCategory/AddMerchantConfig`,
        data: {
            index: index
        },
        success: function (result) {
            console.log(">> ", result);
            index++;
            $(merchantProductByCategorySession).append(result);
        }
    })
}