﻿$(document).ready(function () {
    $("#frmCustomer").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "FullName": {
                required: true
            }, 
            "Email": {
                required: true
            },
            "Phone": {
                required: true
            },
            "Address": {
                required: true
            }
        },
        messages: {
            "FullName": {
                required: "Vui lòng nhập."
            },
            "Email": {
                required: "Vui lòng nhập."
            },
            "Phone": {
                required: "Vui lòng nhập."
            },
            "Address": {
                required: "Vui lòng nhập."
            }
        }
    });
});