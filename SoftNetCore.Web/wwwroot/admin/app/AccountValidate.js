﻿$(document).ready(function () {
    $("#frmAccount").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            //"FullName": {
            //    required: true
            //}, 
            "Username": {
                required: true
            },
            "Password": {
                required: true
            }
        },
        messages: {
            //"FullName": {
            //    required: "Vui lòng nhập họ tên."
            //},
            "Username": {
                required: "Vui lòng nhập tên tài khoản."
            },
            "Password": {
                required: "Vui lòng nhập mật khẩu."
            }
        }
    });
});