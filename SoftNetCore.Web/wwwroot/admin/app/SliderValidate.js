﻿$(document).ready(function () {
    $("#frmSlider").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "Title": {
                required: true
            }, 
            "Position": {
                required: true
            },
            "Description": {
                required: true
            }
        },
        messages: {
            "Title": {
                required: "Vui lòng nhập."
            },
            "Position": {
                required: "Vui lòng nhập."
            },
            "Description": {
                required: "Vui lòng nhập."
            }
        }
    });
});