﻿var grid_selector = "#grid-table";
var pager_selector = "#grid-pager";
var timeoutHnd;

function Search(ev) {
    if (timeoutHnd)
        clearTimeout(timeoutHnd);
    timeoutHnd = setTimeout(gridReload, 500);
}

//Reload gird
function gridReload() {
    var strSearch = jQuery("#form-search").val();
    var strSelectSearch = jQuery("#form-select-search").val();
    var url = '';
    if (strSelectSearch === '') {
        url = "/admin/AdminOrder/LoadData?searchString=" + strSearch;
    }
    else {
        url = "/admin/AdminOrder/LoadData?searchString=" + strSearch + "&searchStatus=" + strSelectSearch;
    }

    jQuery(grid_selector).jqGrid('setGridParam', { url: url, page: 1 }).trigger("reloadGrid");
}

function DeleteData(index) {
    jQuery(grid_selector).jqGrid('setSelection', index);
    var gr = jQuery(grid_selector).jqGrid('getGridParam', 'selrow');
    //var id = jQuery(grid_selector).jqGrid('getCell', gr, 'OrdertID');
    if (gr !== null)
        jQuery(grid_selector).jqGrid('delGridRow', gr, {
            zIndex: 100,
            mtype: 'POST',
            url: "/admin/AdminOrder/Delete/" + gr,
            closeOnEscape: true,
            closeAfterDelete: true,
            recreateForm: true,
            afterComplete: function (data) {
                ShowMessage(data.responseJSON);
            }
        });
    else {
        alert("Please Select Row to delete!");
    }

}

jQuery(function ($) {
    var parent_column = $(grid_selector).closest('[class*="col-"]');
    //dung resize table
    $(window).on('resize.jqGrid', function () {
        $(grid_selector).jqGrid('setGridWidth', parent_column.width());
    });


    //resize on sidebar collapse/expand
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
        if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
            //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
            setTimeout(function () {
                $(grid_selector).jqGrid('setGridWidth', parent_column.width());
            }, 20);
        }
    });

    //if your grid is inside another element, for example a tab pane, you should use its parent's width:
    /**
                $(window).on('resize.jqGrid', function () {
                    var parent_width = $(grid_selector).closest('.tab-pane').width();
    $(grid_selector).jqGrid( 'setGridWidth', parent_width );
    })
    //and also set width when tab pane becomes visible
                $('#myTab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                  if($(e.target).attr('href') == '#mygrid') {
                    var parent_width = $(grid_selector).closest('.tab-pane').width();
    $(grid_selector).jqGrid( 'setGridWidth', parent_width );
    }
    })
    */

    //config table
    jQuery(grid_selector).jqGrid({
        url: "/admin/AdminOrder/LoadData",
        mtype: 'GET',
        datatype: "json",
        height: '50%',
        colNames: ['Id', 'Mã', 'Họ tên', 'Email', 'Số điện thoại', 'Địa chỉ giao hàng', 'Tổng tiền hàng', 'Hình thức thanh toán', 'OrderStatusId', 'Tình trạng', 'Ngày đặt'/*, 'Đánh giá'*/, 'Thao tác'],
        colModel: [
            { name: 'Id', index: 'Id', width: 60, sortname: 'Id', editable: false, key: true, hidden: true, search: false },
            { name: 'Code', index: 'Code', width: 40, sortname: 'Code', sortable: true, editable: false },
            { name: 'FullName', index: 'FullName', width: 60, sortname: 'FullName', sortable: true, editable: false },
            { name: 'Email', index: 'Email', width: 60, sortname: 'Email', sortable: true, editable: false },
            { name: 'Mobi', index: 'Mobi', width: 60, sortname: 'Mobi', sortable: true, editable: false },
            { name: 'Address', index: 'Address', width: 60, sortname: 'Address', sortable: true, editable: false },
            //{ name: 'Total', index: 'Total', width: 60, align: "center", sortname: 'Total', sortable: true, editable: false, formatter: 'currency' },
            //{ name: 'FeeShip', index: 'FeeShip', width: 60, align: "center", sortname: 'FeeShip', sortable: true, editable: false, formatter: 'currency' },
            { name: 'TotalPrice', index: 'TotalPrice', width: 60, align: "center", sortname: 'TotalPrice', sortable: true, editable: false, formatter: 'currency' },
            { name: 'PaymentName', index: 'PaymentName', align: "center", width: 60, sortable: false, editable: false },
            { name: 'OrderStatusId', index: 'OrderStatusId', align: "center", width: 60, sortable: false, editable: false, hidden: true },
            { name: 'StatusName', index: 'StatusName', align: "center", width: 60, sortable: false, editable: false },
            { name: 'CreateTime', index: 'CreateTime', width: 60, sortname: 'CreateTime', align: "center", sortable: true, editable: false, formatter: 'date', formatoptions: { srcformat: 'Y-m-d', newformat: 'd-m-Y' } },
            //{ name: 'RatingContent', index: 'RatingContent', align: "center", width: 60, sortable: false, editable: false },
            { name: 'Action', index: '', width: 180, align: "center", sortable: false, fixed: true, formatter: CreateButtonEdit }
        ],
        viewrecords: true,
        sortname: "CreateTime",
        sortorder: "desc",
        sortable: true,
        emptyrecords: "Dữ liệu không tồn tại",
        rowNum: 15,
        rowList: [15, 25, 35],
        pager: pager_selector,
        altRows: true,
        multiselect: false,
        multikey: "row",
        //multiboxonly: true,
        rownumbers: true,
        loadComplete: function () {
            var table = this;
            setTimeout(function () {
                styleCheckbox(table);
                updateActionIcons(table);
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
        }

        //editurl: "./dummy.php",//nothing is saved
        //caption: "jqGrid with inline editing"
        //,autowidth: true,
        //,
        //grouping:true,
        //groupingView : {
        //     groupField : ['name'],
        //     groupDataSorted : true,
        //     plusicon : 'fa fa-chevron-down bigger-110',
        //     minusicon : 'fa fa-chevron-up bigger-110'
        //},
        //caption: "Grouping"
    });
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    //config button
    jQuery(grid_selector).jqGrid('navGrid', pager_selector,
        { 	//navbar options
            edit: false,
            editicon: 'ace-icon fa fa-pencil blue',
            add: false,
            addicon: 'ace-icon fa fa-plus-circle purple',
            del: false,
            delicon: 'ace-icon fa fa-trash-o red',
            search: false,
            searchicon: 'ace-icon fa fa-search orange',
            refresh: true,
            refreshicon: 'ace-icon fa fa-refresh green',
            view: false,
            viewicon: 'ace-icon fa fa-search-plus grey',
        },
        {
            //edit record form
            //closeAfterEdit: true,
            //width: 700,
            recreateForm: true,
            beforeShowForm: function (e) {
                var form = $(e[0]);
                form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />');
                style_edit_form(form);
            }
        },
        {
            //new record form
            //width: 700,
            closeAfterAdd: true,
            recreateForm: true,
            viewPagerButtons: false,
            beforeShowForm: function (e) {
                var form = $(e[0]);
                form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />');
                style_edit_form(form);
            }
        },
        {
            //delete record form
            recreateForm: true,
            beforeShowForm: function (e) {
                var form = $(e[0]);
                if (form.data('styled')) return false;

                form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />');
                style_delete_form(form);

                form.data('styled', true);
            },
            onClick: function (e) {
                //alert(1);
            }
        },
        {
            //search form
            recreateForm: true,
            afterShowSearch: function (e) {
                var form = $(e[0]);
                form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />');
                style_search_form(form);
            },
            afterRedraw: function () {
                style_search_filters($(this));
            },
            multipleSearch: true
            /**
            multipleGroup:true,
            showQuery: true
            */
        },
        {
            //view record form
            recreateForm: true,
            beforeShowForm: function (e) {
                var form = $(e[0]);
                form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />');
            }
        }
    );

    //it causes some flicker when reloading or navigating grid
    //it may be possible to have some custom formatter to do this as the grid is being created to prevent this
    //or go back to default browser checkbox styles for the grid
    //Config check
    function styleCheckbox(table) {

        $(table).find('input:checkbox').addClass('ace')
            .wrap('<label />')
            .after('<span class="lbl align-top" />');

        $('.ui-jqgrid-labels th[id*="_cb"]:nth-child(2)')
            .find('input.cbox[type=checkbox]').addClass('ace')
            .wrap('<label />').after('<span class="lbl align-top" />');
        $('.ui-jqgrid-labels th[id*="_cb"]:nth-child(2)').find('div').addClass('input-check-all');
    }

    //unlike navButtons icons, action icons in rows seem to be hard-coded
    //you can change them like this in here if you want
    function updateActionIcons(table) {
        var replacement =
        {
            'ui-ace-icon fa fa-pencil': 'ace-icon fa fa-pencil blue',
            'ui-ace-icon fa fa-trash-o': 'ace-icon fa fa-trash-o red',
            'ui-icon-disk': 'ace-icon fa fa-check green',
            'ui-icon-cancel': 'ace-icon fa fa-times red'
        };
        $(table).find('.ui-pg-div span.ui-icon').each(function () {
            var icon = $(this);
            var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
            if ($class in replacement) icon.attr('class', 'ui-icon ' + replacement[$class]);
        });

    }

    //replace icons with FontAwesome icons like above
    function updatePagerIcons(table) {
        var replacement =
        {
            'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
            'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
            'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
            'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
        };
        $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function () {
            var icon = $(this);
            var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

            if ($class in replacement) icon.attr('class', 'ui-icon ' + replacement[$class]);
        });
    }

    //Tootip
    function enableTooltips(table) {
        $('.navtable .ui-pg-button').tooltip({ container: 'body' });
        $(table).find('.ui-pg-div').tooltip({ container: 'body' });
        $('[data-rel=tooltip]').tooltip({ container: 'body' });
    }

    //create btn
    function CreateButtonEdit(cellValue, options, rowObject) {
        var html = "<a href='#' onclick='Update(" + options.rowId + ");' id='btn-edit' class='btn btn-primary radius btn-none-boder magin-right-3px' data-rel='tooltip' title='Cập nhật' data-original-title='Cập nhật'>";
        html += "<i class='ace-icon fa fa-pencil-square-o magin-right-none'></i>";
        html += "</a>";

        var html2 = "<a href='#' onclick='DeleteData(" + options.rowId + ");' id='btn-delete' class='btn btn-danger radius btn-none-boder' data-rel='tooltip' title='Xóa đơn hàng' data-original-title='Xóa đơn hàng'>";
        html2 += "<i class='ace-icon fa fa-trash-o magin-right-none'></i>";
        html2 += "</a>";

        var html3 = "<a href='#' onclick='ShowOrderDetail(" + options.rowId + ");' id='btn-view' class='btn btn-success radius btn-none-boder magin-right-3px' data-rel='tooltip' title='Xem chi tiết' data-original-title='Xem chi tiết'>";
        html3 += "<i class='ace-icon fa fa-search-plus magin-right-none'></i>";
        html3 += "</a>";

        var html4 = "<a href='#' onclick='UpdateStatusOrder(" + options.rowId + "," + rowObject.OrderStatusId + ");' id='btn-updatestatus' class='btn btn-info radius btn-none-boder magin-right-3px' data-rel='tooltip' title='Cập nhật tình trạng đơn hàng' data-original-title='Cập nhật tình trạng đơn hàng'>";
        html4 += "<i class='ace-icon glyphicon glyphicon-share magin-right-none'></i>";
        html4 += "</a>";

        var html5 = "<a href='#' onclick='printDiv(" + options.rowId + ");' id='btn-print' class='btn btn-primary radius btn-none-boder magin-right-3px' data-rel='tooltip' title='In đơn hàng' data-original-title='In đơn hàng'>";
        html5 += "<i class='ace-icon fa fa-print magin-right-none'></i>";
        html5 += "</a>";

        var html6 = "<a href='#' onclick='UpdateRating(" + options.rowId + ");' id='btn-updaterating' class='btn btn-warning radius btn-none-boder magin-right-3px' data-rel='tooltip' title='Xác nhận đánh giá' data-original-title='Xác nhận đánh giá'>";
        html6 += "<i class='ace-icon glyphicon glyphicon-check magin-right-none'></i>";
        html6 += "</a>";

        //return html3 + html4 + html + html5 + html2;
        return html3 + html4 + html6 + html2;
    }

    //create btn
    function FormatImage(cellValue, options, rowObject) {
        var html = "<img id='img' width='30' height='30' src='" + rowObject.Thumb + "' alt='' class='img-thumbnail padding-0'>";
        return html;
    }

    //Load lại page
    $(document).one('ajaxloadstart.page', function (e) {
        $.jgrid.gridDestroy(grid_selector);
        $('.ui-jqdialog').remove();

    });

});
