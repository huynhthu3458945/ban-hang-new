﻿$(document).ready(function () {
    $("#frmArticle").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            //"ArticleCategoryId": {
            //    required: true
            //},
            "Title": {
                required: true
            }, 
            "Position": {
                required: true
            },
            "Description": {
                required: true
            }
        },
        messages: {
            //"ArticleCategoryId": {
            //    required: "Vui lòng nhập."
            //},
            "Title": {
                required: "Vui lòng nhập."
            },
            "Position": {
                required: "Vui lòng nhập."
            },
            "Description": {
                required: "Vui lòng nhập."
            }
        }
    });
});