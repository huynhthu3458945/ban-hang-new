﻿
let PAGE_NUMBER = 208;
let PAGE_SIZE = 30;

function reloadDatable(gridSelector) {
    jQuery(gridSelector).setGridParam({ datatype: 'json' }).trigger('reloadGrid');
}

const productService = {
    fetchProductCategory: function () {
        const domainId = $("#domain").val();
        $.ajax({
            type: 'POST',
            url: '/Admin/AdminProductCategory/FetchProductCategory',
            data: { domainId: domainId },
            success: function (result) {
                if (result.Status) {
                    toastr.success("Cập nhật thành công.", "Thành công !");
                    reloadDatable("#grid-table");
                }
                else {
                    toastr.error("Lỗi hệ thống.", "Không thành công!");
                }
            }
        })
    },
    fetchProductByCategory: function () {
        const productCategoryId = $("#productCategoryId").val();
        const pageNumber = $("#pageNumber").val();
        const pageSize = $("#pageSize").val();

        $.ajax({
            type: 'POST',
            url: '/Admin/AdminProduct/FetchProduct',
            data: {
                productCategoryId: productCategoryId,
                pageNumber: pageNumber,
                pageSize: pageSize
            },
            success: function (result) {
                if (result.Status == true) {
                    toastr.success(result.Message, "Thành công !");
                    reloadDatable("#grid-table");
                    $("#pageNumber").val(parseInt(pageNumber) + 1)
                }
                else {
                    if (result.Message == "Hoàn tất") {
                        toastr.warning(result.Message);
                        reloadDatable("#grid-table");
                    } else {
                        toastr.error(result.Message);
                    }
                }

            }
        })
    },
    autoFetch: function () {
        const productCategoryId = $("#productCategoryId").val();
        const pageNumber = $("#pageNumber").val();
        const pageSize = $("#pageSize").val();

        $.ajax({
            type: 'POST',
            url: '/Admin/AdminProduct/FetchProduct',
            data: {
                productCategoryId: productCategoryId,
                pageNumber: pageNumber,
                pageSize: pageSize
            },
            success: function (result) {
                if (result.Status == true) {
                    toastr.success(result.Message);
                    reloadDatable("#grid-table");
                    $("#pageNumber").val(parseInt(pageNumber) + 1);

                    window.autoFetch = setTimeout(function () {
                        productService.autoFetch();
                    }, 500);
                }
                else {
                    if (result.Message == "Hoàn tất") {
                        toastr.warning(result.Message);
                        reloadDatable("#grid-table");
                    } else {
                        toastr.error(result.Message);
                    }
                   
                    clearTimeout(window.autoFetch)
                }
            }
        })
    },
    updateContentFromDimuadi: function (token) {
        $.ajax({
            type: 'GET',
            url: `/Admin/AdminProduct/UpdateContentsDimuadi?token=${token}`,
            success: function (result) {
                console.log(">> ", result)
            }
        })
    },
    autoFetchCoupon: function (token) {
        console.log(`Fetch >> page ${PAGE_NUMBER}, pageSize ${PAGE_SIZE}`);
        $.ajax({
            type: 'POST',
            url: '/Admin/AdminProduct/FetchCoupon',
            data: {
                token: token,
                pageNumber: PAGE_NUMBER,
                pageSize: PAGE_SIZE
            },
            success: function (result) {
                console.log("Result >> ", result)
                if (result.Status == true) {
                    PAGE_NUMBER++;
                    window.autoFetchCoupon = setTimeout(function () {
                        productService.autoFetchCoupon(token);
                    }, 500);
                    toastr.success(result.Message);
                }
                else {
                    if (result.Message == "Hoàn tất") {
                        toastr.warning(result.Message);
                    } else {
                        toastr.error(result.Message);
                    }

                    clearTimeout(window.autoFetchCoupon)
                }
            }
        })
    }
    
}

$("#productCategoryId").change(function () {
    $("#pageNumber").val(1)
})

function updateProductContents(token) {
    productService.updateContentFromDimuadi(token);
}

function fetchPoupon(token) {
    productService.autoFetchCoupon(token);
}

function groupCouponTypes() {
    $.ajax({
        type: 'GET',
        url: `/Admin/AdminProduct/UpdateCouponTypes`,
        success: function (result) {
            console.log(">> ", result)
        }
    })
}

function updateCoupons() {
    $.ajax({
        type: 'GET',
        url: `/Admin/AdminProduct/UpdateCoupons`,
        success: function (result) {
            console.log(">> ", result)
        }
    })
}

function getAccessTradeProducts(domain, token) {
    const page = 1;
    const pageSize = 100;
    $.ajax({
        type: 'GET',
        url: `/Admin/AdminProduct/GetAccessTradeProducts`,
        data: {
            domain: domain,
            page: page,
            pageSize: pageSize
        },
        success: function (result) {
            console.log(">> ", result)
        }
    })
}

function fetchBannerFromDimuadi(token) {
    $.ajax({
        type: 'GET',
        url: `/Admin/AdminProduct/FetchBannerFromDimuadi`,
        data: {
            token: token,
        },
        success: function (result) {
            console.log(">> ", result)
        }
    })
}

function processFetchProductDataByCategory(productCategoryId) {
    $.ajax({
        type: 'GET',
        url: `/Admin/AdminProduct/ProcessFetchProductDataByCategory`,
        data: {
            productCategoryId: productCategoryId,
        },
        success: function (result) {
            console.log(">> ", result)
        }
    });
}


var letPage = 1;
function fetchAccesstradeData() {
    $.ajax({
        type: 'GET',
        url: `/Admin/AdminProduct/FetchAccesstradeData`,
        data: {
            url: "https://api.accesstrade.vn/v1/datafeeds?domain=shopee.vn",
            page: letPage,
            tokenValue: "5HLiUI7PnW8TFP-3obDt3DS8iZUY9Q-h"
        },
        success: function (result) {
            letPage++;
            fetchAccesstradeData();
            console.log(">> ", result)
        }
    });
}


function startProcess(productCategoryId) {
    $.ajax({
        type: 'GET',
        url: `/Admin/AdminProduct/StartProcess`,
        data: {
            productCategoryId: productCategoryId
        },
        success: function (result) {
            console.log(">> ", result)
        }
    });
}


$(document).ready(function () {

})