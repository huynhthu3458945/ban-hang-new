﻿$(document).ready(function () {
    $("#frmEmployee").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "FullName": {
                required: true
            }, 
            "Phone": {
                required: true
            },
            "Description": {
                required: true
            }
        },
        messages: {
            "FullName": {
                required: "Vui lòng nhập."
            },
            "Phone": {
                required: "Vui lòng nhập."
            },
            "Description": {
                required: "Vui lòng nhập."
            }
        }
    });
});