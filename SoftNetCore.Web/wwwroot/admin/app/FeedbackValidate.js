﻿$(document).ready(function () {
    $("#frmFeedback").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "FullName": {
                required: true
            }, 
            //"Regency": {
            //    required: true
            //},
            "Content": {
                required: true
            }
        },
        messages: {
            "FullName": {
                required: "Vui lòng nhập."
            },
            //"Regency": {
            //    required: "Vui lòng nhập."
            //},
            "Content": {
                required: "Vui lòng nhập."
            }
        }
    });
});