﻿$(document).ready(function () {
    $("#frmCouponType").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "Title": {
                required: true
            },
            "MerchantName": {
                required: true
            }
        },
        messages: {
            "Title": {
                required: "Vui lòng nhập."
            },
            "MerchantName": {
                required: "Vui lòng nhập."
            }
        }
    });
});