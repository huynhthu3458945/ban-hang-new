﻿$(function () {
    ActionMenu();
});

//Active Menu
function ActionMenu() {
    var urlOld = $(location).attr('pathname');

    var url = '';
    if (urlOld.split('/').length > 3) {
        url = '/' + urlOld.split('/')[1] + '/' + urlOld.split('/')[2];
    }
    else {
        url = urlOld;
    }

    //var paraUrl = url.split('/')[1];
    $(".nav-list").each(function (i) {
        var activemain = false;
        //for sub de active
        $(this).find("a").each(function (j) {
            href = $(this).attr('href');

            //kiem tra xem co sub menu khong
            if (href === '#') {
                $(this).parent().find("a").each(function (jj) {
                    href = $(this).attr('href');
                    //xu ly url
                    var hrefCheck = '';
                    if (href.split('/').length > 3) {
                        hrefCheck = '/' + href.split('/')[1] + '/' + href.split('/')[2];
                    }
                    else {
                        hrefCheck = href;
                    }

                    //alert(url + ' | ' + hrefCheck.split('/').length + ' | ');

                    if (url === hrefCheck) {
                        $(this).closest("ul").parent().addClass('active open');
                        activemain = true;
                    }
                });
            }
            //neu khong co sub
            else if (url === href) {
                $(this).parent().addClass('active');
                activemain = true;
            }

            if (urlOld === href) {
                $(this).parent().addClass('active');
            }
        });

        //active main
        if (activemain === true) {
            $(this).addClass('active open');
        }
    });
}

function SubmitFormByID(frmID, isReturn) {
    if (isReturn) {
        //add param return
        $("#isReturn").val('true');
        $('#' + frmID).submit();
    }
    else {
        $("#isReturn").val('false');
        $('#' + frmID).submit();
    }
}

function OnSuccess(data) {
    ShowMessage(data);
}

function OnFailure(data) { }

function ShowMessage(data) {
    if (data.IsReturn)
        window.location.href = data.RedirectUrl;
    else {
        $.gritter.add({
            title: data.Title,
            text: data.Message,
            class_name: data.Type
        });
    }
}


$(document).ready(function () {
    initValidate();
});

function initValidate() {
    $("#frmChangePassword").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "OldPassword": {
                required: true
            },
            "NewPassword": {
                required: true
            },
            "ConfirmPassword": {
                required: true,
                equalTo: "#NewPassword"
            }
        },
        messages: {
            "OldPassword": {
                required: "Vui lòng nhập."
            },
            "NewPassword": {
                required: "Vui lòng nhập."
            },
            "ConfirmPassword": {
                required: "Vui lòng nhập.",
                equalTo: "Nhập lại mật khẩu chưa chính xác"
            }
        }
    });
}

function ChangePassword() {
    var url = $("#ChangePassword").data('url');
    $.get(url, function (data) {
        $("#ChangePassword").html(data);
        $("#ChangePassword").modal('show');
        initValidate();
    });
}