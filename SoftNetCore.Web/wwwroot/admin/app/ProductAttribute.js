﻿$(function () {
    RenderColor();
    RenderSize();
});

// Color
function AddColor() {
    var url = $("#AddColor").data('url');
    $.get(url, function (data) {
        $("#AddColor").html(data);
        $("#AddColor").modal('show');
    });
}

function UpdateColor(id) {
    var url = '/admin/adminproduct/updatecolor?id=' + id;
    $.get(url, function (data) {
        $("#UpdateColor").html(data);
        $("#UpdateColor").modal('show');
    });
}

function DeleteColor(productId, colorId) {
    if (colorId !== '') {
        $.ajax({
            type: 'POST',
            url: '/admin/AdminProduct/DeleteColor',
            async: true,
            dataType: 'json',
            data: { productId: productId, colorId: colorId },
            success: function (result) {
                if (result.Status) {
                    toastr.success("Xóa thành công.", "Thành công !");
                    setTimeout(function () {
                        RenderColor();
                    }, 500); //delay is in milliseconds
                }
            }
        })
            .fail(
                function (jqXHR, textStatus, err) {

                });
    }
}

function RenderColor() {
    $.ajax({
        type: 'GET',
        url: '/admin/AdminProduct/DetailColor' + "?t=" + Math.random(),
        async: true,
        dataType: 'html',
        success: function (result) {
            $('#tbody_color').empty();
            $("#tbody_color").html(result);
        }
    })
        .fail(
            function (jqXHR, textStatus, err) {

            });
}

//Size
function AddSize() {
    var url = $("#AddSize").data('url');
    $.get(url, function (data) {
        $("#AddSize").html(data);
        $("#AddSize").modal('show');
    });
}

function UpdateSize(sizeId, colorId) {
    var url = '/admin/adminproduct/updatesize?sizeId=' + sizeId + '&colorId=' + colorId;
    $.get(url, function (data) {
        $("#UpdateSize").html(data);
        $("#UpdateSize").modal('show');
    });
}

function DeleteSize(productId, sizeId, colorId) {
    if (sizeId !== '' && colorId !== "") {
        $.ajax({
            type: 'POST',
            url: '/admin/AdminProduct/DeleteSize',
            async: true,
            dataType: 'json',
            data: { productId: productId, sizeId: sizeId, colorId: colorId },
            success: function (result) {
                if (result.Status) {
                    toastr.success("Xóa thành công.", "Thành công !");
                    setTimeout(function () {
                        RenderSize();
                    }, 500); //delay is in milliseconds
                    
                }
            }
        })
            .fail(
                function (jqXHR, textStatus, err) {

                });
    }
}

function RenderSize() {
    $.ajax({
        type: 'GET',
        url: '/admin/AdminProduct/DetailSize' + "?t=" + Math.random(),
        async: true,
        dataType: 'html',
        success: function (result) {
            $('#tbody_size').empty();
            $("#tbody_size").html(result);
        }
    })
        .fail(
            function (jqXHR, textStatus, err) {

            });
}

// result
function OnSuccessAttribute(data) {
    $("#AddColor").modal('hide');
    $("#UpdateColor").modal('hide');
    $("#AddSize").modal('hide');
    $("#UpdateSize").modal('hide');
    RenderColor();
    RenderSize();
}

function OnFailureAttribute(data) { }


