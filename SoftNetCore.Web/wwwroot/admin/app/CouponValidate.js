﻿$(document).ready(function () {

    $.validator.addMethod('minStrict', function (value, el, param) {
        return value >= param;
    });

    $("#frmCoupon").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "Code": {
                required: true
            }, 
            "Title": {
                required: true
            }, 
            //"Position": {
            //    required: true
            //},
        },
        messages: {
            "Code": {
                required: "Vui lòng nhập mã giảm giá"
            },
            "Title": {
                required: "Vui lòng nhập tên mã giảm giá."
            },
            //"Position": {
            //    required: "Vui lòng nhập độ ưu tiên."
            //},
        }
    });
});