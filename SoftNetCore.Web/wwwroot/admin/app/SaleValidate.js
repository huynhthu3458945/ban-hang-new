﻿$(document).ready(function () {
    $("#frmSale").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "Title": {
                required: true
            },
            //"PercentSale": {
            //    required: true
            //},
            "TimeStart": {
                required: true
            }
            ,
            "TimeEnd": {
                required: true
            }
        },
        messages: {
            "Title": {
                required: "Vui lòng nhập."
            },
            //"PercentSale": {
            //    required: "Vui lòng nhập."
            //},
            "TimeStart": {
                required: "Vui lòng nhập."
            }
            ,
            "TimeEnd": {
                required: "Vui lòng nhập."
            }
        }
    });
});