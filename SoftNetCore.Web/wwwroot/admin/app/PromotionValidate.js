﻿$(document).ready(function () {
    $("#frmPromotion").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            //"PromotionCategoryId": {
            //    required: true
            //},
            "Title": {
                required: true
            }, 
            "Position": {
                required: true
            },
            "Description": {
                required: true
            }
        },
        messages: {
            //"PromotionCategoryId": {
            //    required: "Vui lòng nhập."
            //},
            "Title": {
                required: "Vui lòng nhập."
            },
            "Position": {
                required: "Vui lòng nhập."
            },
            "Description": {
                required: "Vui lòng nhập."
            }
        }
    });
});