﻿$(document).ready(function () {
    $("#frmBanerSale").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "Title": {
                required: true
            }
        },
        messages: {
            "Title": {
                required: "Vui lòng nhập."
            }
        }
    });
});