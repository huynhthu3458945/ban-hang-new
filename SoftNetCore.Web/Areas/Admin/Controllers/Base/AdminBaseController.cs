﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Web.Models;
using System;

namespace SoftNetCore.Web.Areas.Admin.Controllers.Base
{
    public class AdminBaseController : Controller
    {
        public AdminBaseController()
        {
        }

        protected ActionResult SafeOk(object value)
        {
            var response = new ResponseModel();
            if (value == null)
            {
                response.Status = false;
                response.Type = EnumsTypeMessage.Error;
                response.Data = value;
            };

            return Json(response, new Newtonsoft.Json.JsonSerializerSettings());
        }

        protected ActionResult SafeOk(string message)
        {
            var response = new ResponseModel
            {
                Message = message
            };

            if (string.IsNullOrEmpty(message))
            {
                response.Status = false;
                response.Type = EnumsTypeMessage.Error;
            };

            return Json(response, new Newtonsoft.Json.JsonSerializerSettings());
        }

        protected ActionResult SafeOk(Exception ex)
        {
            var response = new ResponseModel
            {
                Status = false,
                Type = EnumsTypeMessage.Error,
                Message = ex.Message
            };

            return Json(response, new Newtonsoft.Json.JsonSerializerSettings());
        }

        protected ActionResult SafeOk() => Json(true, new Newtonsoft.Json.JsonSerializerSettings());
    }
}
