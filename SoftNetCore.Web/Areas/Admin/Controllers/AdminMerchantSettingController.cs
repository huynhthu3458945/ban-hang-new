﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminMerchantSettingController : Controller
    {
        private readonly ISessionService _sessionService;
        private readonly IMerchantSettingService _merchantSettingService;
        private readonly ILogging _logging;
        public static int INDEX = 0;

        public AdminMerchantSettingController(
            ISessionService sessionService,
            IMerchantSettingService merchantSettingService,
            ILogging logging)
        {
            _sessionService = sessionService;
            _merchantSettingService = merchantSettingService;
            _logging = logging;
        }

        [HttpGet]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            var data = (await _merchantSettingService.GetAllAsync()).AsEnumerable();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords = data.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new { total = totalPages, page, records = totalRecords, rows = data };
            return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());

        }

        [HttpGet]
        public IActionResult Index()
        {
            var enumDomains = Enum.GetValues(typeof(EnumDomains)).Cast<EnumDomains>().ToList();
            ViewBag.EnumDomains = enumDomains;

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Insert()
        {
            var enumDomains = Enum.GetValues(typeof(EnumDomains)).Cast<EnumDomains>().ToList();
            ViewBag.EnumDomains = enumDomains;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Insert(MerchantSettingModel model)
        {
            string typeMessage;
            string mesage;
            try
            {
                var result = await _merchantSettingService.AddAsync(model);
                typeMessage = result.Success ? EnumsTypeMessage.Success : EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory") }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var enumDomains = Enum.GetValues(typeof(EnumDomains)).Cast<EnumDomains>().ToList();
            ViewBag.EnumDomains = enumDomains;

            var data = await _merchantSettingService.FindById(id);

            return View(data);
        }

        [HttpPost]
        public async Task<IActionResult> Update(MerchantSettingModel model)
        {
            string typeMessage;
            string mesage;
            try
            {
                var result = await _merchantSettingService.UpdateAsync(model);
                typeMessage = result.Success ? EnumsTypeMessage.Success : EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory") }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            string typeMessage;
            string mesage;
            try
            {
                var result = await _merchantSettingService.DeleteAsync(id);
                typeMessage = result.Success ? EnumsTypeMessage.Success : EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }
    }
}
