﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminCollectionController : Controller
    {
        #region Fields
        private readonly ICollectionService _collectionService;
        private readonly IProductService _productService;
        private readonly IProductCollectionService _productCollectionService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public AdminCollectionController(ICollectionService collectionService, IProductService productService, IProductCollectionService productCollectionService, ILogging logging)
        {
            _collectionService = collectionService;
            _productService = productService;
            _productCollectionService = productCollectionService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        //[NonAction]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            var data = (await _collectionService.GetDataAsync()).AsEnumerable();
            //Setting Paging
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //Setting Search
            if (!string.IsNullOrEmpty(searchString) && (searchStatus != string.Empty && !string.IsNullOrEmpty(searchStatus)))
            {
                data = data.Where(m => m.Status == searchStatus.ToBool() && m.Title.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(m => m.Title.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchStatus))
            {
                data = data.Where(m => m.Status == searchStatus.ToBool());
            }

            //Get Total Row Count
            int totalRecords = data.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            //Setting Sorting
            if (sord.ToUpper() == "DESC")
            {
                data = data.OrderByDescending(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                data = data.OrderBy(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            //Sending Json Object to View.
            var jsonData = new { total = totalPages, page, records = totalRecords, rows = data };
            return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());

        }

        [HttpGet]
        //[NonAction]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        //[NonAction]
        public IActionResult Insert()
        {
            List<ProductItem> listProductItem = new List<ProductItem>();
            HttpContext.Session.SetString("productCollection", JsonConvert.SerializeObject(listProductItem));
            //
            CollectionModel model = new CollectionModel();
            model.Position = 1;
            model.Status = true;
            return View(model);
        }

        [HttpPost]
        //[NonAction]
        public async Task<IActionResult> Insert(CollectionModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _collectionService.AddAsync(model);
                if (result.Success)
                {
                    var collection = (Collection)result.Data;
                    //
                    typeMessage = EnumsTypeMessage.Success;
                    List<ProductItem> listProductItem = new List<ProductItem>();
                    var productCollection = HttpContext.Session.GetString("productCollection");
                    if (!string.IsNullOrEmpty(productCollection))
                    {
                        listProductItem = JsonConvert.DeserializeObject<List<ProductItem>>(productCollection);
                    }
                    foreach (var item in listProductItem)
                    {
                        ProductCollectionModel model1 = new ProductCollectionModel();
                        model1.ProductId = item.ProductId;
                        model1.CollectionId = collection.Id;
                        model1.CreateTime = DateTime.Now;
                        await _productCollectionService.AddAsync(model1);
                    }
                    HttpContext.Session.SetString("productCollection", "");
                }
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCollection"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            if (id == 0)
            {
                return View();
            }
            var data = await _collectionService.FindById(id);
            var productCollection = await _productCollectionService.GetByCollectionAsync(id);
            //
            List<ProductItem> listProductItem = new List<ProductItem>();
            foreach (var item in productCollection)
            {
                int index = 0;
                ListProductItem.GetItemIndex(listProductItem, item.ProductId, ref index);

                if (listProductItem == null || index == 0)
                {
                    var productItem = ListProductItem.AddList(item.ProductId, item.Product?.Title, item.Product?.Avatar, item.Product.Price.ToDouble(), item.CollectionId);
                    if (listProductItem == null) { listProductItem = productItem; } else { listProductItem.Add(productItem.FirstOrDefault()); }
                }
            }
            HttpContext.Session.SetString("productCollection", JsonConvert.SerializeObject(listProductItem));

            return View(data);
        }

        [HttpPost]
        public async Task<IActionResult> Update(CollectionModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _collectionService.UpdateAsync(model);
                if (result.Success)
                {
                    typeMessage = EnumsTypeMessage.Success;
                    List<ProductItem> listProductItem = new List<ProductItem>();
                    var productCollection = HttpContext.Session.GetString("productCollection");
                    if (!string.IsNullOrEmpty(productCollection))
                    {
                        listProductItem = JsonConvert.DeserializeObject<List<ProductItem>>(productCollection);
                    }
                    foreach (var item in listProductItem)
                    {
                        var check = await _productCollectionService.FindByReferenceId(model.Id, item.ProductId);
                        if (check == null)
                        {
                            ProductCollectionModel model1 = new ProductCollectionModel();
                            model1.ProductId = item.ProductId;
                            model1.CollectionId = model.Id;
                            model1.CreateTime = DateTime.Now;
                            await _productCollectionService.AddAsync(model1);
                        }
                    }
                    HttpContext.Session.SetString("productCollection", "");
                }
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCollection"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<ActionResult> UpdateStatus(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _collectionService.UpdateStatusAsync(id);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCollection"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                await _productCollectionService.DeleteByCollectionAsync(id);
                var result = await _collectionService.DeleteAsync(id);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCollection"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public ActionResult ShowSelectProduct()
        {
            return PartialView();
        }

        [HttpGet]
        public async Task<JsonResult> LoadProduct(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            try
            {
                IList<ProductItem> listProductItem = new List<ProductItem>();
                var productCollection = HttpContext.Session.GetString("productCollection");
                if (!string.IsNullOrEmpty(productCollection))
                {
                    listProductItem = JsonConvert.DeserializeObject<List<ProductItem>>(productCollection);
                }

                var data = (await _productService.GetDataAsync()).AsEnumerable();

                data = data.Where(x => x.Status == true && (!listProductItem.Any(y => y.ProductId == x.Id)));
                //Setting Paging
                int pageIndex = Convert.ToInt32(page) - 1;
                int pageSize = rows;

                //Setting Search
                if (!string.IsNullOrEmpty(searchString) && (searchStatus != string.Empty && !string.IsNullOrEmpty(searchStatus)))
                {
                    data = data.Where(m => m.ProductCategoryId == searchStatus.ToInt() && m.Title.ToLower().Contains(searchString.ToLower()));
                }
                if (!string.IsNullOrEmpty(searchString))
                {
                    data = data.Where(m => m.Title.ToLower().Contains(searchString.ToLower()));
                }
                if (!string.IsNullOrEmpty(searchStatus))
                {
                    data = data.Where(m => m.ProductCategoryId == searchStatus.ToInt());
                }

                //Get Total Row Count
                int totalRecords = data.Count();
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
                //Setting Sorting
                if (sord.ToUpper() == "DESC")
                {
                    data = data.OrderByDescending(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                    data = data.Skip(pageIndex * pageSize).Take(pageSize);
                }
                else
                {
                    data = data.OrderBy(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                    data = data.Skip(pageIndex * pageSize).Take(pageSize);
                }

                //Sending Json Object to View.
                var jsonData = new { total = totalPages, page, records = totalRecords, rows = data };
                return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());
            }
            catch (Exception ex) { }
            return Json("", new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public ActionResult SelectProduct(List<string> listProduct)
        {
            // Đưa sản phẩm vào danh sách collection
            if (listProduct == null)
                return Json(new { Status = true }, new Newtonsoft.Json.JsonSerializerSettings());

            if (listProduct.Count == 0)
                return Json(new { Status = true }, new Newtonsoft.Json.JsonSerializerSettings());

            //Lay list da chon
            List<ProductItem> listProductItem = new List<ProductItem>();
            var productCollection = HttpContext.Session.GetString("productCollection");
            if (!string.IsNullOrEmpty(productCollection))
            {
                listProductItem = JsonConvert.DeserializeObject<List<ProductItem>>(productCollection);
            }

            //xu ly
            listProduct.ForEach(x =>
            {
                if (x != string.Empty)
                {
                    //lay data
                    var data = _productService.GetById(x.ToInt());
                    //Kiem tra
                    bool isExists = false;
                    ListProductItem.CheckExists(listProductItem, x.ToInt(), ref isExists);
                    if (!isExists)
                    {
                        var productItem = ListProductItem.AddList(data.Id, data.Title, data.Avatar, data.Price.ToDouble(), null);
                        listProductItem.Add(productItem.FirstOrDefault());
                    }
                }
            });
            HttpContext.Session.SetString("productCollection", JsonConvert.SerializeObject(listProductItem));
            return Json(new { Status = true }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<IActionResult> DeleteProductCollection(int id, int? idColl)
        {
            var productCollection = HttpContext.Session.GetString("productCollection");
            List<ProductItem> listProductItem = JsonConvert.DeserializeObject<List<ProductItem>>(productCollection);
            int index = 0;
            ListProductItem.Remove(listProductItem, ListProductItem.GetItemIndex(listProductItem, id, ref index));
            HttpContext.Session.SetString("productCollection", JsonConvert.SerializeObject(listProductItem));
            if (idColl > 0)
            {
                await _productCollectionService.DeleteByReferenceIdAsync(idColl.ToInt(), id);
            }

            return Json(new { Status = true }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public ActionResult DetailProductCollection()
        {
            List<ProductItem> listProductItem = new List<ProductItem>();
            var productCollection = HttpContext.Session.GetString("productCollection");
            if (!string.IsNullOrEmpty(productCollection))
            {
                listProductItem = JsonConvert.DeserializeObject<List<ProductItem>>(productCollection);
            }
            return View(listProductItem);
        }
    }
}
