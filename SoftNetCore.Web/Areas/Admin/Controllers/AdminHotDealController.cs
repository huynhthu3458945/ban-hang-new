﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Common.Helpers;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminHotDealController : Controller
    {
        #region Fields
        private readonly IBannerService _bannerService;
        private readonly IProductService _productService;
        private readonly IProductRepository _productRepository;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public AdminHotDealController(IBannerService bannerService, IProductService productService,
            IProductRepository productRepository, ILogging logging)
        {
            _bannerService = bannerService;
            _productService = productService;
            _productRepository = productRepository;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        //[NonAction]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            var data = (await _bannerService.GetDataAsync()).AsEnumerable();
            //Setting Paging
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //Setting Search
            if (!string.IsNullOrEmpty(searchString) && (searchStatus != string.Empty && !string.IsNullOrEmpty(searchStatus)))
            {
                data = data.Where(m => m.Status == searchStatus.ToInt() && m.Title.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(m => m.Title.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchStatus))
            {
                data = data.Where(m => m.Status == searchStatus.ToInt());
            }

            //Get Total Row Count
            int totalRecords = data.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            //Setting Sorting
            if (sord.ToUpper() == "DESC")
            {
                data = data.OrderByDescending(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                data = data.OrderBy(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            //Sending Json Object to View.
            var jsonData = new { total = totalPages, page, records = totalRecords, rows = data };
            return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());

        }


        //Load data Json
        [HttpGet]
        public async Task<JsonResult> LoadProducts(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            try
            {
                //Setting Paging
                int pageIndex = Convert.ToInt32(page) - 1;
                int pageSize = rows;
                //Get Total Row Count
                int totalRecords = !string.IsNullOrEmpty(searchStatus) ? _productService.GetTotalByBanner(searchStatus.ToInt(), true): 0;
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
                var result = !string.IsNullOrEmpty(searchStatus) ? (await _productRepository.GetByBannerAsync(searchStatus.ToInt(), pageSize, pageIndex, searchString, sidx, sord)) : new List<DataAccess.ViewModel.ProductViewModel>();
                //Sending Json Object to View.
                var jsonData = new { total = totalPages, page, records = totalRecords, rows = result };
                return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());
            }
            catch (Exception ex) { }
            return Json("", new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        //[NonAction]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        //[NonAction]
        public async Task<ActionResult> Products()
        {
            ViewBag.DropBanner = await _bannerService.GetAllAsync();
            return View();
        }

        [HttpGet]
        //[NonAction]
        public IActionResult Insert()
        {
            BannerModel model = new BannerModel();
            model.Position = 1;
            model.Status = 1;
            return View(model);
        }

        [HttpPost]
        //[NonAction]
        public async Task<IActionResult> Insert(BannerModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _bannerService.AddAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminHotDeal"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var data = await _bannerService.FindById(id);
            return View(data);
        }

        [HttpPost]
        public async Task<IActionResult> Update(BannerModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _bannerService.UpdateAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminHotDeal"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _bannerService.DeleteAsync(id);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminHotDeal"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<ActionResult> UpdateStatus(int id)
        {
            string typeMessage;
            string mesage;
            try
            {
                var result = await _bannerService.UpdateStatusAsync(id);
                typeMessage = result.Success ? EnumsTypeMessage.Success : EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }
    }
}
