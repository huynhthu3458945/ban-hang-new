﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Model.Request;
using SoftNetCore.Services.BaseRepository;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminOrderController : Controller
    {
        #region Fields
        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly IOrderDetailService _ordersDetailService;
        private readonly IAssessService _assessService;
        private readonly ILogging _logging;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Ctor
        public AdminOrderController(IProductService productService, IOrderService orderService,
                                    IOrderDetailService ordersDetailService, IAssessService assessService, ILogging logging, IUnitOfWork unitOfWork)
        {
            _productService = productService;
            _orderService = orderService;
            _ordersDetailService = ordersDetailService;
            _assessService = assessService;
            _logging = logging;
            _unitOfWork = unitOfWork;
        }
        #endregion

        //Load data Json
        [HttpGet]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            try
            {
                var result = (await _orderService.GetDataAsync()).AsEnumerable();
                //Setting Paging
                int pageIndex = Convert.ToInt32(page) - 1;
                int pageSize = rows;
                var data = result.Select(a => new
                {
                    a.Id,
                    a.FullName,
                    a.Email,
                    a.Mobi,
                    //a.FeeShip,
                    a.TotalPrice, // tổng tiền hàng
                    a.Status,
                    a.OrderStatusId,
                    StatusName = a.OrderStatus?.Title,
                    a.CreateTime,
                    //Total = a.TotalPrice.ToDecimal() - a.FeeShip.ToDecimal(),
                    a.Code,
                    a.Address,
                    //Address = a.Address + ", " + a.District?.Title + ", " + a.Province?.Title,
                    PaymentName = a.PayType?.Title,
                    //RatingContent = (a.Rating != null ? a.Rating + " Sao | " : "") + a.RatingContent + (a.IsRating == 2 ? " | Đã xác nhận" : "")
                });

                //Setting Search
                if (!string.IsNullOrEmpty(searchString) && (searchStatus != string.Empty && !string.IsNullOrEmpty(searchStatus)))
                {
                    data = data.Where(m => m.OrderStatusId == searchStatus.ToInt() && m.FullName.ToLower().Contains(searchString.ToLower()));
                }
                if (!string.IsNullOrEmpty(searchString))
                {
                    data = data.Where(m => m.FullName.ToLower().Contains(searchString.ToLower()));
                }
                if (!string.IsNullOrEmpty(searchStatus))
                {
                    data = data.Where(m => m.OrderStatusId == searchStatus.ToInt());
                }

                //Get Total Row Count
                int totalRecords = data.Count();
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
                //Setting Sorting
                if (sord.ToUpper() == "DESC")
                {
                    data = data.OrderByDescending(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "CreateTime").GetValue(s));
                    data = data.Skip(pageIndex * pageSize).Take(pageSize);
                }
                else
                {
                    data = data.OrderBy(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "CreateTime").GetValue(s));
                    data = data.Skip(pageIndex * pageSize).Take(pageSize);
                }

                //Sending Json Object to View.
                var jsonData = new { total = totalPages, page, records = totalRecords, rows = data };
                return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());
            }
            catch (Exception ex) { }
            return Json("", new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Detail(int id)
        {
            //detail
            var result = await _orderService.FindFullByIdAsync(id.ToInt());
            return View(result);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                await _ordersDetailService.DeleteByOrderAsync(id);
                var result = await _orderService.DeleteAsync(id);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, responseJSON = "Xóa đơn hàng thành công!", RedirectUrl = Url.Action("Index", "AdminOrder"), IsReturn = true }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public ActionResult UpdateStatusOrder(int id, int status)
        {
            List<OrderStatusRequest> data = new List<OrderStatusRequest>();
            data.Add(new OrderStatusRequest { ID = 1, Name = "Chưa xử lý" });
            data.Add(new OrderStatusRequest { ID = 2, Name = "Đã xử lý" });
            data.Add(new OrderStatusRequest { ID = 3, Name = "Đang giao hàng" });
            data.Add(new OrderStatusRequest { ID = 4, Name = "Đã giao hàng" });
            data.Add(new OrderStatusRequest { ID = 5, Name = "Hủy đơn hàng" });

            ViewBag.Status = data;
            ViewBag.OrderStatus = status;
            ViewBag.ID = id;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UpdateStatus(int id, int status)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _orderService.UpdateStatusAsync(id, status);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminOrder"), IsReturn = true }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<IActionResult> UpdateRating(int id)
        {
            List<AssessModel> assList = new List<AssessModel>();
            bool status = false;
            try
            {
                var order = await _orderService.FindById(id);
                if (order.IsRating != 1 || order.OrderStatusId != 4)
                {
                    return Json(new { Status = false, RedirectUrl = Url.Action("Index", "AdminOrder"), IsReturn = true }, new Newtonsoft.Json.JsonSerializerSettings());
                }
                order.IsRating = 2;
                await _orderService.UpdateAsync(order);
                //
                foreach (var item in order.OrderDetails)
                {
                    var assNew = new AssessModel
                    {
                        FullName = order.FullName,
                        Content = order.RatingContent,
                        NumberStar = order.Rating,
                        KeyId = item.ProductId,
                        KeyName = "Product",
                        CreateTime = order.RatingTime
                    };
                    assList.Add(assNew);
                }
                await _assessService.AddRatingAsync(assList);
                status = true;
            }
            catch (Exception ex)
            {
                status = false;
            }
            finally
            {
                if (status == true)
                {
                    foreach (var item in assList)
                    {
                        decimal[] mathResult = await _assessService.GetMathByKeyAsync(item.KeyId.ToInt(), "Product");
                        decimal avg = mathResult[0];
                        decimal count = mathResult[1];
                        await _productService.UpdateRatingAsync(item.KeyId.ToInt(), (int)count, avg);
                    }
                }
            }
            return Json(new { Status = status, RedirectUrl = Url.Action("Index", "AdminOrder"), IsReturn = true }, new Newtonsoft.Json.JsonSerializerSettings());
        }
    }
}
