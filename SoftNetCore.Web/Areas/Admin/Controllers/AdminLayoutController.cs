﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminLayoutController : Controller
    {
        [HttpGet]
        [NonAction]
        public IActionResult AdminMenu()
        {
            
            return View();
        }

        [HttpGet]
        [NonAction]
        public ActionResult AdminTopAccount()
        {
            return View();
        }
    }
}
