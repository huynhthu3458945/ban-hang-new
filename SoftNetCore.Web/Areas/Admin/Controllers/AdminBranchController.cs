﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminBranchController : Controller
    {
        #region Fields
        private readonly IBranchService _branchService;
        private readonly IPartnerService _partnerService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public AdminBranchController(IBranchService branchService,
                                    IPartnerService partnerService,
                                    IProvinceService provinceService,
                                    IDistrictService districtService,
        ILogging logging)
        {
            _branchService = branchService;
            _partnerService = partnerService;
            _provinceService = provinceService;
            _districtService = districtService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        //[NonAction]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {

            var data = (await _branchService.GetDataAsync()).AsEnumerable();
            //Setting Paging
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //Setting Search
            if (!string.IsNullOrEmpty(searchString) && (searchStatus != string.Empty && !string.IsNullOrEmpty(searchStatus)))
            {
                data = data.Where(m => m.PartnerId == searchStatus.ToInt() && m.Title.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(m => m.Title.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchStatus))
            {
                data = data.Where(m => m.PartnerId == searchStatus.ToInt());
            }

            //Get Total Row Count
            int totalRecords = data.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            //Setting Sorting
            if (sord.ToUpper() == "DESC")
            {
                data = data.OrderByDescending(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                data = data.OrderBy(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            //Sending Json Object to View.
            var jsonData = new { total = totalPages, page, records = totalRecords, rows = data };
            return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());

        }

        [HttpGet]
        public JsonResult LoadDataDrop(string provinceId)
        {
            var data = _districtService.GetByProvinceId(provinceId.ToInt());
            return Json(data.Select(x => new SelectListItem { Text = x.Title, Value = x.Id.ToString() }), new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ViewBag.PartnerDrop = await _partnerService.GetByStatusAsync(true);
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Insert()
        {
            List<ProvinceModel> dataProvince = await _provinceService.GetAllAsync();
            List<SelectListItem> dataProvinceDrop = dataProvince.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();

            List<DistrictModel> dataDistrict = await _districtService.GetAllAsync();
            List<SelectListItem> dataDistrictDrop = dataDistrict.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();

            List<PartnerModel> dataPartner = await _partnerService.GetByStatusAsync(true);
            List<SelectListItem> dataPartnerDrop = dataPartner.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();

            ViewBag.ProvinceID = dataProvinceDrop;
            ViewBag.DistrictID = dataDistrictDrop;
            ViewBag.PartnerID = dataPartnerDrop;

            BranchModel model = new BranchModel();
            model.Status = true;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Insert(BranchModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _branchService.AddAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminBranch"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var data = await _branchService.FindById(id);
            List<ProvinceModel> dataProvince = await _provinceService.GetAllAsync();
            List<SelectListItem> dataProvinceDrop = dataProvince.Select(n =>
            new SelectListItem { Value = n.Id.ToString(), Text = n.Title, Selected = data.ProvinceId == n.Id ? true : false }).ToList();
            ViewBag.ProvinceID = dataProvinceDrop;

            List<DistrictModel> dataDistrict = await _districtService.GetAllAsync();
            List<SelectListItem> dataDistrictDrop = dataDistrict.Select(n =>
            new SelectListItem { Value = n.Id.ToString(), Text = n.Title, Selected = data.DistrictId == n.Id ? true : false }).ToList();
            ViewBag.DistrictId = dataDistrictDrop;

            List<PartnerModel> dataPartner = await _partnerService.GetByStatusAsync(true);
            List<SelectListItem> dataPartnerDrop = dataPartner.Select(n =>
            new SelectListItem { Value = n.Id.ToString(), Text = n.Title, Selected = data.PartnerId == n.Id ? true : false }).ToList();
            ViewBag.PartnerID = dataPartnerDrop;

            return View(data);
        }

        [HttpPost]
        public async Task<IActionResult> Update(BranchModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _branchService.UpdateAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminBranch"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _branchService.DeleteAsync(id);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminBranch"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }
    }
}
