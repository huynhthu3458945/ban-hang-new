﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminConfigSystemController : Controller
    {
        #region Fields
        private readonly IConfigSystemService _configService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public AdminConfigSystemController(IConfigSystemService configService, ILogging logging)
        {
            _configService = configService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        //[NonAction]
        public async Task<IActionResult> Index()
        {
            var data = await _configService.FindById(1);
            return View(data);
        }

        [HttpPost]
        public async Task<ActionResult> Update(ConfigSystemModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = model.Id != 0 ? await _configService.UpdateAsync(model) : await _configService.AddAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminConfigSystem"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }
    }
}
