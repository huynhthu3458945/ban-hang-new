﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    [Authorize]
    public class AdminContactController : Controller
    {
        #region Fields
        private readonly IContactService _contactService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public AdminContactController(IContactService contactService, ILogging logging)
        {
            _contactService = contactService;
            _logging = logging;
        }
        #endregion

        //Load data Json
        [HttpGet]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            var data = (await _contactService.GetDataAsync()).AsEnumerable();
            //Setting Paging
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //Setting Search
            if (!string.IsNullOrEmpty(searchString) && (searchStatus != string.Empty && !string.IsNullOrEmpty(searchStatus)))
            {
                data = data.Where(m => m.Status == searchStatus.ToBool()
                              && (m.FullName.ToLower().Contains(searchString.ToLower()) || m.Email.ToLower().Contains(searchString.ToLower()) || m.Mobi.ToLower().Contains(searchString.ToLower())));
            }
            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(m => m.FullName.ToLower().Contains(searchString.ToLower()) || m.Email.ToLower().Contains(searchString.ToLower()) || m.Mobi.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchStatus))
            {
                data = data.Where(m => m.Status == searchStatus.ToBool());
            }

            //Get Total Row Count
            int totalRecords = data.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            //Setting Sorting
            if (sord.ToUpper() == "DESC")
            {
                data = data.OrderByDescending(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "FullName").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                data = data.OrderBy(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "FullName").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }

            //Sending Json Object to View.
            var jsonData = new { total = totalPages, page, records = totalRecords, rows = data };
            return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> ShowContact(int id)
        {
            var data = await _contactService.FindById(id);
            //UPDATE Status
            await _contactService.UpdateStatusAsync(id);
            return View(data);
        }
    }
}
