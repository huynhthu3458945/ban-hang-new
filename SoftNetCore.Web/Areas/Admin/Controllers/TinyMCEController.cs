﻿using Microsoft.AspNetCore.Mvc;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("tiny-mce")]
    public class TinyMCEController : Controller
    {
        [Route("browse")]
        public IActionResult Browse()
        {
            return View();
        }

        [Route("browseavatar")]
        public IActionResult BrowseAvatar()
        {
            return View();
        }

        [Route("browsethumb")]
        public IActionResult BrowseThumb()
        {
            return View();
        }

        [Route("browselistimage")]
        public IActionResult BrowseListImage()
        {
            return View();
        }

        [Route("browsemetaimage")]
        public IActionResult BrowseMetaImage()
        {
            return View();
        }
    }
}
