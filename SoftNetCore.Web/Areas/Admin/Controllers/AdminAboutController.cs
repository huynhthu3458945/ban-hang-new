﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminAboutController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public AdminAboutController(IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        //[NonAction]
        public async Task<IActionResult> Index()
        {
            var data = await _aboutService.GetFisrtAsync(1);
            return View(data);
        }

        [HttpGet]
        //[NonAction]
        public async Task<IActionResult> PolicyPayment()
        {
            var data = await _aboutService.GetFisrtAsync(2);
            return View(data);
        }

        [HttpGet]
        //[NonAction]
        public async Task<IActionResult> PolicySale()
        {
            var data = await _aboutService.GetFisrtAsync(3);
            return View(data);
        }

        [HttpGet]
        //[NonAction]
        public async Task<IActionResult> PolicyShipping()
        {
            var data = await _aboutService.GetFisrtAsync(4);
            return View(data);
        }

        [HttpGet]
        //[NonAction]
        public async Task<IActionResult> PolicyTerms()
        {
            var data = await _aboutService.GetFisrtAsync(5);
            return View(data);
        }

        [HttpGet]
        //[NonAction]
        public async Task<IActionResult> SizeChart()
        {
            var data = await _aboutService.GetFisrtAsync(6);
            return View(data);
        }

        [HttpPost]
        public async Task<ActionResult> Update(AboutModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = model.Id != 0 ? await _aboutService.UpdateAsync(model) : await _aboutService.AddAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminAbout"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }
    }
}
