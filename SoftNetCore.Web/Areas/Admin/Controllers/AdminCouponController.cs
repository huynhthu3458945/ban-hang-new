﻿using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Web.Areas.Admin.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminCouponController : AdminBaseController
    {
        #region Fields
        private readonly ICouponService _couponService;
        private readonly ICouponRepository _couponRepository;
        private readonly ICouponTypeService _couponTypeService;
        private readonly ILogging _logging;
        #endregion

        #region Ctor
        public AdminCouponController(
            ICouponService couponService,
            ICouponRepository couponRepository,
            ICouponTypeService couponTypeService,
            ILogging logging)
        {
            _couponService = couponService;
            _couponRepository = couponRepository;
            _couponTypeService = couponTypeService;
            _logging = logging;
        }
        #endregion

        #region Coupon
        //Load data Json
        [HttpGet]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            try
            {
                //Setting Paging
                int pageIndex = Convert.ToInt32(page) - 1;
                int pageSize = rows;

                //Get Total Row Count
                int totalRecords = string.IsNullOrEmpty(searchStatus) ? _couponService.GetTotalByCouponType(0, 1) : _couponService.GetTotalByCouponType(searchStatus.ToInt(), 1);
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);

                var result = string.IsNullOrEmpty(searchStatus) ? (await _couponRepository.GetAllAsync(pageSize, pageIndex, searchString, sidx, sord)) : (await _couponRepository.GetAllAsync(searchStatus.ToInt(), pageSize, pageIndex, searchString, sidx, sord));
                //Sending Json Object to View.
                var jsonData = new { total = totalPages, page, records = totalRecords, rows = result };
                return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());
            }
            catch (Exception ex) { }
            return Json("", new Newtonsoft.Json.JsonSerializerSettings());
        }

        //[HttpGet]
        //public JsonResult LoadDataDrop(string mainCat)
        //{
        //    var data = CouponTypeBusiness.GetByCouponMainCat(mainCat.ToInt());
        //    return Json(data.Select(x => new SelectListItem { Text = x.Title, Value = x.CouponTypetID.ToString() }), JsonRequestBehavior.AllowGet);
        //}

        public async Task<ActionResult> Index()
        {
            ViewBag.DropCategory = await _couponTypeService.GetAllAsync();
            return View();
        }

        public async Task<ActionResult> Insert()
        {
            var data = await _couponTypeService.GetAllAsync();
            List<SelectListItem> dataCouponCatDrop = data.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();
            ViewBag.CouponTypeId = dataCouponCatDrop;

            CouponModel model = new CouponModel();
            model.Status = 1;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Insert(CouponModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _couponService.AddAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCoupon"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public async Task<ActionResult> Update(int id)
        {
            var data = await _couponService.FindById(id);
            var dataDrop = await _couponTypeService.GetAllAsync();
            List<SelectListItem> dataSelect = dataDrop.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title,
                Selected = data.CouponTypeId == x.Id ? true : false
            }).ToList();
            ViewBag.CouponTypeId = dataSelect;

            return View(data);
        }

        [HttpPost]
        public async Task<ActionResult> Update(CouponModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _couponService.UpdateAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCoupon"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }


        //[HttpPost]
        //public async Task<ActionResult> UpdateStatus(int id)
        //{
        //    string mesage = string.Empty;
        //    string typeMessage = string.Empty;
        //    try
        //    {
        //        var result = await _couponService.UpdateStatusAsync(id);
        //        if (result.Success)
        //            typeMessage = EnumsTypeMessage.Success;
        //        else
        //            typeMessage = EnumsTypeMessage.Error;
        //        mesage = result.Message;
        //    }
        //    catch (Exception ex)
        //    {
        //        mesage = ex.Message;
        //        typeMessage = EnumsTypeMessage.Error;
        //    }

        //    return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCoupon"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        //}

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _couponService.DeleteAsync(id);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCoupon"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }
        #endregion
    }
}
