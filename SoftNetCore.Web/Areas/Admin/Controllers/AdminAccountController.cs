﻿using Hangfire.Dashboard;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Model.Request;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    public class AdminAccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly ILogging _logging;

        public AdminAccountController(
            IAccountService accountService, 
            ILogging logging)
        {
            _accountService = accountService;
            _logging = logging;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/admin", Name = "login")]
        public ActionResult AdminLogin()
        {
            AuthenticateRequest model = new AuthenticateRequest();
            return View(model);
        }

        [HttpPost]
        [Route("/admin", Name = "login")]
        public async Task<IActionResult> AdminLogin(AuthenticateRequest model)
        {
            if (ModelState.IsValid)
            {
                var result = await _accountService.Authenticate(model);
                if (result == null)
                {
                    return View(model);
                }
                // create claims
                List<Claim> claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, result.Id),
                    new Claim(ClaimTypes.NameIdentifier, result.UserName.ToSafetyString()),
                    new Claim(ClaimTypes.MobilePhone, result.Phone.ToSafetyString()),
                    new Claim("Avatar", result.Avatar.ToSafetyString()),
                    new Claim("JwtToken", result.JwtToken.ToSafetyString()),
                    new Claim(ClaimTypes.Email, result.Email.ToSafetyString()),
                    new Claim("Type", result.TypeId.ToSafetyString()),
                };
                // create identity
                ClaimsIdentity identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                // create principal
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                // sign-in
                await HttpContext.SignInAsync(
                        scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                        principal: principal,
                        properties: new AuthenticationProperties
                        {
                            IsPersistent = model.RememberMe, // for 'remember me' feature
                                                             //ExpiresUtc = DateTime.UtcNow.AddMinutes(1)
                        });


                //HttpContext.User.Identity.IsAuthenticated == true;

                if (string.IsNullOrEmpty(model.ReturnUrl)) return Redirect("admin/home/index");

                return Redirect(model.ReturnUrl);
            }
            return View(model);
        }

        [HttpGet]
        [Route("/admin/logout", Name = "admin-logout")]
        public async Task<IActionResult> AdminLogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("/admin");
        }

        [HttpGet]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            var data = (await _accountService.GetDataAsync(1)).AsEnumerable();
            //Setting Paging
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //Setting Search
            if (!string.IsNullOrEmpty(searchString) && (searchStatus != string.Empty && !string.IsNullOrEmpty(searchStatus)))
            {
                data = data.Where(m => m.Status == searchStatus.ToBool() && m.UserName.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(m => m.UserName.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchStatus))
            {
                data = data.Where(m => m.Status == searchStatus.ToBool());
            }

            //Get Total Row Count
            int totalRecords = data.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            //Setting Sorting
            if (sord.ToUpper() == "DESC")
            {
                data = data.OrderByDescending(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "UserName").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                data = data.OrderBy(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "UserName").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            //Sending Json Object to View.
            var jsonData = new { total = totalPages, page, records = totalRecords, rows = data };
            return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());

        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Insert()
        {
            AccountModel model = new AccountModel();
            model.Status = true;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Insert(AccountModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                model.AccountTypeId = 1;
                var result = await _accountService.AddAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminAccount"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var data = await _accountService.FindById(id);
            return View(data);
        }

        [HttpPost]
        public async Task<IActionResult> Update(AccountModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _accountService.UpdateAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminAccount"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _accountService.DeleteAsync(id);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminAccount"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            var useId = "";
            if (string.IsNullOrEmpty(User.Identity.Name) == false)
            {
                useId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.Name))?.Value;
            }

            ChangePasswordRequest model = new ChangePasswordRequest();
            model.Id = useId.ToInt();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordRequest model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _accountService.ChangePassword(model);
                mesage = result.Success ? "Cập nhật thành công" : "Cập nhật thất bại";
                typeMessage = result.Success ? EnumsTypeMessage.Success : EnumsTypeMessage.Error;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = "/admin/home/index", IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }
    }
}