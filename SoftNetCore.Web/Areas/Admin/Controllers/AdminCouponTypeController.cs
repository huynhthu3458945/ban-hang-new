﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Enums;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.BaseServices;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminCouponTypeController : Controller
    {
        private readonly ICouponTypeService _couponTypeService;
        private readonly ILogging _logging;
        public static int INDEX = 0;

        public AdminCouponTypeController(
            ICouponTypeService couponTypeService,
            ILogging logging)
        {
            _couponTypeService = couponTypeService;
            _logging = logging;
        }

        [HttpGet]
        //[NonAction]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            var data = (await _couponTypeService.GetDataAsync()).AsEnumerable();
            //Setting Paging
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //Setting Search
            //if (!string.IsNullOrEmpty(searchString) && (searchStatus != string.Empty && !string.IsNullOrEmpty(searchStatus)))
            //{
            //    data = data.Where(m => m.Status == searchStatus.ToBool() && m.Title.ToLower().Contains(searchString.ToLower()));
            //}
            //if (!string.IsNullOrEmpty(searchString))
            //{
            //    data = data.Where(m => m.Title.ToLower().Contains(searchString.ToLower()));
            //}
            //if (!string.IsNullOrEmpty(searchStatus))
            //{
            //    data = data.Where(m => m.Status == searchStatus.ToBool());
            //}

            //Get Total Row Count
            int totalRecords = data.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            //Setting Sorting
            if (sord.ToUpper() == "DESC")
            {
                data = data.OrderByDescending(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                data = data.OrderBy(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            //Sending Json Object to View.
            var jsonData = new { total = totalPages, page, records = totalRecords, rows = data };
            return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());

        }

        [HttpGet]
        //[NonAction]
        public IActionResult Index()
        {
            var enumDomains = Enum.GetValues(typeof(EnumDomains)).Cast<EnumDomains>().ToList();
            ViewBag.EnumDomains = enumDomains;

            return View();
        }

        [HttpGet]
        //[NonAction]
        public async Task<IActionResult> Insert()
        {
            //List<CouponTypeModel> data = await _couponTypeService.GetAllAsync();
            //List<SelectListItem> dataDrop = data.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();
            //ViewBag.CouponTypeId = dataDrop;

            CouponTypeModel model = new CouponTypeModel();
            return View(model);
        }

        [HttpPost]
        //[NonAction]
        public async Task<IActionResult> Insert(CouponTypeModel model)
        {
            string typeMessage;
            string mesage;
            try
            {
                var result = await _couponTypeService.AddAsync(model);
                typeMessage = result.Success ? EnumsTypeMessage.Success : EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCouponType"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var data = await _couponTypeService.FindById(id);
            //List<CouponTypeModel> dataDrop = await _couponTypeService.GetAllAsync();

            //List<SelectListItem> dataSelect = dataDrop.Select(n =>
            //new SelectListItem { Value = n.Id.ToString(), Text = n.Title, Selected = data.ParentId == n.Id ? true : false }).ToList();
            //ViewBag.CouponTypeId = dataSelect;

            return View(data);
        }

        [HttpPost]
        public async Task<IActionResult> Update(CouponTypeModel model)
        {
            string typeMessage;
            string mesage;
            try
            {
                var result = await _couponTypeService.UpdateAsync(model);
                typeMessage = result.Success ? EnumsTypeMessage.Success : EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCouponType"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            string typeMessage;
            string mesage;
            try
            {
                var result = await _couponTypeService.DeleteAsync(id);
                typeMessage = result.Success ? EnumsTypeMessage.Success : EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminCouponType"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }
    }
}
