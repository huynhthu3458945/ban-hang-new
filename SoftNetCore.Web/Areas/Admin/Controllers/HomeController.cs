﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class HomeController : Controller
    {
        public HomeController() { }

        public IActionResult Index()
        {
            var x = HttpContext.User;

            return View();
        }
    }
}
