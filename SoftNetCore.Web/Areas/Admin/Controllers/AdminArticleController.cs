﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminArticleController : Controller
    {
        #region Fields
        private readonly IArticleService _articleService;
        private readonly IArticleCategoryService _articleCategoryService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public AdminArticleController(IArticleService articleService, IArticleCategoryService articleCategoryService, ILogging logging)
        {
            _articleService = articleService;
            _articleCategoryService = articleCategoryService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        //[NonAction]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {

            var data = (await _articleService.GetDataAsync()).AsEnumerable();
            //Setting Paging
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //Setting Search
            if (!string.IsNullOrEmpty(searchString) && (searchStatus != string.Empty && !string.IsNullOrEmpty(searchStatus)))
            {
                data = data.Where(m => m.ArticleCategoryId == searchStatus.ToInt() && m.Title.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(m => m.Title.ToLower().Contains(searchString.ToLower()));
            }
            if (!string.IsNullOrEmpty(searchStatus))
            {
                data = data.Where(m => m.ArticleCategoryId == searchStatus.ToInt());
            }

            //Get Total Row Count
            int totalRecords = data.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            //Setting Sorting
            if (sord.ToUpper() == "DESC")
            {
                data = data.OrderByDescending(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                data = data.OrderBy(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                data = data.Skip(pageIndex * pageSize).Take(pageSize);
            }
            //Sending Json Object to View.
            var jsonData = new { total = totalPages, page, records = totalRecords, rows = data };
            return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ViewBag.ArticleCategory = await _articleCategoryService.GetByStatusAsync(true);
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Insert()
        {
            var data = await _articleCategoryService.GetByStatusAsync(true);
            List<SelectListItem> dataDrop = data.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();
            ViewBag.ArticleCategoryID = dataDrop;

            ArticleModel model = new ArticleModel();
            model.Position = 1;
            model.Status = true;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Insert(ArticleModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _articleService.AddAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminArticle"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var data = await _articleService.FindById(id);
            var dataDrop = await _articleCategoryService.GetByStatusAsync(true);
            List<SelectListItem> dataSelect = dataDrop.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title,
                Selected = data.ArticleCategoryId == x.Id ? true : false
            }).ToList();
            ViewBag.ArticleCategoryID = dataSelect;

            return View(data);
        }

        [HttpPost]
        public async Task<IActionResult> Update(ArticleModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _articleService.UpdateAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminArticle"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<ActionResult> UpdateStatus(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _articleService.UpdateStatusAsync(id);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminArticle"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _articleService.DeleteAsync(id);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminArticle"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }
    }
}
