﻿using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Web.Areas.Admin.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin/[controller]/[action]")]
    [Route("admin/[controller]/[action]/{id}")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Authorize]
    public class AdminProductController : AdminBaseController
    {
        #region Fields
        private readonly IProductService _productService;
        private readonly IProductRepository _productRepository;
        private readonly ICouponService _couponService;
        private readonly IProductCategoryService _productCategoryService;
        private readonly IBannerService _bannerService;
        private readonly ISaleService _saleService;
        private readonly IAttributeService _attributeService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IMerchantSettingService _merchantSettingService;
        private readonly IConfigSystemService _configSystemService;
        private readonly ILogging _logging;
        #endregion

        #region Ctor
        public AdminProductController(
            IProductService productService,
            IProductRepository productRepository,
            ICouponService couponService,
            IProductCategoryService productCategoryService,
            IBannerService bannerService,
            ISaleService saleService,
            IAttributeService attributeService,
            IProductAttributeService productAttributeService,
            IMerchantSettingService merchantSettingService,
            IConfigSystemService configSystemService,
            ILogging logging)
        {
            _productService = productService;
            _productRepository = productRepository;
            _couponService = couponService;
            _productCategoryService = productCategoryService;
            _bannerService = bannerService;
            _saleService = saleService;
            _attributeService = attributeService;
            _productAttributeService = productAttributeService;
            _merchantSettingService = merchantSettingService;
            _configSystemService = configSystemService;
            _logging = logging;
        }
        #endregion

        #region Product
        //Load data Json
        [HttpGet]
        public async Task<JsonResult> LoadData(string sidx, string sord, int page, int rows, string searchString, string searchStatus)
        {
            try
            {
                //Setting Paging
                int pageIndex = Convert.ToInt32(page) - 1;
                int pageSize = rows;

                //Get Total Row Count
                int totalRecords = string.IsNullOrEmpty(searchStatus) ? _productService.GetTotalByProductCategory(0, true) : _productService.GetTotalByProductCategory(searchStatus.ToInt(), true);
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
                //Setting Sorting
                //if (sord.ToUpper() == "DESC")
                //{
                //    data = data.OrderByDescending(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                //    data = data.Skip(pageIndex * pageSize).Take(pageSize);
                //}
                //else
                //{
                //    data = data.OrderBy(s => s.GetType().GetProperty(!string.IsNullOrEmpty(sidx) ? sidx : "Title").GetValue(s));
                //    data = data.Skip(pageIndex * pageSize).Take(pageSize);
                //}

                var result = string.IsNullOrEmpty(searchStatus) ? (await _productRepository.GetAllAsync(pageSize, pageIndex, searchString, sidx, sord)) : (await _productRepository.GetAllAsync(searchStatus.ToInt(), pageSize, pageIndex, searchString, sidx, sord));
                //Sending Json Object to View.
                var jsonData = new { total = totalPages, page, records = totalRecords, rows = result };
                return Json(jsonData, new Newtonsoft.Json.JsonSerializerSettings());
            }
            catch (Exception ex) { }
            return Json("", new Newtonsoft.Json.JsonSerializerSettings());
        }

        //[HttpGet]
        //public JsonResult LoadDataDrop(string mainCat)
        //{
        //    var data = ProductCategoryBusiness.GetByProductMainCat(mainCat.ToInt());
        //    return Json(data.Select(x => new SelectListItem { Text = x.Title, Value = x.ProductCategorytID.ToString() }), JsonRequestBehavior.AllowGet);
        //}

        public async Task<ActionResult> Index()
        {
            ViewBag.DropCategory = await _productCategoryService.GetMenuAsync();

            ViewBag.ProductCategories = await _productCategoryService.GetAllAsync();

            return View();
        }

        public async Task<ActionResult> Insert()
        {
            // Khi thêm mới Clear thuộc tính cũ ghi tạm
            List<ProductAttributeItem> listAttribute = new List<ProductAttributeItem>();
            HttpContext.Session.SetString("productColor", JsonConvert.SerializeObject(listAttribute));
            HttpContext.Session.SetString("productSize", JsonConvert.SerializeObject(listAttribute));

            //
            var data = await _productCategoryService.GetMenuAsync();
            List<SelectListItem> dataProductCatDrop = data.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();

            var dataSale = await _saleService.GetByStatusAsync(true);
            List<SelectListItem> dataSaleDrop = dataSale.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();

            ViewBag.ProductCategoryId = dataProductCatDrop;
            ViewBag.SaleId = dataSaleDrop;

            ProductModel model = new ProductModel();
            model.Position = 1;
            model.Status = true;
            model.IsProductHot = true;
            model.IsProductNew = true;
            model.StockStatus = true;
            model.SaleDeadLine = null;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Insert(ProductModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _productService.AddAsync(model);
                if (result.Success)
                {
                    typeMessage = EnumsTypeMessage.Success;
                    //
                    var product = (Product)result.Data;
                    // Lấy kết quả biến tạm
                    // Color
                    List<ProductAttributeItem> listColorItem = new List<ProductAttributeItem>();
                    var productColor = HttpContext.Session.GetString("productColor");
                    if (!string.IsNullOrEmpty(productColor))
                    {
                        listColorItem = JsonConvert.DeserializeObject<List<ProductAttributeItem>>(productColor);
                    }
                    // Size
                    List<ProductAttributeItem> listSizeItem = new List<ProductAttributeItem>();
                    var productSize = HttpContext.Session.GetString("productSize");
                    if (!string.IsNullOrEmpty(productSize))
                    {
                        listSizeItem = JsonConvert.DeserializeObject<List<ProductAttributeItem>>(productSize);
                    }
                    foreach (var itemColor in listColorItem)
                    {
                        ProductAttributeModel colorNew = new ProductAttributeModel();
                        colorNew.ProductId = product.Id;
                        colorNew.AttributeId = itemColor.ColorId;
                        colorNew.Avatar1 = itemColor.Avatar1;
                        colorNew.Avatar2 = itemColor.Avatar2;
                        colorNew.ImageList = itemColor.ListImg;
                        colorNew.Status = itemColor.Status;
                        colorNew.CreateTime = DateTime.Now;
                        var resultColor = await _productAttributeService.AddAsync(colorNew);
                        if (resultColor.Success)
                        {
                            var attributeColor = (ProductAttribute)resultColor.Data;
                            foreach (var itemSize in listSizeItem)
                            {
                                if (itemSize.ColorId == attributeColor.AttributeId)
                                {
                                    ProductAttributeModel sizeNew = new ProductAttributeModel();
                                    sizeNew.ProductId = attributeColor.ProductId;
                                    sizeNew.AttributeId = itemSize.SizeId;
                                    sizeNew.Price = itemSize.Price.ToDecimal();
                                    sizeNew.PricePromo = itemSize.PricePromo.ToDecimal();
                                    sizeNew.Quantity = itemSize.Quantity;
                                    sizeNew.Status = itemSize.Status;
                                    sizeNew.ParentId = attributeColor.Id;
                                    sizeNew.CreateTime = DateTime.Now;
                                    await _productAttributeService.AddAsync(sizeNew);
                                }
                            }
                        }
                    }
                    HttpContext.Session.SetString("productColor", "");
                    HttpContext.Session.SetString("productSize", "");
                }
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProduct"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public async Task<ActionResult> Update(int id)
        {
            var data = await _productService.FindById(id);
            var dataDrop = await _productCategoryService.GetMenuAsync();
            var dataSale = await _saleService.GetByStatusAsync(true);
            List<SelectListItem> dataSelect = dataDrop.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title,
                Selected = data.ProductCategoryId == x.Id ? true : false
            }).ToList();
            ViewBag.ProductCategoryId = dataSelect;

            List<SelectListItem> dataSaleDrop = dataSale.Select(n =>
            new SelectListItem { Value = n.Id.ToString(), Text = n.Title, Selected = data.SaleId == n.Id ? true : false }).ToList();
            ViewBag.SaleId = dataSaleDrop;

            //Lấy dữ liệu thuộc tính hiện tại đổ vô list tạm
            //Color
            var listColor = await _productAttributeService.GetByProductId(id, true);
            List<ProductAttributeItem> listColorItem = new List<ProductAttributeItem>();
            foreach (var item in listColor)
            {
                int index = 0;
                ListProductAttributeItem.GetColorItemIndex(listColorItem, item.Id, ref index);

                if (listColorItem == null || index == 0)
                {
                    var productAttributeItem = ListProductAttributeItem.AddList(item.Id, item.ProductId, item.Product?.Title, item.AttributeId, item.Attribute?.Title, null, "", item.Avatar1, item.Avatar2, item.ImageList, item.Price.ToDouble(), item.PricePromo.ToDouble(), item.Quantity.ToInt(), item.Status.ToBool(), item.ParentId);
                    if (listColorItem == null) { listColorItem = productAttributeItem; } else { listColorItem.Add(productAttributeItem.FirstOrDefault()); }
                }
            }
            HttpContext.Session.SetString("productColor", JsonConvert.SerializeObject(listColorItem));

            //Size
            var listSize = await _productAttributeService.GetByProductId(id, false);
            List<ProductAttributeItem> listSizeItem = new List<ProductAttributeItem>();
            foreach (var item in listSize)
            {
                int index = 0;
                ListProductAttributeItem.GetSizeItemIndex(listSizeItem, item.AttributeId.ToInt(), item.Parent.AttributeId.ToInt(), ref index);

                if (listSizeItem == null || index == 0)
                {
                    var productAttributeItem = ListProductAttributeItem.AddList(item.Id, item.ProductId, item.Product?.Title, item.Parent.AttributeId, item.Parent?.Attribute.Title, item.AttributeId, item.Attribute?.Title, item.Avatar1, item.Avatar2, item.ImageList, item.Price.ToDouble(), item.PricePromo.ToDouble(), item.Quantity.ToInt(), item.Status.ToBool(), item.ParentId);
                    if (listSizeItem == null) { listSizeItem = productAttributeItem; } else { listSizeItem.Add(productAttributeItem.FirstOrDefault()); }
                }
            }
            HttpContext.Session.SetString("productSize", JsonConvert.SerializeObject(listSizeItem));

            return View(data);
        }

        [HttpPost]
        public async Task<ActionResult> Update(ProductModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _productService.UpdateAsync(model);
                if (result.Success)
                {
                    typeMessage = EnumsTypeMessage.Success;
                    // Lấy kết quả biến tạm
                    // Color
                    List<ProductAttributeItem> listColorItem = new List<ProductAttributeItem>();
                    var productColor = HttpContext.Session.GetString("productColor");
                    if (!string.IsNullOrEmpty(productColor))
                    {
                        listColorItem = JsonConvert.DeserializeObject<List<ProductAttributeItem>>(productColor);
                    }
                    // Size
                    List<ProductAttributeItem> listSizeItem = new List<ProductAttributeItem>();
                    var productSize = HttpContext.Session.GetString("productSize");
                    if (!string.IsNullOrEmpty(productSize))
                    {
                        listSizeItem = JsonConvert.DeserializeObject<List<ProductAttributeItem>>(productSize);
                    }
                    foreach (var itemColor in listColorItem)
                    {
                        var _color = await _productAttributeService.FindById(itemColor.Id.ToInt());
                        if (_color == null)
                        {
                            ProductAttributeModel colorNew = new ProductAttributeModel();
                            colorNew.ProductId = model.Id;
                            colorNew.AttributeId = itemColor.ColorId;
                            colorNew.Avatar1 = itemColor.Avatar1;
                            colorNew.Avatar2 = itemColor.Avatar2;
                            colorNew.ImageList = itemColor.ListImg;
                            colorNew.Status = itemColor.Status;
                            colorNew.CreateTime = DateTime.Now;
                            var resultColor = await _productAttributeService.AddAsync(colorNew);
                            if (resultColor.Success)
                            {
                                var attributeColor = (ProductAttribute)resultColor.Data;
                                foreach (var itemSize in listSizeItem)
                                {
                                    if (itemSize.ColorId == attributeColor.AttributeId)
                                    {
                                        ProductAttributeModel sizeNew = new ProductAttributeModel();
                                        sizeNew.ProductId = attributeColor.ProductId;
                                        sizeNew.AttributeId = itemSize.SizeId;
                                        sizeNew.Price = itemSize.Price.ToDecimal();
                                        sizeNew.PricePromo = itemSize.PricePromo.ToDecimal();
                                        sizeNew.Quantity = itemSize.Quantity;
                                        sizeNew.Status = itemSize.Status;
                                        sizeNew.ParentId = attributeColor.Id;
                                        sizeNew.CreateTime = DateTime.Now;
                                        await _productAttributeService.AddAsync(sizeNew);
                                    }
                                }
                            }
                        }
                        else
                        {
                            _color.ProductId = model.Id;
                            _color.AttributeId = itemColor.ColorId;
                            _color.Avatar1 = itemColor.Avatar1;
                            _color.Avatar2 = itemColor.Avatar2;
                            _color.ImageList = itemColor.ListImg;
                            _color.Status = itemColor.Status;
                            var resultColor = await _productAttributeService.UpdateAsync(_color);
                            if (resultColor.Success)
                            {
                                foreach (var itemSize in listSizeItem)
                                {
                                    if (itemSize.ColorId == _color.AttributeId)
                                    {
                                        var _size = await _productAttributeService.FindByReferenceId(model.Id, itemColor.ColorId.ToInt(), itemSize.SizeId.ToInt());
                                        if (_size == null)
                                        {
                                            ProductAttributeModel sizeNew = new ProductAttributeModel();
                                            sizeNew.ProductId = _color.ProductId;
                                            sizeNew.AttributeId = itemSize.SizeId;
                                            sizeNew.Price = itemSize.Price.ToDecimal();
                                            sizeNew.PricePromo = itemSize.PricePromo.ToDecimal();
                                            sizeNew.Quantity = itemSize.Quantity;
                                            sizeNew.Status = itemSize.Status;
                                            sizeNew.ParentId = _color.Id;
                                            sizeNew.CreateTime = DateTime.Now;
                                            await _productAttributeService.AddAsync(sizeNew);
                                        }
                                        else
                                        {
                                            _size.ProductId = model.Id;
                                            _size.AttributeId = itemSize.SizeId;
                                            _size.Price = itemSize.Price.ToDecimal();
                                            _size.PricePromo = itemSize.PricePromo.ToDecimal();
                                            _size.Quantity = itemSize.Quantity;
                                            _size.Status = itemSize.Status;
                                            _size.ParentId = _color.Id;
                                            await _productAttributeService.UpdateAsync(_size);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProduct"), IsReturn = model.IsReturn }, new Newtonsoft.Json.JsonSerializerSettings());
        }


        [HttpPost]
        public async Task<ActionResult> UpdateStatus(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result = await _productService.UpdateStatusAsync(id);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProduct"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            try
            {
                var result1 = await _productAttributeService.DeleteByProductIdAsync(id);
                if (result1.Success)
                {
                    var result = await _productService.DeleteAsync(id);
                    if (result.Success)
                        typeMessage = EnumsTypeMessage.Success;
                    else
                        typeMessage = EnumsTypeMessage.Error;
                    mesage = result.Message;
                }
                else
                {
                    typeMessage = EnumsTypeMessage.Error;
                    mesage = "Sản phẩm đã có đơn hàng không thể xóa.";
                }
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = true, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProduct"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }
        #endregion

        [HttpGet]
        public ActionResult ShowSelectProduct()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> FetchProduct(int productCategoryId, int pageNumber, int pageSize)
        {
            string typeMessage;
            string mesage;
            bool status = false;
            try
            {
                var setting = await _merchantSettingService.FindByMerchantId(Data.Enums.EnumDomains.ACCESSTRADE);
                var token = setting?.TokenValue;
                if (pageNumber == 0 || pageSize == 0)
                {
                    var config = await _configSystemService.FindById(1);
                    pageNumber = config.CouponPage.ToInt();
                    pageSize = config.CouponLimit.ToInt();
                }
                var result = await _productService.FetchProductFromDimuadi(productCategoryId, token, pageNumber, pageSize);
                typeMessage = result.Success ? EnumsTypeMessage.Success : EnumsTypeMessage.Error;
                mesage = result.Message;
                status = result.Success;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public async Task<IActionResult> UpdateContentsDimuadi(string token)
        {
            string typeMessage;
            string mesage = string.Empty;
            bool status = false;
            try
            {
                await _productService.UpdateDimuadiProducts(token);
                typeMessage = EnumsTypeMessage.Success;
                status = true;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        /// <summary>
        /// FetchCoupon
        /// </summary>
        /// <param name="token"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> FetchCoupon(string token, int pageNumber, int pageSize)
        {
            string typeMessage;
            string mesage;
            bool status = false;
            try
            {
                if (string.IsNullOrEmpty(token))
                {
                    var setting = await _merchantSettingService.FindByMerchantId(Data.Enums.EnumDomains.ACCESSTRADE);
                    token = setting?.TokenValue;
                }
                if (pageNumber == 0 || pageSize == 0)
                {
                    var config = await _configSystemService.FindById(1);
                    pageNumber = config.CouponPage.ToInt();
                    pageSize = config.CouponLimit.ToInt();
                }
                var result = await _couponService.FetchAccesstradeCouponAsync(token, pageNumber, pageSize);
                typeMessage = result.Success ? EnumsTypeMessage.Success : EnumsTypeMessage.Error;
                mesage = result.Message;
                status = result.Success;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }



        /// <summary>
        /// UpdateCouponTypes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> UpdateCouponTypes()
        {
            string typeMessage = string.Empty;
            string mesage = string.Empty;
            bool status = false;
            try
            {
                await _couponService.UpdateCouponTypesAsync();
                status = true;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }


        /// <summary>
        /// UPDATE COUPONS
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> UpdateCoupons()
        {
            string typeMessage = string.Empty;
            string mesage = string.Empty;
            bool status = false;
            try
            {
                await _couponService.UpdateCouponTypeForCouponsAsync();
                status = true;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }


        /// <summary>
        /// GetAccessTradeProducts
        /// </summary>
        /// <param name="token"></param>
        /// <param name="domain"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAccessTradeProducts(string domain, int page, int pageSize)
        {
            string typeMessage = string.Empty;
            string mesage = string.Empty;
            bool status = false;
            try
            {
                var setting = await _merchantSettingService.FindByMerchantId(Data.Enums.EnumDomains.ACCESSTRADE);
                string token = setting?.TokenValue;
                if (page == 0 || pageSize == 0)
                {
                    var config = await _configSystemService.FindById(1);
                    page = config.CouponPage.ToInt();
                    pageSize = config.CouponLimit.ToInt();
                }
                //
                status = await _productService.GetAccessTradeProductsAsync(domain, token, page, pageSize);
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }


        [HttpGet]
        public async Task<IActionResult> FetchBannerFromDimuadi(string token)
        {
            string typeMessage = string.Empty;
            string mesage = string.Empty;
            bool status = false;
            try
            {
                if (string.IsNullOrEmpty(token))
                {
                    var setting = await _merchantSettingService.FindByMerchantId(Data.Enums.EnumDomains.DIMUADI);
                    token = setting?.TokenValue;
                }
                status = await _bannerService.FetchBannerFromDimuadi(token);
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }

            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage, RedirectUrl = Url.Action("Index", "AdminProductCategory"), IsReturn = false }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        /// <summary>
        /// Đồng bộ Danh mục và sản phẩm theo danh mục
        /// </summary>
        /// <param name="productCategoryId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ProcessFetchProductDataByCategory(int productCategoryId)
        {
            try
            {
                await _productService.FetchProductByCategoryIdAsync(productCategoryId);
            }
            catch (Exception ex)
            {
                return SafeOk(ex);
            }

            return SafeOk();
        }


        [HttpGet]
        public async Task<IActionResult> StartProcess(int productCategoryId)
        {
            try
            {
                /// Tiến trình timeout
                //BackgroundJob.Schedule(() => _logging.Log($"Testing {number} {DateTime.Now}"), TimeSpan.FromSeconds(number));

                /// Tiến trình timeInterval
                //RecurringJob.AddOrUpdate("ReScheduleProcessingAsync", () => _logging.Log($"Testing {number} {DateTime.Now}"), Cron.Minutely);

                RecurringJob.AddOrUpdate($"ReScheduleProcessingAsync {productCategoryId}", () => _productService.FetchProductByCategoryIdAsync(productCategoryId), Cron.Daily);
            }
            catch (Exception ex)
            {
                return SafeOk(ex);
            }

            return SafeOk();
        }


        [HttpGet]
        public async Task<IActionResult> FetchAccesstradeData(string url, int page, int pageSize, string tokenValue)
        {
            try
            {
                await _productService.FetchAccesstradeData(url, page, 200, tokenValue);
            }
            catch (Exception ex)
            {
                return SafeOk(ex);
            }

            return SafeOk();
        }
    }
}
