﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace SoftNetCore.Web.Controllers
{
    public class CollectionController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly ICollectionService _collectionService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public CollectionController(ICollectionService collectionService, IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _collectionService = collectionService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        [Route("danh-sach-bo-suu-tap")]
        public async Task<IActionResult> Index()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _collectionService.GetByStatusAsync(true);
            return View(result);
        }

        [HttpGet]
        [Route("bo-suu-tap")]
        [Route("bo-suu-tap-{title}-{id}")]
        public async Task<IActionResult> Detail(int id)
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _collectionService.FindById(id);
            return View(result);
        }
    }
}
