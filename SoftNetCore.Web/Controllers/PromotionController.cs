﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.Interface;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Net.Http;
using SoftNetCore.Model.Catalog;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using X.PagedList;
using System.Linq;

namespace SoftNetCore.Web.Controllers
{
    public class PromotionController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IPromotionService _promotionService;
        private readonly IPromotionCategoryService _promotionCategoryService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public PromotionController(IPromotionCategoryService promotionCategoryService, IPromotionService promotionService, IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _promotionCategoryService = promotionCategoryService;
            _promotionService = promotionService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        [Route("khuyen-mai")]
        [Route("khuyen-mai-{title}-{id}")]
        public async Task<IActionResult> Index(int? id, int? page)
        {
            string titleCat = "Khuyến mãi";
            int pageNumper = 0;
            int.TryParse(page.ToString(), out pageNumper);
            if (pageNumper == 0)
                pageNumper = 1;

            if (id != null)
            {
                var item = await _promotionCategoryService.FindById(id.ToInt());
                titleCat = $"<span></span><a href=\"/khuyen-mai\">Khuyến Mãi</a>";
                titleCat += $"<span></span>{item.Title}";
            }
            else
            {
                titleCat = "<span></span>Khuyến mãi";
            }
            ViewBag.TitleCat = titleCat;
            ViewBag.CatId = id ?? 0;
            //ViewBag.Catgory = await _articleCategoryService.GetByStatusAsync(true);
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
          
            IPagedList<PromotionModel> data = id == null ? (await _promotionService.GetByStatusAsync(true)).ToPagedList(pageNumper, 15) : (await _promotionService.GetByCategoryAsync(id.ToInt())).ToPagedList(pageNumper, 15);
            return View(data);
        }

        [HttpGet]
        [Route("chi-tiet-khuyen-mai")]
        [Route("chi-tiet-khuyen-mai-{title}-{id}")]
        public async Task<IActionResult> Detail(int id)
        {
            var result = await _promotionService.FindById(id);
            var item = await _promotionCategoryService.FindById(result.PromotionCategoryId.ToInt());
            ViewBag.TitleCat = item?.Title;
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);

            return View(result);
        }
    }
}
