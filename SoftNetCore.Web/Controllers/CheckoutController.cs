﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.Interface;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Net.Http;
using SoftNetCore.Model.Catalog;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using X.PagedList;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System;
using SoftNetCore.Data.Entities;
using Microsoft.AspNetCore.Hosting;

namespace SoftNetCore.Web.Controllers
{
    public class CheckoutController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IOrderService _orderService;
        private readonly ICustomerService _customerService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private readonly IPayTypeService _payTypeService;
        private readonly ISendEmailService _sendEmailService;
        private readonly AppSettings _appSettings;
        private readonly ILogging _logging;
        private readonly IWebHostEnvironment _hostingEnvironment;
        #endregion
        #region Ctor
        public CheckoutController(ICustomerService customerService, IProvinceService provinceService,
                                    IDistrictService districtService, IPayTypeService payTypeService,
                                    IOrderService orderService, ISendEmailService sendEmailService,
                                    IAboutService aboutService,
                                    IOptions<AppSettings> appSettings, ILogging logging,
                                    IWebHostEnvironment hostingEnvironment)
        {
            _aboutService = aboutService;
            _orderService = orderService;
            _customerService = customerService;
            _provinceService = provinceService;
            _districtService = districtService;
            _payTypeService = payTypeService;
            _sendEmailService = sendEmailService;
            _appSettings = appSettings.Value;
            _logging = logging;
            _hostingEnvironment = hostingEnvironment;
        }
        #endregion

        //public async Task<JsonResult> LoadDataDrop(string id)
        //{
        //    double total = 0;
        //    decimal feeShip = 0;
        //    var province = await _provinceService.FindById(id.ToInt());
        //    var districtDrop = await _districtService.GetByProvinceAsync(id.ToInt());
        //    List<SelectListItem> District = districtDrop.Select(n =>
        //    new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();
        //    // Phí giao hàng
        //    if (province != null)
        //        feeShip = province.FeeShip.ToDecimal();
        //    //
        //    List<CartItem> listCart = new List<CartItem>();
        //    var cart = HttpContext.Session.GetString("cart");
        //    if (!string.IsNullOrEmpty(cart))
        //    {
        //        listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
        //        total = ListCartItem.GetTotal(listCart);
        //    }
        //    //
        //    return Json(new { FeeShip = feeShip, TotalAndShip = total.ToDecimal() + feeShip, DistrictDrop = District }, new Newtonsoft.Json.JsonSerializerSettings());
        //}

        [Route("thanh-toan")]
        public async Task<ActionResult> Index()
        {
            double total = 0;
            //decimal feeShip = 0;
            int customerId = -1;
            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                customerId = (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.Name))?.Value).ToInt();
            }
            var customer = await _customerService.FindById(customerId);

            //// Tỉnh thành
            //var provinceDrop = await _provinceService.GetAllAsync();
            //List<SelectListItem> Province = provinceDrop.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title, Selected = customer?.ProvinceId == n.Id ? true : false }).ToList();
            //ViewBag.ProvinceID = Province;
            //// Nếu khách hàng có tỉnh thành
            //if (customer != null && customer.ProvinceId != null)
            //{
            //    // Quận huyện
            //    var distictrop = await _districtService.GetByProvinceAsync(customer.ProvinceId.ToInt());
            //    List<SelectListItem> District = distictrop.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title, Selected = customer?.DistrictId == n.Id ? true : false }).ToList();
            //    ViewBag.DistrictID = District;
            //    // Phí giao hàng
            //    var province = await _provinceService.FindById(customer.ProvinceId.ToInt());
            //    if (province != null)
            //        feeShip = province.FeeShip.ToDecimal();
            //}
            //else
            //{
            //    // Quận huyện
            //    var distictrop = await _districtService.GetByProvinceAsync(Province.Count() > 0 ? Convert.ToInt32(Province.FirstOrDefault().Value) : 1);
            //    List<SelectListItem> District = distictrop.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();
            //    ViewBag.DistrictID = District;
            //    // Phí giao hàng
            //    var province = await _provinceService.FindById(Province.Count() > 0 ? Convert.ToInt32(Province.FirstOrDefault().Value) : 1);
            //    if (province != null)
            //        feeShip = province.FeeShip.ToDecimal();
            //}
            // Thông tin đơn hàng
            List<CartItem> listCart = new List<CartItem>();
            var cart = HttpContext.Session.GetString("cart");
            if (!string.IsNullOrEmpty(cart))
            {
                listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
                total = ListCartItem.GetTotal(listCart);
                //
            }
            ViewBag.CartList = listCart;
            ViewBag.Total = total;
            //ViewBag.FeeShip = feeShip;
            //ViewBag.TotalAndShip = total.ToDecimal() + feeShip;

            // Hình thức thanh toán
            List<PayTypeModel> dataPay = await _payTypeService.GetAllAsync();
            List<SelectListItem> payType = dataPay.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();
            ViewBag.PayTypeID = payType;

            OrderModel model = new OrderModel();
            if (customer != null)
            {
                model.Email = customer.Email;
                model.Mobi = customer.Phone;
                model.FullName = customer.FullName;
                //model.DistrictId = customer.DistrictId;
                //model.ProvinceId = customer.ProvinceId;
                model.Address = customer.Address;
            }
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SaveCheckout(OrderModel model)
        {
            try
            {
                bool isLogin = false;
                int customerId = -1;
                CustomerModel customer = new CustomerModel();
                if (!string.IsNullOrEmpty(User.Identity.Name))
                {
                    customerId = (User.Claims.FirstOrDefault(x => x.Type.Equals("CustomerId"))?.Value).ToInt();

                }
                // xu ly lay hang khach chon
                List<CartItem> listCart = new List<CartItem>();
                var cart = HttpContext.Session.GetString("cart");
                if (string.IsNullOrEmpty(cart)) return Json(new { Status = false, IsLogin = false, UrlReturn = "/" }, new Newtonsoft.Json.JsonSerializerSettings());
                listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
                // xem có login rùi mới mua hang hay ko
                //Kiêm tra login
                if (customerId == -1)
                {
                    customer = await _customerService.GetByCondition(model.Email) ?? await _customerService.GetByCondition(model.Mobi);
                }
                else
                {
                    customer = await _customerService.FindById(customerId);
                }
                // Nếu chưa có tài khoản thì tạo
                // xu ly
                var result = await _orderService.SaveOrderAsync(model, listCart, customer);

                // Lưu đơn hàng thành công
                if (result.Success == true)
                {
                    // gửi email
                    var order = (Order)result.Data;
                    //var province = await _provinceService.FindById(order.ProvinceId.ToInt());
                    //var district = await _districtService.FindById(order.DistrictId.ToInt());
                    sendOrder(order, listCart);
                    // xóa giỏ hàng
                    HttpContext.Session.SetString("cart", "");
                }

                isLogin = customerId != -1 ? true : false;
                // Tra ket qua
                return Json(new { Status = result.Success, IsLogin = isLogin, UrlReturn = "/" }, new Newtonsoft.Json.JsonSerializerSettings());
            }
            catch
            {
                return Json(new { Status = false, IsLogin = false, UrlReturn = "/" }, new Newtonsoft.Json.JsonSerializerSettings());
            }
        }

        private void sendOrder(Order order, List<CartItem> listCart)
        {
            string content = "";
            foreach (var item in listCart)
            {
                content += "<tr>" +
                                "<td align='left' valign='top' style='padding:3px 9px'>" +
                                    "<span>" + item.ProductName + "</span><br>" +
                                "</td>" +
                                "<td align='right' valign='top' style='padding:3px 9px'><span>" + item.Price.ToString("N0") + "</span></td>" +
                                "<td align='center' valign='top' style='padding:3px 9px'>" + item.Quantity + "</td>" +
                                "<td align='right' valign='top' style='padding:3px 9px'><span>" + (item.Price * item.Quantity).ToString("N0") + " ₫</span></td>" +
                            "</tr>";
            }
            //###-Sent Email-###//
            //Khai báo biến chứa đường dẫn đến Email Template
            string emailTemplateUrl = _hostingEnvironment.WebRootPath + "/portal/template/SendOrders.html";
            //3. Định nghĩa 1 Dictionary chứa các tham số và giá trị
            Dictionary<string, string> param = new Dictionary<string, string>();
            //Cấu hình phẩn đầu của mẫu email (Header)
            //Cấu hình nội dung textbox
            param.Add("#Link#", "https://vnsoshop.com/");
            param.Add("#Login#", "https://vnsoshop.com/dang-nhap");
            param.Add("#MailCompany#", _appSettings.EmailCompany);
            param.Add("#Company#", "VNSO SHOP");
            param.Add("#Ngay#", DateTime.Now.Day.ToString());
            param.Add("#Thang#", DateTime.Now.Month.ToString());
            param.Add("#Nam#", DateTime.Now.Year.ToString());
            param.Add("#FullName#", order.FullName);
            param.Add("#Email#", order.Email);
            param.Add("#PhoneNo#", order.Mobi);
            param.Add("#Address#", order.Address);
            param.Add("#Code#", order.Code);
            param.Add("#Payment#", order.PayTypeId == 2 ? "Chuyển khoản" : "Thanh toán khi nhận hàng (COD)");
            param.Add("#Remark#", order.Remark);
            param.Add("#Content#", content);
            //param.Add("#Total#", (order.TotalPrice.GetValueOrDefault(0) - order.FeeShip.GetValueOrDefault(0)).ToString("N0"));
            //param.Add("#FeeShip#", (order.FeeShip.GetValueOrDefault(0)).ToString("N0"));
            param.Add("#TotalPrice#", (order.TotalPrice.GetValueOrDefault(0)).ToString("N0"));

            string subject = "Xác nhận đơn hàng #" + order.Code + " từ VNSOSHOP";
            string body = System.IO.File.ReadAllText(emailTemplateUrl);

            foreach (var item in param)
            {
                body = body.Replace(item.Key, item.Value);
            }
            // gửi email cho cửa hàng
            _sendEmailService.Send(
                to: _appSettings.EmailTo,
                cc: "",
                subject: subject,
                html: body
            );

            // gửi cho khách mua hàng
           // _sendEmailService.Send(
           //    to: order.Email,
           //    cc: _appSettings.EmailTo,
           //    subject: subject,
           //    html: body
           //);
        }
    }
}
