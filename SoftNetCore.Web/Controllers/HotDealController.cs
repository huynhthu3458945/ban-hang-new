﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.DataAccess.ViewModel;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace SoftNetCore.Web.Controllers
{
    public class HotDealController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IBannerService _bannerService;
        private readonly IProductRepository _productRepository;
        private readonly IProductService _productService;

        public int ITEMS_PER_PAGE { get; set; }
        [BindProperty(SupportsGet = true, Name = "p")]
        public int currentPage { get; set; }
        public int countPages { get; set; }
        public int TOTAL_ITEM { get; set; }
        #endregion
        #region Ctor
        public HotDealController(IAboutService aboutService, 
                                 IBannerService bannerService,
                                 IProductRepository productRepository,
                                 IProductService productService)
        {
            _aboutService = aboutService;
            _bannerService = bannerService;
            _productRepository = productRepository;
            _productService = productService;
        }
        #endregion
        [HttpGet]
        [Route("deal-list", Name = "DealList")]
        public async Task<IActionResult> DealList(int? id, int? pageSize = 50, string sortBy = "Id", string sortDir = "DESC")
        {
            IList<BannerModel> result = await _bannerService.GetAllAsync();
            string titleSort = "Mặc định";
            string titleCat = string.Empty;
            ITEMS_PER_PAGE = pageSize.ToInt();
            TOTAL_ITEM = result.Count;
            countPages = (int)Math.Ceiling((double)TOTAL_ITEM / ITEMS_PER_PAGE);

            if (currentPage == 0)
                currentPage = 1;
            if (currentPage > countPages)
                currentPage = countPages;

            ViewBag.TitleCat = titleCat;
            ViewBag.CatId = id.ToInt();
            ViewBag.SortBy = sortBy;
            ViewBag.sortDir = sortDir;
            ViewBag.PageSize = ITEMS_PER_PAGE;
            ViewBag.currentPage = currentPage;
            ViewBag.countPages = countPages;
            ViewBag.Total = TOTAL_ITEM;
            if (sortBy == "Id" && sortDir == "DESC")
            {
                titleSort = "Mới nhất";
            }
            else if (sortBy == "StartDate" && sortDir == "ASC")
            {
                titleSort = "Cũ nhất";
            }
            else if (sortBy == "StartDate" && sortDir == "DESC")
            {
                titleSort = "Mới nhất";
            }
            ViewBag.TitleSort = titleSort;
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            return View(result);
        }

        [HttpGet]
        [Route("deal-hot-{id}", Name = "DealHot")]
        public async Task<IActionResult> DealHot(int? id, string title = "", int? pageSize = 50, string sortBy = "Id", string sortDir = "DESC")
        {
            string titleCat = "";
            string titleSort = "Mặc định";
            ITEMS_PER_PAGE = pageSize.ToInt();
            TOTAL_ITEM = _productService.GetTotalByBanner(id.ToInt(), true);
            countPages = (int)Math.Ceiling((double)TOTAL_ITEM / ITEMS_PER_PAGE);

            if (currentPage == 0)
                currentPage = 1;
            if (currentPage > countPages)
                currentPage = countPages;
            var banner = await _bannerService.FindById(id.ToInt());
            ViewBag.TitleCat = titleCat;
            ViewBag.Banner = banner;
            ViewBag.CatId = id ?? 0;
            ViewBag.SortBy = sortBy;
            ViewBag.sortDir = sortDir;
            ViewBag.PageSize = ITEMS_PER_PAGE;
            ViewBag.currentPage = currentPage;
            ViewBag.countPages = countPages;
            ViewBag.Total = TOTAL_ITEM;

            if (sortBy == "Id" && sortDir == "DESC")
            {
                titleSort = "Mới nhất";
            }
            else if (sortBy == "Price" && sortDir == "ASC")
            {
                titleSort = "Giá: Thấp đến cao";
            }
            else if (sortBy == "Price" && sortDir == "DESC")
            {
                titleSort = "Giá: Cao đến thấp";
            }
            ViewBag.TitleSort = titleSort;
            //
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            IList<ProductViewModel> result = (await _productRepository.GetByBannerAsync(banner.BannerId.ToInt(), ITEMS_PER_PAGE, currentPage, "", sortBy, sortDir));
            return View(result);
        }
    }
}
