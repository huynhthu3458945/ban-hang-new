﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Model.Request;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Controllers
{
    public class OrderController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IOrderService _orderService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public OrderController(IOrderService OrderService, IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _orderService = OrderService;
            _logging = logging;
        }
        #endregion

        [Route("dat-hang-thanh-cong")]
        public async Task<IActionResult> Index()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            return View();
        }

        [HttpGet]
        public ActionResult EvaluationRating(int id)
        {
            return ViewComponent("EvaluationRating", new { id = id });
        }

        [HttpPost]
        public async Task<ActionResult> PostRating(RatingRequest model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            bool status = false;
            try
            {
                model.IsRating = 1;
                var result = await _orderService.UpdateRatingAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
                status = result.Success;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public ActionResult CancelOrder(int id)
        {
            return ViewComponent("CancelOrder", new { id = id });
        }

        [HttpPost]
        public async Task<ActionResult> PostCancelOrder(StatusOrderRequest model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            bool status = false;
            try
            {
                var result = await _orderService.UpdateStatusAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
                status = result.Success;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage }, new Newtonsoft.Json.JsonSerializerSettings());
        }
    }
}
