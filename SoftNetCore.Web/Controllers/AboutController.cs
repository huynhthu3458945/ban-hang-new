﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.Interface;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Net.Http;
using SoftNetCore.Model.Catalog;
using Newtonsoft.Json;

namespace SoftNetCore.Web.Controllers
{
    public class AboutController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public AboutController(IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        [Route("gioi-thieu")]
        public async Task<IActionResult> Index()
        {
            var result = await _aboutService.GetFisrtAsync(1);
            return View(result);
        }

        [HttpGet]
        [Route("huong-dan-thanh-toan")]
        public async Task<IActionResult> PolicyPayment()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _aboutService.GetFisrtAsync(2);
            return View(result);
        }

        [HttpGet]
        [Route("chinh-sach-khuyen-mai")]
        public async Task<IActionResult> PolicySale()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _aboutService.GetFisrtAsync(3);
            return View(result);
        }

        [HttpGet]
        [Route("chinh-sach-giao-hang")]
        public async Task<IActionResult> PolicyShipping()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _aboutService.GetFisrtAsync(4);
            return View(result);
        }

        [HttpGet]
        [Route("dieu-khoan-su-dung")]
        public async Task<IActionResult> PolicyTerms()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _aboutService.GetFisrtAsync(5);
            return View(result);
        }

        [HttpGet]
        [Route("chinh-sach-doi-tra")]
        public async Task<IActionResult> PolicyReturn()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _aboutService.GetFisrtAsync(4);
            return View(result);
        }
    }
}
