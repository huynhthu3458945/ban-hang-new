﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.Interface;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Net.Http;
using SoftNetCore.Model.Catalog;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using X.PagedList;
using System.Collections.Generic;

namespace SoftNetCore.Web.Controllers
{
    public class SearchController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IProductService _productService;
        private readonly IProductCategoryService _productCategoryService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public SearchController(IProductService productService, IProductCategoryService productCategoryService, IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _productService = productService;
            _productCategoryService = productCategoryService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        [Route("tim-kiem")]
        [Route("tim-kiem-{keyword}")]
        public async Task<IActionResult> Index(string keyword, int? page)
        {
            int pageNumper = 0;
            int.TryParse(page.ToString(), out pageNumper);
            if (pageNumper == 0)
                pageNumper = 1;

            ViewBag.KeyWord = keyword;
            if (string.IsNullOrEmpty(keyword))
            {
                List<ProductModel> model = new List<ProductModel>();
                var result = model.ToPagedList(pageNumper, 20);
                return View(result);
            }
            keyword = keyword.ToUrlFormat(true);
            IPagedList<ProductModel> data = (await _productService.SearchAsync(keyword)).ToPagedList(pageNumper, 20);
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            return View(data);
        }
    }
}
