﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace SoftNetCore.Web.Controllers
{
    public class SaleController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly ISaleService _saleService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public SaleController(ISaleService saleService, IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _saleService = saleService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        [Route("danh-sach-giam-gia")]
        public async Task<IActionResult> Index()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _saleService.GetByStatusAsync(true);
            return View(result);
        }

        [HttpGet]
        [Route("giam-gia")]
        [Route("giam-gia-{title}-{id}")]
        public async Task<IActionResult> Detail(int id)
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _saleService.FindById(id);
            return View(result);
        }
    }
}
