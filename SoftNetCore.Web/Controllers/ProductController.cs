﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.Interface;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Net.Http;
using SoftNetCore.Model.Catalog;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using X.PagedList;
using System.Collections.Generic;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.DataAccess.ViewModel;
using System;

namespace SoftNetCore.Web.Controllers
{
    public class ProductController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IProductRepository _productRepository;
        private readonly IProductCategoryService _productCategoryService;
        private readonly IProductService _productService;
        private readonly ILogging _logging;

        public int ITEMS_PER_PAGE { get; set; }
        [BindProperty(SupportsGet = true, Name = "p")]
        public int currentPage { get; set; }
        public int countPages { get; set; }
        public int TOTAL_ITEM { get; set; }

        #endregion
        #region Ctor
        public ProductController(IProductRepository productRepository,
            IProductCategoryService productCategoryService,
            IAboutService aboutService,
            IProductService productService,
            ILogging logging)
        {
            _aboutService = aboutService;
            _productRepository = productRepository;
            _productCategoryService = productCategoryService;
            _productService = productService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        [Route("san-pham-{title}-{id}", Name = "ExistProductCat")]
        [Route("san-pham")]
        public async Task<IActionResult> Index(int? id, string title = "", int? pageSize = 50, string sortBy = "Id", string sortDir = "DESC")
        {
            string titleCat = "Sản phẩm";
            string titleSort = "Mặc định";
            string banner = "";
            ITEMS_PER_PAGE = pageSize.ToInt();
            TOTAL_ITEM = _productService.GetTotalByProductCategory(id.ToInt(), true);
            countPages = (int)Math.Ceiling((double)TOTAL_ITEM / ITEMS_PER_PAGE);

            if (currentPage == 0)
                currentPage = 1;
            if (currentPage > countPages)
                currentPage = countPages;

            if (id.HasValue && id != 0)
            {
                var item = await _productCategoryService.FindById(id.ToInt());
                titleCat = item.Title;
                banner = item.Avatar;
            }

            ViewBag.TitleCat = titleCat;
            ViewBag.Banner = banner;
            ViewBag.CatId = id ?? 0;
            ViewBag.SortBy = sortBy;
            ViewBag.sortDir = sortDir;
            ViewBag.PageSize = ITEMS_PER_PAGE;
            ViewBag.currentPage = currentPage;
            ViewBag.countPages = countPages;
            ViewBag.Total = TOTAL_ITEM;

            if (sortBy == "Id" && sortDir == "DESC")
            {
                titleSort = "Mới nhất";
            }
            else if (sortBy == "Price" && sortDir == "ASC")
            {
                titleSort = "Giá: Thấp đến cao";
            }
            else if (sortBy == "Price" && sortDir == "DESC")
            {
                titleSort = "Giá: Cao đến thấp";
            }
            ViewBag.TitleSort = titleSort;
            //
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            //var result = await _productRepository.GetAllAsync(15, pageNumper, "Id", "DESC");
            IList<ProductViewModel> result = (await _productRepository.GetAllAsync(id.ToInt(), ITEMS_PER_PAGE, currentPage, "", sortBy, sortDir));
            return View(result);
        }

        [HttpGet]
        [Route("chi-tiet-san-pham")]
        [Route("chi-tiet-san-pham-{title}-{id}")]
        public async Task<IActionResult> Detail(int id)
        {
            var result = await _productRepository.GetByIdAsync(id);
            result.ContentModels = new List<ContentViewModel>();
            //if (!string.IsNullOrEmpty(result.Content))
            //    result.ContentModels = JsonConvert.DeserializeObject<List<ContentViewModel>>(result.Content);
            var item = await _productCategoryService.FindById(result.ProductCategoryId.ToInt());
            ViewBag.TitleCat = item.Title;
            result.ImageList = new List<string>();
            if (!string.IsNullOrEmpty(result.ImageListProduct))
                result.ImageList = JsonConvert.DeserializeObject<List<string>>(result.ImageListProduct);
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            return View(result);
        }

    }
}
