﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.DataAccess.ViewModel;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace SoftNetCore.Web.Controllers
{
    public class CouponController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly ICouponRepository _couponRepository;
        private readonly ICouponTypeService _couponTypeService;
        private readonly ICouponService _couponService;

        public int ITEMS_PER_PAGE { get; set; }
        [BindProperty(SupportsGet = true, Name = "p")]
        public int currentPage { get; set; }
        public int countPages { get; set; }
        public int TOTAL_ITEM { get; set; }
        #endregion
        #region Ctor
        public CouponController(IAboutService aboutService, 
            ICouponRepository couponRepository, 
            ICouponTypeService couponTypeService,
            ICouponService couponService)
        {
            _aboutService = aboutService;
            _couponRepository = couponRepository;
            _couponTypeService = couponTypeService;
            _couponService = couponService;
        }
        #endregion
        [HttpGet]
        [Route("coupon")]
        [Route("coupon-{title}-{id}", Name = "ExistCouponType")]
        public async Task<IActionResult> Index(int? id, int? pageSize = 50, string sortBy = "Id", string sortDir = "DESC")
        {
            string titleSort = "Mặc định";
            string titleCat = string.Empty;
            ITEMS_PER_PAGE = pageSize.ToInt();
            TOTAL_ITEM = _couponService.GetTotalByCouponType(id.ToInt(), 1);
            countPages = (int)Math.Ceiling((double)TOTAL_ITEM / ITEMS_PER_PAGE);

            if (currentPage == 0)
                currentPage = 1;
            if (currentPage > countPages)
                currentPage = countPages;

            if (id.HasValue && id != 0)
            {
                var item = await _couponTypeService.FindById(id.ToInt());
                titleCat = item.Title;
            }

            ViewBag.TitleCat = titleCat;
            ViewBag.CatId = id.ToInt();
            ViewBag.SortBy = sortBy;
            ViewBag.sortDir = sortDir;
            ViewBag.PageSize = ITEMS_PER_PAGE;
            ViewBag.currentPage = currentPage;
            ViewBag.countPages = countPages;
            ViewBag.Total = TOTAL_ITEM;
            if (sortBy == "Id" && sortDir == "DESC")
            {
                titleSort = "Mới nhất";
            }
            else if (sortBy == "StartDate" && sortDir == "ASC")
            {
                titleSort = "Cũ nhất";
            }
            else if (sortBy == "StartDate" && sortDir == "DESC")
            {
                titleSort = "Mới nhất";
            }
            ViewBag.TitleSort = titleSort;
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            IList<CouponViewModel> result = await _couponRepository.GetAllAsync(id.ToInt(), ITEMS_PER_PAGE, currentPage, "", sortBy, sortDir);
            foreach (var coupon in result)
            {
                coupon.CouponDetails = JsonConvert.DeserializeObject<List<CouponDetailViewModel>>(coupon.Coupons);
            }
            return View(result);
        }
    }
}
