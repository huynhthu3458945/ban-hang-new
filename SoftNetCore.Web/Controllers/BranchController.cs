﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.Interface;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Net.Http;
using SoftNetCore.Model.Catalog;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using X.PagedList;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace SoftNetCore.Web.Controllers
{
    public class BranchController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IBranchService _branchService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public BranchController(IAboutService aboutService, IBranchService branchService, IProvinceService provinceService, IDistrictService districtService, ILogging logging)
        {
            _aboutService = aboutService;
            _branchService = branchService;
            _provinceService = provinceService;
            _districtService = districtService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        [Route("he-thong")]
        public async Task<IActionResult> Index()
        {
            List<ProvinceModel> dataProvince = await _provinceService.GetByBranchExists();
            List<SelectListItem> dataProvinceDrop = dataProvince.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();

            List<DistrictModel> dataDistrict = await _districtService.GetByBranchExists();
            List<SelectListItem> dataDistrictDrop = dataDistrict.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();

            ViewBag.ProvinceID = dataProvinceDrop;
            ViewBag.DistrictID = dataDistrictDrop;
            //
            BranchModel model = new BranchModel();

            return View(model);
        }

        [HttpGet]
        [Route("chi-tiet-dai-ly")]
        [Route("chi-tiet-dai-ly-{title}-{id}")]
        public async Task<IActionResult> Detail(int id)
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            return View();
        }

        [HttpGet]
        public JsonResult LoadDataDrop(string provinceId)
        {
            var data = _districtService.GetByBranchExistsAndProvinceId(provinceId.ToInt());
            return Json(data.Select(x => new SelectListItem { Text = x.Title, Value = x.Id.ToString() }), new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        public IActionResult Search(int provinceId, int districtId)
        {
            return ViewComponent("SearchBranch", new { provinceId = provinceId, districtId = districtId });
        }
    }
}
