﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.Interface;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Net.Http;
using SoftNetCore.Model.Catalog;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using X.PagedList;
using System.Linq;

namespace SoftNetCore.Web.Controllers
{
    public class ArticleController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IArticleService _articleService;
        private readonly IArticleCategoryService _articleCategoryService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ArticleController(IArticleService articleService, IArticleCategoryService articleCategoryService, IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _articleService = articleService;
            _articleCategoryService = articleCategoryService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        [Route("tin-tuc")]
        [Route("tin-tuc-{title}-{id}")]
        [Route("tin-tuc-{id}")]
        public async Task<IActionResult> Index(int? id, int? page)
        {
            string titleCat = string.Empty;
            int pageNumper = 0;
            int.TryParse(page.ToString(), out pageNumper);
            if (pageNumper == 0)
                pageNumper = 1;

            if (id != null)
            {
                var item = await _articleCategoryService.FindById(id.ToInt());
                titleCat = $"<span></span><a href=\"/tin-tuc\">Tin Tức</a>";
                titleCat += $"<span></span>{item.Title}";
            }
            else
            {
                titleCat = "<span></span>Tin Tức";
            }
            ViewBag.TitleCat = titleCat;
            ViewBag.CatId = id ?? 0;
            //ViewBag.Catgory = await _articleCategoryService.GetByStatusAsync(true);
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            
            IPagedList<ArticleModel> data = id == null ? (await _articleService.GetByStatusAsync(true)).ToPagedList(pageNumper, 15) : (await _articleService.GetByCategoryAsync(id.ToInt())).ToPagedList(pageNumper, 15);
           
            return View(data);
        }

        [HttpGet]
        [Route("chi-tiet-tin-tuc")]
        [Route("chi-tiet-tin-tuc-{title}-{id}")]
        public async Task<IActionResult> Detail(int id)
        {
            var result = await _articleService.FindById(id);
            var item = await _articleCategoryService.FindById(result.ArticleCategoryId.ToInt());
            ViewBag.TitleCat = item?.Title;
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);

            return View(result);
        }
    }
}
