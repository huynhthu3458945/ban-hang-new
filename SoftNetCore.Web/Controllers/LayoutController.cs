﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.Interface;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Net.Http;
using SoftNetCore.Model.Catalog;
using Newtonsoft.Json;


namespace SoftNetCore.Web.Controllers
{
    public class LayoutController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IFaqService _faqService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public LayoutController(IAboutService aboutService, IFaqService faqService, ILogging logging)
        {
            _aboutService = aboutService;
            _faqService = faqService;
            _logging = logging;
        }
        #endregion


        [HttpGet]
        [Route("hoi-dap")]
        public async Task<IActionResult> FAQ()
        { 
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _faqService.GetByStatusAsync(true);
            return View(result);
        }
    }
}
