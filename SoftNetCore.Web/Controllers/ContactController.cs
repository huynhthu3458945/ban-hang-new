﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.Services.Loggings;
using SoftNetCore.Services.Interface;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Net.Http;
using SoftNetCore.Model.Catalog;
using Newtonsoft.Json;
using SoftNetCore.Common.Helpers;
using System;

namespace SoftNetCore.Web.Controllers
{
    public class ContactController : Controller
    {
        #region Fields
        private readonly IContactService _contactService;
        private readonly IAboutService _aboutService;
        private readonly ISendEmailService _sendEmailService;
        private readonly AppSettings _appSettings;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public ContactController(IContactService contactService, IAboutService aboutService, ISendEmailService sendEmailService, IOptions<AppSettings> appSettings, ILogging logging)
        {
            _contactService = contactService;
            _aboutService = aboutService;
            _sendEmailService = sendEmailService;
            _appSettings = appSettings.Value;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        [Route("lien-he")]
        public async Task<IActionResult> Index()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            ContactModel model = new ContactModel();
            return View(model);
        }


        [HttpPost]
        public async Task<ActionResult> SaveContact(ContactModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            bool status = false;
            try
            {
                // Lưu thông tin
                var result = await _contactService.AddAsync(model);
                if (result.Success)
                {
                    // gửi email
                    string content = "[VNSO SHOP.com] - Liên hệ mua hàng";
                    if (!string.IsNullOrEmpty(model.Content))
                        content = model.Content;
                    sendContact(_appSettings.EmailTo, "", content, model.FullName, model.Mobi);
                    typeMessage = EnumsTypeMessage.Success;
                }
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
                status = result.Success;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage }, new Newtonsoft.Json.JsonSerializerSettings());
        }


        private void sendContact(string to, string cc, string content, string fullname, string mobi)
        {
            string message = $@"<p>Nội dung liên hệ:</p>
                             <p>{content}</p>";

            _sendEmailService.Send(
                to: to,
                cc: cc,
                subject: $@"[VNSO SHOP.com] - Khách hàng liên hệ",
                html: $@"<h4>Khách hàng: {fullname} - {mobi}!</h4>
                         {message}
                        <p>Đây là một tin nhắn tự động, vui lòng không trả lời!</p>"
            );
        }
    }
}
