﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoftNetCore.Common.Extensions;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Controllers
{
    public class ShoppingCartController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IProductService _productService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductCategoryService _productCategoryService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor

        public ShoppingCartController(IProductService productService, IProductAttributeService productAttributeService,
                                      IProductCategoryService productCategoryService, IAboutService aboutService, ILogging logging)
        {
            _aboutService = aboutService;
            _productService = productService;
            _productAttributeService = productAttributeService;
            _productCategoryService = productCategoryService;
            _logging = logging;
        }
        #endregion

        [Route("gio-hang")]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            double total = 0;
            //double feeship = 0;
            List<CartItem> listCart = new List<CartItem>();
            var cart = HttpContext.Session.GetString("cart");
            if (!string.IsNullOrEmpty(cart))
            {
                listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
                int totalItem = listCart.Count.ToInt();
                total = ListCartItem.GetTotal(listCart);
                //if (total < 500000)
            }
            ViewBag.Total = total;
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            return View(listCart);
        }

        [HttpGet]
        public ActionResult ReloadCart()
        {
            string html = string.Empty;
            string total = string.Empty;
            List<CartItem> listCart = new List<CartItem>();
            var cart = HttpContext.Session.GetString("cart");
            if (!string.IsNullOrEmpty(cart))
            {
                listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
                foreach (var item in listCart)
                {
                    html += $"<li>";
                    html += $"  <div class=\"shopping-cart-img\">";
                    html += $"      <a href=\"/chi-tiet-san-pham-{item.ProductId}\"><img alt=\"{item.ProductName.Peel(54)}\" src=\"{item.Avatar}\"></a>";
                    html += $"  </div>";
                    html += $"  <div class=\"shopping-cart-title\">";
                    html += $"      <h4><a href=\"/chi-tiet-san-pham-item.ProductId\">{item.ProductName.Peel(54)}</a></h4>";
                    html += $"      <h4 data-price=\"{item.Price}\" class=\"eachPrice\"><span>{item.Quantity} × </span>{String.Format("{0:0,0đ}", item.Price)}</h4>";
                    html += $"  </div>";
                    html += $"  <div class=\"shopping-cart-delete\">";
                    html += $"      <a href=\"#\"><i class=\"fi-rs-cross-small\"></i></a>";
                    html += $"   </div>";
                    html += $"</li>";
                }
                total = String.Format("{0:0,0đ}", listCart.Sum(z => z.Price * z.Quantity));
            }

            return Json(new { status = true, html = html, total = total });
        }

        [HttpGet]
        public async Task<IActionResult> AddItem(int id, int quantity)
        {
            //lấy data
            var data = await _productService.FindById(id);
            //4. Nếu giỏ hàng chưa có thì thêm mới ngược lại thì cộng dồn theo id sản phẩm
            var cart = HttpContext.Session.GetString("cart");
            if (string.IsNullOrEmpty(cart))
            {
                //4.1 Tạo mới đối tượng giỏ hàng
                List<CartItem> listCart = new List<CartItem>();
                //4.2 Thêm vào giỏ hàng với các thuộc tính đã lấy
                listCart = ListCartItem.AddList(data.Id, data.Title, data.Thumb, 0, string.Empty, 0, string.Empty, quantity, data.IsSale ? data.PriceSale.ToDouble() : data.Price.ToDouble(), data.Price.ToDouble(), data.ProductCategoryId.ToInt());
                HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(listCart));
            }
            else
            {
                //4.3 Tạo mới đối tượng giỏ hàng
                List<CartItem> listCart = new List<CartItem>();
                //4.4 Chuyển kiếu Session về dạng List<CartItem> và cộng dồn
                listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
                listCart = ListCartItem.Update(listCart, data.Id, data.Title, data.Thumb, 0, string.Empty, 0, string.Empty, quantity, data.IsSale ? data.PriceSale.ToDouble() : data.Price.ToDouble(), data.Price.ToDouble(), data.ProductCategoryId.ToInt());
                HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(listCart));
            }
            return ViewComponent("CartMini");
        }

        //[HttpGet]
        //public async Task<ActionResult> AddItem(int sizeId, int quantity)
        //{
        //    try
        //    {
        //        //lấy data
        //        var data = await _productService.FindById(sizeId);
        //        //4. Nếu giỏ hàng chưa có thì thêm mới ngược lại thì cộng dồn theo id sản phẩm
        //        var cart = HttpContext.Session.GetString("cart");
        //        if (string.IsNullOrEmpty(cart))
        //        {
        //            //4.1 Tạo mới đối tượng giỏ hàng
        //            List<CartItem> listCart = new List<CartItem>();
        //            //4.2 Thêm vào giỏ hàng với các thuộc tính đã lấy
        //            listCart = ListCartItem.AddList(data.Id, data.Title, data.Thumb, 0, string.Empty, 0, string.Empty, quantity, data.IsSale ? data.PriceSale.ToDouble() : data.Price.ToDouble(), data.Price.ToDouble(), data.ProductCategoryId.ToInt());
        //            HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(listCart));
        //        }
        //        else
        //        {
        //            //4.3 Tạo mới đối tượng giỏ hàng
        //            List<CartItem> listCart = new List<CartItem>();
        //            //4.4 Chuyển kiếu Session về dạng List<CartItem> và cộng dồn
        //            listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
        //            listCart = ListCartItem.Update(listCart, data.Id, data.Title, data.Thumb, 0, string.Empty, 0, string.Empty, quantity, data.IsSale ? data.PriceSale.ToDouble() : data.Price.ToDouble(), data.Price.ToDouble(), data.ProductCategoryId.ToInt());
        //            HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(listCart));
        //        }
        //        return Json(new { Status = true }, new Newtonsoft.Json.JsonSerializerSettings());
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Status = false }, new Newtonsoft.Json.JsonSerializerSettings());
        //    }
        //}

        public ActionResult DeletedReload(int id)
        {
            var cart = HttpContext.Session.GetString("cart");
            if (!string.IsNullOrEmpty(cart))
            {
                List<CartItem> listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
                int index = 0;
                ListCartItem.Remove(listCart, ListCartItem.GetItemIndex(listCart, id, ref index));
                HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(listCart));
            }
            return Redirect(Request.GetTypedHeaders().Referer.ToString());
        }

        //[HttpPost]
        //public ActionResult DeletedReload(int id, int colorId, int sizeId)
        //{
        //    var cart = HttpContext.Session.GetString("cart");
        //    List<CartItem> listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
        //    int index = 0;
        //    ListCartItem.Remove(listCart, ListCartItem.GetItemIndex(listCart, id, colorId, sizeId, ref index));
        //    HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(listCart));
        //    return Redirect(Request.GetTypedHeaders().Referer.ToString());
        //}

        [HttpGet]
        public ActionResult RemoveAllCart()
        {
            HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(new List<CartItem>()));
            return Json(new { status = true });
        }

        [HttpPost]
        public ActionResult Deleted(int id, int colorId, int sizeId)
        {
            var cart = HttpContext.Session.GetString("cart");
            List<CartItem> listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
            int index = 0;
            ListCartItem.Remove(listCart, ListCartItem.GetItemIndex(listCart, id, colorId, sizeId, ref index));
            HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(listCart));
            return Json(new { status = true });
        }

        [HttpGet]
        public ActionResult GetQuantityCart()
        {
            var cart = HttpContext.Session.GetString("cart");
            List<CartItem> listCart = new List<CartItem>();
            if (!string.IsNullOrEmpty(cart))
            {
                listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
                return Json(new { count = listCart.Sum(z => z.Quantity) });
            }
            return Json(new { count = listCart.Sum(z => z.Quantity) });
        }

        [HttpPost]
        public ActionResult Update(string cartModel)
        {
            var cart = HttpContext.Session.GetString("cart");
            List<CartItem> listCart = JsonConvert.DeserializeObject<List<CartItem>>(cart);
            IEnumerable<string> data = cartModel.Split('!');
            foreach (var item in data)
            {
                if (item != "")
                {
                    ListCartItem.UpdateQuantity(listCart, item.Split(';')[1].ToInt(), item.Split(';')[2].ToInt(), item.Split(';')[3].ToInt(), item.Split(';')[0].ToInt());
                }
            }
            HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(listCart));
            return Json(new { status = true });
        }
    }
}
