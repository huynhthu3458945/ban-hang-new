﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SoftNetCore.Common.Constants;
using SoftNetCore.Common.Helpers;
using SoftNetCore.Data.Entities;
using SoftNetCore.Model.Catalog;
using SoftNetCore.Model.Request;
using SoftNetCore.Services.Interface;
using SoftNetCore.Services.Loggings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Controllers
{
    public class CustomerController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly ICustomerService _customerService;
        public readonly IAccountService _accountService;
        public readonly IOrderService _orderService;
        private readonly IProvinceService _provinceService;
        private readonly IDistrictService _districtService;
        private readonly IProductWishlistService _productWishlistService;
        private readonly ILogging _logging;
        #endregion
        #region Ctor
        public CustomerController(ICustomerService customerService, IAccountService accountService,
                                    IProvinceService provinceService, IDistrictService districtService,
                                    IAboutService aboutService,
                                    IProductWishlistService productWishlistService,
                                    IOrderService orderService, ILogging logging)
        {
            _aboutService = aboutService;
            _customerService = customerService;
            _accountService = accountService;
            _orderService = orderService;
            _provinceService = provinceService;
            _districtService = districtService;
            _productWishlistService = productWishlistService;
            _logging = logging;
        }
        #endregion

        [HttpGet]
        [Route("dang-nhap")]
        public async Task<IActionResult> Login()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            AuthenticateRequest model = new AuthenticateRequest();
            model.Username = Request.Cookies[SessionKeys.USERNAME];
            model.Password = Request.Cookies[SessionKeys.PASSWORD];
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> PostLogin(AuthenticateRequest model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _customerService.Authenticate(model);
                    if (result == null || result.TypeId != "2")
                    {
                        return Json(new { Status = false, Title = "Thông báo", Message = "Đăng nhập thất bại!", Type = EnumsTypeMessage.Error }, new Newtonsoft.Json.JsonSerializerSettings());
                    }
                    // create claims
                    List<Claim> claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, result.Id),
                    new Claim(ClaimTypes.NameIdentifier, result.FullName.ToSafetyString()),
                    new Claim(ClaimTypes.MobilePhone, result.Phone.ToSafetyString()),
                    new Claim("Avatar", result.Avatar.ToSafetyString()),
                    new Claim("JwtToken", result.JwtToken.ToSafetyString()),
                    new Claim(ClaimTypes.Email, result.Email.ToSafetyString()),
                    new Claim("CustomerId", result.CustomerId.ToSafetyString()),
                    new Claim("Type", result.TypeId.ToSafetyString()),
                };
                    // create identity
                    ClaimsIdentity identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                    // create principal
                    ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                    // sign-in
                    await HttpContext.SignInAsync(
                            scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                            principal: principal,
                            properties: new AuthenticationProperties
                            {
                                IsPersistent = model.RememberMe, // for 'remember me' feature
                                                                 //ExpiresUtc = DateTime.UtcNow.AddMinutes(1)
                            });
                    if (string.IsNullOrEmpty(model.ReturnUrl))
                        model.ReturnUrl = "/";

                    HttpContext.Response.Cookies.Delete(SessionKeys.USERNAME);
                    HttpContext.Response.Cookies.Delete(SessionKeys.PASSWORD);
                    if (model.RememberMe == true)
                    {
                        HttpContext.Response.Cookies.Append(SessionKeys.USERNAME, model.Username);
                        HttpContext.Response.Cookies.Append(SessionKeys.PASSWORD, model.Password);
                    }

                    return Json(new { Status = true, Title = "Thông báo", Message = "Đăng nhập thành công!", Type = EnumsTypeMessage.Success, ReturnUrl = model.ReturnUrl }, new Newtonsoft.Json.JsonSerializerSettings());
                }
                catch
                {
                    return Json(new { Status = false, Title = "Thông báo", Message = "Đăng nhập thất bại!", Type = EnumsTypeMessage.Error }, new Newtonsoft.Json.JsonSerializerSettings());
                }
            }
            return Json(new { Status = false, Title = "Thông báo", Message = "Đăng nhập thất bại!", Type = EnumsTypeMessage.Error }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        [Route("dang-ky")]
        public async Task<IActionResult> Register()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            CustomerModel model = new CustomerModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> PostRegister(CustomerModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            bool status = false;
            try
            {
                var accountModel = new AccountModel()
                {
                    UserName = model.Phone,
                    Email = model.Email,
                    Password = model.Password,
                    Status = true,
                    AccountTypeId = 2
                };
                var accountResult = await _accountService.AddAsync(accountModel);
                if (accountResult.Success)
                {
                    var account = (Account)accountResult.Data;
                    model.AccountId = account.Id;
                    var result = await _customerService.AddAsync(model);
                    if (result.Success)
                        typeMessage = EnumsTypeMessage.Success;
                    else
                        typeMessage = EnumsTypeMessage.Error;
                    mesage = result.Message;
                    status = result.Success;
                }
                else
                {
                    mesage = accountResult.Message;
                    status = accountResult.Success;
                    typeMessage = EnumsTypeMessage.Error;
                }
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public async Task<JsonResult> LoadDataDrop(string id)
        {
            var districtDrop = await _districtService.GetByProvinceAsync(id.ToInt());
            List<SelectListItem> District = districtDrop.Select(n =>
            new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();
            //
            return Json(new { DistrictDrop = District }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        [Route("tai-khoan")]
        public async Task<IActionResult> MyAccount()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            int customerId = 0;
            if (!string.IsNullOrEmpty(User.Identity.Name))
                customerId = (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.Name))?.Value).ToInt();
            if (customerId == 0) return Redirect("/dang-nhap");
            var customer = await _customerService.FindByIdHaveOrder(customerId);
            ViewBag.Orders = await _orderService.GetByCustomerAsync(customerId);
            ViewBag.Wishlist = await _productWishlistService.GetByCustomerAsync(customerId);

            //// Tỉnh thành
            var provinceDrop = await _provinceService.GetAllAsync();
            List<SelectListItem> Province = provinceDrop.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title, Selected = customer?.ProvinceId == n.Id ? true : false }).ToList();
            ViewBag.ProvinceID = Province;

            //// Nếu khách hàng có tỉnh thành
            //if (result != null && result.ProvinceId != null)
            //{
            //    // Quận huyện
            //    var distictrop = await _districtService.GetByProvinceAsync(result.ProvinceId.ToInt());
            //    List<SelectListItem> District = distictrop.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title, Selected = result?.DistrictId == n.Id ? true : false }).ToList();
            //    ViewBag.DistrictID = District;
            //}
            //else
            //{
            //    // Quận huyện
            //    var distictrop = await _districtService.GetByProvinceAsync(Province.Count() > 0 ? Convert.ToInt32(Province.FirstOrDefault().Value) : 1);
            //    List<SelectListItem> District = distictrop.Select(n => new SelectListItem { Value = n.Id.ToString(), Text = n.Title }).ToList();
            //    ViewBag.DistrictID = District;
            //}
            //
            return View(customer);
        }

        [HttpPost]
        public async Task<ActionResult> PostChange(CustomerModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            bool status = false;
            try
            {
                //if (model.IsChangePass)
                //{
                //    var account = await _accountService.FindById(model.AccountId.ToInt());
                //    account.Password = model.Password;
                //    await _accountService.UpdateAsync(account);
                //}
                var result = await _customerService.UpdateAsync(model);
                if (result.Success)
                    typeMessage = EnumsTypeMessage.Success;
                else
                    typeMessage = EnumsTypeMessage.Error;
                mesage = result.Message;
                status = result.Success;
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpPost]
        public async Task<ActionResult> ChangePass(CustomerModel model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            bool status = false;
            try
            {
                var account = await _accountService.FindById(model.AccountId.ToInt());
                if (account?.Password == model.Password)
                {
                    account.Password = model.NewPassword;
                    var result = await _accountService.UpdateAsync(account);
                    if (result.Success)
                        typeMessage = EnumsTypeMessage.Success;
                    else
                        typeMessage = EnumsTypeMessage.Error;
                    mesage = result.Message;
                    status = result.Success;
                }
                else
                {

                    mesage = "Mật khẩu hiện tại không chính xác";
                    status = false;
                    typeMessage = EnumsTypeMessage.Error;
                }
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        [Route("dang-xuat", Name = "logout")]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("/");
        }

        [HttpGet]
        public ActionResult OrderDetail(int id)
        {
            return ViewComponent("ViewOrder", new { id = id });
        }

        [HttpGet]
        [Route("quen-mat-khau")]
        public async Task<IActionResult> ForgetPassword(ForgotPasswordRequest model)
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> PostForgetPassword(ForgotPasswordRequest model)
        {
            string mesage = string.Empty;
            string typeMessage = string.Empty;
            var hostName = Request.Scheme + "://" + Request.Host.Value;
            bool status = false;
            try
            {
                if (!string.IsNullOrEmpty(model.Email))
                {
                    var result = await _accountService.ForgotPassword(model, hostName);
                    if (result.Success)
                        typeMessage = EnumsTypeMessage.Success;
                    else
                        typeMessage = EnumsTypeMessage.Error;
                    mesage = result.Message;
                    status = result.Success;
                }
                else
                {
                    mesage = "Vui lòng nhập email!";
                    typeMessage = EnumsTypeMessage.Error;
                }
            }
            catch (Exception ex)
            {
                mesage = ex.Message;
                typeMessage = EnumsTypeMessage.Error;
            }
            return Json(new { Status = status, Title = "Thông báo", Message = mesage, Type = typeMessage }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        [HttpGet]
        [Route("yeu-thich")]
        public async Task<IActionResult> WishList()
        {
            int customerId = 0;
            if (!string.IsNullOrEmpty(User.Identity.Name))
                customerId = (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.Name))?.Value).ToInt();
            if (customerId == 0) return Redirect("/dang-nhap");

            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var result = await _customerService.FindByIdHaveOrder(customerId);
            var res = await _productWishlistService.GetByCustomerAsync(result.Id);
            return View(res);
        }

        [HttpGet]
        public async Task<ActionResult> AddWishlist(int productId)
        {
            try
            {
                int customerId = 0;
                if (!string.IsNullOrEmpty(User.Identity.Name))
                    customerId = (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.Name))?.Value).ToInt();
                if (customerId == 0)
                    return Json(new { Status = 2, Message = string.Empty }, new Newtonsoft.Json.JsonSerializerSettings());

                var customer = await _customerService.FindByIdHaveOrder(customerId);
                var model = new ProductWishlistModel();
                model.ProductId = productId;
                model.CustomerId = customerId;
                model.CreateBy = customerId;
                model.CreateTime = DateTime.Now.Date;
                var res = await _productWishlistService.AddAsync(model);
                return Json(new { Status = 1, Message = res.Message }, new Newtonsoft.Json.JsonSerializerSettings());
            }
            catch (Exception ex)
            {
                return Json(new { status = 3 }, new Newtonsoft.Json.JsonSerializerSettings());
            }

        }

        [HttpGet]
        public async Task<ActionResult> GetQuantityWishlist()
        {
            int customerId = 0;
            if (!string.IsNullOrEmpty(User.Identity.Name))
                customerId = (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.Name))?.Value).ToInt();

            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            var res = await _productWishlistService.GetByCustomerAsync(customerId);

            return Json(new { count = res.Count });
        }
        [HttpGet]
        public async Task<ActionResult> RemoveWishlist(int customerId, int productId)
        {
            var res = await _productWishlistService.DeleteByReferenceIdAsync(customerId, productId);
            return Json(new { status = res.Success });
        }
    }
}
