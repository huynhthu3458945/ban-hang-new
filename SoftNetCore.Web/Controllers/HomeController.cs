﻿using Microsoft.AspNetCore.Mvc;
using SoftNetCore.DataAccess.Repository;
using SoftNetCore.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftNetCore.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Fields
        private readonly IAboutService _aboutService;
        private readonly IProductRepository _productRepository;
        #endregion
        #region Ctor
        public HomeController(IAboutService aboutService, IProductRepository productRepository)
        {
            _aboutService = aboutService;
            _productRepository = productRepository;
        }
        #endregion

        public async Task<IActionResult> Index()
        {
            ViewBag.Info = await _aboutService.GetFisrtAsync(1);
            //var data = await _productRepository.GetByHotAsync(20);
            return View();
        }
    }
}
