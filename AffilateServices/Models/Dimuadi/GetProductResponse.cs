﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AffilateServices.Models.Dimuadi
{
   
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    

    public class GetProductResponse
    {
        public int status { get; set; }
        public string message { get; set; }
        public List<Datum> data { get; set; }
        public Paging paging { get; set; }

        public class Datum
        {
            public string id { get; set; }
            public string sku { get; set; }
            public object url { get; set; }
            //public object branch { get; set; }
            public string thumbImage { get; set; }
            public DateTime? startedAt { get; set; }
            public DateTime? endedAt { get; set; }
            public string commissionType { get; set; }
            //public long commissionValue { get; set; }
            public int? maxCommissionRate { get; set; }
            public int? maxCommissionValue { get; set; }
            public int status { get; set; }
            public DateTime createdAt { get; set; }
            public DateTime updatedAt { get; set; }
            //public int productSupplierId { get; set; }
            //public int brandId { get; set; }
            //public object productTypeId { get; set; }
            public int sortOrder { get; set; }
            public int price { get; set; }
            public int quantity { get; set; }
            public string name { get; set; }
            public bool isHot { get; set; }
            //public int supplierId { get; set; }
            //public int priceBeforeDiscount { get; set; }
            public string nameEn { get; set; }
            public bool isPolicyWholesale { get; set; }
            public string shipPrice { get; set; }
            public string priceRange { get; set; }
            public string supplierInfo { get; set; }
            //public int discountAmount { get; set; }
            public string discountType { get; set; }
            public List<string> images { get; set; }
            public List<object> advanceInfo { get; set; }
        }

        public class Paging
        {
            public int page { get; set; }
            public int pageSize { get; set; }
            public int total { get; set; }
            public int totalPage { get; set; }
        }

    }
}
