﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AffilateServices.Models.Dimuadi
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class GetBannerResponse
    {
        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string timeStart { get; set; }
        public string timeStop { get; set; }
        public List<string> avatarHome { get; set; }
        public List<string> avatarDetail { get; set; }
        public List<string> avatarDetailMobile { get; set; }
        public int status { get; set; }
        public int position { get; set; }
        public int sort { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
