﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AffilateServices.Models.Dimuadi
{
    public class Datum
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string shortDesc { get; set; }
        public string description { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public string image { get; set; }
        public int sortOrder { get; set; }
        public int status { get; set; }
        public int parentId { get; set; }
        public int hot { get; set; }
    }

    public class Paging
    {
        public int page { get; set; }
        public int pageSize { get; set; }
        public int total { get; set; }
        public int totalPage { get; set; }
    }

    public class GetProductCategoryResponse
    {
        public int status { get; set; }
        public string message { get; set; }
        public List<Datum> data { get; set; }
        public Paging paging { get; set; }
    }

}
