﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AffilateServices.Models.Dimuadi
{
    public class AttributeData
    {
        public List<object> advanceInfo { get; set; }
    }

    public class Content
    {
        public int id { get; set; }
        public string content { get; set; }
        public int productId { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public List<object> advanceInfo { get; set; }
    }

    public class Data
    {
        public string id { get; set; }
        public object sku { get; set; }
        public object url { get; set; }
        public object branch { get; set; }
        public string thumbImage { get; set; }
        public object startedAt { get; set; }
        public object endedAt { get; set; }
        public string commissionType { get; set; }
        public int commissionValue { get; set; }
        public object maxCommissionRate { get; set; }
        public object maxCommissionValue { get; set; }
        public int status { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public int productSupplierId { get; set; }
        public int brandId { get; set; }
        public object productTypeId { get; set; }
        public int sortOrder { get; set; }
        public int price { get; set; }
        public int quantity { get; set; }
        public string name { get; set; }
        public object shortDesc { get; set; }
        public string description { get; set; }
        public bool isHot { get; set; }
        public int supplierId { get; set; }
        public int priceBeforeDiscount { get; set; }
        public object nameEn { get; set; }
        public bool isPolicyWholesale { get; set; }
        public string shipPrice { get; set; }
        public object priceRange { get; set; }
        public object supplierInfo { get; set; }
        public List<object> advanceInfo { get; set; }
        public object categoryId { get; set; }
        public int companyId { get; set; }
        public string slug { get; set; }
        public int paymentTotal { get; set; }
        public int rateNumber { get; set; }
        public object wholesalePrice { get; set; }
        public AttributeData attributeData { get; set; }
        public int userId { get; set; }
        public string userName { get; set; }
        public int position { get; set; }
        public int voteTotal { get; set; }
        public int voteNumberStars { get; set; }
        public string brand { get; set; }
        public object categoryName { get; set; }
        public int discountAmount { get; set; }
        public string discountType { get; set; }
        public List<string> images { get; set; }
        public List<object> videos { get; set; }
        public List<Content> contents { get; set; }
        public List<object> colors { get; set; }
        public List<object> sizes { get; set; }
    }

    public class GetProductDetailResponse
    {
        public int status { get; set; }
        public string message { get; set; }
        public Data data { get; set; }
    }

}
