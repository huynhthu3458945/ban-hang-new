﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AffilateServices.Models.Common
{
    public class DimuadiResponse<T>
    {
        public int status { get; set; }
        public string message { get; set; }
        public List<T> data { get; set; }
    }
}
