﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AffilateServices.Models.Accesstrade
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Datum
    {
        public List<object> banner { get; set; }
        public string campaign { get; set; }
        public string campaign_id { get; set; }
        public string campaign_name { get; set; }
        public List<Category> categories { get; set; }
        public class Category
        {
            public string category_name { get; set; }
            public string category_name_show { get; set; }
            public string category_no { get; set; }
        }

        public int coin_cap { get; set; }
        public int coin_percentage { get; set; }
        public string content { get; set; }
        public List<Coupon> coupons { get; set; }
        public class Coupon
        {
            public string coupon_code { get; set; }
            public string coupon_desc { get; set; }
            public string coupon_save { get; set; }
            public int? coupon_type { get; set; }
        }

        public string ctime { get; set; }
        public int discount_percentage { get; set; }
        public int discount_value { get; set; }
        public string end_date { get; set; }
        public string id { get; set; }
        public string image { get; set; }
        public string is_hot { get; set; }
        public string link { get; set; }
        public int max_value { get; set; }
        public string merchant { get; set; }
        public int min_spend { get; set; }
        public string name { get; set; }
        public int percentage_used { get; set; }
        public string prod_link { get; set; }
        public int register { get; set; }
        public object shop_id { get; set; }
        public string start_date { get; set; }
        public int status { get; set; }
        public string time_left { get; set; }
    }

    public class GetCouponResponseModel
    {
        public Data data { get; set; }
        public bool success { get; set; }

        public class Data
        {
            public int count { get; set; }
            public int count_current_day_coupon { get; set; }
            public int count_next_day_coupon { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
