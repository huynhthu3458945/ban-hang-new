﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace AffilateServices.Models.Accesstrade
{
    public class GetProductResponseModel
    {
        [JsonPropertyName("data")]
        public List<ProductDatum> Data { get; set; }

        [JsonPropertyName("total")]
        public int Total { get; set; }

        public class ProductDatum
        {
            [JsonPropertyName("aff_link")]
            public string Aff_link { get; set; }

            [JsonPropertyName("campaign")]
            public string Campaign { get; set; }

            [JsonPropertyName("cate")]
            public string Cate { get; set; }

            [JsonPropertyName("desc")]
            public string Desc { get; set; }

            [JsonPropertyName("discount")]
            public decimal Discount { get; set; }

            [JsonPropertyName("discount_amount")]
            public decimal Discount_amount { get; set; }

            [JsonPropertyName("discount_rate")]
            public double Discount_rate { get; set; }

            [JsonPropertyName("domain")]
            public string Domain { get; set; }

            [JsonPropertyName("image")]
            public string Image { get; set; }

            [JsonPropertyName("merchant")]
            public string Merchant { get; set; }

            [JsonPropertyName("name")]
            public string Name { get; set; }

            [JsonPropertyName("price")]
            public decimal Price { get; set; }

            [JsonPropertyName("product_id")]
            public string Product_id { get; set; }

            [JsonPropertyName("promotion")]
            public object Promotion { get; set; }

            [JsonPropertyName("sku")]
            public string Sku { get; set; }

            [JsonPropertyName("status_discount")]
            public int Status_discount { get; set; }

            [JsonPropertyName("update_time")]
            public string UpdateTime { get; set; }

            [JsonPropertyName("url")]
            public string Url { get; set; }
        }
    }
}
