﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public  class StaffDepartmentModel
    {
        public int StaffId { get; set; }
        public int DepartmentId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }

        public virtual DepartmentModel Department { get; set; }
        public virtual staffModel Staff { get; set; }
    }
}
