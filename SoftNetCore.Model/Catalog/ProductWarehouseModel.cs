﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class ProductWarehouseModel
    {
        public int ProductId { get; set; }
        public int WarehouseId { get; set; }
        public DateTime CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }
        public int? Inventory { get; set; }

        public virtual ProductModel Product { get; set; }
        public virtual WarehouseModel Warehouse { get; set; }
    }
}
