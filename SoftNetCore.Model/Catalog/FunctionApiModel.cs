﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class FunctionApiModel
    {
        public int ApiId { get; set; }
        public int FunctionId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool? IsDelete { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual ApiModel Api { get; set; }
        public virtual FunctionModel Function { get; set; }
    }
}
