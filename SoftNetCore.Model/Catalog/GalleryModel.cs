﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class GalleryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public bool Status { get; set; }
        public int? Position { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }
        public int GalleryCategoryId { get; set; }
        public string Alias { get; set; }
        public int? ViewTime { get; set; }
        public bool IsDelete { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual GalleryCategoryModel GalleryCategory { get; set; }
    }
}
