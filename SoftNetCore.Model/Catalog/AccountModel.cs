﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Model.Catalog
{
    public class AccountModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public bool? AcceptTerms { get; set; }
        public string VerificationToken { get; set; }
        public DateTime? VerificationTokenExpiresTime { get; set; }
        public DateTime? VerifiedTime { get; set; }
        public bool IsVerified => VerifiedTime.HasValue || PasswordResetTime.HasValue;
        public string ResetToken { get; set; }
        public DateTime? ResetTokenExpiresTime { get; set; }
        public DateTime? PasswordResetTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? AccountTypeId { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual AccountTypeModel AccountType { get; set; }
    }
}
