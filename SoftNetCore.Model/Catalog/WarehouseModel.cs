﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class WarehouseModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public int? Position { get; set; }
        public bool Status { get; set; }
        public DateTime CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<ProductWarehouseModel> ProductWarehouses { get; set; }
        public virtual ICollection<ShelfModel> Shelves { get; set; }
        public virtual ICollection<StaffWarehouseModel> StaffWarehouses { get; set; }
    }
}
