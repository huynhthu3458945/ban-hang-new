﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class SubscribeModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public bool Status { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool IsReturn { get; set; }

    }
}
