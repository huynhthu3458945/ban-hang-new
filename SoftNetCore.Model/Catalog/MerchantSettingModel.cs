﻿using SoftNetCore.Data.Enums;

namespace SoftNetCore.Model.Catalog
{
    public class MerchantSettingModel
    {
        public int Id { get; set; }

        public EnumDomains MerchantId { get; set; }

        public string Merchant { get { return MerchantId.GetName(); } }

        public string TokenKey { get; set; }

        public string TokenValue { get; set; }
    }
}
