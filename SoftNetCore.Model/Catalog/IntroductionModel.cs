﻿using System;
using System.Collections.Generic;


namespace SoftNetCore.Model.Catalog
{
    public class IntroductionModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? Status { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
        public int? IntroductionTypeId { get; set; }
        public int? Position { get; set; }
        public string Alias { get; set; }
        public bool? IsDelete { get; set; }

        public virtual IntroductionTypeModel IntroductionType { get; set; }
    }
}
