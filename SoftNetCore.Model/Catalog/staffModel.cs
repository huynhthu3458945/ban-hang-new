﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class staffModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Skype { get; set; }
        public string Facebook { get; set; }
        public string Google { get; set; }
        public string Twitter { get; set; }
        public string Viber { get; set; }
        public string Zalo { get; set; }
        public int? Position { get; set; }
        public bool? Status { get; set; }
        public string Regency { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool? Gender { get; set; }
        public string Birthday { get; set; }
        public int? AccoutId { get; set; }
        public int? StaffTypeId { get; set; }
        public int? OrganizationId { get; set; }

        public virtual OrganizationModel Organization { get; set; }
        public virtual StaffTypeModel StaffType { get; set; }
        public virtual ICollection<StaffDepartmentModel> StaffDepartments { get; set; }
        public virtual ICollection<StaffWarehouseModel> StaffWarehouses { get; set; }
    }
}
