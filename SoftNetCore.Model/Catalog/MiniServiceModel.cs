﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class MiniServiceModel
    {
        public string Title { get; set; }
        public string ShortTitle { get; set; }
        public string Url { get; set; }
        public bool? Status { get; set; }
        public string Avatar { get; set; }
        public int Position { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
        public bool? IsDelete { get; set; }

    }
}
