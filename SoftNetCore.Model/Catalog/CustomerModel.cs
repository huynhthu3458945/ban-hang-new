﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class CustomerModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public bool Gender { get; set; }
        public string Birthday { get; set; }
        public string Avatar { get; set; }
        public string Phone { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? CustomerTypeId { get; set; }
        public int? NumberTakeCare { get; set; }
        public string CompanyName { get; set; }
        public int? AccountId { get; set; }
        public int? OrganizationId { get; set; }
        public bool IsDelete { get; set; }
        public string Code { get; set; }
        public bool Status { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string RePassword { get; set; }
        public bool IsChangePass { get; set; }
        public virtual AccountModel Account { get; set; }
        public virtual CustomerTypeModel CustomerType { get; set; }
        public virtual DistrictModel District { get; set; }
        //public virtual Organization Organization { get; set; }
        public virtual ProvinceModel Province { get; set; }
        //public virtual ICollection<OrderModel> Orders { get; set; }
        //public virtual ICollection<ProductWishlistModel> ProductWishlists { get; set; }
    }
}
