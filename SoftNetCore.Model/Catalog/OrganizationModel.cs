﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public partial class OrganizationModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Key { get; set; }
        public bool? Status { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }

        public virtual ICollection<CustomerModel> Customers { get; set; }
        public virtual ICollection<staffModel> staff { get; set; }
    }
}
