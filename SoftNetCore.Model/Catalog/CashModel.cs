﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class CashModel
    {
        public int Id { get; set; }
        public int? AccountCashId { get; set; }
        public string Code { get; set; }
        public int? CashTypeId { get; set; }
        public int? CashGroupId { get; set; }
        public decimal? Value { get; set; }
        public int? CashObjectId { get; set; }
        public bool? IsLock { get; set; }
        public string Note { get; set; }
        public int? CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? Receiver { get; set; }
        public string ReceiverName { get; set; }
        public bool? IsDebt { get; set; }
        public int? TransactionType { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual AccountCashModel AccountCash { get; set; }
        public virtual CashGroupModel CashGroup { get; set; }
        public virtual CashObjectModel CashObject { get; set; }
        public virtual CashTypeModel CashType { get; set; }
    }
}
