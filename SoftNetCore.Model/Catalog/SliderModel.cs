﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class SliderModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public bool Status { get; set; }
        public int? Position { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public string Title1 { get; set; }
        public string Title2 { get; set; }
        public string Title3 { get; set; }
        public bool IsManyImage { get; set; }
        public string ListImage { get; set; }
        public bool IsDelete { get; set; }

        // virtual
        public bool IsReturn { get; set; }
    }
}
