﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Model.Catalog
{
    public class BannerModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public string Url { get; set; }
        public int? Status { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? PositionBannerId { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool? IsDelete { get; set; }
        public string AvatarDetail { get; set; }
        public string AvatarDetailMobile { get; set; }
        public string AvatarHome { get; set; }
        public string BannerId { get; set; }
        public string Description { get; set; }
        public int? Position { get; set; }
        public string TimeStart { get; set; }
        public string TimeStop { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        //public virtual PositionBannerModel PositionBanner { get; set; }
    }
}
