﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class DebtModel
    {
        public int Id { get; set; }
        public int? Receiver { get; set; }
        public string ReceiverName { get; set; }
        public decimal? SumDebt { get; set; }
        public decimal? SumReceivable { get; set; }
        public decimal? Receivable { get; set; }
        public decimal? Debt1 { get; set; }
        public int? CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? CashId { get; set; }

        // virtual
        public bool IsReturn { get; set; }
    }
}
