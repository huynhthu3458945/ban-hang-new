﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Model.Catalog
{
    public class AccountCashModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public bool? Status { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public decimal? BalanceValue { get; set; }
        public bool? IsDelete { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual ICollection<CashModel> Cashes { get; set; }
    }
}
