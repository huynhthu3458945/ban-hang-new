﻿using SoftNetCore.Data.Enums;
using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class ProductCategoryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string Logo { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public string Alias { get; set; }
        public string Schemas { get; set; }
        public int? Position { get; set; }
        public bool Status { get; set; }
        public bool IsDelete { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ParentId { get; set; }

        // virtual
        public string ParentName { get; set; }
        public bool IsExistsChild { get; set; }
        public bool IsReturn { get; set; }


        /// <summary>
        /// Mã danh mục từ nền tảng khác
        /// </summary>
        public string ProductCategoryId { get; set; }
        public int TotalProduct { get; set; }

        public virtual ICollection<MerchantProductByCategoryDto> MerchantProductByCategories { get; set; }

        //public virtual ICollection<ProductModel> Products { get; set; }
    }

    public class MerchantProductByCategoryDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public int PageSize { get; set; }

        public string AccessToken { get; set; }

        public EnumDomains DomainId { get; set; }
    }
}
