﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class FunctionModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Note { get; set; }
        public int? Position { get; set; }
        public bool? Status { get; set; }
        public bool? IsShow { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModuleId { get; set; }
        public int? ParentId { get; set; }
        public int Id { get; set; }
        public bool? IsDelete { get; set; }

        // virtual
        public bool IsReturn { get; set; }

        //public virtual ICollection<AccountFunctionModel> AccountFunctions { get; set; }
        //public virtual ICollection<FunctionApiModel> FunctionApis { get; set; }
        //public virtual ICollection<FunctionGroupFunctionModel> FunctionGroupFunctions { get; set; }
    }
}
