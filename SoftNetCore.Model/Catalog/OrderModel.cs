﻿using System;
using System.Collections.Generic;


namespace SoftNetCore.Model.Catalog
{
    public partial class OrderModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? FeeShip { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Mobi { get; set; }
        public string Mobi2 { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public bool IsDelete { get; set; }
        public string Remark { get; set; }
        public int? PayTypeId { get; set; }
        public int? TransportId { get; set; }
        public bool IsCheckingGoods { get; set; }
        public int? CustomerId { get; set; }
        public string NoteSale { get; set; }
        public decimal? SaleOff { get; set; }
        public string NoteFeeShip { get; set; }
        public int? Rating { get; set; }
        public int? IsRating { get; set; }
        public string RatingContent { get; set; }
        public DateTime? RatingTime { get; set; }
        public int? OrderStatusId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual PayTypeModel PayType { get; set; }
        public virtual CustomerModel Customer { get; set; }
        public virtual DistrictModel District { get; set; }
        public virtual ProvinceModel Province { get; set; }
        public virtual OrderStatusModel OrderStatus { get; set; }
        public virtual ICollection<OrderDetailModel> OrderDetails { get; set; }
    }
}
