﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class StaffWarehouseModel
    {
        public int StaffId { get; set; }
        public int WarehouseId { get; set; }
        public DateTime CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        public virtual staffModel Staff { get; set; }
        public virtual WarehouseModel Warehouse { get; set; }
    }
}
