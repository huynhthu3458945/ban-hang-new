﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Model.Catalog
{
    public class AccountGroupAccountModel
    {
        public int AccountId { get; set; }
        public int GroupAccountId { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual AccountModel Account { get; set; }
        public virtual GroupAccountModel GroupAccount { get; set; }
    }
}
