﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class AccountCouponModel
    {
        public int AccountId { get; set; }
        public int CouponId { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual AccountModel Account { get; set; }
        public virtual CouponModel Coupon { get; set; }
    }
}
