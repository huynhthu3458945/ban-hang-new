﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class ProvinceModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal? FeeShip { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool IsDelete { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        //public virtual ICollection<DistrictModel> Districts { get; set; }
    }
}
