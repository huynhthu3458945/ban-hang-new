﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class CashGroupModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? Position { get; set; }
        public bool? Status { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool? IsDelete { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual ICollection<CashModel> Cashes { get; set; }
    }
}
