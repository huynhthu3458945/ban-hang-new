﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class OrderDetailModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int? ColorId { get; set; }
        public int? SizeId { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }
        public decimal? OriginalPrice { get; set; }
        public string Option { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual AttributeModel Color { get; set; }
        public virtual OrderModel Order { get; set; }
        public virtual ProductModel Product { get; set; }
        public virtual AttributeModel Size { get; set; }
    }
}
