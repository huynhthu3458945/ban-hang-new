﻿using System;
using System.Collections.Generic;


namespace SoftNetCore.Model.Catalog
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public string ImageListProduct { get; set; }
        public List<string> ImageList { get; set; }
        public int? Position { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string Alias { get; set; }
        public decimal? Price { get; set; }
        public decimal? PricePromo { get; set; }
        public decimal? PriceAfterDiscount { get; set; }
        public decimal? DiscountAmount { get; set; }
        public double? DiscountRate { get; set; }
        public int? Sale { get; set; }
        public DateTime? SaleDeadLine { get; set; }
        public int? ViewTime { get; set; }
        public bool? StockStatus { get; set; }
        public int? WishlistView { get; set; }
        public int? SoldView { get; set; }
        public string Schemas { get; set; }
        public int? TotalAssess { get; set; }
        public decimal? ValueAssess { get; set; }
        public bool IsProductNew { get; set; }
        public bool IsProductHot { get; set; }
        public bool Status { get; set; }
        public bool IsDelete { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? CreateBy { get; set; }
        public int? ModifyBy { get; set; }
        public int? ProductCategoryId { get; set; }
        public int? SaleId { get; set; }
        public int? UnitId { get; set; }
        public int? BannerId { get; set; }
        public string ProductId { get; set; } // get from another platform
        public string Sku { get; set; } // Product code
        public string Merchant { get; set; }
        public string Aff_link { get; set; }
        public string Domain { get; set; }
        public string ShareContent { get; set; }
        public string ShipPrice { get; set; }
        public decimal? CommissionValue { get; set; }
        public string CommissionType { get; set; }
        public string Slug { get; set; }


        /// <summary>
        /// Accesstrade
        /// </summary>
        public string RootUrl { get; set; }
        public string UpdateTime { get; set; }


        // virtual
        public bool IsSale { get; set; }
        public decimal? PriceSale { get; set; }
        public int Discount { get; set; }
        public bool IsReturn { get; set; }
        public List<ContentModel> ContentModels { get; set; }
        public virtual ProductCategoryModel ProductCategory { get; set; }
        //public virtual SaleModel SaleNavigation { get; set; }
        //public virtual ICollection<OrderDetailModel> OrderDetails { get; set; }
        //public virtual ICollection<ProductAttributeModel> ProductAttributes { get; set; }
        //public virtual ICollection<ProductCollectionModel> ProductCollections { get; set; }
        //public virtual ICollection<ProductShelfModel> ProductShelves { get; set; }
        //public virtual ICollection<ProductTabModel> ProductTabs { get; set; }
        //public virtual ICollection<ProductWardrobeModel> ProductWardrobes { get; set; }
        //public virtual ICollection<ProductWarehouseModel> ProductWarehouses { get; set; }
        //public virtual ICollection<ProductWishlistModel> ProductWishlists { get; set; }
    }

    public class ContentModel
    {
        public int id { get; set; }
        public string content { get; set; }
        public int productId { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public string [] advanceInfo { get; set; }
    }

    public class ProductTopModel
    {
        public List<ProductModel> ProductSaleList { get; set; }
        public List<ProductModel> ProductHotList { get; set; }
        public List<ProductModel> ProductNewList { get; set; }
        public List<ProductModel> ProductRandomList { get; set; }
    }

    public class ProductAndTotalCategoryModel
    {
        public ProductCategoryModel ProductCategoryModel { get; set; }
        public int Total { get; set; }
    }
}
