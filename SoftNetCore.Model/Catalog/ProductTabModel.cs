﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class ProductTabModel
    {
        public int TabId { get; set; }
        public int ProductId { get; set; }
        public string Value { get; set; }
        public DateTime CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        public virtual ProductModel Product { get; set; }
        public virtual TabModel Tab { get; set; }
    }
}
