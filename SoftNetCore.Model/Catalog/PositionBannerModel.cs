﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Model.Catalog
{
    public class PositionBannerModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Key { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public bool Status { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool IsDelete { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual ICollection<BannerModel> Banners { get; set; }
    }
}
