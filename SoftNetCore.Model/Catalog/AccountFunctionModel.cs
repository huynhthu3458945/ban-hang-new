﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Model.Catalog
{
    public class AccountFunctionModel
    {
        public int FunctionId { get; set; }
        public int AccountId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public bool? IsDelete { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual AccountModel Account { get; set; }
        public virtual FunctionModel Function { get; set; }
    }
}
