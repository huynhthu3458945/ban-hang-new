﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class WarehouseHistoryModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }
        public string Note { get; set; }
        public int? WarehouseFrom { get; set; }
        public int? WarehouseTo { get; set; }
        public int? HistoryTypeId { get; set; }
        public int? Status { get; set; }

        public virtual HistoryTypeModel HistoryType { get; set; }
        public virtual ICollection<WarehouseHistoryDetailModel> WarehouseHistoryDetails { get; set; }
    }
}
