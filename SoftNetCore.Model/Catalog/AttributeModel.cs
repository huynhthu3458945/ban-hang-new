﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class AttributeModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public int? TypeId { get; set; }
        public string Content { get; set; }
        public int? Position { get; set; }
        public bool Status { get; set; }
        public bool IsDelete { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        // virtual
        public string TypeName { get; set; }
        public bool IsReturn { get; set; }
        //public virtual ICollection<OrderDetailModel> OrderDetailColors { get; set; }
        //public virtual ICollection<OrderDetailModel> OrderDetailSizes { get; set; }
        //public virtual ICollection<ProductAttributeModel> ProductAttributes { get; set; }
    }
}
