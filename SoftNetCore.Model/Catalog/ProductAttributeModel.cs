﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class ProductAttributeModel
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? AttributeId { get; set; }
        public string Avatar1 { get; set; }
        public string Avatar2 { get; set; }
        public string ImageList { get; set; }
        public decimal? Price { get; set; }
        public decimal? PricePromo { get; set; }
        public int? Quantity { get; set; }
        public string Value { get; set; }
        public bool Status { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }
        public int? ParentId { get; set; }
        public int? AttributeParentId { get; set; }

        // virtual
        public int? UpdateColorId { get; set; }
        public int? UpdateSizeId { get; set; }
        public bool IsSale { get; set; }
        public decimal? PriceSale { get; set; }
        public int Discount { get; set; }
        public string TypeName { get; set; }
        public bool IsReturn { get; set; }
        public virtual AttributeModel Attribute { get; set; }
        public virtual ProductAttributeModel Parent { get; set; }
        public virtual ProductModel Product { get; set; }
        public virtual ICollection<ProductAttributeModel> InverseParent { get; set; }
    }
}
