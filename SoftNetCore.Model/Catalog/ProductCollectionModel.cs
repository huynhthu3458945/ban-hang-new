﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class ProductCollectionModel
    {
        public int ProductId { get; set; }
        public int CollectionId { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual CollectionModel Collection { get; set; }
        public virtual ProductModel Product { get; set; }
    }
}
