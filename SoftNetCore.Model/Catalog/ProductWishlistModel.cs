﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Model.Catalog
{
    public class ProductWishlistModel
    {
        public int ProductWishlistId { get; set; }
        public int? ProductId { get; set; }
        public int? CustomerId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public bool IsReturn { get; set; }
        public virtual CustomerModel Customer { get; set; }
        public virtual ProductModel Product { get; set; }
    }
}
