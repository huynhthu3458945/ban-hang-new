﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoftNetCore.Model.Catalog
{
    public class FunctionGroupFunctionModel
    {
        public int GroupFunctionId { get; set; }
        public int FunctionId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }

        // virtual
        public bool IsReturn { get; set; }
        public virtual FunctionModel Function { get; set; }
        public virtual GroupFunctionModel GroupFunction { get; set; }
    }
}
