﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class WarehouseHistoryDetailModel
    {
        public int WarehouseHistoryId { get; set; }
        public int ProductId { get; set; }
        public int? RequestNumber { get; set; }
        public int? ActualNumber { get; set; }
        public int? ErrorNumber { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceSale { get; set; }
        public int WarehouseId { get; set; }

        public virtual WarehouseHistoryModel WarehouseHistory { get; set; }
    }
}
