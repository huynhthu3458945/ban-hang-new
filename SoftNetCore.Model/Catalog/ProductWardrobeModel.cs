﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class ProductWardrobeModel
    {
        public int ProductId { get; set; }
        public int WardrobeId { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public int? ModifyBy { get; set; }

        public virtual ProductModel Product { get; set; }
        public virtual WardrobeModel Wardrobe { get; set; }
    }
}
