﻿using System;
using System.Collections.Generic;

namespace SoftNetCore.Model.Catalog
{
    public class CompanySystemModel
    {
        public int Id { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public string Content { get; set; }
        public string ShortContent { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Tel { get; set; }
        public string Hotline { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateTime { get; set; }
        public int? ModifyBy { get; set; }
        public DateTime? ModifyTime { get; set; }
        public string Address { get; set; }
        public bool Status { get; set; }
        public string Title { get; set; }
        public int? Position { get; set; }
        public string Alias { get; set; }
        public bool? IsDelete { get; set; }

        // virtual
        public bool IsReturn { get; set; }
    }
}
