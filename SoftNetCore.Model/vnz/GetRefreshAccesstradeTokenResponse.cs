﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Model.vnz
{
    public class GetRefreshAccesstradeTokenResponse
    {
        public DataDto data { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
        public class DataDto
        {
            public string token { get; set; }
        }
    }
}
