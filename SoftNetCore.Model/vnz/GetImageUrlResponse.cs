﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Model.vnz
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class GetImageUrlResponse
    {
        public string Status { get; set; }
        public string Content { get; set; }
    }
}
