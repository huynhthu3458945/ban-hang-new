﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Model.System
{
    public class ApiUrl
    {
        public string Url { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
    }
}
