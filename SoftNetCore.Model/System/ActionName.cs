﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Model.System
{
    public class ActionName
    {
        public string Controller { get; set; }
        public string Action { get; set; }
    }
}
