﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Model.System
{
    public class PagingModel
    {
        public int currentPage { get; set; }
        public int countPages { get; set; }

        public Func<int?, string> generateUrl { get; set; }
    }
}
