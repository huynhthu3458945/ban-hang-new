﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SoftNetCore.Model.Request
{
    public class OrderStatusRequest
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
