﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SoftNetCore.Model.Request
{
    public class RatingRequest
    {
        [Required]
        public int OrderId { get; set; }

        [Required]
        public int Rating { get; set; }

        [Required]
        public string Content { get; set; }

        public int? IsRating { get; set; }
    }
}
