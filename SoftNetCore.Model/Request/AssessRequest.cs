﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SoftNetCore.Model.Request
{
    public class AssessRequest
    {
        public int ProductId { get; set; }

        public int Total { get; set; }

        public decimal Value { get; set; }
    }
}
