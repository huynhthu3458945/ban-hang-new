﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SoftNetCore.Model.Request
{
    public class StatusOrderRequest
    {
        [Required]
        public int OrderId { get; set; }

        [Required]
        public string Remark { get; set; }

    }
}
