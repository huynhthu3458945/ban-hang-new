﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SoftNetCore.Model.Request
{
    public class AttributeTypeRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
