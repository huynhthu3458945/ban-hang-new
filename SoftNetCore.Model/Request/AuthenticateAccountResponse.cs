﻿using SoftNetCore.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace SoftNetCore.Model.Request
{
    public class AuthenticateAccountResponse
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string JwtToken { get; set; }
        public bool IsVerified { get; set; }
        public string Avatar { get; set; }
        public string TypeId { get; set; }
        public object Permissions { get; set; }
        public object Menus { get; set; }

        [JsonIgnore] // refresh token is returned in http only cookie
        public string RefreshToken { get; set; }
        public string MessageType { get; set; }

        public AuthenticateAccountResponse(Account user, string jwtToken)
        {
            Id = user.Id.ToString();
            UserName = user.UserName;
            TypeId = user.AccountTypeId.ToString();
            JwtToken = jwtToken;
        }
    }
}
