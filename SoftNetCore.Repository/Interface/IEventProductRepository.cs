﻿using SoftNetCore.Data.Entities;
using SoftNetCore.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Repository.Interface
{
    public interface IEventProductRepository : IBaseRepository<EventProduct>
    {
    }
}
