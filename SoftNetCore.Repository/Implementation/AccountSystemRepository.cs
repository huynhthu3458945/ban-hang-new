﻿using AutoMapper;
using SoftNetCore.Data.Entities;
using SoftNetCore.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Repository.Implementation
{
    public class AccountSystemRepository : BaseRepository<AccountSystem>, IAccountSystemRepository
    {
        private readonly KFoodDBContext _context;
        private readonly IMapper _mapper;

        public AccountSystemRepository(KFoodDBContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }
    }

    public interface IAccountSystemRepository : IBaseRepository<AccountSystem>
    {

    }
}
