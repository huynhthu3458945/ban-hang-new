﻿using AutoMapper;
using SoftNetCore.Data.Entities;
using SoftNetCore.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Repository.Implementation
{
    public class PartnerRepository : BaseRepository<Partner>, IPartnerRepository
    {
        private readonly KFoodDBContext _context;
        private readonly IMapper _mapper;

        public PartnerRepository(KFoodDBContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }
    }

    public interface IPartnerRepository : IBaseRepository<Partner>
    {
    }
}
