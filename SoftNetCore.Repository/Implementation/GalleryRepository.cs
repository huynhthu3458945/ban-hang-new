﻿using AutoMapper;
using SoftNetCore.Data.Entities;
using SoftNetCore.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Repository.Implementation
{
    public class GalleryRepository : BaseRepository<Gallery>, IGalleryRepository
    {
        private readonly KFoodDBContext _context;
        private readonly IMapper _mapper;

        public GalleryRepository(KFoodDBContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }
    }

    public interface IGalleryRepository : IBaseRepository<Gallery>
    {
    }
}
