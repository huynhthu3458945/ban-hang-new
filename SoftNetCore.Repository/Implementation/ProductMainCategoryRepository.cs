﻿using AutoMapper;
using SoftNetCore.Data.Entities;
using SoftNetCore.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Repository.Implementation
{
    public class ProductMainCategoryRepository : BaseRepository<ProductMainCategory>, IProductMainCategoryRepository
    {
        private readonly KFoodDBContext _context;
        private readonly IMapper _mapper;

        public ProductMainCategoryRepository(KFoodDBContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }
    }

    public interface IProductMainCategoryRepository : IBaseRepository<ProductMainCategory>
    {
    }
}
