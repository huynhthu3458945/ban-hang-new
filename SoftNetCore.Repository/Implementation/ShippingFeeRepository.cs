﻿using AutoMapper;
using SoftNetCore.Data.Entities;
using SoftNetCore.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Repository.Implementation
{
    public class ShippingFeeRepository : BaseRepository<ShippingFee>, IShippingFeeRepository
    {
        private readonly KFoodDBContext _context;
        private readonly IMapper _mapper;

        public ShippingFeeRepository(KFoodDBContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }
    }

    public interface IShippingFeeRepository : IBaseRepository<ShippingFee>
    {
    }
}
