﻿using AutoMapper;
using SoftNetCore.Data.Entities;
using SoftNetCore.Repository.BaseRepository;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Repository.Implementation
{
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        private readonly KFoodDBContext _context;
        private readonly IMapper _mapper;

        public CustomerRepository(KFoodDBContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }
    }

    public interface ICustomerRepository : IBaseRepository<Customer>
    {
    }
}
