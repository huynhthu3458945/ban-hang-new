﻿using SoftNetCore.Data.Entities;
using SoftNetCore.Repository.BaseRepository;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftNetCore.Repository.Implementation
{
    public class AboutRepository : BaseRepository<About>, IAboutRepository
    {
        private readonly KFoodDBContext _context;
        private readonly IMapper _mapper;

        public AboutRepository(KFoodDBContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }
    }

    public interface IAboutRepository : IBaseRepository<About>
    {

    }
}
