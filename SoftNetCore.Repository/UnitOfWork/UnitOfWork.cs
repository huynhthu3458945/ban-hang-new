﻿using System;
using System.Threading.Tasks;
using SoftNetCore.Data.Entities;

namespace SoftNetCore.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly KFoodDBContext _context;

        public UnitOfWork(KFoodDBContext context)
        {
            _context = context;
        }
        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
