﻿using System;
using System.Threading.Tasks;

namespace SoftNetCore.Repository.UnitOfWork
{
    public interface IUnitOfWork
    {
        Task<bool> SaveAll();
        void Save();
    }
}
